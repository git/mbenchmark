## Autoconf macros for working with Guile.
##
##   Copyright (C) 1998,2001, 2006, 2010, 2012, 2013, 2014, 2020 Free Software Foundation, Inc.
##   Copyright (C) 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public License
## as published by the Free Software Foundation; either version 3 of
## the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301 USA

# serial 1
AC_DEFUN([GUILE_PKG],
 [AC_REQUIRE([PKG_PROG_PKG_CONFIG])
  if test "x$PKG_CONFIG" = x; then
    AC_MSG_ERROR([pkg-config is missing, please install it])
  fi
  _guile_versions_to_search="m4_default([$1], [3.0 2.2 2.0])"
  if test -n "$GUILE_EFFECTIVE_VERSION"; then
    _guile_tmp=""
    for v in $_guile_versions_to_search; do
      if test "$v" = "$GUILE_EFFECTIVE_VERSION"; then
        _guile_tmp=$v
      fi
    done
    if test -z "$_guile_tmp"; then
      AC_MSG_FAILURE([searching for guile development files for versions $_guile_versions_to_search, but previously found $GUILE version $GUILE_EFFECTIVE_VERSION])
    fi
    _guile_versions_to_search=$GUILE_EFFECTIVE_VERSION
  fi
  GUILE_EFFECTIVE_VERSION=""
  _guile_errors=""
  for v in $_guile_versions_to_search; do
    if test -z "$GUILE_EFFECTIVE_VERSION"; then
      AC_MSG_CHECKING([for guile $v])
      PKG_CHECK_EXISTS([guile-$v],
       [AC_MSG_RESULT([found])
        GUILE_EFFECTIVE_VERSION=$v],
       [AC_MSG_RESULT([not found])])
    fi
  done

  if test -z "$GUILE_EFFECTIVE_VERSION"; then
    m4_default([$3], [AC_MSG_ERROR([
No Guile development packages were found.

Please verify that you have Guile installed.  If you installed Guile
from a binary distribution, please verify that you have also installed
the development packages.  If you installed it yourself, you might need
to adjust your PKG_CONFIG_PATH; see the pkg-config man page for more.
])])
  fi
  m4_default([$2], [AC_MSG_NOTICE([found guile $GUILE_EFFECTIVE_VERSION])])
  AC_SUBST([GUILE_EFFECTIVE_VERSION])
 ])
