# mbenchmark.m4 --- Build configuration for MicroBenchmark.
#
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is part of MicroBenchmark.
#
# MicroBenchmark is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# MicroBenchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MicroBenchmark.  If not, see
# <http://www.gnu.org/licenses/>.

# serial 1
AC_DEFUN([MICRO_BENCHMARK_CHECK_COMMON_UTILITIES],
 [AC_PROG_AWK
  AC_PROG_MKDIR_P
  AC_PROG_FGREP
  AC_PROG_GREP
  AC_PROG_SED
  AC_PATH_PROGS([BASH], [bash], [$SHELL])
  AC_PATH_PROGS([MB_PKG_CONFIG], [pkg-config], [])
  AC_PATH_PROGS([MB_MSGATTRIB], [msgattrib],
    [\$(SHELL) \$(top_srcdir)/build-aux/missing msgattrib])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_COMPILER_FLAGS],
 [MB_WARNING_FLAGS="-Wall -Wextra"
  AC_ARG_ENABLE([error-from-warnings],
   [AS_HELP_STRING([--enable-error-from-warnings],
     [Use -Werror for the compilation])],
   [micro_benchmark_enable_werror=$enableval],
   [micro_benchmark_enable_werror=no])

  AC_MSG_CHECKING([if -Werror will be used to compile the code])
  AS_IF([test "x$micro_benchmark_enable_werror" != xyes],
   [AC_MSG_RESULT([no])],
   [AC_MSG_RESULT([yes])
    MB_WARNING_FLAGS="$MB_WARNING_FLAGS -Werror"])
  AC_SUBST([MB_WARNING_FLAGS], [$MB_WARNING_FLAGS])

  AC_ARG_ENABLE([sanitizer],
   [AS_HELP_STRING([--enable-sanitizer[=TYPE]],
     [Enable sanitizer TYPE (address by default) support])],
   [micro_benchmark_enable_sanitizer=$enableval],
   [micro_benchmark_enable_sanitizer=no])

  AC_MSG_CHECKING([if -fsanitizer will be used to compile the code])
  AS_IF([test "x$micro_benchmark_enable_sanitizer" = xno],
   [AC_MSG_RESULT([no])],
   [test "x$micro_benchmark_enable_sanitizer" = xyes],
   [AC_MSG_RESULT([address])
    MB_SANITIZER_CFLAGS="-fsanitize=address"
    AC_SUBST([MB_SANITIZER_CFLAGS], [$MB_SANITIZER_CFLAGS])
    AC_SUBST([MB_SANITIZER_CXXFLAGS], [$MB_SANITIZER_CFLAGS])],
   [AC_MSG_RESULT([$micro_benchmark_enable_sanitizer])
    MB_SANITIZER_CFLAGS="-fsanitize=$micro_benchmark_enable_sanitizer"
    AC_SUBST([MB_SANITIZER_CFLAGS], [$MB_SANITIZER_CFLAGS])
    AC_SUBST([MB_SANITIZER_CXXFLAGS], [$MB_SANITIZER_CFLAGS])
   ])

  AC_ARG_ENABLE([c-static-analysis],
   [AS_HELP_STRING([--disable-c-static-analysis],
     [Disable C static analysis support])],
   [micro_benchmark_enable_analyzer=$enableval],
   [micro_benchmark_enable_analyzer=check])

  AC_MSG_CHECKING([if static analyzer will be used to compile C code])
  AC_MSG_RESULT([$micro_benchmark_enable_analyzer])

  dnl XXX: Double check
  AS_IF([test "x$micro_benchmark_enable_analyzer" = xall],
   [MB_ANALYZER_FLAGS="-fanalyzer"],
   [test "x$micro_benchmark_enable_analyzer" != xno],
   [MB_ANALYZER_FLAGS="-fanalyzer -Wno-analyzer-malloc-leak"])

  AS_IF([test "x$micro_benchmark_enable_analyzer" != xno],
   [oldCFLAGS=$CFLAGS
    AS_IF([test "x$micro_benchmark_enable_analyzer" = xcheck],
     [CFLAGS="$CFLAGS $MB_WARNING_FLAGS $MB_ANALYZER_FLAGS"],
     [CFLAGS="$CFLAGS $MB_ANALYZER_FLAGS"])
    AC_LANG_PUSH([C])
    AC_MSG_CHECKING([if $MB_ANALYZER_FLAGS works for C programs])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[volatile int i;]], [[++i;]])],
     [micro_benchmark_enable_analyzer=yes
      AC_MSG_RESULT([yes])
      AC_SUBST([MB_ANALYZER_CFLAGS], [$MB_ANALYZER_FLAGS])],
     [AS_IF([test "x$micro_benchmark_enable_analyzer" = xcheck],
       [micro_benchmark_enable_analyzer=no
        AC_MSG_RESULT([no])],
       [AC_MSG_FAILURE([Error compiling with $MB_ANALYZER_FLAGS])])
     ])
    AC_LANG_POP([C])
    CFLAGS=$oldCFLAGS
   ])

  AC_ARG_ENABLE([c++-static-analysis],
   [AS_HELP_STRING([--enable-c++-static-analysis],
     [Enable C++ static analysis support])],
   [micro_benchmark_enable_cxx_analyzer=$enableval],
   [micro_benchmark_enable_cxx_analyzer=no])

  AC_MSG_CHECKING([if $MB_ANALYZER_FLAGS will be used to compile C++ code])
  AC_MSG_RESULT([$micro_benchmark_enable_cxx_analyzer])

  AS_IF([test "x$micro_benchmark_enable_cxx_analyzer" != xno],
   [oldCXXFLAGS=$CXXFLAGS
    AS_IF([test "x$micro_benchmark_enable_cxx_analyzer" = xcheck],
     [CXXFLAGS="$CXXFLAGS $MB_WARNING_FLAGS $MB_ANALYZER_FLAGS"],
     [CXXFLAGS="$CXXFLAGS $MB_ANALYZER_FLAGS"])
    AC_LANG_PUSH([C++])
    AC_MSG_CHECKING([if $MB_ANALYZER_FLAGS works for C++ programs])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[volatile int i;]], [[++i;]])],
     [micro_benchmark_enable_cxx_analyzer=yes
      AC_MSG_RESULT([yes])
      AC_SUBST([MB_ANALYZER_CXXFLAGS], [$MB_ANALYZER_FLAGS])],
     [AS_IF([test "x$micro_benchmark_enable_cxx_analyzer" = xcheck],
       [micro_benchmark_enable_cxx_analyzer=no
        AC_MSG_RESULT([no])],
       [AC_MSG_FAILURE([Error compiling with $MB_ANALYZER_FLAGS])])
     ])
    AC_LANG_POP([C++])
    CXXFLAGS=$oldCXXFLAGS
   ])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_COMMON_FUNCTIONS],
 [AC_CHECK_FUNCS([open_memstream strdup tmpfile vasprintf], [], [])
  AC_CHECK_FUNCS([getopt_long], [], [AC_MSG_ERROR([getopt_long needed])])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_COMMON_HEADERS],
 [AC_HEADER_ASSERT
  AC_HEADER_STDBOOL
  AC_CHECK_HEADERS([execinfo.h], [], [])
  AC_CHECK_HEADERS([getopt.h], [], [AC_MSG_ERROR([getopt.h not found])])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_TRACES],
 [AC_ARG_ENABLE([traces],
   [AS_HELP_STRING([--enable-traces],
     [Emit log calls of trace level])],
   [micro_benchmark_have_traces=$enableval],
   [micro_benchmark_have_traces=no])

  AC_MSG_CHECKING([if trace messages will be generated by the library])
  AS_IF([test "x$micro_benchmark_have_traces" != xyes],
    [AC_DEFINE([MICRO_BENCHMARK_BUILD_WITHOUT_TRACES], [1],
      [Do not emit trace calls when defined])
     AC_MSG_RESULT([no])],
    [AC_MSG_RESULT([$micro_benchmark_have_traces])])

  AC_MSG_CHECKING([for the library log level])
  AC_ARG_WITH([log-level],
   [AS_HELP_STRING([--with-log-level=LEVEL],
     [Select the default log level (trace, debug, info, warn, error)])],
   [micro_benchmark_default_log_level=$enableval],
   [micro_benchmark_default_log_level=default])
  AC_MSG_RESULT([$micro_benchmark_default_log_level])

  AS_IF([test "x$micro_benchmark_default_log_level" = xdefault],
    [],
    [test "x$micro_benchmark_default_log_level" = xtrace],
    [AC_DEFINE([MICRO_BENCHMARK_DEFAULT_LOG_LEVEL],
      [MICRO_BENCHMARK_TRACE_LEVEL],
      [Default log level])],
    [test "x$micro_benchmark_default_log_level" = xdebug],
    [AC_DEFINE([MICRO_BENCHMARK_DEFAULT_LOG_LEVEL],
      [MICRO_BENCHMARK_DEBUG_LEVEL],
      [Default log level])],
    [test "x$micro_benchmark_default_log_level" = xinfo],
    [AC_DEFINE([MICRO_BENCHMARK_DEFAULT_LOG_LEVEL],
      [MICRO_BENCHMARK_INFO_LEVEL],
      [Default log level])],
    [test "x$micro_benchmark_default_log_level" = xwarn],
    [AC_DEFINE([MICRO_BENCHMARK_DEFAULT_LOG_LEVEL],
      [MICRO_BENCHMARK_WARNING_LEVEL],
      [Default log level])],
    [test "x$micro_benchmark_default_log_level" = xerror],
    [AC_DEFINE([MICRO_BENCHMARK_DEFAULT_LOG_LEVEL],
      [MICRO_BENCHMARK_ERROR_LEVEL],
      [Default log level])],
    [AC_MSG_ERROR([Log level provided is not valid])])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_COVERAGE],
 [AC_ARG_ENABLE([coverage],
   [AS_HELP_STRING([--enable-coverage],
     [Check library code coverage by the test suite])],
   [micro_benchmark_enable_coverage=$enableval],
   [micro_benchmark_enable_coverage=no])
  dnl TODO: Disable or fail on cross-compilation
  AC_MSG_CHECKING([if the code must be compiled with coverage support])
  AC_MSG_RESULT([$micro_benchmark_enable_coverage])

  AS_IF([test "x$micro_benchmark_enable_coverage" != xno],
   [rm -f tartest.tar.gz tartest
    : ${GNU_TAR=tar}
    AC_ARG_VAR([GNU_TAR], [GNU tar binary used for coverage])
    micro_benchmark_tar=${GNU_TAR:-tar}
    AC_MSG_CHECKING([if $micro_benchmark_tar accepts czf])
    touch tartest
    AS_IF([$micro_benchmark_tar czf tartest.tar.gz tartest],
      [AC_MSG_RESULT([yes])],
      [test "x$micro_benchmark_enable_coverage" != xauto],
      [AC_MSG_RESULT([no])
       AC_MSG_ERROR([$micro_benchmark_tar czf does not work as needed])],
      [micro_benchmark_enable_coverage=no
       AC_MSG_RESULT([no])
       rm -f tartest tartest.tar.gz
      ])
   ])

  AS_IF([test "x$micro_benchmark_enable_coverage" != xno],
   [AC_MSG_CHECKING([whether $micro_benchmark_tar tf works])
    v=`$micro_benchmark_tar tf tartest.tar.gz`
    AS_IF([test "x$v" = xtartest],
      [AC_MSG_RESULT([yes])],
      [test "x$micro_benchmark_enable_coverage" != xauto],
      [AC_MSG_RESULT([no])
       AC_MSG_ERROR([$micro_benchmark_tar tf does not work as needed])],
      [micro_benchmark_enable_coverage=no
       AC_MSG_RESULT([no])
       rm -f tartest
      ])
    rm -f tartest.tar.gz
   ])

  AS_IF([test "x$micro_benchmark_enable_coverage" != xno],
   [AC_MSG_CHECKING([if $micro_benchmark_tar --transform is usable])
    $micro_benchmark_tar czf tartest.tar.gz --transform=s,tartest,a, tartest
    v=`$micro_benchmark_tar tf tartest.tar.gz`
    AS_IF([test "x$v" = xa],
      [AC_MSG_RESULT([yes])
       AC_SUBST([GNU_TAR], [$micro_benchmark_tar])],
      [test "x$micro_benchmark_enable_coverage" != xauto],
      [AC_MSG_RESULT([no])
       AC_MSG_ERROR([$micro_benchmark_tar --transform does not work as needed])],
      [micro_benchmark_enable_coverage=no
       AC_MSG_RESULT([no])
      ])
    rm -f tartest tartest.tar.gz
   ])

  AS_IF([test "x$micro_benchmark_enable_coverage" != xno],
   [AC_REQUIRE([AC_PROG_LN_S])
    AC_CHECK_PROGS([READLINK], [readlink], [not-found])
    AS_IF([test "x$READLINK" = "xnot-found"],
     [AC_MSG_RESULT([no])
      AC_MSG_ERROR([readlink needed for coverage targets])],
     [AC_MSG_CHECKING([if $READLINK -f is usable])
      touch mbtest.a
      $LN_S mbtest.a mbtest.b
      v=`$READLINK -f mbtest.b`
      rm mbtest.a mbtest.b
      AS_IF([test "x$v" = x],
       [AS_IF([test "x$micro_benchmark_enable_coverage" != xauto],
         [AC_MSG_RESULT([no])
          AC_MSG_ERROR([$READLINK -f does not work])],
         [AC_MSG_RESULT([no])
          micro_benchmark_enable_coverage=no])],
       [f=`basename $v`
        AS_IF([test "x$f" = "xmbtest.a"],
          [AC_MSG_RESULT([yes])],
          [test "x$micro_benchmark_enable_coverage" != xauto],
          [AC_MSG_RESULT([no])
           AC_MSG_ERROR([$READLINK -f does not follow links])],
          [AC_MSG_RESULT([no])
           micro_benchmark_enable_coverage=no
          ])
        ])
     ])
   ])

  AS_IF([test "x$micro_benchmark_enable_coverage" != xno],
   [AC_REQUIRE([AC_PROG_FGREP])
    AC_PATH_PROGS([GCOV], [gcov], [gcov-not-found])
    AS_IF([test "x$GCOV" = "xgcov-not-found"],
     [AC_PATH_PROGS([LLVM_COV], [llvm-cov], [llvm-cov-not-found])
      AS_IF([test "x$LLVM_COV" = xllvm-cov-not-found],
       [AS_IF([test "x$micro_benchmark_enable_coverage" != xauto],
         [AC_MSG_ERROR([Neither gcov nor llvm-cov were found])],
         [AC_MSG_NOTICE([gcov was not found on the path])
          micro_benchmark_enable_coverage=no
         ])],
       [AC_MSG_CHECKING([if $LLVM_COV supports the needed options])
        args="--demangled-names --source-prefix /x --source-prefix /y"
        args="$args --relative-only --preserve-paths"
        o=`LC_ALL=C $LLVM_COV gcov $args --help 2>&1 || echo Unknown`
        err=`echo "$o" | $FGREP -e Unknown -e 'zero or one'`
        AS_IF([test "x$err" = x],
         [AC_MSG_RESULT([yes])
          GCOV="$LLVM_COV gcov"
          AC_SUBST([GCOV], [$GCOV])],
         [test "x$micro_benchmark_enable_coverage" = xauto],
         [AC_MSG_RESULT([no])
          micro_benchmark_enable_coverage=no],
         [AC_MSG_RESULT([no])
          AC_MSG_ERROR([$LLVM_COV does not support the needed options])])
       ])
     ])
   ])

  AS_IF([test "x$micro_benchmark_enable_coverage" != xno],
   [AC_REQUIRE([AC_PROG_CC])
    AC_LANG_PUSH([C])
    oldLIBS=$LIBS
    oldCFLAGS=$CFLAGS
    CFLAGS="--coverage"
    LIBS="--coverage -lgcov"
    AC_MSG_CHECKING([if $LIBS produce valid C binaries])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[int a;]], [[a = 1;]])],
     [AC_MSG_RESULT([yes])],
     [AS_IF([test "x$micro_benchmark_enable_coverage" = xauto],
       [AC_MSG_RESULT([no])
        micro_benchmark_enable_coverage=no],
       [AC_MSG_RESULT([no])
        AC_MSG_ERROR([Coverage flags break C++ compilation and/or linkage])])
     ])
    LIBS=$oldLIBS
    CFLAGS=$oldCFLAGS
    AC_LANG_POP([C])
   ])

  AS_IF([test "x$micro_benchmark_enable_coverage" != xno],
   [AC_REQUIRE([AC_PROG_CXX])
    AC_LANG_PUSH([C++])
    oldLIBS=$LIBS
    oldCXXFLAGS=$CXXFLAGS
    CXXFLAGS="--coverage"
    LIBS="--coverage -lgcov"
    AC_MSG_CHECKING([if $LIBS produce valid C++ binaries])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[int a;]], [[a = 1;]])],
     [AC_MSG_RESULT([yes])],
     [AS_IF([test "x$micro_benchmark_enable_coverage" = xauto],
       [AC_MSG_RESULT([no])
        micro_benchmark_enable_coverage=no],
       [AC_MSG_FAILURE([Coverage flags break C++ compilation and/or linkage])])
     ])
    LIBS=$oldLIBS
    CXXFLAGS=$oldCXXFLAGS
    AC_LANG_POP([C++])
   ])

  AS_IF([test "x$micro_benchmark_enable_coverage" != xno],
   [micro_benchmark_enable_coverage=yes
    AC_ARG_VAR([GCOV_FLAGS], [Flags used for gcov])
    AC_DEFINE([MB_COVERAGE_ENABLED], [1],
     [Whether the code coverage is enabled])],
   [AC_DEFINE([MB_COVERAGE_ENABLED], [0],
     [Whether the code coverage is enabled])
   ])

  AM_CONDITIONAL([ENABLE_COVERAGE],
   [test "x$micro_benchmark_enable_coverage" = xyes])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_CXX],
 [AC_ARG_ENABLE([c++],
   [AS_HELP_STRING([--disable-c++],
     [Do not compile C++ bindings, even when a C++14+ compiler is available])],
   [micro_benchmark_enable_cxx=$enableval],
   [micro_benchmark_enable_cxx=check])

  AC_MSG_CHECKING([if C++ binding is requested])
  AC_MSG_RESULT([$micro_benchmark_enable_cxx])
  AS_IF([test "x$micro_benchmark_enable_cxx" != xno],
   [AX_CXX_COMPILE_STDCXX([14], [], [optional])
    AS_IF([test "x$HAVE_CXX14" = x0],
     [AS_IF([test "x$micro_benchmark_enable_cxx" != xcheck],
       [AC_MSG_RESULT([no])
        AC_MSG_ERROR([C++14 compiler not found])],
       [micro_benchmark_enable_cxx=no])])

    AC_LANG_PUSH([C++])
    AS_IF([test "x$micro_benchmark_enable_cxx" != xno],
     [AC_MSG_CHECKING([inline namespace usability])
      AC_LINK_IFELSE([AC_LANG_PROGRAM([[\
         namespace A { inline namespace B {} }
         namespace A { namespace B { extern bool test; }}
         bool A::test = true;]],
         [[auto v = A::test;]])],
     [AC_MSG_RESULT([yes])],
     [AS_IF([test "x$micro_benchmark_enable_cxx" != xcheck],
       [AC_MSG_FAILURE([C++ compiler does not support inline namespace])],
       [AC_MSG_RESULT([not supported])
        micro_benchmark_enable_cxx=no])])
     ])

    AS_IF([test "x$micro_benchmark_enable_cxx" != xno],
     [AC_MSG_CHECKING([C++ asm extended syntax usability])
       AC_LINK_IFELSE([AC_LANG_PROGRAM([[static int global;]],
          [[int& r = global;
            asm volatile ("" : "+X" (r) :: "memory");]])],
        [AC_MSG_RESULT([yes])
         micro_benchmark_enable_cxx=yes],
        [AS_IF([test "x$micro_benchmark_enable_cxx" != xcheck],
          [AC_MSG_FAILURE([C++ compiler does not support extended asm syntax])],
          [AC_MSG_RESULT([not supported])
           micro_benchmark_enable_cxx=no])])
     ])
    AC_LANG_POP([C++])
   ])
  AM_CONDITIONAL([ENABLE_CXX], [test "x$micro_benchmark_enable_cxx" = xyes])
  AM_COND_IF([ENABLE_CXX],
   [AC_CONFIG_FILES([include/mbenchmark/c++/version.hpp])
    AC_CONFIG_FILES([tests/c++/print.tap], [chmod +x tests/c++/print.tap])
   ])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_GUILE],
 [AC_ARG_ENABLE([guile],
   [AS_HELP_STRING([--disable-guile],
     [Do not compile guile bindings, even when they are available])],
   [micro_benchmark_enable_guile=$enableval],
   [micro_benchmark_enable_guile=check])

  AC_MSG_CHECKING([if Guile binding is requested])
  AC_MSG_RESULT([$micro_benchmark_enable_guile])
  AS_IF([test "x$micro_benchmark_enable_guile" != xno],
   [AC_MSG_CHECKING([if open_memstream or tmpfile are available])
    AS_IF([test "x$ac_cv_func_open_memstream" = xyes],
     [AC_MSG_RESULT([yes])],
     [test "x$ac_cv_func_tmpfile" = xyes],
     [AC_MSG_RESULT([yes])],
     [test "x$micro_benchmark_enable_guile" != xcheck],
     [AC_MSG_RESULT([no])
      AC_MSG_ERROR([missing functions for guile bindings])],
     [micro_benchmark_enable_guile=no
      AC_MSG_RESULT([no])
     ])
   ])

  AS_IF([test "x$micro_benchmark_enable_guile" != xno],
   [GUILE_PKG([3.0 2.2], [:],
     [AS_IF([test "x$micro_benchmark_enable_guile" != xcheck],
       [AC_MSG_ERROR([Guile not found.])],
       [micro_benchmark_enable_guile=no])
     ])
   ])

  AS_IF([test "x$micro_benchmark_enable_guile" != xno],
   [AS_IF([test "x$GUILE_EFFECTIVE_VERSION" = x],
     [AS_IF([test "x$micro_benchmark_enable_guile" != xcheck],
      [AC_MSG_ERROR([Guile not found])],
      [micro_benchmark_enable_guile=no])],
     [GUILE_PROGS
      GUILE_FLAGS
      GUILE_SITE_DIR
      AC_SUBST([GUILE_EFFECTIVE_VERSION])

      AC_MSG_CHECKING([for Guile datarootdir])
      GUILE_DATAROOTDIR=`$PKG_CONFIG --print-errors --variable=datarootdir guile-$GUILE_EFFECTIVE_VERSION`
      AC_MSG_RESULT([$GUILE_DATAROOTDIR])

      AC_MSG_CHECKING([for Guile .scm subdirectory])
      GUILE_SCM_BASE=`echo $GUILE_SITE | $SED -e "s,^$GUILE_DATAROOTDIR/,,"`
      AS_IF([test "x$GUILE_SCM_BASE" = x],
       [AS_IF([test "x$micro_benchmark_enable_guile" != xcheck],
         [AC_MSG_RESULT([no])
          AC_MSG_ERROR([Empty site subdirectory])],
         [AC_MSG_RESULT([empty])
          micro_benchmark_enable_guile=no])],
       [AC_MSG_RESULT([$GUILE_SCM_BASE])])
      AC_SUBST([GUILE_SCM_BASE], [$GUILE_SCM_BASE])

      AC_MSG_CHECKING([for Guile libdir])
      GUILE_LIBDIR=`$PKG_CONFIG --print-errors --variable=libdir guile-$GUILE_EFFECTIVE_VERSION`
      AC_MSG_RESULT([$GUILE_LIBDIR])

      AC_MSG_CHECKING([for Guile .go subdirectory])
      GUILE_GO_BASE=`echo $GUILE_SITE_CCACHE | $SED -e "s,^$GUILE_LIBDIR/,,"`
      AS_IF([test "x$GUILE_GO_BASE" = x],
       [AS_IF([test "x$micro_benchmark_enable_guile" != xcheck],
         [AC_MSG_RESULT([no])
          AC_MSG_ERROR([Empty site-ccache subdirectory])],
         [AC_MSG_RESULT([empty])
          micro_benchmark_enable_guile=no])],
       [AC_MSG_RESULT([$GUILE_GO_BASE])])
      AC_SUBST([GUILE_GO_BASE], [$GUILE_GO_BASE])

      AC_MSG_CHECKING([for Guile extension subdirectory])
      GUILE_EXT_BASE=`echo $GUILE_EXTENSION | $SED -e "s,^$GUILE_LIBDIR/,,"`
      AS_IF([test "x$GUILE_GO_BASE" = x],
       [AS_IF([test "x$micro_benchmark_enable_guile" != xcheck],
         [AC_MSG_RESULT([no])
          AC_MSG_ERROR([Empty extension subdirectory])],
         [AC_MSG_RESULT([empty])
          micro_benchmark_enable_guile=no])],
       [AC_MSG_RESULT([$GUILE_EXT_BASE])])
      AC_SUBST([GUILE_EXT_BASE], [$GUILE_EXT_BASE])

      GUILE_MODULE_AVAILABLE([micro_benchmark_guile_has_exceptions],
       [(ice-9 exceptions)])
      micro_benchmark_exceptions_version=new
      AS_IF([test "x$micro_benchmark_guile_has_exceptions" = xno],
       [micro_benchmark_exceptions_version=old])
      AC_SUBST([EXCEPTIONS_VERSION], [$micro_benchmark_exceptions_version])

      micro_benchmark_enable_guile=yes
      AC_ARG_VAR([GUILEFLAGS], [Flags used for Guile execution])
      : ${GUILDFLAGS=-O2}
      AC_ARG_VAR([GUILDFLAGS], [Flags used for Guile compilation])
     ])
   ])
  AM_CONDITIONAL([HAS_ICE9_EXCEPTIONS],
   [test "x$micro_benchmark_guile_has_exceptions" = xyes])
  AM_CONDITIONAL([ENABLE_GUILE], [test "x$micro_benchmark_enable_guile" = xyes])
  AM_COND_IF([ENABLE_GUILE],
   [AC_CONFIG_FILES([guile/mbenchmark/c/config.scm
                     guile/mbenchmark/config.scm
                     guile/mbenchmark/exceptions.scm])
    AC_CONFIG_FILES([tests/guile/main.tap], [chmod +x tests/guile/main.tap])
   ])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_LIBM],
 [oldLIBS=$LIBS
  LIBS=
  LIBM_LIBS=
  AC_SEARCH_LIBS([pow], [m], [LIBM_LIBS=$LIBS], [AC_MSG_ERROR([pow not found])])
  AC_SUBST([LIBM_LIBS], [$LIBM_LIBS])
  LIBS=$oldLIBS
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_LIBUNISTRING],
 [LIBUNISTRING_CPPFLAGS=
  AC_ARG_WITH([libunistring],
   [AS_HELP_STRING([--with-libunistring=DIR],
     [Provide libunistring installation prefix DIR])],
   [micro_benchmark_with_unistring=$withval],
   [micro_benchmark_with_unistring=])

  AC_ARG_WITH([libunistring-include],
   [AS_HELP_STRING([--with-libunistring-include=DIR],
     [Provide libunistring installation include DIR])],
   [micro_benchmark_with_unistring_include=$withval],
   [AS_IF([test "x$micro_benchmark_with_unistring" != x],
     [micro_benchmark_with_unistring_include="$micro_benchmark_with_unistring"/include],
     [micro_benchmark_with_unistring_include=])
   ])

  AC_ARG_WITH([libunistring-libs],
   [AS_HELP_STRING([--with-libunistring-libs=DIR],
     [Provide libunistring installation lib DIR])],
   [micro_benchmark_with_unistring_libs=$withval],
   [AS_IF([test "x$micro_benchmark_with_unistring" != x],
     [micro_benchmark_with_unistring_libs="$micro_benchmark_with_unistring"/lib],
     [micro_benchmark_with_unistring_libs=])
   ])

  oldCPPFLAGS=$CPPFLAGS
  CPPFLAGS=
  AS_IF([test "x$micro_benchmark_with_unistring_include" != x],
   [CPPFLAGS="-I$micro_benchmark_with_unistring_include"])

  AC_CHECK_HEADERS([unitypes.h unistr.h uniconv.h], [],
   [AC_MSG_ERROR([libunistring headers not found])])
  AC_CHECK_HEADERS([uniwidth.h uninorm.h unistdio.h], [],
   [AC_MSG_ERROR([libunistring headers not found])])

  oldLIBS=$LIBS
  LIBS=
  AS_IF([test "x$micro_benchmark_with_unistring_libs" != x],
   [LIBS="-L $micro_benchmark_with_unistring_libs"])
  LIBS="$LIBS -lunistring"
  AC_LANG_PUSH([C])
  AC_MSG_CHECKING([libunistring provides needed functions])
  AC_LINK_IFELSE(
   [AC_LANG_PROGRAM(
     [[#include <stdio.h>
       #include <stdlib.h>

       #include <unitypes.h>
       #include <unistr.h>
       #include <uniconv.h>
       #include <uniwidth.h>
       #include <uninorm.h>
       #include <unistdio.h>
     ]],
     [[uint8_t *str = u8_strconv_from_locale ("test");
       size_t s = u8_strlen (str);
       size_t w = u8_strwidth (str, locale_charset ());
       uint8_t *n = u8_normalize (UNINORM_NFC, str, s, NULL, NULL);
       int ret = ulc_fprintf (stdout, "%U", n);
       free (n);
       free (str);
     ]])],
    [LIBUNISTRING_LIBS=$LIBS
     LIBUNISTRING_CPPFLAGS=$CPPFLAGS
     AC_MSG_RESULT([yes])],
    [AC_MSG_FAILURE([some functions are not available])])
  AC_LANG_POP([C])

  AC_SUBST([LIBUNISTRING_LIBS], [$LIBUNISTRING_LIBS])
  AC_SUBST([LIBUNISTRING_CPPFLAGS], [$LIBUNISTRING_CPPFLAGS])
  LIBS=$oldLIBS
  CPPFLAGS=$oldCPPFLAGS
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_LIBGMP],
 [AC_ARG_ENABLE([multi-precision],
   [AS_HELP_STRING([--enable-multi-precision],
     [Enable multi-precision statistics (requires GMP)])],
   [micro_benchmark_enable_mp=$withval],
   [micro_benchmark_enable_mp=check])

  AC_ARG_WITH([libgmp],
   [AS_HELP_STRING([--with-libgmp=DIR],
     [Provide libgmp installation prefix DIR])],
   [micro_benchmark_with_libgmp=$withval],
   [micro_benchmark_with_libgmp=])

  AC_ARG_WITH([libgmp-include],
   [AS_HELP_STRING([--with-libgmp-include=DIR],
     [Provide libgmp installation include DIR])],
   [micro_benchmark_with_libgmp_include=$withval],
   [AS_IF([test "x$micro_benchmark_with_libgmp" != x],
     [micro_benchmark_with_libgmp_include="$micro_benchmark_with_libgmp"/include],
     [micro_benchmark_with_libgmp_include=])
   ])

  AC_ARG_WITH([libgmp-libs],
   [AS_HELP_STRING([--with-libgmp-libs=DIR],
     [Provide libgmp installation lib DIR])],
   [micro_benchmark_with_libgmp_libs=$withval],
   [AS_IF([test "x$micro_benchmark_with_libgmp" != x],
     [micro_benchmark_with_libgmp_libs="$micro_benchmark_with_libgmp"/lib],
     [micro_benchmark_with_libgmp_libs=])
   ])

  LIBGMP_CPPFLAGS=
  LIBGMP_LIBS=
  oldCPPFLAGS=$CPPFLAGS
  CPPFLAGS=
  oldLIBS=$LIBS
  LIBS=

  AS_IF([test "x$micro_benchmark_enable_mp" != xno],
   [AS_IF([test "x$micro_benchmark_with_libgmp_include" != x],
     [CPPFLAGS="-I$micro_benchmark_with_libgmp_include"])

    AC_CHECK_HEADERS([gmp.h], [],
     [AS_IF([test "x$micro_benchmark_enable_mp" = xcheck],
       [micro_benchmark_enable_mp=no],
       [AC_MSG_ERROR([gmp.h not found])])])
   ])

  AS_IF([test "x$micro_benchmark_enable_mp" != xno],
   [AS_IF([test "x$micro_benchmark_with_libgmp_libs" != x],
     [LIBS="-L $micro_benchmark_with_libgmp_libs"])
    LIBS="$LIBS -lgmp"
    AC_LANG_PUSH([C])
    AC_MSG_CHECKING([libgmp provides needed functions])
    AC_LINK_IFELSE(
     [AC_LANG_PROGRAM(
       [[#include <gmp.h>
       ]],
     [[mpz_t val;
       mpz_inits (val, NULL);
       mpz_set_ui (val, 1000000000);
       mpz_clears (val, NULL);
     ]])],
    [micro_benchmark_enable_mp=yes
     LIBGMP_LIBS=$LIBS
     LIBGMP_CPPFLAGS=$CPPFLAGS
     AC_MSG_RESULT([yes])
     AC_SUBST([LIBGMP_LIBS], [$LIBGMP_LIBS])
     AC_SUBST([LIBGMP_CPPFLAGS], [$LIBGMP_CPPFLAGS])
     AC_DEFINE([HAVE_GMP], [1], [Defined if gmp is available])],
    [AS_IF([test "x$micro_benchmark_enable_mp" = xcheck],
       [micro_benchmark_enable_mp=no],
       [AC_MSG_ERROR([some libgmp functions are not available])])])
    AC_LANG_POP([C])
   ])

  LIBS=$oldLIBS
  CPPFLAGS=$oldCPPFLAGS

  AM_CONDITIONAL([ENABLE_MP], [test "x$micro_benchmark_enable_mp" != xno])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_TIME_TYPES],
 [AC_CHECK_HEADERS([time.h],
   [AC_CHECK_TYPE([struct timespec],
     [micro_benchmark_have_timespec=yes
      AC_DEFINE([HAVE_STRUCT_TIMESPEC], [1],
       [struct timespec is available on this system])],
     [micro_benchmark_have_timespec=no],
     [[#include <time.h>
     ]])
   ],
   [AC_MSG_ERROR([time.h not found])])

  AC_CHECK_HEADERS([sys/time.h],
   [micro_benchmark_have_sys_time=yes
    AC_CHECK_TYPE([struct timeval],
     [micro_benchmark_have_timeval=yes
      AC_DEFINE([HAVE_STRUCT_TIMEVAL], [1],
       [struct timeval is available on this system])],
     [micro_benchmark_have_timeval=no],
     [[#include <sys/time.h>]])

    AC_CHECK_TYPE([__itimer_which_t],
     [AC_DEFINE([HAVE_UU_ITIMER_WHICH_T], [1],
       [Determine if __itimer_which_t is used to mock get/setitimer])],
     [], [[#include <sys/time.h>]])
   ],
   [micro_benchmark_have_sys_time=no
    micro_benchmark_have_timeval=no])

  AM_CONDITIONAL([HAVE_TIMESPEC], [test "x$micro_benchmark_have_timespec" = xyes])
  AM_CONDITIONAL([HAVE_TIMEVAL], [test "x$micro_benchmark_have_timeval" = xyes])

  AS_IF([test "x$micro_benchmark_have_timeval" = xno \
         && test "x$micro_benchmark_have_timespec" = xno],
   [AC_MSG_ERROR([No time type available])])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_CLOCK_TYPES],
 [AC_MSG_CHECKING([for CLOCK_TAI])
  micro_benchmark_have_clock_tai=
  AC_COMPILE_IFELSE(
   [AC_LANG_PROGRAM(
     [[#include <time.h>
     ]],
     [[clockid_t c = CLOCK_TAI]])],
   [micro_benchmark_have_clock_tai=yes
    AC_DEFINE([HAVE_CLOCK_TAI], [1], [The system has CLOCK_TAI])],
   [micro_benchmark_have_clock_tai=no])
  AC_MSG_RESULT([$micro_benchmark_have_clock_tai])

  micro_benchmark_have_clock_process=
  AC_MSG_CHECKING([for CLOCK_PROCESS_CPUTIME_ID])
  AC_COMPILE_IFELSE(
   [AC_LANG_PROGRAM(
     [[#include <time.h>
     ]],
     [[clockid_t c = CLOCK_PROCESS_CPUTIME_ID]])],
   [micro_benchmark_have_clock_process=yes
    AC_DEFINE([HAVE_CLOCK_PROCESS_CPUTIME_ID], [1],
     [The system has CLOCK_PROCESS_CPUTIME_ID])],
   [micro_benchmark_have_clock_process=no])
  AC_MSG_RESULT([$micro_benchmark_have_clock_process])

  micro_benchmark_have_clock_thread=
  AC_MSG_CHECKING([for CLOCK_THREAD_CPUTIME_ID])
  AC_COMPILE_IFELSE(
   [AC_LANG_PROGRAM(
     [[#include <time.h>
     ]],
     [[clockid_t c = CLOCK_THREAD_CPUTIME_ID]])],
   [micro_benchmark_have_clock_thread=yes
    AC_DEFINE([HAVE_CLOCK_THREAD_CPUTIME_ID], [1],
     [The system has CLOCK_THREAD_CPUTIME_ID])],
   [micro_benchmark_have_clock_thread=no])
  AC_MSG_RESULT([$micro_benchmark_have_clock_thread])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_CLOCK_GETTIME],
 [AC_ARG_ENABLE([clock-gettime],
   [AS_HELP_STRING([--enable-clock-gettime],
     [Enable the clock_gettime timing interface])],
   [micro_benchmark_have_clock_gettime=$enableval],
   [micro_benchmark_have_clock_gettime=check])

  AC_MSG_CHECKING([if clock_gettime support is requested])
  AC_MSG_RESULT([$micro_benchmark_have_clock_gettime])

  AS_IF([test "x$micro_benchmark_have_clock_gettime" != xno],
   [AC_MSG_CHECKING([if struct timespec is available])
    AC_MSG_RESULT([$micro_benchmark_have_timespec])
    AS_IF([test "x$micro_benchmark_have_timespec" != xyes],
     [AS_IF([test "x$micro_benchmark_have_clock_gettime" != xcheck],
       [AC_MSG_ERROR([struct timespec is needed for clock gettime])],
       [micro_benchmark_have_clock_gettime=no])
     ])
   ])

  AS_IF([test "x$micro_benchmark_have_clock_gettime" != xno],
   [oldLIBS=$LIBS
    LIBS=
    AC_SEARCH_LIBS([clock_gettime], [rt],
     [micro_benchmark_have_clock_gettime=yes
      AC_DEFINE([HAVE_CLOCK_GETTIME], [1], [Have clock_gettime function])],
     [AS_IF([test "x$micro_benchmark_have_clock_gettime" != xcheck],
       [AC_MSG_ERROR([clock_gettime was enabled but was not found])],
       [micro_benchmark_have_clock_gettime=no])])

    AC_SUBST([CLOCK_GETTIME_LIBS], [$LIBS])
    LIBS=$oldLIBS
   ])

  AM_CONDITIONAL([HAVE_CLOCK_GETTIME],
    [test "x$micro_benchmark_have_clock_gettime" = xyes])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_GETTIMEOFDAY],
 [AC_ARG_ENABLE([gettimeofday],
   [AS_HELP_STRING([--enable-gettimeofday],
     [Enable the gettimeofday timing interface])],
   [micro_benchmark_have_gettimeofday=$enableval],
   [micro_benchmark_have_gettimeofday=check])

  AC_MSG_CHECKING([if gettimeofday support is requested])
  AC_MSG_RESULT([$micro_benchmark_have_gettimeofday])

  AS_IF([test "x$micro_benchmark_have_gettimeofday" != xno],
   [AC_MSG_CHECKING([if struct timeval is available])
    AC_MSG_RESULT([$micro_benchmark_have_timeval])
    AS_IF([test "x$micro_benchmark_have_timeval" != xyes],
     [AS_IF([test "x$micro_benchmark_have_gettimeofday" != xcheck],
       [AC_MSG_ERROR([struct timeval is needed for gettimeofday])],
       [micro_benchmark_have_gettimeofday=no])
     ])
   ])

  AS_IF([test "x$micro_benchmark_have_gettimeofday" != xno],
   [AC_CHECK_FUNC([gettimeofday],
     [micro_benchmark_have_gettimeofday=yes
      AC_DEFINE([HAVE_GETTIMEOFDAY], [1], [Have gettimeofday function])],
     [AS_IF([test "x$micro_benchmark_have_gettimeofday" != xcheck],
       [AC_MSG_ERROR([gettimeofday was enabled but was not found])],
       [micro_benchmark_have_gettimeofday=no])])])

  AM_CONDITIONAL([HAVE_GETTIMEOFDAY],
    [test "x$micro_benchmark_have_gettimeofday" = xyes])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_ITIMER],
 [AC_ARG_ENABLE([itimer],
   [AS_HELP_STRING([--enable-itimer],
     [Enable the setitimer interface])],
   [micro_benchmark_have_itimer=$enableval],
   [micro_benchmark_have_itimer=check])

  AC_MSG_CHECKING([if itimer support is requested])
  AC_MSG_RESULT([$micro_benchmark_have_itimer])

  AS_IF([test "x$micro_benchmark_have_itimer" != xno],
   [AC_MSG_CHECKING([if struct timeval is available])
    AC_MSG_RESULT([$micro_benchmark_have_timeval])
    AS_IF([test "x$micro_benchmark_have_timeval" != xyes],
     [AS_IF([test "x$micro_benchmark_have_itimer" != xcheck],
       [AC_MSG_ERROR([struct timeval is needed for itimer])],
       [micro_benchmark_have_itimer=no])
     ])
   ])

  AS_IF([test "x$micro_benchmark_have_itimer" != xno],
   [AC_MSG_CHECKING([if the required signals are available])
    AC_LANG_PUSH([C])
    AC_COMPILE_IFELSE(
     [AC_LANG_PROGRAM(
       [[#include <signal.h>
       ]],
       [[int sig = SIGALRM;
         sig = SIGVTALRM;
         sig = SIGPROF;
       ]])],
     [AC_MSG_RESULT([yes])],
     [AS_IF([test "x$micro_benchmark_have_itimer" = xcheck],
       [AC_MSG_RESULT([no])
        micro_benchmark_have_itimer=no],
       [AC_MSG_FAILURE([A needed alarm symbol cannot be found])])
     ])
   ])

  AS_IF([test "x$micro_benchmark_have_itimer" != xno],
   [AC_MSG_CHECKING([if the required timers are available])
    AC_LANG_PUSH([C])
    AC_COMPILE_IFELSE(
     [AC_LANG_PROGRAM(
       [[#include <sys/time.h>
       ]],
       [[int timer = ITIMER_PROF;
         timer = ITIMER_VIRTUAL;
         timer = ITIMER_REAL;
       ]])],
     [AC_MSG_RESULT([yes])],
     [AS_IF([test "x$micro_benchmark_have_itimer" = xcheck],
       [AC_MSG_RESULT([no])
        micro_benchmark_have_itimer=no],
       [AC_MSG_FAILURE([A needed timer symbol cannot be found])])
     ])
   ])

  AS_IF([test "x$micro_benchmark_have_itimer" != xno],
   [AC_CHECK_FUNC([setitimer],
     [micro_benchmark_have_itimer=yes
      AC_DEFINE([HAVE_ITIMER], [1], [Have itimer functions])],
     [AS_IF([test "x$micro_benchmark_have_itimer" = xcheck],
       [AC_MSG_RESULT([no])
        micro_benchmark_have_itimer=no],
       [AC_MSG_FAILURE([itimer was requested but it was not found])])
     ])
   ])

  AM_CONDITIONAL([HAVE_ITIMER],
    [test "x$micro_benchmark_have_itimer" = xyes])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_TIMER_T],
 [AC_ARG_ENABLE([timer-t],
   [AS_HELP_STRING([--enable-timer-t],
     [Enable the create_timer timing interface])],
   [micro_benchmark_have_timer_t=$enableval],
   [micro_benchmark_have_timer_t=check])

  AC_MSG_CHECKING([if timer_t support is requested])
  AC_MSG_RESULT([$micro_benchmark_have_timer_t])

  AS_IF([test "x$micro_benchmark_have_timer_t" != xno],
   [AC_MSG_CHECKING([if struct timespec is available])
    AC_MSG_RESULT([$micro_benchmark_have_timespec])
    AS_IF([test "x$micro_benchmark_have_timespec" != xyes],
     [AS_IF([test "x$micro_benchmark_have_timer_t" != xcheck],
       [AC_MSG_ERROR([struct timespec is needed for timer_t])],
       [micro_benchmark_have_timer_t=no])
     ])
   ])

  AS_IF([test "x$micro_benchmark_have_timer_t" != xno],
   [AC_CHECK_HEADERS([signal.h], [],
     [AS_IF([test "x$micro_benchmark_have_timer_t" != xcheck],
       [AC_MSG_ERROR([timer_t was enabled but was not found])],
       [micro_benchmark_have_timer_t=no])])

    oldLIBS=$LIBS
    LIBS=
    AC_SEARCH_LIBS([timer_create], [rt],
     [micro_benchmark_have_timer_t=yes
      AC_DEFINE([HAVE_TIMER_T], [1], [Have timer_t functions])],
     [AS_IF([test "x$micro_benchmark_have_timer_t" != xcheck],
       [AC_MSG_ERROR([create_timer was enabled but was not found])],
       [micro_benchmark_have_timer_t=no])])

    AC_SUBST([TIMER_T_LIBS], [$LIBS])
    LIBS=$oldLIBS])

  AM_CONDITIONAL([HAVE_TIMER_T],
    [test "x$micro_benchmark_have_timer_t" = xyes])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_TIMERFD],
 [AC_ARG_ENABLE([timerfd],
   [AS_HELP_STRING([--enable-timerfd],
     [Enable the timerfd timing interface])],
   [micro_benchmark_have_timerfd=$enableval],
   [micro_benchmark_have_timerfd=check])

  AC_MSG_CHECKING([if create_timerfd is requested])
  AC_MSG_RESULT([$micro_benchmark_have_timerfd])

  AS_IF([test "x$micro_benchmark_have_timerfd" != xno],
   [AC_MSG_CHECKING([if struct timespec is available])
    AC_MSG_RESULT([$micro_benchmark_have_timespec])
    AS_IF([test "x$micro_benchmark_have_timespec" != xyes],
     [AS_IF([test "x$micro_benchmark_have_timerfd" != xcheck],
       [AC_MSG_ERROR([struct timespec is needed for timerfd])],
       [micro_benchmark_have_timerfd=no])
     ])
   ])

  AS_IF([test "x$micro_benchmark_have_timerfd" != xno],
   [AC_CHECK_HEADERS([sys/timerfd.h], [],
     [AS_IF([test "x$micro_benchmark_have_timerfd" != xcheck],
       [AC_MSG_ERROR([timerfd header was not found])],
       [micro_benchmark_have_timerfd=no])])

    AC_CHECK_FUNC([timerfd_create],
     [micro_benchmark_have_timerfd=yes
      AC_DEFINE([HAVE_TIMERFD], [1], [Have timerfd functions])],
     [AS_IF([test "x$micro_benchmark_have_timerfd" != xcheck],
       [AC_MSG_ERROR([timerfd_create was enabled was not found])],
       [micro_benchmark_have_timerfd=no])])
   ])

  AM_CONDITIONAL([HAVE_TIMERFD],
    [test "x$micro_benchmark_have_timerfd" = xyes])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_CLOCK_T],
 [AC_CHECK_TYPE([clock_t], [],
   [AC_MSG_ERROR([clock_t type not found])],
   [[#include <time.h>]])
  AC_CHECK_SIZEOF([clock_t])

  AC_CHECK_FUNC([clock], [],
   [AC_MSG_ERROR([clock function not found])])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_TIME_T],
 [AC_CHECK_TYPE([time_t], [],
   [AC_MSG_ERROR([time_t type not found])],
   [[#include <time.h>]])

  AC_CHECK_FUNC([time], [],
   [AC_MSG_ERROR([time function not found])])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_CLOCKS],
 [MICRO_BENCHMARK_CHECK_TIME_TYPES
  MICRO_BENCHMARK_CHECK_CLOCK_TYPES
  MICRO_BENCHMARK_CHECK_CLOCK_GETTIME
  MICRO_BENCHMARK_CHECK_GETTIMEOFDAY
  MICRO_BENCHMARK_CHECK_CLOCK_T
  MICRO_BENCHMARK_CHECK_TIME_T
  MICRO_BENCHMARK_CHECK_ITIMER
  MICRO_BENCHMARK_CHECK_TIMER_T
  MICRO_BENCHMARK_CHECK_TIMERFD
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_TEST_LOCALE],
 [AC_MSG_CHECKING([for a suitable locale for tests])
  mb_test_locale=C
  AS_IF([locale -a | $FGREP -q en_US.UTF-8],
   [mb_test_locale=en_US.UTF-8],
   [locale -a | $FGREP -q C.UTF-8],
   [mb_test_locale=C.UTF-8])
  AC_MSG_RESULT([$mb_test_locale])
  AC_SUBST([TEST_ENV_LOCALE], [$mb_test_locale])
 ])

AC_DEFUN([MICRO_BENCHMARK_PRINT_RESULT],
 [AS_BOX([      MicroBenchmark $PACKAGE_VERSION: Build Options       ])
  AS_ECHO([])

  AS_ECHO(["Generic Options:"])
  AS_ECHO_N(["  - Compiler warning are errors: ............ "])
  AS_ECHO(["$micro_benchmark_enable_werror"])
  AS_ECHO_N(["  - Compile with -fsanitize: ................ "])
  AS_ECHO(["$micro_benchmark_enable_sanitizer"])
  AS_ECHO_N(["  - Compile C with -fanalyzer: .............. "])
  AS_ECHO(["$micro_benchmark_enable_analyzer"])
  AS_ECHO_N(["  - Compile C++ with -fanalyzer: ............ "])
  AS_ECHO(["$micro_benchmark_enable_cxx_analyzer"])
  AS_ECHO_N(["  - Emit assert calls: ...................... "])
  AS_IF([test "x$enable_assert" = x],
    [AS_ECHO([yes])],
    [AS_ECHO(["$enable_assert"])])
  AS_ECHO_N(["  - Generate coverage report: ............... "])
  AS_ECHO(["$micro_benchmark_enable_coverage"])
  AS_ECHO([])

  AS_ECHO(["Logging Features:"])
  AS_ECHO_N(["  - Default log level: ...................... "])
  AS_ECHO(["$micro_benchmark_default_log_level"])
  AS_ECHO_N(["  - Emit trace messages: .................... "])
  AS_ECHO(["$micro_benchmark_have_traces"])
  AS_ECHO([])

  AS_ECHO(["Statistics Features:"])
  AS_ECHO_N(["  - Basic: .................................. "])
  AS_ECHO([yes])
  AS_ECHO_N(["  - Multi-precision: ........................ "])
  AS_ECHO(["$micro_benchmark_enable_mp"])
  AS_ECHO([])

  AS_ECHO(["Clock Features:"])
  AS_ECHO_N(["  - Have CLOCK_TAI: ......................... "])
  AS_ECHO(["$micro_benchmark_have_clock_tai"])
  AS_ECHO_N(["  - Have CLOCK_PROCESS_CPUTIME_ID: .......... "])
  AS_ECHO(["$micro_benchmark_have_clock_process"])
  AS_ECHO_N(["  - Have CLOCK_THREAD_CPUTIME_ID: ........... "])
  AS_ECHO(["$micro_benchmark_have_clock_thread"])
  AS_ECHO_N(["  - Build timespec helpers: ................. "])
  AS_ECHO(["$micro_benchmark_have_timespec"])
  AS_ECHO_N(["  - Build timeval helpers: .................. "])
  AS_ECHO(["$micro_benchmark_have_timeval"])
  AS_ECHO([])

  AS_ECHO(["Chronometers:"])
  AS_ECHO_N(["  - clock-t: ................................ "])
  AS_ECHO([yes])
  AS_ECHO_N(["  - time-t: ................................. "])
  AS_ECHO([yes])
  AS_ECHO_N(["  - clock_gettime: .......................... "])
  AS_ECHO(["$micro_benchmark_have_clock_gettime"])
  AS_ECHO_N(["  - gettimeofday: ........................... "])
  AS_ECHO(["$micro_benchmark_have_gettimeofday"])
  AS_ECHO([])

  AS_ECHO(["Timers:"])
  AS_ECHO_N(["  - chrono-adapter: ......................... "])
  AS_ECHO([yes])
  AS_ECHO_N(["  - itimer: ................................. "])
  AS_ECHO(["$micro_benchmark_have_itimer"])
  AS_ECHO_N(["  - timer-t: ................................ "])
  AS_ECHO(["$micro_benchmark_have_timer_t"])
  AS_ECHO_N(["  - timerfd: ................................ "])
  AS_ECHO(["$micro_benchmark_have_timerfd"])
  AS_ECHO([])

  AS_ECHO(["Languages:"])
  AS_ECHO_N(["  - C: ...................................... "])
  AS_ECHO([yes])
  AS_ECHO_N(["  - C++: .................................... "])
  AS_ECHO(["$micro_benchmark_enable_cxx"])
  AS_ECHO_N(["  - Guile: .................................. "])
  AS_ECHO(["$micro_benchmark_enable_guile"])
  AS_ECHO([])

  AS_BOX([           Now, you can run make -j           ])
  AS_ECHO([])
 ])
