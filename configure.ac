# configure.ac --- Build configuration for MicroBenchmark.
#
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is part of MicroBenchmark.
#
# MicroBenchmark is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Benchmark is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MicroBenchmark.  If not, see
# <http://www.gnu.org/licenses/>.
AC_PREREQ([2.69])
AC_INIT([MicroBenchmark], [0.0], [bug-mbenchmark@nongnu.org],
  [mbenchmark], [https://www.nongnu.org/mbenchmark/])

AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_SRCDIR([include/mbenchmark/all.h])
AC_CONFIG_MACRO_DIRS([m4])
AC_CONFIG_HEADERS([include/config.h])
AC_REQUIRE_AUX_FILE([tap-driver.sh])
AH_TOP([
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
])

AM_INIT_AUTOMAKE([-Wall -Werror gnu subdir-objects])
AM_SILENT_RULES([yes])

AC_USE_SYSTEM_EXTENSIONS

# Gettext
AM_GNU_GETTEXT([external])
AM_GNU_GETTEXT_VERSION([0.21])

MICRO_BENCHMARK_CHECK_COMMON_UTILITIES
MICRO_BENCHMARK_CHECK_TEST_LOCALE

# Libtool initialization
AC_PROG_CC
AC_PROG_CC_C99
AS_IF([test "x$ac_cv_prog_cc_c99" = xno],
 [AC_MSG_ERROR([C99 compiler is required to build the library])])
AC_PROG_CXX
AM_PROG_AR
LT_INIT
AC_SUBST([LIBTOOL_DEPS])
AC_SUBST([OBJDIR], [$objdir])

MICRO_BENCHMARK_CHECK_COMMON_HEADERS
MICRO_BENCHMARK_CHECK_COMMON_FUNCTIONS
MICRO_BENCHMARK_CHECK_COMPILER_FLAGS

# Library checks
MICRO_BENCHMARK_CHECK_COVERAGE
MICRO_BENCHMARK_CHECK_TRACES
MICRO_BENCHMARK_CHECK_LIBGMP
MICRO_BENCHMARK_CHECK_LIBM
MICRO_BENCHMARK_CHECK_LIBUNISTRING
MICRO_BENCHMARK_CHECK_CLOCKS

# Bindings
MICRO_BENCHMARK_CHECK_CXX
MICRO_BENCHMARK_CHECK_GUILE

# Output
AC_CONFIG_FILES([Makefile
                 build-aux/test-env.sh
                 po/Makefile.in])
AC_CONFIG_FILES([tests/print.tap], [chmod +x tests/print.tap])
AC_OUTPUT

MICRO_BENCHMARK_PRINT_RESULT

dnl Local Variables:
dnl mode: autoconf
dnl End:
