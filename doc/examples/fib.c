/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.h>
#include <stdio.h>
#include <string.h>

/* Main can be placed anywhere.  */
MICRO_BENCHMARK_MAIN ();

/* Recursive implementation -- overflows with small values!.  */
long
fibrec (int x)
{
  if (x > 1)
    return fibrec (x - 1) + fibrec (x - 2);
  if (x < 0)
    return fibrec (x + 2) - fibrec (x + 1);
  return x;
}

/* Iterative implementation -- overflows with small values!.  */
long
fibit (int x)
{
  long curr, last;
#define impl(c, l, start, sign) \
  last = l; \
  curr = c; \
  for (int i = start; i < sign x; ++i) \
    { \
      long tmp = last sign curr; \
      last = curr; \
      curr = tmp; \
    } \
  return curr

  if (x > 0)
    {
      impl (1, 0, 1, +);
    }
  impl (0, 1, 0, -);
#undef impl
}

/* Prepare the pointer and its value.  */
static int *
set_up (micro_benchmark_test_state s)
{
  static int value = 0;

  value = micro_benchmark_state_get_size (s, 0);
  value *= micro_benchmark_state_get_size (s, 1) ? 1 : -1;
  return &value;
}

static void
tear_down (micro_benchmark_test_state s, int *ptr)
{
  char buf[100];
  const char *name = micro_benchmark_state_get_name (s);
  if (strcmp (name, "test_fibit") == 0)
    snprintf (buf, sizeof (buf) - 1, "fibit/%d", *ptr);
  else
    snprintf (buf, sizeof (buf) - 1, "fibrec/%d", *ptr);
  buf[sizeof (buf) - 1] = '\0';
  micro_benchmark_state_set_name (s, buf);
}

/* Recursive implementation test.  */
static void
test_fibrec (int *r)
{
  long fib = fibrec (*r);
  MICRO_BENCHMARK_DO_NOT_OPTIMIZE (fib);
}

MICRO_BENCHMARK_REGISTER_AUTO_TEST (set_up, test_fibrec, tear_down);

/* Iterative implementation test.  */
static void
test_fibit (int *r)
{
  long fib = fibit (*r);
  MICRO_BENCHMARK_DO_NOT_OPTIMIZE (fib);
}

MICRO_BENCHMARK_REGISTER_AUTO_TEST (set_up, test_fibit, tear_down);

/* Constraints on the recursive test.  */
static void
rec_constraints (micro_benchmark_test_case test)
{
  const size_t sizes[] = { 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30 };
  const size_t ssizes = sizeof (sizes) / sizeof (*sizes);
  micro_benchmark_test_case_add_dimension (test, ssizes, sizes);

  const size_t sign[] = { 0, 1 };
  const size_t ssign = sizeof (sign) / sizeof (*sign);
  micro_benchmark_test_case_add_dimension (test, ssign, sign);

  micro_benchmark_clock_time mt = { 3, 0 };
  micro_benchmark_test_case_set_max_time (test, mt);
  micro_benchmark_test_case_limit_iterations (test, 0, 1000000);
}

MICRO_BENCHMARK_CONSTRAINT_TEST ("test_fibrec", rec_constraints);

/* Constraints on the iterative test, this is enough.  */
static void
it_constraints (micro_benchmark_test_case test)
{
  const size_t sizes[] = { 10, 20, 30, 40 };
  const size_t ssizes = sizeof (sizes) / sizeof (*sizes);
  micro_benchmark_test_case_add_dimension (test, ssizes, sizes);

  const size_t sign[] = { 0, 1 };
  const size_t ssign = sizeof (sign) / sizeof (*sign);
  micro_benchmark_test_case_add_dimension (test, ssign, sign);

  micro_benchmark_clock_time mt = { 3, 0 };
  micro_benchmark_test_case_set_max_time (test, mt);
  micro_benchmark_test_case_limit_iterations (test, 0, 1000000);
}

MICRO_BENCHMARK_CONSTRAINT_TEST ("test_fibit", it_constraints);
