@c Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
@c
@c This file is part of MicroBenchmark.
@c
@c Permission is granted to copy, distribute and/or modify this
@c document under the terms of the GNU Free Documentation License,
@c Version 1.3 or any later version published by the Free Software
@c Foundation; with no Invariant Sections, no Front-Cover Texts, and
@c no Back-Cover Texts.
@c
@c Aadditionally, this example is licensed under the GNU General
@c Public License, Version 3 or any later version published by the
@c Free Software Foundation.
@c
@c A copy of the GNU Free Documentation License and the GNU General
@c Public License are distributed along with MicroBenchmark. If not,
@c see <http://www.gnu.org/licenses/>.
@c
The usual output looks like this:
@example
@verbatim
(suite (name "lisp")
  (test (name "Self-Test")
    (execution (number 0)
      (name "empty")
      (total-time 0.326749 "s")
      (iterations 65536)
      (iteration-time
        (mean 22.072614 "ns")
        (std-deviation 4.105545 "ns")))
    (execution (number 1)
      (name "barrier")
      (total-time 0.326562 "s")
      (iterations 65536)
      (iteration-time
        (mean 21.374931 "ns")
        (std-deviation 1.093298 "ns")))
    (execution (number 2)
      (name "read+write")
      (total-time 0.333746 "s")
      (iterations 65536)
      (iteration-time
        (mean 28.694221 "ns")
        (std-deviation 3.253493 "ns")))
    (execution (number 3)
      (name "addition")
      (total-time 0.345400 "s")
      (iterations 65536)
      (iteration-time
        (mean 38.210674 "ns")
        (std-deviation 25.125504 "ns")))
    (execution (number 4)
      (name "multiplication")
      (total-time 0.339907 "s")
      (iterations 65536)
      (iteration-time
        (mean 35.148271 "ns")
        (std-deviation 5.568858 "ns")))
    (execution (number 5)
      (name "empty")
      (total-time 0.332254 "s")
      (iterations 65536)
      (iteration-time
        (mean 21.904489 "ns")
        (std-deviation 1.905702 "ns")))
    (number-of-runs 6))
  (test (name "test")
    (execution (number 0)
      (name "test")
      (total-time 5.087626 "s")
      (iterations 3981)
      (iteration-time
        (mean 1.240041 "ms")
        (std-deviation 0.074364 "ms")))
    (number-of-runs 1))
  (number-of-tests 2))
@end verbatim
@end example


And here are all the possible values:
@example
@verbatim
(suite (name "lisp")
  (test (name "Self-Test")
    (execution (number 0)
      (name "empty")
      (dimensions (0))
      (total-time 0.314982 "s")
      (total-iterations 65536)
      (iterations 65536)
      (total-samples 57)
      (used-samples 57)
      (iteration-time
        (mean 20.941117 "ns")
        (std-deviation 5.240370 "ns")
        (variance 27.461475 "ns²"))
      (sample-time
        (mean 24.478947 "μs")
        (std-deviation 16.031848 "μs")
        (variance 257.020146 "μs²"))
      (sample-iterations
        (mean 1149.754386)
        (std-deviation 629.857787)
        (variance 396720.831454))
      (custom-meters (total 0)))
    (execution (number 1)
      (name "barrier")
      (dimensions (1))
      (total-time 0.323575 "s")
      (total-iterations 65536)
      (iterations 65536)
      (total-samples 57)
      (used-samples 57)
      (iteration-time
        (mean 21.163393 "ns")
        (std-deviation 0.844636 "ns")
        (variance 0.713410 "ns²"))
      (sample-time
        (mean 24.239070 "μs")
        (std-deviation 13.764559 "μs")
        (variance 189.463080 "μs²"))
      (sample-iterations
        (mean 1149.754386)
        (std-deviation 652.575592)
        (variance 425854.902882))
      (custom-meters (total 0)))
    (execution (number 2)
      (name "read+write")
      (dimensions (2))
      (total-time 0.322978 "s")
      (total-iterations 65536)
      (iterations 65536)
      (total-samples 57)
      (used-samples 57)
      (iteration-time
        (mean 27.766774 "ns")
        (std-deviation 2.136562 "ns")
        (variance 4.564897 "ns²"))
      (sample-time
        (mean 31.624877 "μs")
        (std-deviation 17.903620 "μs")
        (variance 320.539599 "μs²"))
      (sample-iterations
        (mean 1149.754386)
        (std-deviation 652.000419)
        (variance 425104.545739))
      (custom-meters (total 0)))
    (execution (number 3)
      (name "addition")
      (dimensions (3))
      (total-time 0.324984 "s")
      (total-iterations 65536)
      (iterations 65536)
      (total-samples 58)
      (used-samples 58)
      (iteration-time
        (mean 31.652676 "ns")
        (std-deviation 7.132115 "ns")
        (variance 50.867060 "ns²"))
      (sample-time
        (mean 37.170897 "μs")
        (std-deviation 27.209318 "μs")
        (variance 740.347009 "μs²"))
      (sample-iterations
        (mean 1129.931034)
        (std-deviation 656.896073)
        (variance 431512.451301))
      (custom-meters (total 0)))
    (execution (number 4)
      (name "multiplication")
      (dimensions (4))
      (total-time 0.323793 "s")
      (total-iterations 65536)
      (iterations 65536)
      (total-samples 57)
      (used-samples 57)
      (iteration-time
        (mean 32.703045 "ns")
        (std-deviation 0.937458 "ns")
        (variance 0.878827 "ns²"))
      (sample-time
        (mean 37.474596 "μs")
        (std-deviation 21.326054 "μs")
        (variance 454.800575 "μs²"))
      (sample-iterations
        (mean 1149.754386)
        (std-deviation 656.408880)
        (variance 430872.617168))
      (custom-meters (total 0)))
    (execution (number 5)
      (name "empty")
      (dimensions (5))
      (total-time 0.326234 "s")
      (total-iterations 65536)
      (iterations 65536)
      (total-samples 58)
      (used-samples 58)
      (iteration-time
        (mean 21.576884 "ns")
        (std-deviation 2.229484 "ns")
        (variance 4.970598 "ns²"))
      (sample-time
        (mean 24.337345 "μs")
        (std-deviation 14.608577 "μs")
        (variance 213.410528 "μs²"))
      (sample-iterations
        (mean 1129.931034)
        (std-deviation 660.495265)
        (variance 436253.995160))
      (custom-meters (total 0)))
    (number-of-runs 6))
  (test (name "test")
    (execution (number 0)
      (name "test")
      (total-time 5.082051 "s")
      (total-iterations 3938)
      (iterations 3938)
      (total-samples 311)
      (used-samples 311)
      (iteration-time
        (mean 1.265120 "ms")
        (std-deviation 0.067238 "ms")
        (variance 0.004521 "ms²"))
      (sample-time
        (mean 15.978674 "ms")
        (std-deviation 8.834413 "ms")
        (variance 78.046855 "ms²"))
      (sample-iterations
        (mean 12.662379)
        (std-deviation 7.027033)
        (variance 49.379193))
      (custom-meters (total 0)))
    (number-of-runs 1))
  (number-of-tests 2))
@end verbatim
@end example
