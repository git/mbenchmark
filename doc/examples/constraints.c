/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.h>
#include <unistd.h>

static void
limit_iterations (micro_benchmark_test_case test)
{
  micro_benchmark_test_case_limit_iterations (test, 300, 400);
  micro_benchmark_test_case_limit_samples (test, 2, 5);
}

static void
test_1 (void *ptr)
{
  (void) ptr;
  /* This test code will run for 300 to 400 iterations, taking
     measurements each 2 to 5 iterations. */
  usleep (10000);
}

MICRO_BENCHMARK_REGISTER_SIMPLE_TEST (test_1);
MICRO_BENCHMARK_CONSTRAINT_TEST ("test_1", limit_iterations);

static void
limit_time (micro_benchmark_test_case test)
{
  micro_benchmark_clock_time max = { 1, 0 };
  micro_benchmark_test_case_set_max_time (test, max);
}

static void
test_2 (void *ptr)
{
  (void) ptr;
  /* This test code will run for 1 second. */
  usleep (10000);
}

MICRO_BENCHMARK_REGISTER_SIMPLE_TEST (test_2);
MICRO_BENCHMARK_CONSTRAINT_TEST ("test_2", limit_time);

MICRO_BENCHMARK_MAIN ();
