/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.hpp>
#include <chrono>
#include <iostream>
#include <thread>

/* TODO: Separate examples.  */
namespace micro_benchmark_test
{
  // Common base without state
  class functor_base
  {
  protected:
    using ms = std::chrono::milliseconds;

  public:
    static void
    constraints (micro_benchmark::test_case& test)
    { test.add_dimension ({100, 200, 300}); }

    ms
    set_up (micro_benchmark::state const& v) const
    { return ms{v.sizes ().front ()}; }

    void
    tear_down (micro_benchmark::state const&, ms const& s) const
    { std::cout << "Tear down test with " << s.count () << "ms\n"; }
  };

  // Common base with state
  class void_base
  {
  protected:
    using ms = std::chrono::milliseconds;
    ms value = ms{1000};

  public:
    static void
    constraints (micro_benchmark::test_case& test)
    { test.add_dimension ({100, 200, 300}); }

    void
    set_up (micro_benchmark::state const& v)
    { value = ms{v.sizes ().front ()}; }

    void
    tear_down (micro_benchmark::state const&)
    { std::cout << "Tear down test with " << value.count () << "ms\n"; }
  };

  // Automatic test, object without state.
  struct auto_functor : functor_base
  {
    void
    operator() (ms t) const
    { std::this_thread::sleep_for (t); }

  };

  // Automatic test, object with state.
  struct auto_void : void_base
  {
    void
    operator() () const
    { std::this_thread::sleep_for (value); }
  };


  // Directed test, object without state.
  struct test_functor : functor_base
  {
    void
    operator() (micro_benchmark::state& s, ms t) const
    { while (s.keep_running ()) std::this_thread::sleep_for (t); }
  };

  // Directed test, object with state.
  struct test_void : void_base
  {
    void
    operator() (micro_benchmark::state& s) const
    { while (s.keep_running ()) std::this_thread::sleep_for (value); }
  };

  // Declare test objects
  constexpr auto_functor auto_fun{};
  constexpr test_functor test_fun{};
  constexpr auto_void auto_void{};
  constexpr test_void test_void{};

  // Register tests.
  auto afunctor = micro_benchmark::register_class("auto-functor", auto_fun);
  auto dfunctor = micro_benchmark::register_class("directed-functor", test_fun);
  auto avoid = micro_benchmark::register_class("auto-object", auto_void);
  auto dvoid = micro_benchmark::register_class("directed-object", test_void);
}

MICRO_BENCHMARK_MAIN ();
