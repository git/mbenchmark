;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <https://www.gnu.org/licenses/>.
;;
;; Additionally, permission is granted to copy, distribute and/or
;; modify this document under the terms of the GNU Free Documentation
;; License, Version 1.3 or any later version published by the Free
;; Software Foundation; with no Invariant Sections, no Front-Cover
;; Texts, and no Back-Cover Texts. A copy of the license is included
;; in the section entitled "GNU Free Documentation License".
(use-modules (mbenchmark))

(define (make-random-selector n)
  (map (lambda _ (eqv? 0 (random 2)))
       (make-list n)))

(define (make-set-up name)
  (define (get-name a b it)
    (cond ((and (eqv? a 1000) (eqv? b 20000))
           (string-append name "/small/" (number->string it)))
          ((and (eqv? a 100000) (eqv? b 20000))
           (string-append name "/mid-l/" (number->string it)))
          ((and (eqv? a 1000) (eqv? b 20000000))
           (string-append name "/mid-h/" (number->string it)))
          (else
           (string-append name "/big/" (number->string it)))))
  (lambda (state)
    (let* ((sizes (state-sizes state))
           (sel (make-random-selector (caddr sizes))))
      (set-state-name! state (apply get-name sizes))
      (append sizes (list sel)))))

(let* ((dimensions '((1000 1000000000000000)
                     (2000 2000000000000000)
                     (16 64 128)))
       (max-iterations 100000)
       (add-test (lambda (name sname fun)
                   (register-test! name #:test fun
                                   #:set-up (make-set-up sname)
                                   #:dimensions dimensions
                                   #:max-iterations max-iterations))))
  (add-test "addition" "add"
            (lambda (a b it br)
              (let loop ((curr 0)
                         (i it))
                (if (> i 0)
                    (loop (+ a b curr) (- i 1))
                    curr))))

  (add-test "branched-addition" "if+add"
            (lambda (a b it br)
              (let loop ((i it) (branch br) (curr 0))
                (if (> i 0)
                    (loop (- i 1) (cdr branch)
                          (+ (if (car branch) a b) curr))
                    curr))))

  (add-test "multiplication" "mult"
            (lambda (a b it br)
              (let loop ((curr 0)
                         (i it))
                (if (> i 0)
                    (loop (* a b curr) (- i 1))
                    curr))))

  (add-test "branched-multiplication" "if+mult"
            (lambda (a b it br)
              (let loop ((i it) (branch br) (curr 0))
                (if (> i 0)
                    (loop (- i 1) (cdr branch)
                          (* (if (car branch) a b) curr))
                    curr)))))

(main (command-line))
