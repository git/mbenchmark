;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <https://www.gnu.org/licenses/>.
;;
;; Additionally, permission is granted to copy, distribute and/or
;; modify this document under the terms of the GNU Free Documentation
;; License, Version 1.3 or any later version published by the Free
;; Software Foundation; with no Invariant Sections, no Front-Cover
;; Texts, and no Back-Cover Texts. A copy of the license is included
;; in the section entitled "GNU Free Documentation License".
(use-modules (mbenchmark))

(define (test-1)
  (usleep 10000))

(register-test! "test-1" #:test test-1
                ;; This test will run for 300 to 400 iterations...
                #:min-iterations 300
                #:max-iterations 400
                ;; ... taking measurements each 2 to 5 iterations.
                #:min-sample-iterations 2
                #:max-sample-iterations 5)

(register-test! "test-2" #:test test-1
                ;; This test will run for 1 second.
                #:max-time 1)

(main (command-line))
