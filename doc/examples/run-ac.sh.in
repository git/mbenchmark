#!@SHELL@
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is part of MicroBenchmark.
#
# MicroBenchmark is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
#
# MicroBenchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MicroBenchmark.  If not, see
# <http://www.gnu.org/licenses/>.
set -e

# Installation variables.
prefix='@prefix@'
libdir='@libdir@'
cxxlibdir='@cxxlibdir@'
guilesrcdir='@guilesrcdir@'
guilegodir='@guilegodir@'
m4dir='@m4dir@'
SED='@SED@'
GUILE='@GUILE@'
: ${AUTORECONF:=autoreconf}
: ${MAKE:=make}
: ${MAKEOPTS:='-j'}

# Parse arguments
destdir="$1"
in_dir="$2"
in_file="$3"
instldlp="${destdir}${libdir}"
if [ -n "${cxxlibdir}" ]; then
    instldlp="${instldlp}:${destdir}${cxxlibdir}"
fi
config_flag=
case "${in_file}" in
    *.c)
        out_file=benchmark.c
        config_flag="--with-micro-benchmark=${destdir}${prefix}"
        ;;
    *.cxx)
        out_file=benchmark.cxx
        config_flag="--with-micro-benchmark=${destdir}${prefix}"
        ;;
    *.scm)
        out_file=benchmark.scm
        old="${GUILE_LOAD_PATH:+:}${GUILE_LOAD_PATH}"
        GUILE_LOAD_PATH="${destdir}${guilesrcdir}${old}"
        export GUILE_LOAD_PATH
        old="${GUILE_LOAD_COMPILED_PATH:+:}${GUILE_LOAD_COMPILED_PATH}"
        GUILE_LOAD_COMPILED_PATH="${destdir}${guilegodir}${old}"
        export GUILE_LOAD_COMPILED_PATH
        ;;
    *)
        echo "Unknown language for $in_file" >&2
        exit 1
        ;;
esac

# Prepare sources.
dir=`mktemp -d -p .`
#trap "rm -rf $dir" EXIT
cp "${in_file}" "${dir}/${out_file}"
cp "${in_dir}/Makefile.am" "${dir}"
# XXX: Use aclocal instead of local copy
mkdir "${dir}/m4"
cp -r "${destdir}${m4dir}/mbenchmark.m4" "${dir}/m4"
${SED} -e 's/[@]PACKAGE_VERSION@/@PACKAGE_VERSION@/' \
       -e 's/[@]PACKAGE_BUGREPORT@/@PACKAGE_BUGREPORT@/' \
       -e 's/[@]GUILE_VERSION@/@GUILE_EFFECTIVE_VERSION@/' \
       "${in_dir}/configure.ac" > "${dir}/configure.ac"

# Compile and run project.
(
    set -e
    cd "$dir"
    ${AUTORECONF} -fvi
    ./configure "${config_flag}"
    ${MAKE} ${MAKEOPTS}

    # Run executable
    case "${in_file}" in
        *.scm)
            GUILE_AUTO_COMPILE=0
            export GUILE_AUTO_COMPILE
            ${GUILE} benchmark.scm
            ;;
        *)
            LD_LIBRARY_PATH="${instldlp}${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"
            export LD_LIBRARY_PATH
            ./benchmark
            ;;
    esac
)

rm -rf "$dir"
trap "" EXIT
