/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.h>

#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

static void *
set_up (micro_benchmark_test_state state)
{
  size_t *s = (size_t *) malloc (sizeof (size_t) * 2);
  if (!s)
    abort ();
  s[0] = micro_benchmark_state_get_size (state, 0);
  s[1] = micro_benchmark_state_get_size (state, 1);
  return s;
}

static void
test (void *ptr)
{
  size_t *arr = ptr;
  usleep (arr[0] + arr[1]);
}

static void
tear_down (micro_benchmark_test_state state, void *ptr)
{
  (void) state;
  free (ptr);
}

static const struct micro_benchmark_test_definition test_def = {
  true, set_up, tear_down, test, NULL
};

int
main (int argc, char **argv)
{
  /*  Hopefully, silence warnings.  */
  (void) argc;
  (void) argv;

  micro_benchmark_suite suite = micro_benchmark_suite_create ("sample-lisp");

  micro_benchmark_test_case tcase =
    micro_benchmark_suite_register_test (suite, "test", &test_def);

  static const size_t sizes0[] = { 1, 5, 10 };
  static const size_t num_sizes0 = sizeof (sizes0) / sizeof (*sizes0);
  micro_benchmark_test_case_add_dimension (tcase, num_sizes0, sizes0);

  static const size_t sizes1[] = { 100, 500, 1000 };
  static const size_t num_sizes1 = sizeof (sizes1) / sizeof (*sizes1);
  micro_benchmark_test_case_add_dimension (tcase, num_sizes1, sizes1);

  micro_benchmark_suite_run (suite);
  micro_benchmark_report report = micro_benchmark_suite_get_report (suite);
  micro_benchmark_write_report (report, stdout, MICRO_BENCHMARK_LISP_OUTPUT);
  micro_benchmark_suite_release (suite);

  return EXIT_SUCCESS;
}
