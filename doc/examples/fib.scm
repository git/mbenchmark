;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <https://www.gnu.org/licenses/>.
;;
;; Additionally, permission is granted to copy, distribute and/or
;; modify this document under the terms of the GNU Free Documentation
;; License, Version 1.3 or any later version published by the Free
;; Software Foundation; with no Invariant Sections, no Front-Cover
;; Texts, and no Back-Cover Texts. A copy of the license is included
;; in the section entitled "GNU Free Documentation License".
(use-modules (mbenchmark))

(define (fibrec n)
  (cond ((> n 1) (+ (fibrec (- n 2)) (fibrec (- n 1))))
        ((< n 0) (- (fibrec (+ n 2)) (fibrec (+ n 1))))
        (else n)))

(define (fibit n)
  (define (up curr last p)
    (if (> p 0)
        (up (+ curr last) curr (- p 1))
        curr))
  (define (down curr last p)
    (if (< p 0)
        (down (- last curr) curr (+ p 1))
        curr))
  (cond ((> n 1) (up 1 0 (- n 1)))
        ((< n 0) (down 0 1 n))
        (else n)))

(define (set-up state)
  (define (doit size sign)
    (if (eqv? sign 0)
	(list size)
	(list (- size))))
  (apply doit (state-sizes state)))

(define (tear-down state n)
  (let ((base (if (equal? (state-name state) "fibit") "fibit/" "fibrec/")))
    (set-state-name! state (string-append base (number->string n)))))

(register-test! "fibit"
                #:test fibit
                #:set-up set-up
                #:tear-down tear-down
                #:dimensions '((10 50 100 500 1000 5000 10000 50000 100000)
                               (0 1))
                #:max-iterations 1000000)
(register-test! "fibrec"
                #:test fibrec
                #:set-up set-up
                #:tear-down tear-down
                #:dimensions '((10 12 14 16 18 20 22 24 26 28 30)
                               (0 1))
                #:max-iterations 1000000)

(main (command-line))
