/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.hpp>

#include <chrono>
#include <thread>

MICRO_BENCHMARK_MAIN ();

namespace
{
  using micro_benchmark::with_constraints;
  auto constraints =
    [v1 = 10, v2 = 15, it = 10000] (micro_benchmark::test_case& test)
    {
      test.add_dimension ({v1, v2});
      test.limit_iterations (it, it);
    };

  auto set_up =
    [pos = 0] (micro_benchmark::state const& state)
    {
      auto sizes = state.sizes ();
      return sizes.at (pos);
    };

  auto test_factorial =
    [init = 1] (size_t value)
    {
      size_t ret = init;
      micro_benchmark::do_not_optimize (ret);
      for (size_t i = value; i > 1; --i)
        ret *= i;
      micro_benchmark::do_not_optimize (ret);
    };

  auto rtest = micro_benchmark::register_test (with_constraints, "auto",
                                               constraints, test_factorial,
                                               set_up);

  auto test_factorial2 =
    [init = 1] (micro_benchmark::state& s, size_t value)
    {
      while (s.keep_running ())
        {
          size_t ret = init;
          micro_benchmark::do_not_optimize (ret);
          for (size_t i = value; i > 1; --i)
            ret *= i;
          micro_benchmark::do_not_optimize (ret);
        }
    };

  auto dtest = micro_benchmark::register_test (with_constraints, "directed",
                                               constraints, test_factorial2,
                                               set_up);
}
