/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.hpp>
#include <chrono>
#include <thread>

namespace
{
  using micro_benchmark::with_constraints;

  void
  test_1 (std::vector<std::size_t> const&)
  {
    std::this_thread::sleep_for (std::chrono::milliseconds(10));
  }

  void
  limit_iterations (micro_benchmark::test_case& test)
  {
    test.limit_iterations (300, 400);
    test.limit_samples (2, 5);
  }

  auto rt1 = micro_benchmark::register_test (with_constraints,
                                             "test_1", limit_iterations,
                                             test_1);
  void
  test_2 (std::vector<std::size_t> const&)
  {
    std::this_thread::sleep_for (std::chrono::milliseconds(10));
  }

  void
  limit_time (micro_benchmark::test_case& test)
  {
    test.max_time (std::chrono::seconds (1));
  }

  auto rt2 = micro_benchmark::register_test (with_constraints,
                                             "test_2", limit_time,
                                             test_2);
}

MICRO_BENCHMARK_MAIN ();

