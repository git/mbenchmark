/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.hpp>
#include <functional> /*  std::plus and std::less  */
#include <string>

/* Main can be placed anywhere.  */
MICRO_BENCHMARK_MAIN ();

namespace
{
  /* Recursive implementation.  */
  template <typename T, typename I = int>
  T
  fibrec (I x)
  {
    if (x > 1)
      return fibrec<T> (x - 1) + fibrec<T> (x - 2);
    if (x < 0)
      return fibrec<T> (x + 2) - fibrec<T> (x + 1);
    return x;
  }

  /* Iterative implementation.  */
  template <typename T, typename I = int>
  T
  fibit (I x)
  {
    auto doit = [x] (T curr, T last, I start, auto&& op)
    {
      for (I i = start; i < op (0, x); ++i)
        {
          T tmp = op (last, curr);
          last = curr;
          curr = tmp;
        }
      return curr;
    };
    if (x > 0)
      return doit (T{1}, T{0}, 1, std::plus<T>{});
    return doit (T{0}, T{1}, 0, std::minus<T>{});
  }

  /* Prepare the pointer and its value.  */
  int
  set_up (micro_benchmark::state const& s)
  {
    auto sizes = s.sizes ();
    int value = sizes.at (0);
    value *= sizes.at (1) ? 1 : -1;
    return value;
  }

  void
  tear_down (micro_benchmark::state& s, int v)
  {
    constexpr auto fibit_base = "fibit/";
    constexpr auto fibrec_base = "fibrec/";
    std::string name = s.get_name ();
    if (name == "test_fibit")
      s.set_name (fibit_base + std::to_string (v));
    else
      s.set_name (fibrec_base + std::to_string (v));
  }

  /* Constraints on the recursive test.  */
  void
  rec_constraints (micro_benchmark::test_case& test)
  {
    test.add_dimension ({ 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30 });
    test.add_dimension ({ 0, 1 });

    test.limit_iterations (0, 1000000);
  }

  /* Register the recursive test with its constraints.  */
  void
  rtestfun (int v)
  {
    auto r = fibrec<long> (v);
    micro_benchmark::do_not_optimize (r);
  }

  auto rtest =
    micro_benchmark::register_test (micro_benchmark::with_constraints,
                                    "test_fibrec", rec_constraints,
                                    rtestfun, set_up, tear_down);

  /* Constraints on the iterative test, this is enough.  */
  void
  it_constraints (micro_benchmark::test_case& test)
  {
    test.add_dimension ({ 10, 20, 30, 40 });
    test.add_dimension ({ 0, 1 });

    test.limit_iterations (0, 1000000);
  }

  /* Register the iterative test with its constraints.  */
  void
  itestfun (int v)
  {
    auto r = fibit<long> (v);
    micro_benchmark::do_not_optimize (r);
  }

  auto itest =
    micro_benchmark::register_test (micro_benchmark::with_constraints,
                                    "test_fibit", it_constraints,
                                    itestfun, set_up, tear_down);
}
