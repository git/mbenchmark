/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.h>
#include <unistd.h>

static void
one_dimension (micro_benchmark_test_case test)
{
  /* This test will have three dimensions, but four different values
     on them, so only one execution will be performed.  */
  const size_t sizes[] = { 1, 2, 3, 4 };
  const size_t ssizes = sizeof (sizes) / sizeof (*sizes);
  micro_benchmark_test_case_add_dimension (test, ssizes, sizes);
}

static size_t *
set_up_1d (micro_benchmark_test_state state)
{
  /* Prepare the data */
  static size_t s;
  s = micro_benchmark_state_get_size (state, 0) * 100000;
  return &s;
}

static void
test_1d (size_t *sz)
{
  /* test code */
  usleep (*sz);
}

MICRO_BENCHMARK_REGISTER_AUTO_TEST (set_up_1d, test_1d, 0);
MICRO_BENCHMARK_CONSTRAINT_TEST ("test_1d", one_dimension);

static void
three_dimensions (micro_benchmark_test_case test)
{
  /* This test will have three dimensions, but no variation on them,
     so only one execution will be performed.  */
  const size_t sizes[] = { 1, 2, 3 };
  micro_benchmark_test_case_add_dimension (test, 1, sizes + 0);
  micro_benchmark_test_case_add_dimension (test, 1, sizes + 1);
  micro_benchmark_test_case_add_dimension (test, 1, sizes + 2);
}

static size_t *
set_up_3d (micro_benchmark_test_state state)
{
  /* Prepare the data */
  static size_t s[3];
  s[0] = micro_benchmark_state_get_size (state, 0) * 10000;
  s[1] = micro_benchmark_state_get_size (state, 1) * 100000;
  s[2] = micro_benchmark_state_get_size (state, 2) * 1000000;
  return s;
}

static void
test_3d (size_t *sz)
{
  /* test code */
  usleep (sz[0] + sz[1] + sz[2]);
}

MICRO_BENCHMARK_REGISTER_AUTO_TEST (set_up_3d, test_3d, 0);
MICRO_BENCHMARK_CONSTRAINT_TEST ("test_3d", three_dimensions);

MICRO_BENCHMARK_MAIN ();
