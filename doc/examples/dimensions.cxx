/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.hpp>
#include <chrono>
#include <thread>

namespace
{
  using micro_benchmark::with_constraints;

  void
  one_dimension (micro_benchmark::test_case& test)
  {
    test.add_dimension ({ 1, 2, 3 });
  }

  void
  test_1d (std::vector<std::size_t> const& v)
  {
    std::this_thread::sleep_for (std::chrono::milliseconds (v[0] * 100));
  }

  auto t1d = micro_benchmark::register_test (with_constraints, "test1d",
                                             one_dimension, test_1d);

  void
  three_dimensions (micro_benchmark::test_case& test)
  {
    test.add_dimension ({ 1 });
    test.add_dimension ({ 10 });
    test.add_dimension ({ 100 });
  }

  void
  test_3d (std::vector<std::size_t> const& v)
  {
    std::this_thread::sleep_for (std::chrono::milliseconds (v[0] + v[1] + v[2]));
  }

  auto t3d = micro_benchmark::register_test (with_constraints, "test3d",
                                             three_dimensions, test_3d);

}

MICRO_BENCHMARK_MAIN ();

