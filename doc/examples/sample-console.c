/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * Additionally, permission is granted to copy, distribute and/or
 * modify this document under the terms of the GNU Free Documentation
 * License, Version 1.3 or any later version published by the Free
 * Software Foundation; with no Invariant Sections, no Front-Cover
 * Texts, and no Back-Cover Texts. A copy of the license is included
 * in the section entitled "GNU Free Documentation License".
 */
#include <mbenchmark/all.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

static void *
set_up (micro_benchmark_test_state state)
{
  unsigned *s = (unsigned *) malloc (sizeof (unsigned));
  if (!s)
    abort ();
  *s = micro_benchmark_state_get_size (state, 0);
  return s;
}

static void
test_dynamic (unsigned *t)
{
  usleep (*t);
}

static void
tear_down (micro_benchmark_test_state state, void *ptr)
{
  (void) state;
  free (ptr);
}

static void
test_static (void *ptr)
{
  (void) ptr;
  usleep (1000);
}

static const struct micro_benchmark_test_definition test_dynamic_def = {
  true, set_up, tear_down, (micro_benchmark_auto_test_fun) test_dynamic, NULL
};

static const struct micro_benchmark_test_definition test_static_def = {
  true, NULL, NULL, (micro_benchmark_auto_test_fun) test_static, NULL
};

int
main (int argc, char **argv)
{
  /*  Hopefully, silence warnings.  */
  (void) argc;
  (void) argv;

  micro_benchmark_suite suite = micro_benchmark_suite_create (argv[0]);
  micro_benchmark_suite_register_test (suite, "static", &test_static_def);

  micro_benchmark_test_case dynamic_case =
    micro_benchmark_suite_register_test (suite, "dynamic", &test_dynamic_def);
  static const size_t sizes[] = { 10, 100, 1000, 10000, 100000, 1000000 };
  static const size_t n_sizes = sizeof (sizes) / sizeof (*sizes);
  micro_benchmark_test_case_add_dimension (dynamic_case, n_sizes, sizes);

  micro_benchmark_suite_run (suite);
  micro_benchmark_report res = micro_benchmark_suite_get_report (suite);

  micro_benchmark_print_report (res);
  micro_benchmark_suite_release (suite);

  return EXIT_SUCCESS;
}
