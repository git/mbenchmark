;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; Copying and distribution of this file, with or without
;; modification, are permitted in any medium without royalty.  This
;; file is offered as-is, without any warranty.
;;
;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((compile-command . "build-aux/build.sh")
	 (indent-tabs-mode . nil)))
 (makefile-mode . ((indent-tabs-mode . t))))
