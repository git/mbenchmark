/* suite.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/suite.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

static int auto_stage = 0;

static const char *auto_test_name = "auto-test";
static const char *auto_exec_name = "auto-test-exec";
static void *
auto_set_up (micro_benchmark_test_state state)
{
  assert (state);
  assert (auto_stage == 0);
  const char *name = micro_benchmark_state_get_name (state);
  assert (strcmp (name, auto_test_name) == 0);
  micro_benchmark_state_set_name (state, auto_exec_name);
  auto_stage = 1;
  return 0;
}

static void
auto_test (void *ptr)
{
  assert (!ptr);
  assert (auto_stage == 1);
  auto_stage = 2;
}

static void
auto_tear_down (micro_benchmark_test_state state, void *ptr)
{
  assert (state);
  assert (!ptr);
  assert (auto_stage == 2);
  const char *name = micro_benchmark_state_get_name (state);
  assert (strcmp (name, auto_exec_name) == 0);
  auto_stage = 3;
}

static int test_stage = 0;

static const char *test_name = "test";
static const char *exec_name = "test-exec";

static void *
set_up (micro_benchmark_test_state state)
{
  assert (state);
  assert (test_stage == 0);
  const char *name = micro_benchmark_state_get_name (state);
  assert (strcmp (name, test_name) == 0);
  micro_benchmark_state_set_name (state, exec_name);
  test_stage = 1;
  return 0;
}

static void
test (micro_benchmark_test_state state)
{
  assert (state);
  assert (test_stage == 1);
  test_stage = 2;
}

static void
tear_down (micro_benchmark_test_state state, void *ptr)
{
  assert (state);
  assert (!ptr);
  assert (test_stage == 2);
  const char *name = micro_benchmark_state_get_name (state);
  assert (strcmp (name, exec_name) == 0);
  test_stage = 3;
}

static int rogue_stage = 0;
static const char *rogue_name = "rogue";

static void
rogue_test (micro_benchmark_test_state state)
{
  /* This test should not crash, but no data is collected. */
  (void) state;
  rogue_stage++;
}

static void
test_rogue_report (micro_benchmark_report report)
{
  micro_benchmark_test_report test_rep =
    micro_benchmark_report_get_test_report (report, 3);
  assert (test_rep);

  const char *test_rep_name = micro_benchmark_test_report_get_name (test_rep);
  assert (strcmp (test_rep_name, rogue_name) == 0);

  size_t n_exec = micro_benchmark_test_report_get_num_executions (test_rep);
  assert (n_exec == 1);

  micro_benchmark_exec_report exec_rep =
    micro_benchmark_test_report_get_exec_report (test_rep, 0);
  assert (test_rep);

  const char *exec_rep_name = micro_benchmark_exec_report_get_name (exec_rep);
  assert (strcmp (exec_rep_name, rogue_name) == 0);

  size_t time_samples =
    micro_benchmark_exec_report_num_time_samples (exec_rep);
  assert (time_samples == 0);

  size_t iterations = micro_benchmark_exec_report_get_iterations (exec_rep);
  assert (iterations == 0);

  size_t total_iterations =
    micro_benchmark_exec_report_total_iterations (exec_rep);
  assert (total_iterations == 0);

  size_t n_samples = micro_benchmark_exec_report_total_samples (exec_rep);
  assert (n_samples == 0);

  size_t used_samples = micro_benchmark_exec_report_used_samples (exec_rep);
  assert (used_samples == 0);

  micro_benchmark_stats_value it =
    micro_benchmark_exec_report_iteration_time (exec_rep);
  assert (it.unit == MICRO_BENCHMARK_STATS_UNIT_NONE);

  micro_benchmark_stats_value st =
    micro_benchmark_exec_report_sample_time (exec_rep);
  assert (st.unit == MICRO_BENCHMARK_STATS_UNIT_NONE);

  micro_benchmark_stats_value si =
    micro_benchmark_exec_report_sample_iterations (exec_rep);
  assert (si.unit == MICRO_BENCHMARK_STATS_UNIT_NONE);
}

int
main (int argc, char **argv)
{
  (void) argv;
  (void) argc;
  static const char *suite_name = "test-suite";
  micro_benchmark_suite suite = micro_benchmark_suite_create (suite_name);
  assert (suite);

  const char *name = micro_benchmark_suite_get_name (suite);
  assert (strcmp (name, suite_name) == 0);

  assert (micro_benchmark_suite_get_number_of_tests (suite) == 0);

  struct micro_benchmark_test_definition auto_test_def = {
    true, auto_set_up, auto_tear_down, auto_test, NULL
  };
  micro_benchmark_test_case auto_case =
    micro_benchmark_suite_register_test (suite, auto_test_name,
                                         &auto_test_def);

  assert (auto_case);
  micro_benchmark_test_case_skip_iterations (auto_case, 0);
  micro_benchmark_test_case_limit_iterations (auto_case, 1, 1);

  // Added self-test automagically.
  assert (micro_benchmark_suite_get_number_of_tests (suite) == 2);
  assert (!auto_stage);
  assert (!test_stage);
  assert (!rogue_stage);

  struct micro_benchmark_test_definition test_def = {
    false, set_up, tear_down, NULL, test
  };
  micro_benchmark_test_case test_case =
    micro_benchmark_suite_register_test (suite, test_name, &test_def);

  assert (test_case);
  micro_benchmark_test_case_skip_iterations (test_case, 0);
  micro_benchmark_test_case_limit_iterations (test_case, 1, 1);

  assert (micro_benchmark_suite_get_number_of_tests (suite) == 3);
  assert (!auto_stage);
  assert (!test_stage);
  assert (!rogue_stage);

  struct micro_benchmark_test_definition rogue_def = {
    false, NULL, NULL, NULL, rogue_test
  };
  micro_benchmark_test_case rogue_case =
    micro_benchmark_suite_register_test (suite, rogue_name, &rogue_def);

  assert (rogue_case);
  micro_benchmark_test_case_skip_iterations (rogue_case, 100);
  micro_benchmark_test_case_limit_iterations (rogue_case, 200, 300);

  assert (micro_benchmark_suite_get_number_of_tests (suite) == 4);
  assert (!auto_stage);
  assert (!test_stage);
  assert (!rogue_stage);

  micro_benchmark_suite_run (suite);
  assert (auto_stage == 3);
  assert (test_stage == 3);
  assert (rogue_stage == 1);
  auto_stage = 0;
  test_stage = 0;
  rogue_stage = 0;

  micro_benchmark_report report = micro_benchmark_suite_get_report (suite);
  assert (report);
  const char *rname = micro_benchmark_report_get_name (report);
  assert (strcmp (rname, suite_name) == 0);

  size_t n_tests = micro_benchmark_report_get_number_of_tests (report);
  assert (n_tests == 4);

  micro_benchmark_test_report auto_test_rep =
    micro_benchmark_report_get_test_report (report, 1);
  assert (auto_test_rep);

  const char *auto_test_rep_name =
    micro_benchmark_test_report_get_name (auto_test_rep);
  assert (strcmp (auto_test_rep_name, auto_test_name) == 0);

  size_t n_exec_auto =
    micro_benchmark_test_report_get_num_executions (auto_test_rep);
  assert (n_exec_auto == 1);

  micro_benchmark_exec_report auto_exec_rep =
    micro_benchmark_test_report_get_exec_report (auto_test_rep, 0);
  assert (auto_test_rep);

  const char *auto_exec_rep_name =
    micro_benchmark_exec_report_get_name (auto_exec_rep);
  assert (strcmp (auto_exec_rep_name, auto_exec_name) == 0);

  micro_benchmark_test_report test_rep =
    micro_benchmark_report_get_test_report (report, 2);
  assert (test_rep);

  const char *test_rep_name = micro_benchmark_test_report_get_name (test_rep);
  assert (strcmp (test_rep_name, test_name) == 0);

  size_t n_exec_test =
    micro_benchmark_test_report_get_num_executions (test_rep);
  assert (n_exec_test == 1);

  micro_benchmark_exec_report exec_rep =
    micro_benchmark_test_report_get_exec_report (test_rep, 0);
  assert (test_rep);

  const char *exec_rep_name = micro_benchmark_exec_report_get_name (exec_rep);
  assert (strcmp (exec_rep_name, exec_name) == 0);

  test_rogue_report (report);

  assert (!auto_stage);
  assert (!test_stage);
  assert (!rogue_stage);
  micro_benchmark_suite_release (suite);
  assert (!auto_stage);
  assert (!test_stage);
  assert (!rogue_stage);

  return EXIT_SUCCESS;
}
