/* clock-gettime.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/clock/chrono-provider.h"

#include <assert.h>
#include <string.h>
#include <time.h>

static bool clock_getres_called = false;
static struct timespec clock_getres_value;
static int clock_getres_return = 0;

int
clock_getres (clockid_t c, struct timespec *t)
{
  (void) c;
  assert (!clock_getres_called);
  clock_getres_called = true;
  assert (t);
  *t = clock_getres_value;
  return clock_getres_return;
}

static bool clock_gettime_called = false;
static struct timespec clock_gettime_value;
static int clock_gettime_return = 0;

int
clock_gettime (clockid_t c, struct timespec *t)
{
  (void) c;
  assert (!clock_gettime_called);
  clock_gettime_called = true;
  assert (t);
  *t = clock_gettime_value;
  return clock_gettime_return;
}

/*
  Tests.
*/
void
test_clock (micro_benchmark_clock_type type)
{
  clock_gettime_called = false;
  clock_getres_called = false;
  struct timespec resol = { 1, 1 };
  clock_getres_value = resol;
  micro_benchmark_chronometer_provider prov =
    MICRO_BENCHMARK_CHRONO_PROVIDER_CLOCK_GETTIME;
  micro_benchmark_meter t = micro_benchmark_chronometer_create (type, prov);
  assert (t);
  assert (!clock_gettime_called);
  assert (clock_getres_called);
  clock_getres_called = false;

  const char *name = micro_benchmark_stats_meter_get_name (t);
  assert (strcmp (name, "clock_gettime") == 0);
  micro_benchmark_stats_sample_type stype =
    micro_benchmark_stats_meter_get_sample_type (t);
  assert (stype == MICRO_BENCHMARK_SAMPLE_TIME);
  assert (!clock_gettime_called);
  assert (!clock_getres_called);

  micro_benchmark_clock_time res =
    micro_benchmark_stats_meter_get_min_resolution (t).time;
  assert (!clock_gettime_called);
  assert (!clock_getres_called);
  assert (res.seconds == 1);
  assert (res.nanoseconds == 1);

  res = micro_benchmark_stats_meter_get_max_resolution (t).time;
  assert (!clock_gettime_called);
  assert (!clock_getres_called);
  assert (res.seconds > 1);
  assert (res.nanoseconds == 0);

  struct timespec t1 = { 0, 999999999 };
  clock_gettime_value = t1;
  micro_benchmark_stats_meter_start (t);
  assert (clock_gettime_called);
  clock_gettime_called = false;
  struct timespec t2 = { 2, 0 };
  clock_gettime_value = t2;
  micro_benchmark_stats_meter_stop (t);
  assert (clock_gettime_called);
  clock_gettime_called = false;
  struct timespec t3 = { 20, 1 };
  clock_gettime_value = t3;
  micro_benchmark_stats_meter_restart (t);
  assert (clock_gettime_called);
  clock_gettime_called = false;
  struct timespec t4 = { 21, 0 };
  clock_gettime_value = t4;
  micro_benchmark_stats_meter_stop (t);
  assert (clock_gettime_called);
  clock_gettime_called = false;
  micro_benchmark_clock_time v =
    micro_benchmark_stats_meter_get_sample (t).time;
  assert (!clock_gettime_called);
  assert (!clock_getres_called);
  assert (v.seconds == 2);
  assert (v.nanoseconds == 0);

  micro_benchmark_chronometer_release (t);
  assert (!clock_gettime_called);
  assert (!clock_getres_called);
}

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;
  test_clock (MICRO_BENCHMARK_CLOCK_REALTIME);
  test_clock (MICRO_BENCHMARK_CLOCK_MONOTONIC);
  test_clock (MICRO_BENCHMARK_CLOCK_PROCESS);
  test_clock (MICRO_BENCHMARK_CLOCK_THREAD);
  return 0;
}
