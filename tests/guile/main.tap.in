#!@BASH@
# @configure_input@
# main.tap.in --- MicroBenchmark Guile test suite.
#
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is part of MicroBenchmark.
#
# MicroBenchmark is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Benchmark is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MicroBenchmark.  If not, see
# <http://www.gnu.org/licenses/>.

set -e
. "${abs_top_srcdir}/tests/tap.sh"

print_version

trap 'echo "Bail out! ${counter} An unexpected error ocurred"' EXIT

do_exit ()
{
    trap "" EXIT
    exit $1
}

mod='(use-modules (mbenchmark))'
rt='(register-test! "test" #:test (lambda _ (usleep 100000))'
rt="${rt} #:max-iterations 10)"
cl='(main (command-line))'

test_direct_empty_args ()
{
    e="${mod} ${rt} (main '(\"test\"))"
    rm -rf .tgde.*
    dir=`mktemp -d -p . .tgde.XXXXX`
    if ${GUILE} -c "$e" > "${dir}/out" 2>&1; then
        if test -z "$(cat ${dir}/out)"; then
            print_error "Direct main call did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out"; then
            print_error "Direct main call failed"
            print_diagnostic_file "${dir}/out"

        elif $FGREP -q -e 'Suite: test' "${dir}/out"; then
            print_ok "Direct main call"

        else
            print_error "Direct main call didn't use provided name"
            print_diagnostic_file "${dir}/out"
        fi
    else
        ret=$?
        print_error "Direct main call failed with ${ret}"
        if test -e "${dir}/out"; then
            print_diagnostic_file "${dir}/out"
        fi
    fi

    rm -rf "${dir}"
}
add_test test_direct_empty_args

test_empty_args ()
{
    e="${mod} ${rt} ${cl}"
    rm -rf .tge.*
    dir=`mktemp -d -p . .tge.XXXXX`
    if ${GUILE} -c "$e" > "${dir}/out" 2>&1; then
        if test -z "$(cat ${dir}/out)"; then
            print_error "Empty args did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out"; then
            print_error "Empty args failed"
            print_diagnostic_file "${dir}/out"

        elif $FGREP -q -e 'Suite:' "${dir}/out"; then
            print_ok "Empty args"

        else
            print_error "Empty args did not produce valid output"
            print_diagnostic_file "${dir}/out"
        fi
    else
        ret=$?
        print_error "Empty args failed with ${ret}"
        if test -e "${dir}/out"; then
            print_diagnostic_file "${dir}/out"
        fi
    fi

    rm -rf "${dir}"
}
add_test test_empty_args

test_only_double_dash ()
{
    e="${mod} ${rt} ${cl}"
    rm -rf .tgdd.*
    dir=`mktemp -d -p . .tgdd.XXXXX`
    if ${GUILE} -c "$e" -- > "${dir}/out" 2>&1; then
        if test -z "$(cat ${dir}/out)"; then
            print_error "Only double dash did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out"; then
            print_error "Only double dash failed"
            print_diagnostic_file "${dir}/out"

        elif $FGREP -q -e 'Suite:' "${dir}/out"; then
            print_ok "Only double dash"

        else
            print_error "Only double dash did not produce valid output"
            print_diagnostic_file "${dir}/out"
        fi
    else
        ret=$?
        print_error "Only double dash failed with ${ret}"
        if test -e "${dir}/out"; then
            print_diagnostic_file "${dir}/out"
        fi
    fi

    rm -rf "${dir}"
}
add_test test_only_double_dash


test_direct_suite_name ()
{
    e="${mod} ${rt} (main '(\"test\" \"--suite-name\" \"foo\"))"
    rm -rf .tgds.*
    dir=`mktemp -d -p . .tgds.XXXXX`
    if ${GUILE} -c "$e" > "${dir}/out" 2>&1; then
        if test -z "$(cat ${dir}/out)"; then
            print_error "Direct suite name did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out"; then
            print_error "Direct suite name failed"
            print_diagnostic_file "${dir}/out"

        elif $FGREP -q -e 'Suite: foo' "${dir}/out"; then
            print_ok "Direct suite name"

        else
            print_error "Direct suite name didn't use provided name"
            print_diagnostic_file "${dir}/out"
        fi
    else
        ret=$?
        print_error "Direct suite name failed with ${ret}"
        if test -e "${dir}/out"; then
            print_diagnostic_file "${dir}/out"
        fi
    fi

    rm -rf "${dir}"
}
add_test test_direct_suite_name


test_suite_name ()
{
    e="${mod} ${rt} ${cl}"
    rm -rf .tgsn.*
    dir=`mktemp -d -p . .tgsn.XXXXX`
    if ${GUILE} -c "$e" --suite-name=test > "${dir}/out1" 2>&1; then
        if test -z "$(cat ${dir}/out1)"; then
            print_error "Suite name wo/dash did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out1"; then
            print_error "Suite name wo/dash failed"
            print_diagnostic_file "${dir}/out1"

        elif $FGREP -q -e 'Suite: test' "${dir}/out1"; then
            print_ok "Suite name wo/dash"

        else
            print_error "Suite name wo/dash did not produce valid output"
            print_diagnostic_file "${dir}/out1"
        fi
    else
        ret=$?
        print_error "Suite name wo/dash failed with ${ret}"
        if test -e "${dir}/out1"; then
            print_diagnostic_file "${dir}/out1"
        fi
    fi

    if ${GUILE} -c "$e" -- --suite-name=test > "${dir}/out2" 2>&1; then
        if test -z "$(cat ${dir}/out2)"; then
            print_error "Suite name w/dash did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out2"; then
            print_error "Suite name w/dash failed"
            print_diagnostic_file "${dir}/out2"

        elif $FGREP -q -e 'Suite: test' "${dir}/out2"; then
            print_ok "Suite name w/dash"

        else
            print_error "Suite name w/dash did not produce valid output"
            print_diagnostic_file "${dir}/out2"
        fi
    else
        ret=$?
        print_error "Suite name w/dash with ${ret}"
        if test -e "${dir}/out2"; then
            print_diagnostic_file "${dir}/out2"
        fi
    fi

    rm -rf "${dir}"
}
add_test test_suite_name 2

test_direct_main ()
{
    rm -rf .tgdm.*
    dir=`mktemp -d -p . .tgpm.XXXXX`
    echo "${mod}" > "${dir}/script.scm"
    echo "${rt}" >> "${dir}/script.scm"
    if ${GUILE} -e main "${dir}/script.scm" > "${dir}/out1" 2>&1; then
        if test -z "$(cat ${dir}/out1)"; then
            print_error "Direct main did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out1"; then
            print_error "Direct main failed"
            print_diagnostic_file "${dir}/out1"

        elif $FGREP -q -e 'Suite:' "${dir}/out1"; then
            print_ok "Direct main"

        else
            print_error "Direct main did not produce valid output"
            print_diagnostic_file "${dir}/out1"
        fi
    else
        ret=$?
        print_error "Direct main failed with ${ret}"
        if test -e "${dir}/out1"; then
            print_diagnostic_file "${dir}/out1"
        fi
    fi

    if ${GUILE} -e main "${dir}/script.scm" --suite-name="test" \
                > "${dir}/out2" 2>&1; then
        if test -z "$(cat ${dir}/out2)"; then
            print_error "Direct main w/name did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out2"; then
            print_error "Direct main w/name failed"
            print_diagnostic_file "${dir}/out2"

        elif $FGREP -q -e 'Suite:' "${dir}/out2"; then
            print_ok "Direct main w/name"

        else
            print_error "Direct main w/name did not produce valid output"
            print_diagnostic_file "${dir}/out2"
        fi
    else
        ret=$?
        print_error "Direct main w/name failed with ${ret}"
        if test -e "${dir}/out2"; then
            print_diagnostic_file "${dir}/out2"
        fi
    fi

    if ${GUILE} -e main "${dir}/script.scm" --help \
                > "${dir}/out3" 2>&1; then
        if test -z "$(cat ${dir}/out3)"; then
            print_error "Direct main w/help did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out3"; then
            print_error "Direct main w/help failed"
            print_diagnostic_file "${dir}/out3"

        elif $FGREP -q -e 'MicroBenchmark' "${dir}/out3"; then
            print_ok "Direct main w/help"

        else
            print_error "Direct main w/help did not produce valid output"
            print_diagnostic_file "${dir}/out3"
        fi
    else
        ret=$?
        print_error "Direct main w/help failed with ${ret}"
        if test -e "${dir}/out3"; then
            print_diagnostic_file "${dir}/out3"
        fi
    fi

    if ${GUILE} -e main "${dir}/script.scm" --version \
                > "${dir}/out4" 2>&1; then
        if test -z "$(cat ${dir}/out4)"; then
            print_error "Direct main w/version did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out4"; then
            print_error "Direct main w/version failed"
            print_diagnostic_file "${dir}/out4"

        elif $FGREP -q -e 'MicroBenchmark' "${dir}/out4"; then
            print_ok "Direct main w/version version"

        else
            print_error "Direct main w/version did not produce valid output"
            print_diagnostic_file "${dir}/out4"
        fi
    else
        ret=$?
        print_error "Direct main w/version failed with ${ret}"
        if test -e "${dir}/out4"; then
            print_diagnostic_file "${dir}/out4"
        fi
    fi

    rm -rf "${dir}"
}
add_test test_direct_main 4

test_interactive_main ()
{
    rm -rf .tgdm.*
    dir=`mktemp -d -p . .tgdm.XXXXX`
    echo "${mod}" > "${dir}/script.scm"
    echo "${rt}" >> "${dir}/script.scm"
    if echo '(exit (main (command-line)))' \
            | ${GUILE} -l "${dir}/script.scm" > "${dir}/out1" 2>&1; then
        if test -z "$(cat ${dir}/out1)"; then
            print_error "Interactive main did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out1"; then
            print_error "Interactive main failed"
            print_diagnostic_file "${dir}/out1"

        elif $FGREP -q -e 'Suite:' "${dir}/out1"; then
            print_ok "Interactive main"

        else
            print_error "Interactive main did not produce valid output"
            print_diagnostic_file "${dir}/out1"
        fi
    else
        ret=$?
        print_error "Interactive main failed with ${ret}"
        if test -e "${dir}/out1"; then
            print_diagnostic_file "${dir}/out1"
        fi
    fi

    if echo '(exit (main (command-line)))' \
            | ${GUILE} -l "${dir}/script.scm" -- \
                       > "${dir}/out2" 2>&1; then
        if test -z "$(cat ${dir}/out2)"; then
            print_error "Interactive main empty did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out2"; then
            print_error "Interactive main empty failed"
            print_diagnostic_file "${dir}/out2"

        elif $FGREP -q -e 'Suite:' "${dir}/out2"; then
            print_ok "Interactive main empty"

        else
            print_error "Interactive main empty did not produce valid output"
            print_diagnostic_file "${dir}/out2"
        fi
    else
        ret=$?
        print_error "Interactive main empty failed with ${ret}"
        if test -e "${dir}/out2"; then
            print_diagnostic_file "${dir}/out2"
        fi
    fi

    if echo '(exit (main (command-line)))' \
            | ${GUILE} -l "${dir}/script.scm" -- \
                       --suite-name="test" > "${dir}/out3" 2>&1; then
        if test -z "$(cat ${dir}/out3)"; then
            print_error "Interactive main w/name did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out3"; then
            print_error "Interactive main w/name failed"
            print_diagnostic_file "${dir}/out3"

        elif $FGREP -q -e 'Suite:' "${dir}/out3"; then
            print_ok "Interactive main w/name"

        else
            print_error "Interactive main w/name did not produce valid output"
            print_diagnostic_file "${dir}/out3"
        fi
    else
        ret=$?
        print_error "Interactive main w/name failed with ${ret}"
        if test -e "${dir}/out3"; then
            print_diagnostic_file "${dir}/out3"
        fi
    fi

    if echo '(exit (main (command-line)))' \
            | ${GUILE} -l "${dir}/script.scm" -- --help \
                       > "${dir}/out4" 2>&1; then
        if test -z "$(cat ${dir}/out4)"; then
            print_error "Interactive main w/help did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out4"; then
            print_error "Interactive main w/help failed"
            print_diagnostic_file "${dir}/out4"

        elif $FGREP -q -e 'MicroBenchmark' "${dir}/out4"; then
            print_ok "Interactive main w/help"

        else
            print_error "Interactive main w/help did not produce valid output"
            print_diagnostic_file "${dir}/out4"
        fi
    else
        ret=$?
        print_error "Interactive main w/help failed with ${ret}"
        if test -e "${dir}/out4"; then
            print_diagnostic_file "${dir}/out4"
        fi
    fi

    if echo '(exit (main (command-line)))' \
            | ${GUILE} -l "${dir}/script.scm" -- --version \
                       > "${dir}/out5" 2>&1; then
        if test -z "$(cat ${dir}/out5)"; then
            print_error "Interactive main w/version did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out5"; then
            print_error "Interactive main w/version failed"
            print_diagnostic_file "${dir}/out5"

        elif $FGREP -q -e 'MicroBenchmark' "${dir}/out5"; then
            print_ok "Interactive main w/version version"

        else
            print_error "Interactive main w/version did not produce valid output"
            print_diagnostic_file "${dir}/out5"
        fi
    else
        ret=$?
        print_error "Interactive main w/version failed with ${ret}"
        if test -e "${dir}/out5"; then
            print_diagnostic_file "${dir}/out5"
        fi
    fi

    rm -rf "${dir}"
}
add_test test_interactive_main 5

test_script ()
{
    rm -rf .tgs.*
    dir=`mktemp -d -p . .tgs.XXXXX`
    echo "${mod}" > "${dir}/script.scm"
    echo "${rt}" >> "${dir}/script.scm"
    echo "${cl}" >> "${dir}/script.scm"
    if ${GUILE} -s "${dir}/script.scm" > "${dir}/out1" 2>&1; then
        if test -z "$(cat ${dir}/out1)"; then
            print_error "Script did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out1"; then
            print_error "Script failed"
            print_diagnostic_file "${dir}/out1"

        elif $FGREP -q -e 'Suite:' "${dir}/out1"; then
            print_ok "Script"

        else
            print_error "Script did not produce valid output"
            print_diagnostic_file "${dir}/out1"
        fi
    else
        ret=$?
        print_error "Script failed with ${ret}"
        if test -e "${dir}/out1"; then
            print_diagnostic_file "${dir}/out1"
        fi
    fi

    if ${GUILE} -s "${dir}/script.scm" -- > "${dir}/out2" 2>&1; then
        if test -z "$(cat ${dir}/out2)"; then
            print_error "Script w/dash did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out2"; then
            print_error "Script w/dash failed"
            print_diagnostic_file "${dir}/out2"

        elif $FGREP -q -e 'Suite:' "${dir}/out2"; then
            print_ok "Script w/dash"

        else
            print_error "Script w/dash did not produce valid output"
            print_diagnostic_file "${dir}/out2"
        fi
    else
        ret=$?
        print_error "Script w/dash failed with ${ret}"
        if test -e "${dir}/out2"; then
            print_diagnostic_file "${dir}/out2"
        fi
    fi

    if ${GUILE} -s "${dir}/script.scm" --suite-name="test" > "${dir}/out3" 2>&1; then
        if test -z "$(cat ${dir}/out3)"; then
            print_error "Script w/name did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out3"; then
            print_error "Script w/name failed"
            print_diagnostic_file "${dir}/out3"

        elif $FGREP -q -e 'Suite:' "${dir}/out3"; then
            print_ok "Script w/name"

        else
            print_error "Script w/name did not produce valid output"
            print_diagnostic_file "${dir}/out3"
        fi
    else
        ret=$?
        print_error "Script w/name failed with ${ret}"
        if test -e "${dir}/out3"; then
            print_diagnostic_file "${dir}/out3"
        fi
    fi

    if ${GUILE} -s "${dir}/script.scm" -- --suite-name="test" > "${dir}/out4" 2>&1; then
        if test -z "$(cat ${dir}/out4)"; then
            print_error "Script w/dash+name did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out4"; then
            print_error "Script w/dash+name failed"
            print_diagnostic_file "${dir}/out4"

        elif $FGREP -q -e 'Suite:' "${dir}/out4"; then
            print_ok "Script w/dash+name"

        else
            print_error "Script w/dash+name did not produce valid output"
            print_diagnostic_file "${dir}/out4"
        fi
    else
        ret=$?
        print_error "Script w/dash+name failed with ${ret}"
        if test -e "${dir}/out4"; then
            print_diagnostic_file "${dir}/out4"
        fi
    fi

    if ${GUILE} -s "${dir}/script.scm" --help > "${dir}/out5" 2>&1; then
        if test -z "$(cat ${dir}/out5)"; then
            print_error "Script help did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out5"; then
            print_error "Script help failed"
            print_diagnostic_file "${dir}/out5"

        elif $FGREP -q -e 'MicroBenchmark' "${dir}/out5"; then
            print_ok "Script help"

        else
            print_error "Script help did not produce valid output"
            print_diagnostic_file "${dir}/out5"
        fi
    else
        ret=$?
        print_error "Script help failed with ${ret}"
        if test -e "${dir}/out5"; then
            print_diagnostic_file "${dir}/out5"
        fi
    fi

    if ${GUILE} -s "${dir}/script.scm" --version > "${dir}/out6" 2>&1; then
        if test -z "$(cat ${dir}/out6)"; then
            print_error "Script version did not emit any output"

        elif $FGREP -q -e ice-9 -e boot "${dir}/out6"; then
            print_error "Script version failed"
            print_diagnostic_file "${dir}/out6"

        elif $FGREP -q -e 'MicroBenchmark' "${dir}/out6"; then
            print_ok "Script version"

        else
            print_error "Script version did not produce valid output"
            print_diagnostic_file "${dir}/out6"
        fi
    else
        ret=$?
        print_error "Script version failed with ${ret}"
        if test -e "${dir}/out6"; then
            print_diagnostic_file "${dir}/out6"
        fi
    fi

    rm -rf "${dir}"
}
add_test test_script 6


run_tests
do_exit 0

# Local Variables:
# mode: sh
# End:
