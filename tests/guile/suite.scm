;;; suite.scm --- MicroBenchmark Guile library test suite.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Suite tests.
;;
;;; Code:
(use-modules (mbenchmark)
             (srfi srfi-64))

(test-begin "suite")

(let ((suite (make-suite "t1")))
  (test-assert "not-false" suite)
  (test-equal "provided-name" "t1" (suite-name suite))
  (test-eqv "no-tests" 0 (suite-number-of-tests suite)))

(let ((suite (make-suite "t2")) (has-run? #f))
  (add-test! suite "test" #:test (lambda _ (set! has-run? #t)))
  (test-eqv "test+self" 2 (suite-number-of-tests suite))
  (test-assert "not-run" (not has-run?)))

(let ((suite (make-suite "t3")) (has-run? #f))
  (add-test! suite "test" #:test (lambda _ (set! has-run? #t))
             #:max-iterations 1)
  (run-suite! suite)
  (test-assert "has-run" has-run?))

(test-end "suite")
