;;; test.scm --- MicroBenchmark Guile library test suite.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Test Case Lifecycle Tests.
;;
;;; Code:
(use-modules (mbenchmark)
             (srfi srfi-64))

(test-begin "test")

(let ((suite (make-suite "t1"))
      (test-run 0)
      (setup-run? #f)
      (tear-down-run? #f))
  (add-test! suite "test" #:set-up (lambda _ (set! setup-run? #t) '())
             #:tear-down (lambda _ (set! tear-down-run? #t))
             #:test (lambda _ (set! test-run (+ test-run 1)))
             #:max-iterations 10
             #:min-iterations 10
             #:min-sample-iterations 2
             #:max-sample-iterations 2)
  (test-assert "not set-up-run" (not setup-run?))
  (test-assert "not tear-down-run" (not setup-run?))
  (test-eqv "not test-run" 0 test-run)
  (run-suite! suite)
  (test-assert "set-up-run" setup-run?)
  (test-assert "tear-down-run" setup-run?)
  (test-eqv "test-run" 10 test-run)

  (for-each-report (get-report suite)
                   (lambda (report)
                     (test-equal "test-name" "test" (exec-report-name report))
                     (test-equal "test-name2" "test"
                                 (exec-report-test-name report))
                     (test-eqv "total-samples" 5
                               (exec-report-total-samples report))
                     (test-eqv "total-iterations" 10
                               (exec-report-total-iterations report))
                     (test-eqv "iterations" 10
                               (exec-report-iterations report)))
                   #:self-test #f))

(test-end "test")
