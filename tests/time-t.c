/* time-t.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/clock/chrono-provider.h"

#include <assert.h>
#include <string.h>
#include <time.h>

static bool time_called;
static time_t time_return;

time_t
time (time_t *ptr)
{
  assert (!time_called);
  time_called = true;
  if (ptr)
    *ptr = time_return;
  return time_return;
}

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;
  time_called = false;
  micro_benchmark_meter t =
    micro_benchmark_chronometer_create (MICRO_BENCHMARK_CLOCK_REALTIME,
                                        MICRO_BENCHMARK_CHRONO_PROVIDER_TIME_T);
  assert (t);
  assert (!time_called);
  assert (strcmp (micro_benchmark_stats_meter_get_name (t), "time_t") == 0);
  micro_benchmark_stats_sample_type type =
    micro_benchmark_stats_meter_get_sample_type (t);
  assert (type == MICRO_BENCHMARK_SAMPLE_TIME);

  micro_benchmark_clock_time res =
    micro_benchmark_stats_meter_get_min_resolution (t).time;
  assert (res.seconds == 1);
  assert (res.nanoseconds == 0);

  res = micro_benchmark_stats_meter_get_max_resolution (t).time;
  assert (res.seconds > 1);
  assert (res.nanoseconds == 0);

  assert (!time_called);
  time_return = (time_t) 1;
  micro_benchmark_stats_meter_start (t);
  assert (time_called);
  time_called = false;
  time_return = (time_t) 2;
  micro_benchmark_stats_meter_stop (t);
  assert (time_called);
  time_called = false;
  time_return = (time_t) 10;
  micro_benchmark_stats_meter_restart (t);
  assert (time_called);
  time_called = false;
  time_return = (time_t) 11;
  micro_benchmark_stats_meter_stop (t);
  assert (time_called);
  time_called = false;
  micro_benchmark_clock_time v =
    micro_benchmark_stats_meter_get_sample (t).time;
  assert (!time_called);
  assert (v.seconds == 2);
  assert (v.nanoseconds == 0);

  micro_benchmark_chronometer_release (t);
  assert (!time_called);
  return 0;
}
