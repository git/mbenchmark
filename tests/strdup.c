/* strdup.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/utility/functions.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

char *mock_strdup (const char *other);

#ifdef strdup
#undef strdup
#endif
#define strdup(x) mock_strdup (x)

#include "internal/local-inline.h"
#include "internal/utility/xalloc.h"

static char *strdup_ret = NULL;
static const char *strdup_param = NULL;

char *
mock_strdup (const char *other)
{
  assert (other);
  strdup_param = other;
  return strdup_ret;
}

static void
expected_error ()
{
  exit (EXIT_SUCCESS);
}

int
main (int argc, char **argv)
{
  assert (argc);
  assert (argv);
#ifdef HAVE_STRDUP
  static const char msg[] = "Test";
  static char ret[] = "Test";
  strdup_ret = ret;
  char *cpy = xstrdup (msg);
  assert (strdup_param == msg);
  assert (cpy == ret);

  strdup_ret = NULL;
  micro_benchmark_set_error_handler (expected_error);
  cpy = xstrdup (cpy);
  return EXIT_FAILURE;
#else
  return 77;
#endif
}
