/* stats-mp.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/stats/time-sample.h"
#include "internal/stats/multi-precision.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static void
check_double (double found, double expected)
{
  static const double epsilon = 0.001;
  fprintf (stderr, "Expected %f, found %f (ep %f)\n", expected, found,
           epsilon);
  assert (expected == found || fabs (expected - found) < epsilon);
}

static void
check_size (size_t found, size_t expected)
{
  fprintf (stderr, "Expected %zu, found %zu\n", expected, found);
  assert (expected == found);
}

static void
check_stat_value (micro_benchmark_stats_value found,
                  micro_benchmark_stats_value expected)
{
  check_size (found.unit, expected.unit);
  check_double (found.mean, expected.mean);
  check_double (found.variance, expected.variance);
  check_double (found.std_deviation, expected.std_deviation);
}

static void
check_values (micro_benchmark_time_stats_values expected, size_t n_samples,
              micro_benchmark_time_samples samples)
{
  micro_benchmark_time_stats_values ret = mbstats_mp_ (n_samples, samples);
  assert (ret.total_samples == n_samples);
  check_size (ret.total_samples, expected.total_samples);
  check_size (ret.samples, expected.samples);
  check_size (ret.iterations, expected.iterations);
  check_stat_value (ret.iteration_time, expected.iteration_time);
  check_stat_value (ret.sample_time, expected.sample_time);
  check_stat_value (ret.sample_iterations, expected.sample_iterations);
}

static void
test_empty ()
{
  static const struct micro_benchmark_time_sample_ samples[] = {
  };

  static const micro_benchmark_time_stats_values expected = {
    0, 0, 0,
    {MICRO_BENCHMARK_STATS_UNIT_NONE, INFINITY, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_UNIT_NONE, INFINITY, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_UNIT_NONE, INFINITY, INFINITY, INFINITY}
  };
  check_values (expected, sizeof (samples) / sizeof (*samples), samples);
}

static void
test_one ()
{
  static const struct micro_benchmark_time_sample_ s1min[] = {
    {false, 2, {120, 0}},
  };

  static const micro_benchmark_time_stats_values e1min = {
    sizeof (s1min) / sizeof (*s1min), 1, 2,
    {MICRO_BENCHMARK_STATS_TIME_MIN, 1.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_TIME_MIN, 2.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 2.0, INFINITY, INFINITY}
  };
  check_values (e1min, sizeof (s1min) / sizeof (*s1min), s1min);

  static const struct micro_benchmark_time_sample_ s1s[] = {
    {false, 10, {10, 0}},
  };

  static const micro_benchmark_time_stats_values e1s = {
    sizeof (s1s) / sizeof (*s1s), 1, 10,
    {MICRO_BENCHMARK_STATS_TIME_S, 1.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_TIME_S, 10.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 10.0, INFINITY, INFINITY}
  };
  check_values (e1s, sizeof (s1s) / sizeof (*s1s), s1s);

  static const struct micro_benchmark_time_sample_ s50ms[] = {
    {false, 20, {1, 0}},
  };

  static const micro_benchmark_time_stats_values e50ms = {
    sizeof (s50ms) / sizeof (*s50ms), 1, 20,
    {MICRO_BENCHMARK_STATS_TIME_MS, 50.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_TIME_S, 1.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 20.0, INFINITY, INFINITY}
  };
  check_values (e50ms, sizeof (s50ms) / sizeof (*s50ms), s50ms);

  static const struct micro_benchmark_time_sample_ s801us[] = {
    {false, 1, {0, 801000}},
  };

  static const micro_benchmark_time_stats_values e801us = {
    sizeof (s801us) / sizeof (*s801us), 1, 1,
    {MICRO_BENCHMARK_STATS_TIME_US, 801.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_TIME_US, 801.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 1.0, INFINITY, INFINITY}
  };
  check_values (e801us, sizeof (s801us) / sizeof (*s801us), s801us);

  static const struct micro_benchmark_time_sample_ s999ns[] = {
    {false, 10000, {0, 9990000}},
  };

  static const micro_benchmark_time_stats_values e999ns = {
    sizeof (s999ns) / sizeof (*s999ns), 1, 10000,
    {MICRO_BENCHMARK_STATS_TIME_NS, 999.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_TIME_MS, 9.99, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 10000.0, INFINITY, INFINITY}
  };
  check_values (e999ns, sizeof (s999ns) / sizeof (*s999ns), s999ns);
}

static void
test_two ()
{
  static const struct micro_benchmark_time_sample_ s2min[] = {
    {false, 1, {120, 0}},
    {false, 1, {120, 0}},
  };

  static const micro_benchmark_time_stats_values e2min = {
    sizeof (s2min) / sizeof (*s2min), 2, 2,
    {MICRO_BENCHMARK_STATS_TIME_MIN, 2.0, 0.0, 0.0},
    {MICRO_BENCHMARK_STATS_TIME_MIN, 2.0, 0.0, 0.0},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 1.0, 0.0, 0.0}
  };
  check_values (e2min, sizeof (s2min) / sizeof (*s2min), s2min);

  static const struct micro_benchmark_time_sample_ s1s[] = {
    {false, 10, {10, 0}},
    {false, 20, {20, 0}},
  };

  static const micro_benchmark_time_stats_values e1s = {
    sizeof (s1s) / sizeof (*s1s), 2, 30,
    {MICRO_BENCHMARK_STATS_TIME_S, 1.0, 0.0, 0.0},
    {MICRO_BENCHMARK_STATS_TIME_S, 15.0, 50.0, 7.071067},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 15.0, 50.0, 7.071067}
  };
  check_values (e1s, sizeof (s1s) / sizeof (*s1s), s1s);

  static const struct micro_benchmark_time_sample_ s1us[] = {
    {false, 100000, {0, 100000000}},
    {false, 1000000, {1, 0}},
  };

  static const micro_benchmark_time_stats_values e1us = {
    sizeof (s1us) / sizeof (*s1us), 2, 1100000,
    {MICRO_BENCHMARK_STATS_TIME_US, 1.0, 0.0, 0.0},
    {MICRO_BENCHMARK_STATS_TIME_MS, 550.0, 405000.0, 636.3961},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 550000, 405000000000.0, 636396.103}
  };
  check_values (e1us, sizeof (s1us) / sizeof (*s1us), s1us);
}

static void
test_one_with_discarded ()
{
  static const struct micro_benchmark_time_sample_ s999us[] = {
    {true, 20, {10, 0}},
    {true, 30, {20, 1000}},
    {false, 1, {0, 999000}},
    {true, 50, {0, 1000000}},
    {true, 200, {200, 123456789}},
  };

  static const micro_benchmark_time_stats_values e999us = {
    sizeof (s999us) / sizeof (*s999us), 1, 1,
    {MICRO_BENCHMARK_STATS_TIME_US, 999.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_TIME_US, 999.0, INFINITY, INFINITY},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 1, INFINITY, INFINITY}
  };
  check_values (e999us, sizeof (s999us) / sizeof (*s999us), s999us);
}

static void
test_two_with_discarded ()
{
  static const struct micro_benchmark_time_sample_ s1s[] = {
    {false, 1, {1, 800000000}},
    {true, 1000, {9, 12}},
    {true, 20000, {0, 0}},
    {true, 1000, {0, 2000}},
    {true, 20000, {20, 10000}},
    {false, 1, {1, 800000000}},
    {true, 1000, {30, 444}},
    {true, 12, {400, 555}},
  };

  static const micro_benchmark_time_stats_values e1s = {
    sizeof (s1s) / sizeof (*s1s), 2, 2,
    {MICRO_BENCHMARK_STATS_TIME_S, 1.8, 0.0, 0.0},
    {MICRO_BENCHMARK_STATS_TIME_S, 1.8, 0.0, 0.0},
    {MICRO_BENCHMARK_STATS_ITERATIONS, 1, 0.0, 0.0}
  };
  check_values (e1s, sizeof (s1s) / sizeof (*s1s), s1s);
}

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;
  test_empty ();
  test_one ();
  test_one_with_discarded ();
  test_two ();
  test_two_with_discarded ();
  return EXIT_SUCCESS;
}
