/* print-helper.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include <mbenchmark/all.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/*  For usleep.  */
#include <unistd.h>


static micro_benchmark_output_values all_values = {
  true, true, true, true, true, true, true, true,
  MICRO_BENCHMARK_STAT_ALL, MICRO_BENCHMARK_STAT_ALL,
  MICRO_BENCHMARK_STAT_ALL
};

static int sleep_time;

static void
test (void *not_used)
{
  (void) not_used;
  usleep (sleep_time);
}

int
main (int argc, char **argv)
{
  assert (argc == 8);

  /*   Default output values.  */
  micro_benchmark_output_values def =
    micro_benchmark_get_default_output_values ();

  /*  Test parameters.  */
  const char *suite_name = argv[1];
  const char *test_name = argv[2];
  int skip = atoi (argv[3]);
  int iterations = atoi (argv[4]);
  int sample_it = atoi (argv[5]);
  sleep_time = atoi (argv[6]);
  int option = atoi (argv[7]);
  micro_benchmark_test_definition test_def = {
    true, NULL, NULL, test, NULL
  };

  micro_benchmark_suite suite = micro_benchmark_suite_create (suite_name);
  assert (suite);

  micro_benchmark_test_case tc =
    micro_benchmark_suite_register_test (suite, test_name, &test_def);
  assert (tc);

  micro_benchmark_test_case_skip_iterations (tc, skip);
  micro_benchmark_test_case_limit_iterations (tc, iterations, iterations);
  micro_benchmark_test_case_limit_samples (tc, sample_it, sample_it);

  micro_benchmark_suite_run (suite);
  micro_benchmark_report rep = micro_benchmark_suite_get_report (suite);
  switch (option)
    {
    default:
      return EXIT_FAILURE;

      /* Test printing default values.  */
    case 1:
      micro_benchmark_print_report (rep);
      micro_benchmark_print_custom_report (rep, &def);
      micro_benchmark_write_report (rep, stdout,
                                    MICRO_BENCHMARK_CONSOLE_OUTPUT);
      micro_benchmark_write_custom_report (rep, stdout,
                                           MICRO_BENCHMARK_CONSOLE_OUTPUT,
                                           &def);
      break;

      /* Test printing default values, lisp format.  */
    case 2:
      micro_benchmark_write_report (rep, stdout, MICRO_BENCHMARK_LISP_OUTPUT);
      micro_benchmark_write_custom_report (rep, stdout,
                                           MICRO_BENCHMARK_LISP_OUTPUT, &def);
      break;


      /* Test printing default values, text format.  */
    case 3:
      micro_benchmark_write_report (rep, stdout, MICRO_BENCHMARK_TEXT_OUTPUT);
      micro_benchmark_write_custom_report (rep, stdout,
                                           MICRO_BENCHMARK_TEXT_OUTPUT, &def);
      break;


      /* Test printing all values, default format.  */
    case 4:
      micro_benchmark_set_default_output_values (all_values);

      micro_benchmark_print_report (rep);
      micro_benchmark_print_custom_report (rep, &all_values);
      micro_benchmark_write_report (rep, stdout,
                                    MICRO_BENCHMARK_CONSOLE_OUTPUT);
      micro_benchmark_write_custom_report (rep, stdout,
                                           MICRO_BENCHMARK_CONSOLE_OUTPUT,
                                           &all_values);
      break;


      /* Test printing all values, lisp format.  */
    case 5:
      micro_benchmark_set_default_output_values (all_values);

      micro_benchmark_write_report (rep, stdout, MICRO_BENCHMARK_LISP_OUTPUT);
      micro_benchmark_write_custom_report (rep, stdout,
                                           MICRO_BENCHMARK_LISP_OUTPUT,
                                           &all_values);
      break;


      /* Test printing all values, text format.  */
    case 6:
      micro_benchmark_set_default_output_values (all_values);

      micro_benchmark_write_report (rep, stdout, MICRO_BENCHMARK_TEXT_OUTPUT);
      micro_benchmark_write_custom_report (rep, stdout,
                                           MICRO_BENCHMARK_TEXT_OUTPUT,
                                           &all_values);
      break;
    }

  micro_benchmark_suite_release (suite);
  return EXIT_SUCCESS;
}
