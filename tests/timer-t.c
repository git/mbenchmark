/* timer-t.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/clock/timer-provider.h"

#include <assert.h>
#include <signal.h>
#include <string.h>
#include <time.h>

static bool clock_getres_called = false;
static struct timespec clock_getres_value;
static int clock_getres_return = 0;

int
clock_getres (clockid_t c, struct timespec *t)
{
  (void) c;
  assert (!clock_getres_called);
  clock_getres_called = true;
  assert (t);
  *t = clock_getres_value;
  return clock_getres_return;
}

static bool timer_create_called = false;
static clockid_t timer_create_clock;
static bool timer_create_with_sigev;
static struct sigevent timer_create_sigev;
static int timer_create_return = 0;

int
timer_create (clockid_t c, struct sigevent *restrict s, timer_t *restrict t)
{
  (void) c;
  assert (!timer_create_called);
  timer_create_called = true;
  timer_create_clock = c;
  if (s)
    {
      timer_create_with_sigev = true;
      timer_create_sigev = *s;
    }
  else
    timer_create_with_sigev = false;
  assert (t);
  return timer_create_return;
}

static bool timer_delete_called = false;
static int timer_delete_return = 0;

int
timer_delete (timer_t t)
{
  (void) t;
  assert (!timer_delete_called);
  timer_delete_called = true;
  return timer_delete_return;
}

static bool timer_settime_called = false;
static int timer_settime_flags;
static struct itimerspec timer_settime_value;
static struct itimerspec timer_settime_old_value;
static int timer_settime_return = 0;

int
timer_settime (timer_t t, int f, const struct itimerspec *restrict n,
               struct itimerspec *restrict o)
{
  (void) t;
  assert (!timer_settime_called);
  timer_settime_called = true;
  timer_settime_flags = f;
  assert (n);
  timer_settime_value = *n;
  if (o)
    *o = timer_settime_old_value;
  return timer_settime_return;
}

static bool timer_gettime_called = false;
static struct itimerspec timer_gettime_value;
static int timer_gettime_return = 0;

int
timer_gettime (timer_t t, struct itimerspec *restrict c)
{
  (void) t;
  assert (!timer_gettime_called);
  timer_gettime_called = true;
  assert (c);
  *c = timer_gettime_value;
  return timer_gettime_return;
}

static clockid_t
mbclock_to_clockid (micro_benchmark_clock_type type)
{
  switch (type)
    {
    default:
      assert (0);

    case MICRO_BENCHMARK_CLOCK_MONOTONIC:
      return CLOCK_MONOTONIC;

#ifdef HAVE_CLOCK_PROCESS_CPUTIME_ID
    case MICRO_BENCHMARK_CLOCK_PROCESS:
      return CLOCK_PROCESS_CPUTIME_ID;
#endif

#ifdef HAVE_CLOCK_THREAD_CPUTIME_ID
    case MICRO_BENCHMARK_CLOCK_THREAD:
      return CLOCK_THREAD_CPUTIME_ID;
#endif

    case MICRO_BENCHMARK_CLOCK_REALTIME:
#ifdef HAVE_CLOCK_TAI
      return CLOCK_TAI;
#else
      return CLOCK_REALTIME;
#endif
    }
}

static void
test_clock (micro_benchmark_clock_type type)
{
  static const struct itimerspec zero;
  struct timespec res = { 0, 100 };
  micro_benchmark_timer_provider prov =
    MICRO_BENCHMARK_TIMER_PROVIDER_TIMER_T;

  timer_create_called = false;
  timer_delete_called = false;
  timer_gettime_called = false;
  timer_settime_called = false;
  clock_getres_called = false;
  clock_getres_value = res;
  micro_benchmark_timer timer = micro_benchmark_timer_create (type, prov);
  assert (timer);
  assert (timer_create_called);
  timer_create_called = false;
  assert (timer_create_clock == mbclock_to_clockid (type));
  assert (clock_getres_called);
  clock_getres_called = false;
  assert (!timer_gettime_called);
  assert (!timer_settime_called);
  assert (!timer_delete_called);

  micro_benchmark_clock_time r = micro_benchmark_timer_get_resolution (timer);
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_gettime_called);
  assert (!timer_settime_called);
  assert (r.seconds == 0);
  assert (r.nanoseconds == 100);
  assert (!timer_delete_called);

  const char *name = micro_benchmark_timer_get_name (timer);
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_gettime_called);
  assert (!timer_settime_called);
  assert (strcmp (name, "timer_t") == 0);
  assert (!timer_delete_called);

  timer_settime_old_value = zero;
  micro_benchmark_clock_time deadline = { 5, 1 };
  micro_benchmark_timer_start (timer, deadline);
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_gettime_called);
  assert (timer_settime_called);
  timer_settime_called = false;
  assert (timer_settime_value.it_value.tv_sec == 5);
  assert (timer_settime_value.it_value.tv_nsec == 1);
  assert (timer_settime_value.it_interval.tv_sec == 0);
  assert (timer_settime_value.it_interval.tv_nsec == 0);
  assert (!timer_delete_called);

  timer_gettime_value = timer_settime_value;
  assert (micro_benchmark_timer_is_running (timer));
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_settime_called);
  assert (timer_gettime_called);
  timer_gettime_called = false;
  assert (!timer_delete_called);

  struct timespec ret1 = { 0 };
  ret1.tv_sec = 1;
  ret1.tv_nsec = 100000000;
  struct itimerspec iret1 = { 0 };
  iret1.it_value = ret1;
  timer_settime_old_value = iret1;
  micro_benchmark_timer_stop (timer);
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_gettime_called);
  assert (timer_settime_called);
  timer_settime_called = false;
  assert (timer_settime_value.it_value.tv_sec == 0);
  assert (timer_settime_value.it_value.tv_nsec == 0);
  assert (timer_settime_value.it_interval.tv_sec == 0);
  assert (timer_settime_value.it_interval.tv_nsec == 0);
  assert (!timer_delete_called);

  timer_gettime_value = zero;
  assert (micro_benchmark_timer_is_running (timer));
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_settime_called);
  assert (timer_gettime_called);
  timer_gettime_called = false;
  assert (!timer_delete_called);

  timer_settime_old_value = zero;
  micro_benchmark_timer_restart (timer);
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_gettime_called);
  assert (timer_settime_called);
  timer_settime_called = false;
  assert (timer_settime_value.it_value.tv_sec == ret1.tv_sec);
  assert (timer_settime_value.it_value.tv_nsec == ret1.tv_nsec);
  assert (timer_settime_value.it_interval.tv_sec == 0);
  assert (timer_settime_value.it_interval.tv_nsec == 0);
  assert (!timer_delete_called);

  timer_gettime_value = zero;
  assert (!micro_benchmark_timer_is_running (timer));
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_settime_called);
  assert (timer_gettime_called);
  timer_gettime_called = false;
  assert (!timer_delete_called);

  timer_settime_old_value = zero;
  micro_benchmark_timer_stop (timer);
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (timer_settime_called);
  timer_settime_called = false;
  assert (timer_settime_value.it_value.tv_sec == 0);
  assert (timer_settime_value.it_value.tv_nsec == 0);
  assert (timer_settime_value.it_interval.tv_sec == 0);
  assert (timer_settime_value.it_interval.tv_nsec == 0);
  assert (!timer_gettime_called);
  assert (!timer_delete_called);

  timer_gettime_value = zero;
  assert (!micro_benchmark_timer_is_running (timer));
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_settime_called);
  assert (timer_gettime_called);
  timer_gettime_called = false;
  assert (!timer_delete_called);

  micro_benchmark_clock_time elapsed = micro_benchmark_timer_elapsed (timer);
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_settime_called);
  assert (!timer_gettime_called);
  assert (elapsed.seconds == 5);
  assert (elapsed.nanoseconds == 1);
  assert (!timer_delete_called);

  micro_benchmark_timer_release (timer);
  assert (!timer_create_called);
  assert (!clock_getres_called);
  assert (!timer_gettime_called);
  assert (!timer_settime_called);
  assert (timer_delete_called);
  timer_delete_called = false;
}

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;
  test_clock (MICRO_BENCHMARK_CLOCK_REALTIME);
  test_clock (MICRO_BENCHMARK_CLOCK_MONOTONIC);
#ifdef HAVE_CLOCK_PROCESS_CPUTIME_ID
  test_clock (MICRO_BENCHMARK_CLOCK_PROCESS);
#endif
#ifdef HAVE_CLOCK_THREAD_CPUTIME_ID
  test_clock (MICRO_BENCHMARK_CLOCK_THREAD);
#endif
  return 0;
}
