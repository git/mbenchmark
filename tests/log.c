/* log.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#ifdef MICRO_BENCHMARK_BUILD_WITHOUT_TRACES
#undef MICRO_BENCHMARK_BUILD_WITHOUT_TRACES
#endif
#include "internal/utility/log.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

static FILE *
create_tmpfile (void)
{
#if HAVE_TMPFILE
  return tmpfile ();
#else
  exit (77);
#endif
}

#define MICRO_BENCHMARK_MODULE "log-test"

/*  TODO: Check printed messages.  */
static void
test_error (void)
{
  FILE *out = create_tmpfile ();
  micro_benchmark_log_set_output (out);
  long initial = ftell (out);
  MB_TRACE ("trace msg");
  long curr = ftell (out);
  assert (initial == curr);
  MB_DEBUG ("debug msg");
  curr = ftell (out);
  assert (initial == curr);
  MB_INFO ("info msg");
  curr = ftell (out);
  assert (initial == curr);
  MB_WARN ("warn msg");
  curr = ftell (out);
  assert (initial == curr);
  MB_ERROR ("ok");
  curr = ftell (out);
  assert (initial != curr);
  micro_benchmark_log_set_output (NULL);
  fclose (out);
}

static void
test_warning (void)
{
  FILE *out = create_tmpfile ();
  micro_benchmark_log_set_output (out);
  long initial = ftell (out);
  MB_TRACE ("trace msg");
  long curr = ftell (out);
  assert (initial == curr);
  MB_DEBUG ("debug msg");
  curr = ftell (out);
  assert (initial == curr);
  MB_INFO ("info msg");
  curr = ftell (out);
  assert (initial == curr);
  MB_WARN ("warn msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_ERROR ("ok");
  curr = ftell (out);
  assert (initial != curr);
  micro_benchmark_log_set_output (NULL);
  fclose (out);
}

static void
test_info (void)
{
  FILE *out = create_tmpfile ();
  micro_benchmark_log_set_output (out);
  long initial = ftell (out);
  MB_TRACE ("trace msg");
  long curr = ftell (out);
  assert (initial == curr);
  MB_DEBUG ("debug msg");
  curr = ftell (out);
  assert (initial == curr);
  MB_INFO ("info msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_WARN ("warn msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_ERROR ("ok");
  curr = ftell (out);
  assert (initial != curr);
  micro_benchmark_log_set_output (NULL);
  fclose (out);
}

static void
test_debug (void)
{
  FILE *out = create_tmpfile ();
  micro_benchmark_log_set_output (out);
  long initial = ftell (out);
  MB_TRACE ("trace msg");
  long curr = ftell (out);
  assert (initial == curr);
  MB_DEBUG ("debug msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_INFO ("info msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_WARN ("warn msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_ERROR ("ok");
  curr = ftell (out);
  assert (initial != curr);
  micro_benchmark_log_set_output (NULL);
  fclose (out);
}

static void
test_trace (void)
{
  FILE *out = create_tmpfile ();
  micro_benchmark_log_set_output (out);
  long initial = ftell (out);
  MB_TRACE ("trace msg");
  long curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_DEBUG ("debug msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_INFO ("info msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_WARN ("warn msg");
  curr = ftell (out);
  assert (initial != curr);
  initial = curr;
  MB_ERROR ("ok");
  curr = ftell (out);
  assert (initial != curr);
  micro_benchmark_log_set_output (NULL);
  fclose (out);
}

static void
test_global_levels (void)
{
  micro_benchmark_set_log_level (MICRO_BENCHMARK_ERROR_LEVEL);
  test_error ();
  micro_benchmark_set_log_level (MICRO_BENCHMARK_WARNING_LEVEL);
  test_warning ();
  micro_benchmark_set_log_level (MICRO_BENCHMARK_INFO_LEVEL);
  test_info ();
  micro_benchmark_set_log_level (MICRO_BENCHMARK_DEBUG_LEVEL);
  test_debug ();
  micro_benchmark_set_log_level (MICRO_BENCHMARK_TRACE_LEVEL);
  test_trace ();
}

static void
test_module_levels (void)
{
  micro_benchmark_set_log_level (MICRO_BENCHMARK_TRACE_LEVEL);
  micro_benchmark_set_module_log_level (MICRO_BENCHMARK_MODULE,
                                        MICRO_BENCHMARK_ERROR_LEVEL);
  test_error ();
  micro_benchmark_set_module_log_level (MICRO_BENCHMARK_MODULE,
                                        MICRO_BENCHMARK_WARNING_LEVEL);
  test_warning ();
  micro_benchmark_set_module_log_level (MICRO_BENCHMARK_MODULE,
                                        MICRO_BENCHMARK_INFO_LEVEL);
  test_info ();
  micro_benchmark_set_module_log_level (MICRO_BENCHMARK_MODULE,
                                        MICRO_BENCHMARK_DEBUG_LEVEL);
  test_debug ();
  micro_benchmark_set_module_log_level (MICRO_BENCHMARK_MODULE,
                                        MICRO_BENCHMARK_TRACE_LEVEL);
  test_trace ();
}

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;

  test_global_levels ();
  test_module_levels ();
  return EXIT_SUCCESS;
}
