/* alloc.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/suite.h"
#include "mbenchmark/report/io.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef HAVE_EXECINFO_H
#include <unistd.h>
#include <execinfo.h>
#endif

#ifndef __SANITIZE_ADDRESS__
/* TODO: Adjust memory values  */
#define MAX_MEMORY 1024 * 1024 * 10
#define MAX_POINTERS 1024 * 20
#define ALIGN_VALUE sizeof (void *)

struct mem
{
  size_t last;
  size_t last_given;
  size_t last_taken;
  void *given[MAX_POINTERS];
  void *taken[MAX_POINTERS];
  char buffer[MAX_MEMORY];
#ifdef HAVE_EXECINFO_H
#define MAX_LEVELS 20
  void *given_syms[MAX_POINTERS][MAX_LEVELS];
  int given_n_syms[MAX_POINTERS];
  void *taken_syms[MAX_POINTERS][MAX_LEVELS];
  int taken_n_syms[MAX_POINTERS];
#endif
};

static struct mem heap_mem = { 0 };

static int in_backtrace = 0;
static size_t first_alloc = 0;
static size_t last_alloc = 0;

static void
check_available_mem (void)
{
  if (heap_mem.last >= MAX_MEMORY)
    {
      fprintf (stderr, "Exhausted memory: %zu (%zu).\n", heap_mem.last,
               (size_t) MAX_MEMORY);
      abort ();
    }
}

static void *
get_ptr (size_t size)
{
  if (size == 0)
    return NULL;
  while (((heap_mem.last + (size_t) heap_mem.buffer) % ALIGN_VALUE) != 0)
    heap_mem.last++;

  check_available_mem ();

#ifdef HAVE_EXECINFO_H
  if (!in_backtrace)
    {
      in_backtrace = 1;
      heap_mem.given_n_syms[heap_mem.last_given] =
        backtrace (&heap_mem.given_syms[heap_mem.last_given][0], MAX_LEVELS);
      in_backtrace = 0;
    }
  else
    heap_mem.given_n_syms[heap_mem.last_given] = 0;
#endif

  void *ret = heap_mem.buffer + heap_mem.last;
  heap_mem.last += size;
  if (heap_mem.last_given >= MAX_POINTERS)
    abort ();

  heap_mem.given[heap_mem.last_given++] = ret;
  while (((heap_mem.last + (size_t) heap_mem.buffer) % ALIGN_VALUE) != 0)
    heap_mem.last++;
  check_available_mem ();
  return ret;
}

void *
malloc (size_t size)
{
  return get_ptr (size);
}

static void
do_release (void *ptr)
{
  for (size_t i = 0; i < heap_mem.last_taken; ++i)
    if (ptr == heap_mem.taken[i])
      abort ();

  for (size_t i = 0; i < heap_mem.last_given; ++i)
    if (ptr == heap_mem.given[i])
      {
        if (heap_mem.last_taken >= MAX_POINTERS)
          abort ();
#ifdef HAVE_EXECINFO_H
        if (in_backtrace)
          heap_mem.taken_n_syms[heap_mem.last_taken] = 0;
        else
          {
            in_backtrace = 1;
            heap_mem.taken_n_syms[heap_mem.last_taken] =
              backtrace (&heap_mem.taken_syms[heap_mem.last_taken][0],
                         MAX_LEVELS);
            in_backtrace = 0;
          }
#endif
        heap_mem.taken[heap_mem.last_taken++] = ptr;
        return;
      }
  abort ();
}

void
free (void *ptr)
{
  if (!ptr)
    return;
  do_release (ptr);
}

void *
calloc (size_t n, size_t size)
{
  void *ptr = get_ptr (size * n);
  if (ptr)
    memset (ptr, 0, size * n);
  return ptr;
}

void *
realloc (void *ptr, size_t size)
{
  void *ret = get_ptr (size);
  if (ptr)
    {
      if (ret)
        /* Copy garbage, do not bother with housekeeping...  */
        memmove (ret, ptr, size);
      do_release (ptr);
    }
  return ret;
}
#endif

static void *
set_up (micro_benchmark_test_state state)
{
  assert (state);
  micro_benchmark_state_set_name (state, "test name");
  return 0;
}

static void
empty_test (void *ptr)
{
  assert (!ptr);
}

static void
tear_down (micro_benchmark_test_state state, void *ptr)
{
  assert (state);
  assert (!ptr);
  micro_benchmark_state_set_name (state, "test name2");
}

static void
check_mem (void)
{
  int left = 0;
  int errors = 0;
#ifndef __SANITIZE_ADDRESS__
  if (heap_mem.last_given == heap_mem.last_taken)
    return;
  for (size_t i = 0; i < heap_mem.last_given; ++i)
    {
      void *taken = NULL;
      for (size_t j = 0; j < heap_mem.last_taken; ++j)
        if (heap_mem.taken[j] == heap_mem.given[i])
          {
            if (taken)
              assert (taken && !"Double free");

            taken = heap_mem.taken[j];
          }
      if (!taken)
        {
          fprintf (stderr, "Memory missing %p (%zu/%p-%p)\n",
                   heap_mem.given[i], i, heap_mem.buffer,
                   heap_mem.buffer + heap_mem.last);
#ifdef HAVE_EXECINFO_H
          fprintf (stderr, "Given:\n");
          if (heap_mem.given_n_syms[i] > 0)
            backtrace_symbols_fd (&heap_mem.given_syms[i][0],
                                  heap_mem.given_n_syms[i], STDERR_FILENO);
          else
            fprintf (stderr, "in backtrace call\n");
#endif
          left++;
          if (i >= first_alloc && i < last_alloc)
            errors++;
        }
    }
#endif
  fprintf (stderr, "Allocations left: %d\n", left);
  fprintf (stderr, "Fatal errors: %d\n", errors);
  assert (errors == 0);
}

int
main (int argc, char **argv)
{
  assert (argc);
  assert (argv);
  atexit (check_mem);
#ifndef __SANITIZE_ADDRESS__
#ifdef HAVE_EXECINFO_H
  void *ptr;
  in_backtrace = 1;
  backtrace (&ptr, 1);
  in_backtrace = 0;
#endif
  /* Start checking here as the deallocations performed by backtrace
     and init calls may come after check_mem.  */
  first_alloc = heap_mem.last_given;
#endif

  static const char *suite_name = "test-alloc";
  char suite_name_def[] = "test-alloc";
  micro_benchmark_suite suite = micro_benchmark_suite_create (suite_name_def);
  suite_name_def[0] = '\0';
  assert (suite);

  const char *name = micro_benchmark_suite_get_name (suite);
  assert (strcmp (name, suite_name) == 0);

  struct micro_benchmark_test_definition test_def = {
    true, set_up, tear_down, empty_test, NULL
  };
  static const char *test_name = "auto-test";
  char test_cname[] = "auto-test";
  micro_benchmark_test_case auto_case =
    micro_benchmark_suite_register_test (suite, test_cname,
                                         &test_def);
  test_cname[0] = '\0';

  assert (auto_case);

  micro_benchmark_clock_time t = { 1, 0 };
  micro_benchmark_test_case_set_max_time (auto_case, t);

  static const size_t dim0[] = { 1, 2 };
  micro_benchmark_test_case_add_dimension (auto_case, 1, dim0);

  static const size_t dim1[] = { 20, 30 };
  micro_benchmark_test_case_add_dimension (auto_case, 2, dim1);

  micro_benchmark_test_case_skip_iterations (auto_case, 100);
  micro_benchmark_test_case_limit_iterations (auto_case, 50000, 100000);
  micro_benchmark_test_case_limit_samples (auto_case, 1024, 2048);

  micro_benchmark_suite_run (suite);

  micro_benchmark_report report = micro_benchmark_suite_get_report (suite);
  const char *rname = micro_benchmark_report_get_name (report);
  assert (strcmp (rname, suite_name) == 0);

  size_t n_tests = micro_benchmark_report_get_number_of_tests (report);
  assert (n_tests == 2);

  micro_benchmark_test_report test_rep =
    micro_benchmark_report_get_test_report (report, 1);
  assert (test_rep);

  const char *test_rep_name = micro_benchmark_test_report_get_name (test_rep);
  assert (strcmp (test_rep_name, test_name) == 0);

  const size_t n_exec =
    micro_benchmark_test_report_get_num_executions (test_rep);
  assert (n_exec == 2);

  for (size_t i = 0; i < n_exec; ++i)
    {
      micro_benchmark_exec_report exec_rep =
        micro_benchmark_test_report_get_exec_report (test_rep, i);
      assert (exec_rep);

      const char *exec_rep_name =
        micro_benchmark_exec_report_get_name (exec_rep);
      assert (strcmp (exec_rep_name, "test name2") == 0);
    }

#ifndef __SANITIZE_ADDRESS__
  /* End checking here as fprintf and iconv_open do not free their
     allocated memory before check_mem call at exit.  */
  last_alloc = heap_mem.last_given;
#else

  micro_benchmark_print_report (report);
  static const micro_benchmark_output_values def = {
    true, true, true, true, true, true, true, true,
    MICRO_BENCHMARK_STAT_ALL, MICRO_BENCHMARK_STAT_ALL,
    MICRO_BENCHMARK_STAT_ALL
  };

  micro_benchmark_print_custom_report (report, &def);
  micro_benchmark_write_report (report, stdout,
                                MICRO_BENCHMARK_CONSOLE_OUTPUT);
  micro_benchmark_write_report (report, stdout, MICRO_BENCHMARK_TEXT_OUTPUT);
  micro_benchmark_write_report (report, stdout, MICRO_BENCHMARK_LISP_OUTPUT);
  micro_benchmark_write_custom_report (report, stdout,
                                       MICRO_BENCHMARK_CONSOLE_OUTPUT, &def);
  micro_benchmark_write_custom_report (report, stdout,
                                       MICRO_BENCHMARK_TEXT_OUTPUT, &def);
  micro_benchmark_write_custom_report (report, stdout,
                                       MICRO_BENCHMARK_LISP_OUTPUT, &def);
#endif
  micro_benchmark_suite_release (suite);

  return EXIT_SUCCESS;
}
