/* xstrdup.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#ifdef HAVE_STRDUP
#undef HAVE_STRDUP
#endif

#include "internal/local-inline.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

int
main (int argc, char **argv)
{
  assert (argc);
  assert (argv);
  static const char msg[] = "some long message Δシ";
  char *res = xstrdup (msg);
  assert (res);
  assert (res != msg);
  assert (strcmp (res, msg) == 0);

  size_t len = strlen (res) + 1;
  assert (len == sizeof (msg));
  xfree (res, len);

  return EXIT_SUCCESS;
}
