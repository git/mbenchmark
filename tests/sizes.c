/* sizes.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/all.h"

#include <assert.h>
#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static const size_t sizes0[] = { 1, 2, 3 };
static const size_t sizes1[] = { 10, 20, 30 };
static const size_t sizes2[] = { 1, 2 };

static const size_t n_sizes0 = sizeof (sizes0) / sizeof (*sizes0);
static const size_t n_sizes1 = sizeof (sizes1) / sizeof (*sizes1);
static const size_t n_sizes2 = sizeof (sizes2) / sizeof (*sizes2);

static void
check_permutation (micro_benchmark_test_state state)
{
  static size_t last = 0;
  static const size_t ep0[] = {
    1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3
  };
  static const size_t ep1[] = {
    10, 10, 10, 20, 20, 20, 30, 30, 30, 10, 10, 10, 20, 20, 20, 30, 30, 30
  };
  static const size_t ep2[] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2
  };

  assert (last < sizeof (ep0) / sizeof (*ep0));
  assert (last < sizeof (ep1) / sizeof (*ep1));
  assert (last < sizeof (ep2) / sizeof (*ep2));
  size_t p0 = micro_benchmark_state_get_size (state, 0);
  size_t p1 = micro_benchmark_state_get_size (state, 1);
  size_t p2 = micro_benchmark_state_get_size (state, 2);
  assert (ep0[last] == p0);
  assert (ep1[last] == p1);
  assert (ep2[last] == p2);
  last++;
}

static size_t executed_test = 0;

static void *
set_up (micro_benchmark_test_state state)
{
  assert (state);
  assert (micro_benchmark_state_get_dimensions (state) == 3);
  check_permutation (state);
  ++executed_test;
  return NULL;
}

static void
test (void *p)
{
  assert (!p);
}

static const struct micro_benchmark_test_definition test_def = {
  true, set_up, NULL, test, NULL
};

/* XXX: This test fails on aarch64 with EAGAIN on timer_create.  */
static void
error_handler (void)
{
  if (errno == EAGAIN)
    exit (77);
  exit (EXIT_FAILURE);
}

int
main (int argc, char **argv)
{
  /* XXX: See error_handler.  */
  micro_benchmark_set_error_handler (error_handler);
  const char *name = argv[argc - 1];
  micro_benchmark_suite suite = micro_benchmark_suite_create (name);

  micro_benchmark_test_case tcase =
    micro_benchmark_suite_register_test (suite, "test", &test_def);
  assert (tcase);
  micro_benchmark_test_case_add_dimension (tcase, n_sizes0, sizes0);
  micro_benchmark_test_case_add_dimension (tcase, n_sizes1, sizes1);
  micro_benchmark_test_case_add_dimension (tcase, n_sizes2, sizes2);
  micro_benchmark_test_case_skip_iterations (tcase, 0);
  micro_benchmark_test_case_limit_iterations (tcase, 1, 1);

  micro_benchmark_suite_run (suite);
  micro_benchmark_suite_release (suite);
  assert (executed_test == 18);

  return EXIT_SUCCESS;
}
