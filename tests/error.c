/* error.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/all.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static bool should_fail = false;

/*  Use time as it is an easy function to mock, guaranteed to be
    provided and computationally cheap.  The default error_handler is
    ok for every other test.  */
time_t
time (time_t *t)
{
  static time_t counter = (time_t) 100;
  if (should_fail)
    return (time_t) -1;
  if (t)
    *t = counter;
  return counter++;
}

static micro_benchmark_meter meter;

static void
error_handler (void)
{
  bool ev = should_fail;
  should_fail = false;
  /*  Avoid leaks.  */
  micro_benchmark_chronometer_release (meter);
  exit (ev ? EXIT_SUCCESS : EXIT_FAILURE);
}

int
main (int argc, char **argv)
{
  (void) argv;
  (void) argc;
  micro_benchmark_set_error_handler (error_handler);
  micro_benchmark_clock_type t = MICRO_BENCHMARK_CLOCK_REALTIME;
  micro_benchmark_chronometer_provider p =
    MICRO_BENCHMARK_CHRONO_PROVIDER_TIME_T;
  meter = micro_benchmark_chronometer_create (t, p);
  should_fail = false;
  micro_benchmark_stats_meter_start (meter);
  should_fail = true;
  micro_benchmark_stats_meter_stop (meter);
  micro_benchmark_chronometer_release (meter);
  return EXIT_FAILURE;
}
