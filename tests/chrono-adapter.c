/* chrono-adapter.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/clock/timer-provider.h"

#include <assert.h>
#include <string.h>
#include <time.h>

static const char *meter_name = "test-chrono";
static micro_benchmark_clock_time min_res = { 1, 0 };
static micro_benchmark_clock_time max_res = { 1000, 0 };

static micro_benchmark_clock_type ctype = MICRO_BENCHMARK_CLOCK_REALTIME;

static bool init_called = false;
static void
init (micro_benchmark_meter_data *d, const void *t)
{
  assert (d);
  assert (t);
  assert (*(micro_benchmark_clock_type *) t == ctype);
  d->name = meter_name;
  d->ptr = NULL;
  d->sample_type = MICRO_BENCHMARK_SAMPLE_TIME;
  d->min_resolution.time = min_res;
  d->max_resolution.time = max_res;
  init_called = true;
}

static bool cleanup_called = false;
static void
cleanup (const micro_benchmark_meter_data *d)
{
  assert (d);
  assert (!d->ptr);
  assert (d->name == meter_name);
  cleanup_called = true;
}

static bool start_called = false;
static void
start (void *d)
{
  assert (!d);
  start_called = true;
}

static bool stop_called = false;
static void
stop (void *d)
{
  assert (!d);
  stop_called = true;
}

static bool restart_called = false;
static void
restart (void *d)
{
  assert (!d);
  restart_called = true;
}

static bool get_sample_called = false;
static micro_benchmark_stats_meter_sample returned_sample;

static micro_benchmark_stats_meter_sample
get_sample (void *d)
{
  assert (!d);
  get_sample_called = true;
  return returned_sample;
}

static micro_benchmark_meter_definition def = {
  {NULL, "test-clock", MICRO_BENCHMARK_SAMPLE_TIME, {}, {}},
  init, cleanup, start, stop, restart, get_sample
};

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;
  init_called = false;
  cleanup_called = false;
  start_called = false;
  stop_called = false;
  restart_called = false;
  get_sample_called = false;
  micro_benchmark_timer timer =
    micro_benchmark_timer_from_provided_meter (ctype, &def);
  assert (timer);
  assert (init_called);
  init_called = false;
  assert (!cleanup_called);
  assert (!start_called);
  assert (!stop_called);
  assert (!restart_called);
  assert (!get_sample_called);

  const char *name = micro_benchmark_timer_get_name (timer);
  assert (strcmp (name, "chrono-adapter") == 0);
  assert (!init_called);
  assert (!cleanup_called);
  assert (!start_called);
  assert (!stop_called);
  assert (!restart_called);
  assert (!get_sample_called);

  micro_benchmark_clock_time res =
    micro_benchmark_timer_get_resolution (timer);
  assert (min_res.seconds == res.seconds);
  assert (min_res.nanoseconds == res.nanoseconds);
  assert (!init_called);
  assert (!cleanup_called);
  assert (!start_called);
  assert (!stop_called);
  assert (!restart_called);
  assert (!get_sample_called);

  micro_benchmark_clock_time deadline = { 5, 0 };
  micro_benchmark_timer_start (timer, deadline);
  assert (!init_called);
  assert (!cleanup_called);
  assert (start_called);
  start_called = false;
  assert (!stop_called);
  assert (!restart_called);
  assert (!get_sample_called);

  micro_benchmark_clock_time ret1 = { 1, 500000000 };
  returned_sample.time = ret1;
  micro_benchmark_timer_stop (timer);
  assert (!init_called);
  assert (!cleanup_called);
  assert (!start_called);
  assert (stop_called);
  stop_called = false;
  assert (!restart_called);
  assert (get_sample_called);
  get_sample_called = false;
  assert (micro_benchmark_timer_is_running (timer));

  micro_benchmark_timer_restart (timer);
  assert (!init_called);
  assert (!cleanup_called);
  assert (!start_called);
  assert (!stop_called);
  assert (restart_called);
  restart_called = false;
  assert (!get_sample_called);

  micro_benchmark_clock_time ret2 = { 3, 499999999 };
  returned_sample.time = ret2;
  micro_benchmark_timer_stop (timer);
  assert (!init_called);
  assert (!cleanup_called);
  assert (!start_called);
  assert (stop_called);
  stop_called = false;
  assert (!restart_called);
  assert (get_sample_called);
  get_sample_called = false;
  assert (micro_benchmark_timer_is_running (timer));

  micro_benchmark_timer_restart (timer);
  assert (!init_called);
  assert (!cleanup_called);
  assert (!start_called);
  assert (!stop_called);
  assert (restart_called);
  restart_called = false;
  assert (!get_sample_called);

  micro_benchmark_clock_time ret3 = { 0, 1 };
  returned_sample.time = ret3;
  micro_benchmark_timer_stop (timer);
  assert (!init_called);
  assert (!cleanup_called);
  assert (!start_called);
  assert (stop_called);
  stop_called = false;
  assert (!restart_called);
  assert (get_sample_called);
  get_sample_called = false;
  assert (!micro_benchmark_timer_is_running (timer));

  micro_benchmark_clock_time elapsed = micro_benchmark_timer_elapsed (timer);
  assert (elapsed.seconds == 5);
  assert (elapsed.nanoseconds == 0);
  assert (!init_called);
  assert (!cleanup_called);
  assert (!start_called);
  assert (!stop_called);
  assert (!restart_called);
  assert (!get_sample_called);
  assert (!micro_benchmark_timer_is_running (timer));

  micro_benchmark_timer_release (timer);
  assert (!init_called);
  assert (cleanup_called);
  assert (!start_called);
  assert (!stop_called);
  assert (!restart_called);
  assert (!get_sample_called);

  return 0;
}
