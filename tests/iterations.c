/* iterations.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/all.h"

#include <assert.h>
#include <stddef.h>

static void
test (micro_benchmark_test_state state)
{
  assert (micro_benchmark_state_get_data (state) == NULL);
  int iterations = 0;
  while (micro_benchmark_state_keep_running (state))
    ++iterations;
  assert (iterations == 1);
}

MICRO_BENCHMARK_REGISTER_TEST (test);

static int iterations = 0;

static void
test_auto (void *ptr)
{
  assert (ptr == NULL);
  ++iterations;
}

MICRO_BENCHMARK_REGISTER_AUTO_TEST (NULL, test_auto, NULL);

static void
test_constraints (micro_benchmark_test_case test)
{
  micro_benchmark_test_case_skip_iterations (test, 0);
  micro_benchmark_test_case_limit_iterations (test, 1, 1);
}

MICRO_BENCHMARK_CONSTRAINT_TEST ("test", test_constraints);

static void
test_auto_constraints (micro_benchmark_test_case test)
{
  micro_benchmark_test_case_skip_iterations (test, 1);
  micro_benchmark_test_case_limit_iterations (test, 2, 2);
}

MICRO_BENCHMARK_CONSTRAINT_TEST ("test_auto", test_auto_constraints);

int
main (int argc, char **argv)
{
  assert (iterations == 0);
  int ret = micro_benchmark_main (argc, argv);
  assert (iterations == 3);
  return ret;
}
