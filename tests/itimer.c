/* itimer.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/clock/timer-provider.h"

#include <assert.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>

#if HAVE_UU_ITIMER_WHICH_T
typedef __itimer_which_t which_type;
#else
typedef int which_type;
#endif

static bool setitimer_called = false;
static which_type setitimer_expected;
static struct itimerval setitimer_value;
static struct itimerval setitimer_old_value;
static int setitimer_return = 0;

int
setitimer (which_type which, const struct itimerval *restrict n,
           struct itimerval *restrict o)
{
  assert (which == setitimer_expected);
  assert (!setitimer_called);
  setitimer_called = true;
  assert (n);
  setitimer_value = *n;
  if (o)
    *o = setitimer_old_value;
  return setitimer_return;
}

static bool getitimer_called = false;
static which_type getitimer_expected;
static struct itimerval getitimer_value;
static int getitimer_return = 0;

int
getitimer (which_type which, struct itimerval *restrict c)
{
  assert (which == getitimer_expected);
  assert (!getitimer_called);
  getitimer_called = true;
  assert (c);
  *c = getitimer_value;
  return getitimer_return;
}

static bool signal_called = false;
typedef void (*sighnd_type) (int);
static bool check_signal_sig = false;
static int signal_sig;
static sighnd_type signal_hnd = NULL;
static sighnd_type signal_return = SIG_DFL;

sighnd_type
signal (int sig, sighnd_type hnd)
{
  assert (!signal_called);
  signal_called = true;
  if (!check_signal_sig)
    signal_sig = sig;
  assert (signal_sig == sig);
  signal_hnd = hnd;
  return signal_return;
}

static int
mbclock_to_itimer (micro_benchmark_clock_type type)
{
  switch (type)
    {
    default:
      assert (0);

    case MICRO_BENCHMARK_CLOCK_MONOTONIC:
      return ITIMER_PROF;

    case MICRO_BENCHMARK_CLOCK_PROCESS:
      return ITIMER_VIRTUAL;

    case MICRO_BENCHMARK_CLOCK_REALTIME:
      return ITIMER_REAL;
    }
}

static void
test_signal (micro_benchmark_clock_type type)
{
  static const struct itimerval zero;
  setitimer_expected = mbclock_to_itimer (type);
  getitimer_expected = mbclock_to_itimer (type);
  micro_benchmark_timer_provider prov = MICRO_BENCHMARK_TIMER_PROVIDER_ITIMER;

  signal_called = false;
  getitimer_called = false;
  setitimer_called = false;
  micro_benchmark_timer timer = micro_benchmark_timer_create (type, prov);
  assert (timer);
  assert (!getitimer_called);
  assert (!setitimer_called);

  micro_benchmark_clock_time r = micro_benchmark_timer_get_resolution (timer);
  assert (!getitimer_called);
  assert (!setitimer_called);
  assert (r.seconds == 0);
  assert (r.nanoseconds == 1000);

  const char *name = micro_benchmark_timer_get_name (timer);
  assert (!getitimer_called);
  assert (!setitimer_called);
  assert (strcmp (name, "itimer") == 0);

  setitimer_old_value = zero;
  micro_benchmark_clock_time deadline = { 5, 1 };
  signal_hnd = NULL;
  signal_return = SIG_DFL;
  micro_benchmark_timer_start (timer, deadline);
  assert (!getitimer_called);
  assert (setitimer_called);
  setitimer_called = false;
  assert (setitimer_value.it_value.tv_sec == 5);
  assert (setitimer_value.it_value.tv_usec == 0);
  assert (setitimer_value.it_interval.tv_sec == 0);
  assert (setitimer_value.it_interval.tv_usec == 0);

  assert (signal_called);
  signal_called = false;
  check_signal_sig = true;
  assert (signal_hnd);
  signal_hnd (signal_sig);

  assert (!micro_benchmark_timer_is_running (timer));
  assert (!setitimer_called);
  assert (!getitimer_called);
  assert (!signal_called);

  setitimer_value = zero;
  micro_benchmark_timer_stop (timer);
  assert (setitimer_called);
  setitimer_called = false;
  assert (!getitimer_called);
  assert (!signal_called);

  signal_return = signal_hnd;
  signal_hnd = NULL;
  micro_benchmark_timer_release (timer);
  assert (!getitimer_called);
  assert (!setitimer_called);
  assert (signal_called);
  signal_called = false;
  check_signal_sig = false;
  assert (signal_hnd == SIG_DFL);
}

static void
test_clock (micro_benchmark_clock_type type)
{
  static const struct itimerval zero;
  setitimer_expected = mbclock_to_itimer (type);
  getitimer_expected = mbclock_to_itimer (type);
  micro_benchmark_timer_provider prov = MICRO_BENCHMARK_TIMER_PROVIDER_ITIMER;

  signal_called = false;
  getitimer_called = false;
  setitimer_called = false;
  micro_benchmark_timer timer = micro_benchmark_timer_create (type, prov);
  assert (timer);
  assert (!getitimer_called);
  assert (!setitimer_called);

  micro_benchmark_clock_time r = micro_benchmark_timer_get_resolution (timer);
  assert (!getitimer_called);
  assert (!setitimer_called);
  assert (r.seconds == 0);
  assert (r.nanoseconds == 1000);

  const char *name = micro_benchmark_timer_get_name (timer);
  assert (!getitimer_called);
  assert (!setitimer_called);
  assert (strcmp (name, "itimer") == 0);

  setitimer_old_value = zero;
  micro_benchmark_clock_time deadline = { 5, 1000 };
  signal_hnd = NULL;
  signal_return = SIG_DFL;
  micro_benchmark_timer_start (timer, deadline);
  assert (!getitimer_called);
  assert (signal_called);
  signal_called = false;
  check_signal_sig = true;
  assert (signal_hnd);
  assert (setitimer_called);
  setitimer_called = false;
  assert (setitimer_value.it_value.tv_sec == 5);
  assert (setitimer_value.it_value.tv_usec == 1);
  assert (setitimer_value.it_interval.tv_sec == 0);
  assert (setitimer_value.it_interval.tv_usec == 0);

  getitimer_value = setitimer_value;
  assert (micro_benchmark_timer_is_running (timer));
  assert (!setitimer_called);
  assert (getitimer_called);
  getitimer_called = false;
  assert (!signal_called);

  struct timeval ret1 = { 0 };
  ret1.tv_sec = 1;
  ret1.tv_usec = 100000;
  struct itimerval iret1 = { 0 };
  iret1.it_value = ret1;
  setitimer_old_value = iret1;
  micro_benchmark_timer_stop (timer);
  assert (!getitimer_called);
  assert (setitimer_called);
  setitimer_called = false;
  assert (setitimer_value.it_value.tv_sec == 0);
  assert (setitimer_value.it_value.tv_usec == 0);
  assert (setitimer_value.it_interval.tv_sec == 0);
  assert (setitimer_value.it_interval.tv_usec == 0);
  assert (!signal_called);

  getitimer_value = zero;
  assert (micro_benchmark_timer_is_running (timer));
  assert (!setitimer_called);
  assert (getitimer_called);
  getitimer_called = false;
  assert (!signal_called);

  setitimer_old_value = zero;
  micro_benchmark_timer_restart (timer);
  assert (!getitimer_called);
  assert (setitimer_called);
  setitimer_called = false;
  assert (setitimer_value.it_value.tv_sec == ret1.tv_sec);
  assert (setitimer_value.it_value.tv_usec == ret1.tv_usec);
  assert (setitimer_value.it_interval.tv_sec == 0);
  assert (setitimer_value.it_interval.tv_usec == 0);
  assert (!signal_called);

  getitimer_value = zero;
  assert (!micro_benchmark_timer_is_running (timer));
  assert (!setitimer_called);
  assert (getitimer_called);
  getitimer_called = false;
  assert (!signal_called);

  setitimer_old_value = zero;
  micro_benchmark_timer_stop (timer);
  assert (setitimer_called);
  setitimer_called = false;
  assert (setitimer_value.it_value.tv_sec == 0);
  assert (setitimer_value.it_value.tv_usec == 0);
  assert (setitimer_value.it_interval.tv_sec == 0);
  assert (setitimer_value.it_interval.tv_usec == 0);
  assert (!getitimer_called);
  assert (!signal_called);

  getitimer_value = zero;
  assert (!micro_benchmark_timer_is_running (timer));
  assert (!setitimer_called);
  assert (getitimer_called);
  getitimer_called = false;
  assert (!signal_called);

  micro_benchmark_clock_time elapsed = micro_benchmark_timer_elapsed (timer);
  assert (!setitimer_called);
  assert (!getitimer_called);
  assert (elapsed.seconds == 5);
  assert (elapsed.nanoseconds == 1000);
  assert (!signal_called);

  signal_return = signal_hnd;
  signal_hnd = NULL;
  micro_benchmark_timer_release (timer);
  assert (!getitimer_called);
  assert (!setitimer_called);
  assert (signal_called);
  signal_called = false;
  check_signal_sig = false;
  assert (signal_hnd == SIG_DFL);
}

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;
  test_clock (MICRO_BENCHMARK_CLOCK_REALTIME);
  test_clock (MICRO_BENCHMARK_CLOCK_MONOTONIC);
  test_clock (MICRO_BENCHMARK_CLOCK_PROCESS);

  test_signal (MICRO_BENCHMARK_CLOCK_REALTIME);
  test_signal (MICRO_BENCHMARK_CLOCK_MONOTONIC);
  test_signal (MICRO_BENCHMARK_CLOCK_PROCESS);
  return 0;
}
