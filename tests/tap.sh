# tap.sh --- MicroBenchmark TAP test suite shell helper.
#
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is part of MicroBenchmark.
#
# MicroBenchmark is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Benchmark is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MicroBenchmark.  If not, see
# <http://www.gnu.org/licenses/>.

: ${DEBUG:=no}
if [ "x$DEBUG" != xno ]; then
    PS4='# + '
    set -x
fi

counter=0
print_error ()
{
    echo "not ok ${counter} - $1"
    counter=$((counter + 1))
}

print_ok ()
{
    echo "ok ${counter} - $1"
    counter=$((counter + 1))
}

tests=""
n_tests=0
add_test ()
{
    tests="$tests $1"
    n=${2:-1}
    n_tests=$((n_tests + n))
}

print_version ()
{
    echo "TAP version 13"
    if [ "x${BASH}" = "x" ]; then
        echo '1..0 # SKIP bash needed for this test'
        exit 0
    fi
}

print_diagnostic ()
{
    echo "# $*"
}

print_diagnostic_file ()
{
    print_diagnostic "File $1"
    print_diagnostic '---------------------'
    cat "$1" | $SED -e 's,^,# ,' "$1"
    print_diagnostic '---------------------'
}

run_tests ()
{
    echo "1..${n_tests}"
    counter=1
    # Execute test functions
    for test in ${tests}; do
        $test
    done
}

# Local Variables:
# mode: sh
# End:
