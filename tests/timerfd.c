/* timerfd.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/clock/timer-provider.h"

#include <assert.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>

static bool clock_getres_called = false;
static struct timespec clock_getres_value;
static int clock_getres_return = 0;

int
clock_getres (clockid_t c, struct timespec *t)
{
  (void) c;
  assert (!clock_getres_called);
  clock_getres_called = true;
  assert (t);
  *t = clock_getres_value;
  return clock_getres_return;
}

static bool timerfd_create_called = false;
static clockid_t timerfd_create_clock;
static int timerfd_create_flags;
static int timerfd_create_return;

int
timerfd_create (clockid_t c, int f)
{
  assert (!timerfd_create_called);
  timerfd_create_called = true;
  timerfd_create_clock = c;
  timerfd_create_flags = f;
  timerfd_create_return = open ("/dev/zero", S_IRUSR);
  return timerfd_create_return;
}

static bool timerfd_gettime_called = false;
static struct itimerspec timerfd_gettime_value;
static int timerfd_gettime_return = 0;

int
timerfd_gettime (int t, struct itimerspec *restrict c)
{
  assert (t == timerfd_create_return);
  assert (!timerfd_gettime_called);
  timerfd_gettime_called = true;
  assert (c);
  *c = timerfd_gettime_value;
  return timerfd_gettime_return;
}

static bool timerfd_settime_called = false;
static int timerfd_settime_flags;
static struct itimerspec timerfd_settime_value;
static struct itimerspec timerfd_settime_old_value;
static int timerfd_settime_return = 0;

int
timerfd_settime (int t, int f, const struct itimerspec *restrict c,
                 struct itimerspec *restrict old)
{
  (void) f;
  assert (t == timerfd_create_return);
  assert (!timerfd_settime_called);
  timerfd_settime_called = true;
  timerfd_settime_flags = f;
  assert (c);
  timerfd_settime_value = *c;
  if (old)
    *old = timerfd_settime_old_value;
  return timerfd_settime_return;
}

static clockid_t
mbclock_to_clockid (micro_benchmark_clock_type type)
{
  switch (type)
    {
    default:
      assert (0);

    case MICRO_BENCHMARK_CLOCK_MONOTONIC:
      return CLOCK_MONOTONIC;

#ifdef HAVE_CLOCK_PROCESS_CPUTIME_ID
    case MICRO_BENCHMARK_CLOCK_PROCESS:
      return CLOCK_PROCESS_CPUTIME_ID;
#endif

#ifdef HAVE_CLOCK_THREAD_CPUTIME_ID
    case MICRO_BENCHMARK_CLOCK_THREAD:
      return CLOCK_THREAD_CPUTIME_ID;
#endif

    case MICRO_BENCHMARK_CLOCK_REALTIME:
#ifdef HAVE_CLOCK_TAI
      return CLOCK_TAI;
#else
      return CLOCK_REALTIME;
#endif
    }
}

void
test_clock (micro_benchmark_clock_type type)
{
  static const struct itimerspec zero;
  struct timespec res = { 0, 100 };
  clock_getres_value = res;
  micro_benchmark_timer_provider prov =
    MICRO_BENCHMARK_TIMER_PROVIDER_TIMERFD;
  micro_benchmark_timer timer = micro_benchmark_timer_create (type, prov);
  assert (timer);
  assert (timerfd_create_called);
  timerfd_create_called = false;
  assert (timerfd_create_clock == mbclock_to_clockid (type));
  assert (clock_getres_called);
  clock_getres_called = false;
  assert (!timerfd_gettime_called);
  assert (!timerfd_settime_called);

  micro_benchmark_clock_time r = micro_benchmark_timer_get_resolution (timer);
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_gettime_called);
  assert (!timerfd_settime_called);
  assert (r.seconds == 0);
  assert (r.nanoseconds == 100);

  const char *name = micro_benchmark_timer_get_name (timer);
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_gettime_called);
  assert (!timerfd_settime_called);
  assert (strcmp (name, "timerfd") == 0);

  timerfd_settime_old_value = zero;
  micro_benchmark_clock_time deadline = { 5, 1 };
  micro_benchmark_timer_start (timer, deadline);
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_gettime_called);
  assert (timerfd_settime_called);
  timerfd_settime_called = false;
  assert (timerfd_settime_value.it_value.tv_sec == 5);
  assert (timerfd_settime_value.it_value.tv_nsec == 1);
  assert (timerfd_settime_value.it_interval.tv_sec == 0);
  assert (timerfd_settime_value.it_interval.tv_nsec == 0);

  timerfd_gettime_value = timerfd_settime_value;
  assert (micro_benchmark_timer_is_running (timer));
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_settime_called);
  assert (timerfd_gettime_called);
  timerfd_gettime_called = false;

  struct timespec ret1;
  ret1.tv_sec = 1;
  ret1.tv_nsec = 100000000;
  struct itimerspec iret1 = { 0 };
  iret1.it_value = ret1;
  timerfd_settime_old_value = iret1;
  micro_benchmark_timer_stop (timer);
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (timerfd_settime_called);
  timerfd_settime_called = false;
  assert (!timerfd_gettime_called);
  assert (timerfd_settime_value.it_value.tv_sec == 0);
  assert (timerfd_settime_value.it_value.tv_nsec == 0);
  assert (timerfd_settime_value.it_interval.tv_sec == 0);
  assert (timerfd_settime_value.it_interval.tv_nsec == 0);

  timerfd_gettime_value = zero;
  assert (micro_benchmark_timer_is_running (timer));
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_settime_called);
  assert (timerfd_gettime_called);
  timerfd_gettime_called = false;

  timerfd_settime_old_value = zero;
  micro_benchmark_timer_restart (timer);
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_gettime_called);
  assert (timerfd_settime_called);
  timerfd_settime_called = false;
  assert (timerfd_settime_value.it_value.tv_sec == ret1.tv_sec);
  assert (timerfd_settime_value.it_value.tv_nsec == ret1.tv_nsec);
  assert (timerfd_settime_value.it_interval.tv_sec == 0);
  assert (timerfd_settime_value.it_interval.tv_nsec == 0);

  timerfd_gettime_value = zero;
  assert (!micro_benchmark_timer_is_running (timer));
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_settime_called);
  assert (timerfd_gettime_called);
  timerfd_gettime_called = false;

  timerfd_settime_old_value = zero;
  micro_benchmark_timer_stop (timer);
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (timerfd_settime_called);
  timerfd_settime_called = false;
  assert (timerfd_settime_value.it_value.tv_sec == 0);
  assert (timerfd_settime_value.it_value.tv_nsec == 0);
  assert (timerfd_settime_value.it_interval.tv_sec == 0);
  assert (timerfd_settime_value.it_interval.tv_nsec == 0);
  assert (!timerfd_gettime_called);

  timerfd_gettime_value = zero;
  assert (!micro_benchmark_timer_is_running (timer));
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_settime_called);
  assert (timerfd_gettime_called);
  timerfd_gettime_called = false;

  micro_benchmark_clock_time elapsed = micro_benchmark_timer_elapsed (timer);
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_settime_called);
  assert (!timerfd_gettime_called);
  assert (elapsed.seconds == 5);
  assert (elapsed.nanoseconds == 1);

  micro_benchmark_timer_release (timer);
  assert (!timerfd_create_called);
  assert (!clock_getres_called);
  assert (!timerfd_gettime_called);
  assert (!timerfd_settime_called);
}

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;
  test_clock (MICRO_BENCHMARK_CLOCK_REALTIME);
  test_clock (MICRO_BENCHMARK_CLOCK_MONOTONIC);
#ifdef HAVE_CLOCK_PROCESS_CPUTIME_ID
  test_clock (MICRO_BENCHMARK_CLOCK_PROCESS);
#endif
#ifdef HAVE_CLOCK_THREAD_CPUTIME_ID
  test_clock (MICRO_BENCHMARK_CLOCK_THREAD);
#endif
  return 0;
}
