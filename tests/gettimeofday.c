/* gettimeofday.c --- MicroBenchmark library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include "mbenchmark/clock/chrono-provider.h"

#include <assert.h>
#include <string.h>
#include <sys/time.h>

static bool gettimeofday_called = false;
static struct timeval gettimeofday_value;
static int gettimeofday_return = 0;

int
gettimeofday (struct timeval *restrict t, void *restrict tz)
{
  assert (!gettimeofday_called);
  gettimeofday_called = true;
  assert (!tz);
  assert (t);
  *t = gettimeofday_value;
  return gettimeofday_return;
}

int
main (int argc, char **argv)
{
  (void) argc;
  (void) argv;
  gettimeofday_called = false;
  micro_benchmark_clock_type type = MICRO_BENCHMARK_CLOCK_REALTIME;
  micro_benchmark_chronometer_provider prov =
    MICRO_BENCHMARK_CHRONO_PROVIDER_GETTIMEOFDAY;
  micro_benchmark_meter t = micro_benchmark_chronometer_create (type, prov);
  assert (t);
  assert (!gettimeofday_called);

  const char *name = micro_benchmark_stats_meter_get_name (t);
  assert (strcmp (name, "gettimeofday") == 0);
  micro_benchmark_stats_sample_type stype =
    micro_benchmark_stats_meter_get_sample_type (t);
  assert (stype == MICRO_BENCHMARK_SAMPLE_TIME);
  assert (!gettimeofday_called);

  micro_benchmark_clock_time res =
    micro_benchmark_stats_meter_get_min_resolution (t).time;
  assert (!gettimeofday_called);
  assert (res.seconds == 0);
  assert (res.nanoseconds == 1000);

  res = micro_benchmark_stats_meter_get_max_resolution (t).time;
  assert (!gettimeofday_called);
  assert (res.seconds > 1);
  assert (res.nanoseconds == 0);

  struct timeval t1 = { 0, 999999 };
  gettimeofday_value = t1;
  micro_benchmark_stats_meter_start (t);
  assert (gettimeofday_called);
  gettimeofday_called = false;
  struct timeval t2 = { 2, 0 };
  gettimeofday_value = t2;
  micro_benchmark_stats_meter_stop (t);
  assert (gettimeofday_called);
  gettimeofday_called = false;
  struct timeval t3 = { 20, 1 };
  gettimeofday_value = t3;
  micro_benchmark_stats_meter_restart (t);
  assert (gettimeofday_called);
  gettimeofday_called = false;
  struct timeval t4 = { 21, 0 };
  gettimeofday_value = t4;
  micro_benchmark_stats_meter_stop (t);
  assert (gettimeofday_called);
  gettimeofday_called = false;
  micro_benchmark_clock_time v =
    micro_benchmark_stats_meter_get_sample (t).time;
  assert (!gettimeofday_called);
  assert (v.seconds == 2);
  assert (v.nanoseconds == 0);

  micro_benchmark_chronometer_release (t);
  assert (!gettimeofday_called);
  return 0;
}
