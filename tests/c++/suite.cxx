/* suite.cxx --- MicroBenchmark C++ library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include <mbenchmark/all.hpp>

#include <cassert>
#include <cstddef>
#include <string>
#include <vector>

static bool static_executed = false;

static void
constraints (micro_benchmark::test_case& tc)
{
  std::vector<std::size_t> v{1, 2};
  tc.add_dimension (v);
  assert (tc.number_of_dimensions () == 1);
  assert (tc.get_dimension (0) == v);

  tc.limit_iterations (1, 1);
  assert (tc.max_iterations () == 1);
  assert (tc.min_iterations () == 1);

  tc.limit_samples (1, 1);
  assert (tc.max_sample_iterations () == 1);
  assert (tc.min_sample_iterations () == 1);
}

static void
static_test (std::vector<std::size_t> const&)
{
  static_executed = true;
}

static bool runtime_executed = false;

static void
runtime_test (std::vector<std::size_t> const&)
{
  runtime_executed = true;
}

static auto r =
  micro_benchmark::register_test (micro_benchmark::with_constraints, "static",
                                  constraints, static_test);

class test
{
  static bool setup_run;
  static bool constraints_run;
  static bool teardown_run;
  static bool test_run;
public:
  static void
  check ()
  {
    assert (setup_run);
    assert (test_run);
    assert (teardown_run);
    assert (constraints_run);
    setup_run = false;
    teardown_run = false;
    test_run = false;
    constraints_run = false;
  }

  static void
  ready (bool should_cons)
  {
    assert (!setup_run);
    assert (!test_run);
    assert (!teardown_run);
    if (should_cons)
      assert (constraints_run);
    else
      assert (!constraints_run);
  }

  template <typename T>
  static void
  constraints (T&& t)
  {
    constraints_run = true;
    ::constraints (t);
  }

  template <typename... T>
  int
  set_up (T&&...)
  {
    setup_run = true;
    return 1;
  }

  template <typename... T>
  void
  tear_down (T&&...)
  {
    teardown_run = true;
  }

  template <typename... T>
  void
  operator() (T&&...)
  {
    test_run = true;
  }
};

bool test::setup_run = false;
bool test::teardown_run = false;
bool test::test_run = false;
bool test::constraints_run = false;

static void
test_runtime ()
{
  test::ready (false);
  std::string name{"runtime"};
  micro_benchmark::suite s(name);
  assert (s.name () == name);

  std::string test_name{"test"};
  s.register_test (micro_benchmark::with_constraints, test_name,
                   constraints, runtime_test);
  auto tests = s.tests ();
  assert (tests.size () == 1);

  assert (!static_executed);
  assert (!runtime_executed);
  s.run ();
  assert (!static_executed);
  assert (runtime_executed);
  runtime_executed = false;
  test::ready (false);
}

static void
test_main (int argc, char *argv[])
{
  test::ready (false);
  assert (!static_executed);
  assert (!runtime_executed);
  micro_benchmark::main (argc, argv);
  assert (static_executed);
  assert (!runtime_executed);
  test::ready (false);
}

static void
test_class ()
{
  test::ready (false);
  assert (!static_executed);
  assert (!runtime_executed);
  micro_benchmark::suite s("rclass");

  s.register_class ("test", test{});
  auto tests = s.tests ();
  assert (tests.size () == 1);

  test::ready (true);
  assert (!static_executed);
  assert (!runtime_executed);
  s.run ();
  assert (!static_executed);
  assert (!runtime_executed);
  test::check ();
}

int
main (int argc, char *argv[])
{
  test_runtime ();
  test_class ();
  test_main (argc, argv);
  return 0;
}
