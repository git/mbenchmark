/* print-helper.cxx --- MicroBenchmark C++ library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include <mbenchmark/all.hpp>

#include <cassert>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <thread>

namespace
{
  using micro_benchmark::suite;
  using micro_benchmark::with_constraints;
  using micro_benchmark::test_case;
  using micro_benchmark::io::output_type;
  using micro_benchmark::io::output_values;
  using micro_benchmark::io::output_stat;

  constexpr output_values all_values = {
    true, true, true, true, true, true, true, true,
    output_stat::all, output_stat::all, output_stat::all
  };
}

int
main (int argc, char* argv[])
{
  assert (argc == 8);

  /*   Default output values.  */
  auto def = micro_benchmark::io::default_output_values ();

  /*  Test parameters.  */
  char const* suite_name = argv[1];
  char const* test_name = argv[2];
  int skip = std::atoi (argv[3]);
  int iterations = std::atoi (argv[4]);
  int sample_it = std::atoi (argv[5]);
  std::chrono::microseconds to_sleep (std::atoi (argv[6]));
  int option = std::atoi (argv[7]);

  /*  Test constraints.  */
  auto constraints = [=] (test_case& tc)
  {
    tc.skip_iterations (skip);
    tc.limit_iterations (iterations, iterations);
    tc.limit_samples (sample_it, sample_it);
  };

  /*  Test definition.  */
  auto test = [=] (auto&&)
  {
    std::this_thread::sleep_for (to_sleep);
  };

  suite s(suite_name);
  s.register_test (with_constraints, test_name, constraints, test);
  s.run ();
  auto report = s.stored_report ();
  switch (option)
    {
    default:
      return EXIT_FAILURE;

      /* Test printing default values.  */
    case 1:
      report.print ();
      report.print (stdout);
      report.print (std::cout);
      report.print (output_type::console);
      report.print (stdout, output_type::console);
      report.print (std::cout, output_type::console);
      report.print (def);
      report.print (stdout, def);
      report.print (std::cout, def);
      report.print (stdout, output_type::console, def);
      report.print (std::cout, output_type::console, def);
      std::cout << report;
      std::cout << report.with_args (output_type::console);
      std::cout << report.with_args (output_type::console, def);
      break;

      /* Test printing default values, lisp format.  */
    case 2:
      report.print (output_type::lisp);
      report.print (stdout, output_type::lisp);
      report.print (std::cout, output_type::lisp);
      report.print (stdout, output_type::lisp, def);
      report.print (std::cout, output_type::lisp, def);
      std::cout << report.with_args (output_type::lisp);
      std::cout << report.with_args (output_type::lisp, def);
      break;


      /* Test printing default values, text format.  */
    case 3:
      report.print (output_type::text);
      report.print (stdout, output_type::text);
      report.print (std::cout, output_type::text);
      report.print (stdout, output_type::text, def);
      report.print (std::cout, output_type::text, def);
      std::cout << report.with_args (output_type::text);
      std::cout << report.with_args (output_type::text, def);
      break;


      /* Test printing all values, default format.  */
    case 4:
      micro_benchmark::io::default_output_values (all_values);

      report.print ();
      report.print (stdout);
      report.print (std::cout);
      report.print (output_type::console);
      report.print (stdout, output_type::console);
      report.print (std::cout, output_type::console);
      report.print (all_values);
      report.print (stdout, all_values);
      report.print (std::cout, all_values);
      report.print (stdout, output_type::console, all_values);
      report.print (std::cout, output_type::console, all_values);
      std::cout << report;
      std::cout << report.with_args (output_type::console);
      std::cout << report.with_args (output_type::console, all_values);
      break;


      /* Test printing all values, lisp format.  */
    case 5:
      micro_benchmark::io::default_output_values (all_values);

      report.print (output_type::lisp);
      report.print (stdout, output_type::lisp);
      report.print (std::cout, output_type::lisp);
      report.print (stdout, output_type::lisp, all_values);
      report.print (std::cout, output_type::lisp, all_values);
      std::cout << report.with_args (output_type::lisp);
      std::cout << report.with_args (output_type::lisp, all_values);
      break;


      /* Test printing all values, text format.  */
    case 6:
      micro_benchmark::io::default_output_values (all_values);

      report.print (output_type::text);
      report.print (stdout, output_type::text);
      report.print (std::cout, output_type::text);
      report.print (stdout, output_type::text, all_values);
      report.print (std::cout, output_type::text, all_values);
      std::cout << report.with_args (output_type::text);
      std::cout << report.with_args (output_type::text, all_values);
      break;
    }
  return EXIT_SUCCESS;
}
