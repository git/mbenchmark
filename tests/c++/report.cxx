/* report.cxx --- MicroBenchmark C++ library test suite. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/*  Ensure assert is available.  */
#ifdef NDEBUG
#undef NDEBUG
#endif

#include <mbenchmark/all.hpp>

#include <cassert>
#include <cstddef>
#include <iterator>
#include <string>
#include <vector>

static void
constraints (micro_benchmark::test_case& tc)
{
  std::vector<std::size_t> v{1, 2};
  tc.add_dimension (v);
  assert (tc.number_of_dimensions () == 1);
  assert (tc.get_dimension (0) == v);

  tc.skip_iterations (0);

  tc.limit_iterations (1, 1);
  assert (tc.max_iterations () == 1);
  assert (tc.min_iterations () == 1);

  tc.limit_samples (1, 1);
  assert (tc.max_sample_iterations () == 1);
  assert (tc.min_sample_iterations () == 1);
}

static void
test (std::vector<std::size_t> const&)
{
}

static void
test_report_range ()
{
  using micro_benchmark::suite;
  using micro_benchmark::exec_report;
  using micro_benchmark::with_constraints;

  std::string const name{"report-test"};
  suite s{name.c_str ()};
  std::string const test_name{"test"};
  s.register_test (with_constraints, test_name, constraints, test);
  s.run ();

  auto r = s.stored_report ();
  assert (r.size () == 2);
  assert (r.name () == name);

  auto&& tv = *r.rbegin ();
  assert (tv.name () == test_name);
  assert (std::distance (tv.begin (), tv.end ()) == 2);
  for (auto&& e: tv)
    {
      assert (e.iterations () == 1);
      assert (e.total_iterations () == 1);
      assert (e.total_samples () == 1);
      assert (e.used_samples () == 1);
      /* TODO:  Compare more values.  */
    }

  static constexpr auto self_execs = 6;
  static constexpr auto expected_execs = self_execs + 2;
  auto v = r.executions ();

  std::vector<exec_report const*> ids;
  for (auto&& e: v)
    ids.push_back (&e);
  assert (ids.size () == expected_execs);

  auto it = ids.rbegin ();
  auto rv = r.reverse_executions ();
  for (auto&& e: rv)
    {
      assert (it != ids.rend ());
      assert (&e == *it);
      ++it;
    }
}

int
main (int, char **)
{
  test_report_range ();
  return 0;
}
