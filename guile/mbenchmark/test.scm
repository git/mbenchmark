;;; test.scm --- MicroBenchmark library.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Test case definition.
;;
;;; Code:
(define-module (mbenchmark test)
  #:use-module (mbenchmark c test)
  #:use-module (mbenchmark suite)
  #:use-module (mbenchmark state)
  #:use-module (srfi srfi-9)
  #:export (register-test!
            add-test!
            test-case?

            test-set-constraints!
            test-add-dimension!
            test-dimensions
            test-skip-iterations!
            test-iterations-to-skip
            test-limit-iterations!
            test-min-iterations
            test-max-iterations
            test-limit-samples!
            test-max-sample-iterations
            test-min-sample-iterations
            test-set-max-time!
            test-max-time))

(define-record-type <test-case>
  (make-test-case suite test)
  test-case?
  (suite test-case-suite)
  (test test-case-c-object))

(define (default-set-up state) (state-sizes state))
(define (default-tear-down . args) #t)

;; Dimensions doesn't *set* dimensions, but adds them.
(define (set-constraints-int test dimensions skip-iterations
                             min-iterations max-iterations
                             min-sample-iterations max-sample-iterations
                             max-time)
  (when dimensions
    (for-each (lambda (sizes)
                (add-dimension!/internal test sizes))
              dimensions))

  (when skip-iterations
    (skip-iterations!/internal test skip-iterations))

  (when (or min-iterations max-iterations)
    (let ((min (or min-iterations 0))
          (max (or max-iterations 0)))
      (limit-iterations!/internal test min max)))

  (when (or min-sample-iterations max-sample-iterations)
    (let ((min (or min-sample-iterations 0))
          (max (or max-sample-iterations 0)))
      (limit-samples!/internal test min max)))

  (when max-time
    (if (pair? max-time)
        (set-max-time!/internal test (car max-time) (cdr max-time))
        (set-max-time!/internal test max-time 0))))


(define* (test-set-constraints! test #:key skip-iterations!
                                min-iterations max-iterations
                                min-sample-iterations max-sample-iterations
                                max-time)
  (set-constraints-int (test-case-c-object test) #f skip-iterations!
                       min-iterations max-iterations
                       min-sample-iterations max-sample-iterations
                       max-time))

(define (make-test-function f direct)
  (define (auto-function state args)
    (when (keep-running? state)
      (apply f args)
      (auto-function state args)))
  (define (directed-function state args)
    (apply f state args))
  (if direct directed-function auto-function))

(define* (register-test! name #:key test direct dimensions skip-iterations!
                         min-iterations max-iterations
                         min-sample-iterations max-sample-iterations
                         max-time set-up tear-down)

  (define (apply-constraints tc)
    (set-constraints-int tc dimensions skip-iterations!
                         min-iterations max-iterations
                         min-sample-iterations max-sample-iterations
                         max-time))

  (register-test!/internal name
                           (make-test-function test direct)
                           (or set-up default-set-up)
                           (or tear-down default-tear-down))
  (register-constraint!/internal name apply-constraints))

(define* (add-test! suite name #:key test direct dimensions skip-iterations!
                    min-iterations max-iterations
                    max-sample-iterations min-sample-iterations
                    max-time set-up tear-down)
  (let ((c-test (add-test!/internal suite name
                                    (make-test-function test direct)
                                    (or set-up default-set-up)
                                    (or tear-down default-tear-down))))
    (set-constraints-int c-test dimensions skip-iterations!
                         min-iterations max-iterations
                         min-sample-iterations max-sample-iterations
                         max-time)
    (make-test-case suite c-test)))

(define (test-add-dimension! test sizes)
  (add-dimension!/internal (test-case-c-object test) sizes))

(define (test-dimensions test)
  (dimensions/internal (test-case-c-object test)))

(define (test-skip-iterations! test it)
  (skip-iterations!/internal (test-case-c-object test) it))

(define (test-iterations-to-skip test)
  (iterations-to-skip/internal (test-case-c-object test)))

(define* (test-limit-iterations! test #:key (min 0) (max 0))
  (limit-iterations!/internal (test-case-c-object test) min max))

(define (test-max-iterations test)
  (max-iterations/internal (test-case-c-object test)))

(define (test-min-iterations test)
  (min-iterations/internal (test-case-c-object test)))

(define* (test-limit-samples! test #:key (min 0) (max 0))
  (limit-iterations!/internal (test-case-c-object test) min max))

(define (test-max-sample-iterations test)
  (max-sample-iterations/internal (test-case-c-object test)))

(define (test-min-sample-iterations test)
  (min-sample-iterations/internal (test-case-c-object test)))

(define (set-test-max-time! test time)
  (if (pair? time)
      (set-max-time!/internal (test-case-c-object test) (car time) (cdr time))
      (set-max-time!/internal (test-case-c-object test) time 0)))

(define (test-max-time test)
  (max-time/internal (test-case-c-object test)))
