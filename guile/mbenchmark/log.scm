;;; log.scm --- MicroBenchmark library.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Library logging.
;;
;;; Code:
(define-module (mbenchmark log)
  #:use-module (mbenchmark c log)
  #:use-module (mbenchmark exceptions)
  #:export (set-log-level!
            set-module-log-level!))

(define (set-log-level! level)
  (cond ((eq? level 'error) (set-log-level!/internal log/error))
        ((eq? level 'warn) (set-log-level!/internal log/warn))
        ((eq? level 'info) (set-log-level!/internal log/info))
        ((eq? level 'debug) (set-log-level!/internal log/debug))
        ((eq? level 'trace) (set-log-level!/internal log/trace))
        (else (raise-exception-with-message "Unknown log level"))))

(define (set-module-log-level! module level)
  (cond ((eq? level 'error) (set-module-log-level!/internal module log/error))
        ((eq? level 'warn) (set-module-log-level!/internal module log/warn))
        ((eq? level 'info) (set-module-log-level!/internal module log/info))
        ((eq? level 'debug) (set-module-log-level!/internal module log/debug))
        ((eq? level 'trace) (set-module-log-level!/internal module log/trace))
        (else (raise-exception-with-message "Unknown log level"))))
