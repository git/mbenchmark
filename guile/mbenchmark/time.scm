;;; time.scm --- MicroBenchmark library.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Time representation.
;;
;;; Code:
(define-module (mbenchmark time)
  #:use-module (srfi srfi-9)
  #:export (<elapsed-time>
            make-elapsed-time
            elapsed-time?
            elapsed-time-seconds
            elapsed-time-nanoseconds

            <time-sample>
            make-time-sample
            time-sample?
            time-sample-discarded?
            time-sample-iterations
            time-sample-elapsed-time))

(define-record-type <elapsed-time>
  (make-elapsed-time seconds nanoseconds)
  elapsed-time?
  (seconds elapsed-time-seconds)
  (nanoseconds elapsed-time-nanoseconds))

(define-record-type <time-sample>
  (make-time-sample discarded iterations elapsed-time)
  time-sample?
  (discarded time-sample-discarded?)
  (iterations time-sample-iterations)
  (elapsed-time time-sample-elapsed-time))
