;;; suite.scm --- MicroBenchmark library.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Test Suite, collection of Test Cases.
;;
;;; Code:
(define-module (mbenchmark suite)
  #:use-module (mbenchmark c suite)
  #:use-module (mbenchmark report)
  #:use-module (mbenchmark exceptions)
  #:export (suite?)
  #:re-export (get-report
               make-suite
               suite-name
               suite-number-of-tests
               run-suite!))

(define (suite? suite)
  (ignore-exceptions
   (lambda ()
     (suite?/internal suite))))
