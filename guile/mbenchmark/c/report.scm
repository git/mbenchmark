;;; report.scm --- MicroBenchmark library.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Internal module, functions exported from C.
;;
;;; Code:
(define-module (mbenchmark c report)
  #:use-module (mbenchmark c config)
  #:export (output/console
            output/lisp
            output/text

            stats/mean
            stats/variance
            stats/std-deviation
            stats/basic
            stats/all

            values/self-test
            values/size-constraints
            values/total-time
            values/total-iterations
            values/iterations
            values/total-samples
            values/samples
            values/extra-data
            values/iteration-time
            values/sample-time
            values/sample-iterations

            units/none
            units/iteration
            units/min
            units/s
            units/ms
            units/us
            units/ns

            get-report/internal
            report-name/internal
            exec-report-name/internal
            print-report/internal
            for-each-report/internal
            fold-reports/internal

            exec-report-sizes/internal
            exec-report-time-samples/internal
            exec-report-iterations/internal
            exec-report-total-iterations/internal
            exec-report-total-time/internal
            exec-report-total-samples/internal
            exec-report-used-samples/internal
            exec-report-iteration-time/internal
            exec-report-sample-time/internal
            exec-report-sample-iterations/internal
            exec-report-extra-data/internal))

(load-from-lib "report")
