;;; report.scm --- MicroBenchmark library.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; TODO: Exec report access
;;
;;; Code:
(define-module (mbenchmark report)
  #:use-module (mbenchmark c report)
  #:use-module (mbenchmark time)
  #:use-module (mbenchmark stats)
  #:use-module (srfi srfi-9)
  #:export (get-report
            report?
            report-name
            print-report
            for-each-report
            fold-reports

            exec-report?
            exec-report-name
            exec-report-test-name
            exec-report-sizes
            exec-report-time-samples
            exec-report-iterations
            exec-report-total-iterations
            exec-report-total-time
            exec-report-total-samples
            exec-report-used-samples
            exec-report-iteration-time
            exec-report-sample-time
            exec-report-sample-iterations
            exec-report-extra-data)
  #:re-export (output/console
               output/lisp
               output/text

               stats/mean
               stats/variance
               stats/std-deviation
               stats/basic
               stats/all

               elapsed-time?
               elapsed-time-seconds
               elapsed-time-nanoseconds

               time-sample?
               time-sample-discarded?
               time-sample-iterations
               time-sample-elapsed-time

               stat-value?
               stat-value-unit
               stat-value-mean
               stat-value-std-deviation
               stat-value-variance))

;; We have to keep the suite around, as the memory is managed by it.
(define-record-type <report>
  (make-report/record suite report)
  report?
  (suite report-suite)
  (report report-c-object))

(define (report-name report)
  (report-name/internal (report-c-object report)))

(define (get-report suite)
  (make-report/record suite (get-report/internal suite)))

(define* (print-report report #:key
                       (type 'c-default)
                       (port 'c-default)
                       (self-test 'c-default)
                       (size-constraints 'c-default)
                       (total-time 'c-default)
                       (total-iterations 'c-default)
                       (samples 'c-default)
                       (extra-data 'c-default)
                       (iteration-time 'c-default)
                       (sample-time 'c-default)
                       (sample-iterations 'c-default))
  (let ((vps `((,values/self-test . ,self-test)
               (,values/size-constraints . ,size-constraints)
               (,values/total-time . ,total-time)
               (,values/total-iterations . ,total-iterations)
               (,values/samples . ,samples)
               (,values/extra-data . ,extra-data)
               (,values/iteration-time . ,iteration-time)
               (,values/sample-time . ,sample-time)
               (,values/sample-iterations . ,sample-iterations)))
        (r (if (report? report) report (get-report report))))
    (print-report/internal (report-c-object r)
                           (if (eq? type 'c-default) #f type)
                           (if (eq? port 'c-default) #f port)
                           (filter (lambda (x)
                                     (not (eq? (cdr x) 'c-default)))
                                   vps))))

(define-record-type <exec-report>
  (make-exec-report/record suite report test-name)
  exec-report?
  (suite exec-report-suite)
  (report exec-report-c-object)
  (test-name exec-report-test-name))

(define (exec-report-name report)
  (exec-report-name/internal (exec-report-c-object report)))

(define* (for-each-report report f #:key (self-test #t))
  (let ((s (report-suite report)))
    (for-each-report/internal (report-c-object report)
                              (lambda (name e)
                                (let ((r (make-exec-report/record s e name)))
                                  (f r)))
                              self-test)))


(define* (fold-reports report f init #:key (self-test #t))
  (let ((s (report-suite report)))
    (fold-reports/internal (report-c-object report)
                           (lambda (name e curr)
                             (let ((r (make-exec-report/record s e name)))
                               (f r curr)))
                           init self-test)))

(define (exec-report-sizes report)
  (exec-report-sizes/internal (exec-report-c-object report)))

(define (exec-report-time-samples report)
  (define (make-sample d i t)
    (make-time-sample d i (make-elapsed-time (car t) (cdr t))))
  (map (lambda (v)
         (apply make-sample v))
       (exec-report-time-samples/internal (exec-report-c-object report))))

(define (exec-report-iterations report)
  (exec-report-iterations/internal (exec-report-c-object report)))

(define (exec-report-total-iterations report)
  (exec-report-total-iterations/internal (exec-report-c-object report)))

(define (exec-report-total-time report)
  (let ((t (exec-report-total-time/internal (exec-report-c-object report))))
    (make-elapsed-time (car t) (cdr t))))

(define (exec-report-total-samples report)
  (exec-report-total-samples/internal (exec-report-c-object report)))

(define (exec-report-used-samples report)
  (exec-report-used-samples/internal (exec-report-c-object report)))

(define (make-time-stat unit mean stdd var)
  (define unit-s
    (cond ((eqv? unit units/min) 'min)
          ((eqv? unit units/s) 's)
          ((eqv? unit units/ms) 'ms)
          ((eqv? unit units/us) 'us)
          ((eqv? unit units/ns) 'ns)
          ((eqv? unit units/none) 'none)
          ;; TODO: Raise exception?
          (else 'not-valid)))
  (make-stat-value unit-s mean stdd var))

(define (exec-report-iteration-time report)
  (let ((v (exec-report-iteration-time/internal (exec-report-c-object report))))
    (apply make-time-stat v)))

(define (exec-report-sample-time report)
  (let ((v (exec-report-sample-time/internal (exec-report-c-object report))))
    (apply make-time-stat v)))

(define (make-iteration-stat unit mean stdd var)
  (define unit-s
    (cond ((eqv? unit units/iteration) 'iterations)
          ((eqv? unit units/none) 'none)
          ;; TODO: Raise exception?
          (else 'not-valid)))
  (make-stat-value unit-s mean stdd var))

(define (exec-report-sample-iterations report)
  (let ((v (exec-report-sample-iterations/internal (exec-report-c-object report))))
    (apply make-iteration-stat v)))

(define (exec-report-extra-data report)
  (exec-report-extra-data/internal (exec-report-c-object report)))
