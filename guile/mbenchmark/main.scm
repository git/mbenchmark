;;; main.scm --- MicroBenchmark library.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Remove first double dash from guile and call main.
;;
;;; Code:
(define-module (mbenchmark main)
  #:use-module (mbenchmark c main)
  #:use-module (mbenchmark exceptions)
  #:export (main))

(define (main args)
  (cond ((null? args)
         (raise-exception-with-message "Missing arguments"))
        ((and (> (length args) 2) (equal? (cadr args) "--"))
         (display "WARNING: Removing leading '--'.  ")
         (display "Check guile --help for options that stop argument processing.")
         (newline)
         (main/internal (cons (car args) (cddr args))))
        (else (main/internal args))))
