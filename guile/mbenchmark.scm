;;; mbenchmark.scm --- MicroBenchmark library.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; All modules combined.
;;
;;; Code:
(define-module (mbenchmark)
  #:use-module (mbenchmark log)
  #:use-module (mbenchmark main)
  #:use-module (mbenchmark report)
  #:use-module (mbenchmark state)
  #:use-module (mbenchmark stats)
  #:use-module (mbenchmark suite)
  #:use-module (mbenchmark test)
  #:use-module (mbenchmark time)
  #:re-export (;; From (mbenchmark main)
               main

               ;; From (mbenchmark log)
               set-log-level!
               set-module-log-level!

               ;; From (mbenchmark suite)
               suite?
               make-suite
               suite-name
               suite-number-of-tests
               run-suite!

               ;; From (mbenchmark test)
               register-test!
               add-test!
               test-case?
               test-set-constraints!
               test-add-dimension!
               test-dimensions
               test-skip-iterations!
               test-iterations-to-skip
               test-limit-iterations!
               test-min-iterations
               test-max-iterations
               test-limit-samples!
               test-max-sample-iterations
               test-min-sample-iterations
               test-set-max-time!
               test-max-time

               ;; From (mbenchmark state)
               state?
               keep-running?
               state-sizes
               state-name
               set-state-name!

               ;; From (mbenchmark time)
               elapsed-time?
               elapsed-time-seconds
               elapsed-time-nanoseconds

               time-sample?
               time-sample-discarded?
               time-sample-iterations
               time-sample-elapsed-time

               ;; From (mbenchmark stats)
               stat-value?
               stat-value-unit
               stat-value-mean
               stat-value-std-deviation
               stat-value-variance

               ;; From (mbenchmark report)
               output/console
               output/lisp
               output/text
               stats/mean
               stats/variance
               stats/std-deviation
               stats/basic
               stats/all

               get-report
               report?
               report-name
               print-report
               for-each-report
               fold-reports

               exec-report?
               exec-report-name
               exec-report-test-name
               exec-report-sizes
               exec-report-time-samples
               exec-report-iterations
               exec-report-total-iterations
               exec-report-total-time
               exec-report-total-samples
               exec-report-used-samples
               exec-report-iteration-time
               exec-report-sample-time
               exec-report-sample-iterations
               exec-report-extra-data))
