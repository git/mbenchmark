/* stats.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_STATS_H
#define MICRO_BENCHMARK_STATS_H

#include "mbenchmark/common.h"
#include "mbenchmark/stats/meter.h"
#include "mbenchmark/stats/values.h"
#include "mbenchmark/stats/sample.h"
#include "mbenchmark/stats/custom.h"
#include "mbenchmark/stats/time-sample.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Get default statistical calculator.
 *
 * \return Function called to process the time samples by default.
 */
micro_benchmark_custom_time_calculator
micro_benchmark_get_default_time_calculator (void);

/**
 * Set default statistical calculator.
 *
 * \param calc Function called to process the time samples by default
 *             or \code{NULL} to restore the default provided by the
 *             library.
 */
void
micro_benchmark_set_default_time_calculator (micro_benchmark_custom_time_calculator);

MICRO_BENCHMARK_END_DECLS
#endif
