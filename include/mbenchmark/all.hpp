/* all.hpp --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_ALL_HPP
#define MICRO_BENCHMARK_ALL_HPP

#include "mbenchmark/c++/version.hpp"
#include "mbenchmark/c++/registry.hpp"
#include "mbenchmark/c++/report.hpp"
#include "mbenchmark/c++/state.hpp"
#include "mbenchmark/c++/suite.hpp"
#include "mbenchmark/c++/test.hpp"
#include "mbenchmark/c++/utility.hpp"

#endif
