/* suite.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_SUITE_H
#define MICRO_BENCHMARK_SUITE_H

#include "mbenchmark/test.h"
#include "mbenchmark/report/suite.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * \brief Internal representation of a benchmark suite.
 */
struct micro_benchmark_suite_;

/**
 * \brief Opaque type for benchmark suites.
 *
 * This type has to be created by micro_benchmark_suite_create and
 * released with micro_benchmark_suite_release.
 */
typedef struct micro_benchmark_suite_ *micro_benchmark_suite;

/**
 * \brief Create a benchmark suite.
 *
 * \parem name Name of the micro benchmark.
 * \return A new micro benchmark.
 */
micro_benchmark_suite micro_benchmark_suite_create (const char *name);

/**
 * \brief Release the test suite.
 *
 * \param suite Value returned by micro_benchmark_suite_create.
 */
void micro_benchmark_suite_release (micro_benchmark_suite suite);

/**
 * \brief The name of the suite.
 *
 * \param suite Benchmark suite.
 * \return The associated name.
 */
const char *micro_benchmark_suite_get_name (micro_benchmark_suite suite);

/**
 * \brief The number of tests on the suite.
 *
 * \param suite Benchmark suite.
 * \return The number of registered tests.
 */
size_t micro_benchmark_suite_get_number_of_tests (micro_benchmark_suite
                                                  suite);

/**
 * \brief Stored test on the suite.
 *
 * \param suite Benchmark suite.
 * \param pos Position of the test.
 * \return The number of registered tests.
 */
micro_benchmark_test_case
micro_benchmark_suite_get_test (micro_benchmark_suite suite, size_t pos);

/**
 * \brief Run the test suite.
 *
 * \param suite Benchmark suite to be launched.
 * \return The number of tests run.
 */
size_t micro_benchmark_suite_run (micro_benchmark_suite suite);

/**
 * \brief Create a new test case.
 *
 * \param suite Benchmark suite.
 * \param name Name for the test case.
 * \param test Definition of the test.
 * \return The internal reference to the test case.
 */
micro_benchmark_test_case
micro_benchmark_suite_register_test (micro_benchmark_suite suite,
                                     const char *name,
                                     micro_benchmark_test test);

/**
 * \brief Data gathered through the execution of the tests.
 *
 * \param suite Benchmark suite.
 * \return Opaque pointer whose lifetime is managed by the suite.
 */
micro_benchmark_report
micro_benchmark_suite_get_report (micro_benchmark_suite suite);

MICRO_BENCHMARK_END_DECLS
#endif
