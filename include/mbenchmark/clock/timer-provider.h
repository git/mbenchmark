/* timer-provider.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CLOCK_TIMER_PROVIDER_H
#define MICRO_BENCHMARK_CLOCK_TIMER_PROVIDER_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/timer.h"
#include "mbenchmark/clock/chrono-provider.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Clock provided implementation. */
typedef enum micro_benchmark_timer_provider
{
  MICRO_BENCHMARK_TIMER_PROVIDER_DEFAULT,
  MICRO_BENCHMARK_TIMER_PROVIDER_ITIMER,
  MICRO_BENCHMARK_TIMER_PROVIDER_TIMER_T,
  MICRO_BENCHMARK_TIMER_PROVIDER_TIMERFD,
  MICRO_BENCHMARK_TIMER_PROVIDER_FROM_METER,
  MICRO_BENCHMARK_TIMER_PROVIDER_USER_PROVIDED = -1
} micro_benchmark_timer_provider;

/**
 * Set the default timer used for the time measures.
 * \param provider Timer used by default.
 */
void
micro_benchmark_timer_set_default_provider (micro_benchmark_timer_provider);


/** Allocate new default timer. */
micro_benchmark_timer micro_benchmark_timer_create_default (void);

/** Allocate new timer. */
micro_benchmark_timer
micro_benchmark_timer_create (micro_benchmark_clock_type,
                              micro_benchmark_timer_provider);

/** Allocate new chronometer adapter. */
micro_benchmark_timer
micro_benchmark_timer_from_meter (micro_benchmark_clock_type,
                                  micro_benchmark_chronometer_provider);

/** Allocate new chronometer adapter with the provided chronometer. */
micro_benchmark_timer
micro_benchmark_timer_from_provided_meter (micro_benchmark_clock_type,
                                           micro_benchmark_meter);

/** Allocate new timer from the template. */
micro_benchmark_timer
micro_benchmark_timer_from_template (micro_benchmark_clock_type,
                                     micro_benchmark_timer);

/** Release allocated timer. */
void micro_benchmark_timer_release (micro_benchmark_timer);


MICRO_BENCHMARK_END_DECLS
#endif
