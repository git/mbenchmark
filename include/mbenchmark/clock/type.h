/* type.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CLOCK_TYPE_H
#define MICRO_BENCHMARK_CLOCK_TYPE_H

#include "mbenchmark/common.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Type of the time measured by the clock. */
typedef enum micro_benchmark_clock_type
{
  /** Wall clock time: beware of leap seconds.  */
  MICRO_BENCHMARK_CLOCK_REALTIME,
  /** Non-decreasing clock time.  */
  MICRO_BENCHMARK_CLOCK_MONOTONIC,
  /** CPU time used by this process.  */
  MICRO_BENCHMARK_CLOCK_PROCESS,
  /** CPU time used by this thread.  */
  MICRO_BENCHMARK_CLOCK_THREAD
} micro_benchmark_clock_type;

MICRO_BENCHMARK_END_DECLS
#endif
