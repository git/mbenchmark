/* timer-inline.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CLOCK_TIMER_INLINE_H
#define MICRO_BENCHMARK_CLOCK_TIMER_INLINE_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/timer.h"

#include <assert.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

MICRO_BENCHMARK_INLINE void
micro_benchmark_timer_init (micro_benchmark_timer_definition *timer,
                            micro_benchmark_clock_type type)
{
  assert (timer);
  assert (timer->init);
  timer->init (&timer->data, type, NULL);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_timer_init_with_extra (micro_benchmark_timer_definition
                                       *timer,
                                       micro_benchmark_clock_type type,
                                       const void *extra)
{
  assert (timer);
  assert (timer->init);
  timer->init (&timer->data, type, extra);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_timer_cleanup (micro_benchmark_timer timer)
{
  assert (timer);
  assert (timer->cleanup);
  timer->cleanup (&timer->data);
}

MICRO_BENCHMARK_INLINE micro_benchmark_clock_type
micro_benchmark_timer_get_type (micro_benchmark_timer timer)
{
  assert (timer);
  return timer->data.type;
}

MICRO_BENCHMARK_INLINE const char *
micro_benchmark_timer_get_name (micro_benchmark_timer timer)
{
  assert (timer);
  return timer->data.name;
}

MICRO_BENCHMARK_INLINE micro_benchmark_clock_time
micro_benchmark_timer_get_resolution (micro_benchmark_timer timer)
{
  assert (timer);
  return timer->data.resolution;
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_timer_start (micro_benchmark_timer timer,
                             micro_benchmark_clock_time time)
{
  assert (timer);
  assert (timer->start);
  timer->start (timer->data.ptr, time);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_timer_stop (micro_benchmark_timer timer)
{
  assert (timer);
  assert (timer->stop);
  timer->stop (timer->data.ptr);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_timer_restart (micro_benchmark_timer timer)
{
  assert (timer);
  assert (timer->restart);
  timer->restart (timer->data.ptr);
}

MICRO_BENCHMARK_INLINE bool
micro_benchmark_timer_is_running (micro_benchmark_timer timer)
{
  assert (timer);
  assert (timer->is_running);
  return timer->is_running (timer->data.ptr);
}

MICRO_BENCHMARK_INLINE micro_benchmark_clock_time
micro_benchmark_timer_elapsed (micro_benchmark_timer timer)
{
  assert (timer);
  assert (timer->elapsed);
  return timer->elapsed (timer->data.ptr);
}

MICRO_BENCHMARK_END_DECLS
#endif
