/* chrono-provider.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CLOCK_CHRONO_PROVIDER_H
#define MICRO_BENCHMARK_CLOCK_CHRONO_PROVIDER_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/type.h"
#include "mbenchmark/stats/meter.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Chronometer implementations. */
typedef enum micro_benchmark_chronometer_provider
{
  /** \see{micro_benchmark_chronometer_set_default}. */
  MICRO_BENCHMARK_CHRONO_PROVIDER_DEFAULT,
  /** \code{time_t} chronomenter. */
  MICRO_BENCHMARK_CHRONO_PROVIDER_TIME_T,
  /** \code{clock_t} chronomenter. */
  MICRO_BENCHMARK_CHRONO_PROVIDER_CLOCK_T,
  /** \code{gettimeofday} chronomenter. */
  MICRO_BENCHMARK_CHRONO_PROVIDER_GETTIMEOFDAY,
  /** \code{clock_gettime} chronomenter. */
  MICRO_BENCHMARK_CHRONO_PROVIDER_CLOCK_GETTIME,
  /** User provided chronomenter. */
  MICRO_BENCHMARK_CHRONO_PROVIDER_USER_PROVIDED = -1
} micro_benchmark_chronometer_provider;

/** Allocate new default chronometer. */
micro_benchmark_meter micro_benchmark_chronometer_create_default (void);

/** Allocate new chronometer. */
micro_benchmark_meter
micro_benchmark_chronometer_create (micro_benchmark_clock_type,
                                    micro_benchmark_chronometer_provider);

/** Allocate new chronometer from a template. */
micro_benchmark_meter
micro_benchmark_chronometer_from_meter (micro_benchmark_clock_type,
                                        micro_benchmark_meter);

/** Release allocated chronometer. */
void micro_benchmark_chronometer_release (micro_benchmark_meter);

/** Use \code{meter} as the default chronometer template. */
void micro_benchmark_chronometer_set_default (micro_benchmark_meter meter);

/** Default chronometer template. */
micro_benchmark_meter micro_benchmark_chronometer_get_default (void);

MICRO_BENCHMARK_END_DECLS
#endif
