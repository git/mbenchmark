/* timer.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CLOCK_TIMER_H
#define MICRO_BENCHMARK_CLOCK_TIMER_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/time.h"
#include "mbenchmark/clock/type.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_timer_data
{
  /** Internal data of the clock implementation.  */
  void *ptr;

  /** Name of the timer.  */
  const char *name;

  /** Type of the timer.  */
  micro_benchmark_clock_type type;

  /** Resolution of the timer clock.  */
  micro_benchmark_clock_time resolution;

} micro_benchmark_timer_data;

/**
 * Initialize the internal data of the meter.
 *
 * \param data this pointer.
 * \param type Type of the clock
 * \param extra Extra data provided to the constructor.
 */
typedef void (*micro_benchmark_timer_init_fun) (micro_benchmark_timer_data *,
                                                micro_benchmark_clock_type,
                                                const void *);

/**
 * Release the internal data of the meter.
 *
 * \param data this pointer.
 */
typedef void
  (*micro_benchmark_timer_cleanup_fun) (const micro_benchmark_timer_data *);

/**
 * Start the timer.
 *
 * \param ptr Pointer stored on data.
 * \param deadline Total amount of time to be running.
 */
typedef void (*micro_benchmark_timer_start_fun) (void *,
                                                 micro_benchmark_clock_time);

/**
 * Stop the timer.
 * \param ptr Pointer stored on data.
 */
typedef void (*micro_benchmark_timer_stop_fun) (void *);

/**
 * Restarts the timer from the .
 * \param ptr Pointer stored on data.
 */
typedef void (*micro_benchmark_timer_restart_fun) (void *);

/**
 * Check if the timer has expired.
 * \param ptr Pointer stored on data.
 * \return true if the timer has not expired yet.
 */
typedef bool (*micro_benchmark_timer_running_fun) (void *);

  /**
   * Total time elapsed.
   * \param ptr Pointer stored on data.
   * \return Timer accumulated while the timer was running.
   */
typedef micro_benchmark_clock_time
  (*micro_benchmark_timer_elapsed_fun) (void *);

typedef struct micro_benchmark_timer_definition
{
  /** Instance data.  */
  micro_benchmark_timer_data data;

  /** Instance initialization.  */
  micro_benchmark_timer_init_fun init;

  /** Instance release.  */
  micro_benchmark_timer_cleanup_fun cleanup;

  /** Start the timer.  */
  micro_benchmark_timer_start_fun start;

  /** Stop the timer.  */
  micro_benchmark_timer_stop_fun stop;

  /** Restart the timer.  */
  micro_benchmark_timer_restart_fun restart;

  /** Check timer expiration.  */
  micro_benchmark_timer_running_fun is_running;

  /** Check elapsed time.  */
  micro_benchmark_timer_elapsed_fun elapsed;

} micro_benchmark_timer_definition;

typedef const micro_benchmark_timer_definition *micro_benchmark_timer;

/**
 * Set the default timer used for the time measures.
 * \param c Timer used by default.
 */
void micro_benchmark_timer_set_default (micro_benchmark_timer c);

/**
 * Default clock.
 * \return Default timer used for measures.
 */
micro_benchmark_timer micro_benchmark_timer_get_default (void);

MICRO_BENCHMARK_END_DECLS
#include "mbenchmark/clock/timer-inline.h"
#endif
