/* state.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_STATE_H
#define MICRO_BENCHMARK_STATE_H

#include "mbenchmark/common.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * \brief Internal representation of a test state.
 */
struct micro_benchmark_test_state_;

/**
 * \brief Opaque type for benchmark test case control.
 */
typedef struct micro_benchmark_test_state_ *micro_benchmark_test_state;

/**
 * \brief The number of dimensions for the provided test.
 *
 * \param state The test case state.
 * \return The number of dimensions provided to the test.
 */
size_t
micro_benchmark_state_get_dimensions (micro_benchmark_test_state state);

/**
 * \brief Test size of the provided dimension.
 *
 * \param state The test case state.
 * \param state The dimension to check.
 * \return The size assigned to the test case for the provided dimension.
 */
size_t
micro_benchmark_state_get_size (micro_benchmark_test_state state,
                                size_t dimension);

/**
 * \brief User provided data.
 *
 * \param state The test case state.
 * \return The pointer returned by the set_up function.
 */
void *micro_benchmark_state_get_data (micro_benchmark_test_state state);

/**
 * \brief Customization of the execution name.
 *
 * \param state The test case state.
 * \param name User provided name for this execution.
 */
void micro_benchmark_state_set_name (micro_benchmark_test_state state,
                                     const char *name);

/**
 * \brief Execution name.
 *
 * \param state The test case state.
 * \return Name of this test execution.
 */
const char *micro_benchmark_state_get_name (micro_benchmark_test_state state);

/**
 * \brief Main driver of the test execution.
 *
 * Performs the the bookkeeping for the benchmark test case and checks
 * the constraints.
 *
 * \param state The test case state.
 * \return false if the collected data seems enough, true otherwise.
 */
bool micro_benchmark_state_keep_running (micro_benchmark_test_state state);

MICRO_BENCHMARK_END_DECLS
#endif
