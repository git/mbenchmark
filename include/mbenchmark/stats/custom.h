/* custom.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_STATS_CUSTOM_H
#define MICRO_BENCHMARK_STATS_CUSTOM_H

#include "mbenchmark/common.h"
#include "mbenchmark/report/extra.h"
#include "mbenchmark/stats/sample.h"
#include "mbenchmark/stats/time-sample.h"
#include "mbenchmark/stats/values.h"

#include <assert.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Allocate internal data of the collector.
 * \return Pointer to the internal data.
 */
typedef void *(*micro_benchmark_custom_sample_create_data_fun) (void);

/**
 * Release the internal data of the collector.
 * \param ptr Pointer to the internal data.
 */
typedef void (*micro_benchmark_custom_sample_release_data_fun) (void *);

/**
 * Parse the collected samples into the internal data.
 * \param ptr Pointer to the internal data.
 * \param samples Array of samples
 * \param size Size of the array.
 */
typedef void
  (*micro_benchmark_custom_sample_collector_fun)
  (void *, size_t, micro_benchmark_stats_generic_samples);

/**
 * Allocate internal data of the collector.
 * \return Pointer to the internal data.
 */
typedef micro_benchmark_stats_sample_type
  (*micro_benchmark_custom_sample_type_fun) (void);

typedef struct micro_benchmark_custom_sample_collector
{
  /** Internal data initialization.  */
  micro_benchmark_custom_sample_create_data_fun create_data;

  /** Internal data release.  */
  micro_benchmark_custom_sample_release_data_fun release_data;

  /** Data collection.  */
  micro_benchmark_custom_sample_collector_fun parse_samples;

  /** Data formatted for output.  */
  micro_benchmark_report_extractor_fun get_data;

  /** Sample parsed by this collector.  */
  micro_benchmark_custom_sample_type_fun sample_type;
} micro_benchmark_custom_sample_collector;

/**
 * Aggregate statistical values from time samples.
 * \param size Size of the array.
 * \param s Array of time samples.
 * \return Aggregated data.
 */
typedef micro_benchmark_time_stats_values
  (*micro_benchmark_custom_time_calculator) (size_t size,
                                             micro_benchmark_time_samples s);

typedef struct micro_benchmark_custom_meter
{
  /** Meter used as template.  */
  micro_benchmark_meter meter;
  /** Extra data provided to the meter.  */
  const void *extra;
  /** Data collection associated to the custom meter.  */
  micro_benchmark_custom_sample_collector collector;
} micro_benchmark_custom_meter;


MICRO_BENCHMARK_END_DECLS
#endif
