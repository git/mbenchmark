/* meter.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_STATS_METER_H
#define MICRO_BENCHMARK_STATS_METER_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/time.h"
#include "mbenchmark/stats/sample.h"

#include <stdint.h>
#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Internal data of a meter.  */
typedef struct micro_benchmark_meter_data
{
  /** Internal data of the instance.  */
  void *ptr;

  /** Name of the meter.  */
  const char *name;

  /** Discriminator for the union returned by the meter.  */
  micro_benchmark_stats_sample_type sample_type;

  /** Minimum resolution of the samples provided.  */
  micro_benchmark_stats_meter_sample min_resolution;

  /** Maximum resolution of the samples provided.  */
  micro_benchmark_stats_meter_sample max_resolution;

} micro_benchmark_meter_data;

/**
 * Initialize the internal data of the meter and the sample_type.
 *
 * \param data Instance data pointer.
 * \param extra_data Extra data provided to the initialization.
 */
typedef void (*micro_benchmark_meter_init_fun) (micro_benchmark_meter_data *,
                                                const void *);

/**
 * Release the internal data of the meter.
 *
 * \param data Instance data pointer.
 */
typedef void
  (*micro_benchmark_meter_cleanup_fun) (const micro_benchmark_meter_data *);

/**
 * Start the measuring process.
 *
 * \param ptr Instance pointer.
 */
typedef void (*micro_benchmark_meter_start_fun) (void *);

/**
 * Stop the measuring process.
 * \param ptr Instance pointer.
 */
typedef void (*micro_benchmark_meter_stop_fun) (void *);

/**
 * Restart the measuring process.
 * \param ptr Instance pointer.
 */
typedef void (*micro_benchmark_meter_restart_fun) (void *);

/**
 * Retrieve the measured data.
 * \param ptr Instance pointer.
 */
typedef micro_benchmark_stats_meter_sample
  (*micro_benchmark_meter_get_sample_fun) (void *);

/** Definition of a generic measure device.  */
typedef struct micro_benchmark_meter_definition
{
  /** Instance data.  */
  micro_benchmark_meter_data data;

  micro_benchmark_meter_init_fun init;

  micro_benchmark_meter_cleanup_fun cleanup;

  micro_benchmark_meter_start_fun start;

  micro_benchmark_meter_stop_fun stop;

  micro_benchmark_meter_restart_fun restart;

  micro_benchmark_meter_get_sample_fun get_sample;

} micro_benchmark_meter_definition;

typedef const micro_benchmark_meter_definition *micro_benchmark_meter;

MICRO_BENCHMARK_END_DECLS
#include "mbenchmark/stats/meter-inline.h"
#endif
