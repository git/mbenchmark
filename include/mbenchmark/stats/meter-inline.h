/* meter-inline.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_STATS_METER_INLINE_H
#define MICRO_BENCHMARK_STATS_METER_INLINE_H

#include "mbenchmark/common.h"
#include "mbenchmark/stats/meter.h"

#include <assert.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

MICRO_BENCHMARK_INLINE void
micro_benchmark_stats_meter_init (micro_benchmark_meter_definition *meter)
{
  assert (meter);
  assert (meter->init);
  meter->init (&meter->data, NULL);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_stats_meter_init_with_data (micro_benchmark_meter_definition
                                            *meter, const void *extra_data)
{
  assert (meter);
  assert (meter->init);
  meter->init (&meter->data, extra_data);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_stats_meter_cleanup (micro_benchmark_meter meter)
{
  assert (meter);
  assert (meter->cleanup);
  meter->cleanup (&meter->data);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_stats_meter_start (micro_benchmark_meter meter)
{
  assert (meter);
  assert (meter->start);
  meter->start (meter->data.ptr);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_stats_meter_stop (micro_benchmark_meter meter)
{
  assert (meter);
  assert (meter->stop);
  meter->stop (meter->data.ptr);
}

MICRO_BENCHMARK_INLINE void
micro_benchmark_stats_meter_restart (micro_benchmark_meter meter)
{
  assert (meter);
  assert (meter->restart);
  meter->restart (meter->data.ptr);
}

MICRO_BENCHMARK_INLINE micro_benchmark_stats_meter_sample
micro_benchmark_stats_meter_get_sample (micro_benchmark_meter meter)
{
  assert (meter);
  assert (meter->get_sample);
  return meter->get_sample (meter->data.ptr);
}

MICRO_BENCHMARK_INLINE const char *
micro_benchmark_stats_meter_get_name (micro_benchmark_meter meter)
{
  assert (meter);
  return meter->data.name;
}

MICRO_BENCHMARK_INLINE micro_benchmark_stats_sample_type
micro_benchmark_stats_meter_get_sample_type (micro_benchmark_meter meter)
{
  assert (meter);
  return meter->data.sample_type;
}

MICRO_BENCHMARK_INLINE micro_benchmark_stats_meter_sample
micro_benchmark_stats_meter_get_min_resolution (micro_benchmark_meter meter)
{
  assert (meter);
  return meter->data.min_resolution;
}

MICRO_BENCHMARK_INLINE micro_benchmark_stats_meter_sample
micro_benchmark_stats_meter_get_max_resolution (micro_benchmark_meter meter)
{
  assert (meter);
  return meter->data.max_resolution;
}

MICRO_BENCHMARK_END_DECLS
#endif
