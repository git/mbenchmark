/* values.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_STATS_VALUES_H
#define MICRO_BENCHMARK_STATS_VALUES_H

#include "mbenchmark/common.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef enum micro_benchmark_stats_unit
{
  MICRO_BENCHMARK_STATS_UNIT_NONE,
  MICRO_BENCHMARK_STATS_ITERATIONS,
  MICRO_BENCHMARK_STATS_TIME_MIN,
  MICRO_BENCHMARK_STATS_TIME_S,
  MICRO_BENCHMARK_STATS_TIME_MS,
  MICRO_BENCHMARK_STATS_TIME_US,
  MICRO_BENCHMARK_STATS_TIME_NS
} micro_benchmark_stats_unit;

typedef struct micro_benchmark_stats_value
{
  micro_benchmark_stats_unit unit;
  double mean;
  double variance;
  double std_deviation;
} micro_benchmark_stats_value;

typedef struct micro_benchmark_time_stats_values
{
  /** Number of samples measured.  */
  size_t total_samples;

  /** Number of samples taken into account.  */
  size_t samples;

  /** Total number of iterations.  */
  size_t iterations;

  /** Statistical values of the time per iteration.  */
  micro_benchmark_stats_value iteration_time;

  /** Statistical values of the time per sample.  */
  micro_benchmark_stats_value sample_time;

  /** Statistical values of the iterations per sample.  */
  micro_benchmark_stats_value sample_iterations;
} micro_benchmark_time_stats_values;

MICRO_BENCHMARK_END_DECLS
#endif
