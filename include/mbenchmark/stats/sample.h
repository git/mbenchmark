/* sample.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_STATS_SAMPLE_H
#define MICRO_BENCHMARK_STATS_SAMPLE_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/time.h"

#include <stdint.h>
#include <stddef.h>

#define MICRO_BENCHMARK_SAMPLE_SB_SIZE sizeof (int64_t)

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Discriminant for the type returned by a measure device. */
typedef enum micro_benchmark_stats_sample_type
{
  /** Integral type.  */
  MICRO_BENCHMARK_SAMPLE_INT64,
  /** Modular arithmetic type.  */
  MICRO_BENCHMARK_SAMPLE_UINT64,
  /** Floating point type.  */
  MICRO_BENCHMARK_SAMPLE_DOUBLE,
  /** Time duration type.  */
  MICRO_BENCHMARK_SAMPLE_TIME,
  /** Small buffer type.  */
  MICRO_BENCHMARK_SAMPLE_SMALL_BUFFER,
  /** TODO: Too opaque, to be really designed  */
  MICRO_BENCHMARK_SAMPLE_USER_PROVIDED,
} micro_benchmark_stats_sample_type;

/**  Type returned by a measure device.  */
typedef union micro_benchmark_stats_meter_sample
{
  /** Integral value.  */
  int64_t integer;
  /** Modular arithmetic value.  */
  uint64_t modular;
  /** Floating point value.  */
  double floating_point;
  /** Time duration.  */
  micro_benchmark_clock_time time;
  /** Small buffer for user provided data.  */
  unsigned char buffer[MICRO_BENCHMARK_SAMPLE_SB_SIZE];
  /** TODO: Too opaque, to be really designed   */
  void *user_provided;
} micro_benchmark_stats_meter_sample;

/** Execution time sample.  */
typedef struct micro_benchmark_stats_generic_sample_data
{
  bool discarded;
  size_t iterations;
  micro_benchmark_stats_meter_sample value;
} micro_benchmark_stats_generic_sample_data;

typedef const micro_benchmark_stats_generic_sample_data
  *micro_benchmark_stats_generic_sample;

typedef const micro_benchmark_stats_generic_sample_data
  *micro_benchmark_stats_generic_samples;

MICRO_BENCHMARK_END_DECLS
#endif
