/* meters.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_TEST_METERS_H
#define MICRO_BENCHMARK_TEST_METERS_H

#include "mbenchmark/common.h"
#include "mbenchmark/test/case.h"
#include "mbenchmark/clock/chrono-provider.h"
#include "mbenchmark/stats/custom.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Chronometer used.
 *
 * \param tc The test case.
 * \param type Clock type.
 * \param p Implementation provider.
 */
void
micro_benchmark_test_case_set_chrono (micro_benchmark_test_case tc,
                                      micro_benchmark_clock_type type,
                                      micro_benchmark_chronometer_provider p);

/**
 * Chronometer used.
 *
 * \param tc The test case.
 * \param type Clock type.
 * \param m Custom chronometer.
 */
void
micro_benchmark_test_case_set_custom_chrono (micro_benchmark_test_case tc,
                                             micro_benchmark_clock_type type,
                                             micro_benchmark_meter m);

/**
 * Set custom statistical calculator.
 *
 * \param tc The test case.
 * \param calculator Function called to process the time samples.
 */
void
micro_benchmark_test_case_set_calculator (micro_benchmark_test_case tc,
                                          micro_benchmark_custom_time_calculator);

/**
 * Add custom meter.
 *
 * \param tc The test case.
 * \param m Additional meter used to collect samples.
 */
void
micro_benchmark_test_case_add_meter (micro_benchmark_test_case tc,
                                     micro_benchmark_custom_meter m);

MICRO_BENCHMARK_END_DECLS
#endif
