/* iterations.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_TEST_ITERATIONS_H
#define MICRO_BENCHMARK_TEST_ITERATIONS_H

#include "mbenchmark/common.h"
#include "mbenchmark/test/case.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Skip the provided iterations before start measuring.
 *
 * \param tc The test case.
 * \param iterations_to_skip Number of iterations not taken into account.
 */
void
micro_benchmark_test_case_skip_iterations (micro_benchmark_test_case tc,
                                           size_t iterations_to_skip);

/**
 * Test limits.
 * \param tc The test case.
 * \return Number of iterations not taken into account.
 */
size_t
micro_benchmark_test_case_iterations_to_skip (micro_benchmark_test_case tc);

/**
 * Limit the total number of iterations performed, not including
 * skipped ones.
 *
 * \param tc The test case.
 * \param min_iterations Minimum number of iterations.
 * \param max_iterations Maximum number of iterations.
 */
void
micro_benchmark_test_case_limit_iterations (micro_benchmark_test_case tc,
                                            size_t min_iterations,
                                            size_t max_iterations);

/**
 * Test limits.
 * \param tc The test case.
 * \return Minimum number of iterations performed.
 */
size_t
micro_benchmark_test_case_min_iterations (micro_benchmark_test_case tc);

/**
 * Test limits.
 * \param tc The test case.
 * \return Maximum number of iterations performed.
 */
size_t
micro_benchmark_test_case_max_iterations (micro_benchmark_test_case tc);

/**
 * Limit the total number of iterations performed on each sample.
 *
 * \param tc The test case.
 * \param min_iterations Minimum number of iterations.
 * \param max_iterations Maximum number of iterations.
 */
void
micro_benchmark_test_case_limit_samples (micro_benchmark_test_case tc,
                                         size_t min_iterations,
                                         size_t max_iterations);

/**
 * Test limits.
 * \param tc The test case.
 * \return Minimum number of iterations per sample.
 */
size_t
micro_benchmark_test_case_min_sample_iterations (micro_benchmark_test_case);

/**
 * Test limits.
 * \param tc The test case.
 * \return Maximum number of iterations per sample.
 */
size_t
micro_benchmark_test_case_max_sample_iterations (micro_benchmark_test_case);

MICRO_BENCHMARK_END_DECLS
#endif
