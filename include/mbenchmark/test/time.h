/* time.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_TEST_TIME_H
#define MICRO_BENCHMARK_TEST_TIME_H

#include "mbenchmark/common.h"
#include "mbenchmark/test/case.h"
#include "mbenchmark/clock/time.h"
#include "mbenchmark/clock/timer-provider.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Maximum execution time of the test code per execution.
 *
 * \param tc The test case.
 * \param d Effective deadline per execution.
 */
void
micro_benchmark_test_case_set_max_time (micro_benchmark_test_case tc,
                                        micro_benchmark_clock_time d);

/**
 * Maximum execution time of the test code per execution.
 *
 * \param tc The test case.
 * \return Value for the test case.
 */
micro_benchmark_clock_time
micro_benchmark_test_case_get_max_time (micro_benchmark_test_case tc);

MICRO_BENCHMARK_END_DECLS
#endif
