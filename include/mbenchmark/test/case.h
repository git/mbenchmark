/* case.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_TEST_CASE_H
#define MICRO_BENCHMARK_TEST_CASE_H

#include "mbenchmark/common.h"
#include "mbenchmark/test/definition.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Internal definition of the registered test case.
 */
struct micro_benchmark_test_case_definition;

typedef struct micro_benchmark_test_case_definition
  micro_benchmark_test_case_definition;

typedef micro_benchmark_test_case_definition *micro_benchmark_test_case;

/**
 * Name of the test case.
 *
 * \param test The test case.
 * \return The name provided at registration time.
 */
const char *micro_benchmark_test_case_get_name (micro_benchmark_test_case);

/**
 * Definition of the test case.
 *
 * \param test The test case.
 * \return Definition used for the test case.
 */
micro_benchmark_test
micro_benchmark_test_case_get_definition (micro_benchmark_test_case test);

/**
 * Activation status of the test case.
 *
 * \param tc The test case.
 * \return \code{true} if the test is enabled.
 */
bool micro_benchmark_test_case_is_enabled (micro_benchmark_test_case tc);

/**
 * Disable or enable the test case.
 *
 * \param tc The test case.
 * \param e false to skip the test.
 */
void micro_benchmark_test_case_set_enabled (micro_benchmark_test_case tc,
                                            bool e);
/**
 * Provide specific data for the test case.
 *
 * \param tc The test case.
 * \param ptr Data pointer.
 * \param cleanup Function called to release the provided data.
 */
void micro_benchmark_test_case_set_data (micro_benchmark_test_case tc,
                                         void *, void (*)(void *));
/**
 * Data provided by micro_benchmark_test_case_set_data.
 *
 * \param tc The test case.
 */
void *micro_benchmark_test_case_get_data (micro_benchmark_test_case tc);

MICRO_BENCHMARK_END_DECLS
#endif
