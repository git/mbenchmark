/* definition.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_TEST_DEFINITION_H
#define MICRO_BENCHMARK_TEST_DEFINITION_H

#include "mbenchmark/common.h"
#include "mbenchmark/state.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef void *(*micro_benchmark_set_up_fun) (micro_benchmark_test_state);

typedef void (*micro_benchmark_tear_down_fun) (micro_benchmark_test_state,
                                               void *);

typedef void (*micro_benchmark_auto_test_fun) (void *);

typedef void (*micro_benchmark_test_fun) (micro_benchmark_test_state);

/**
 * Test definition.
 */
typedef struct micro_benchmark_test_definition
{
  /**  Whether the test is controlled or automatic.  */
  bool is_auto;

  /**  Called before each execution of the test.  */
  micro_benchmark_set_up_fun set_up;

  /**  Called after each execution of the test.  */
  micro_benchmark_tear_down_fun tear_down;

  /**  Called several times to collect meaningful data.  */
  micro_benchmark_auto_test_fun auto_test;

  /**  Controlled execution through the state.  */
  micro_benchmark_test_fun test;
} micro_benchmark_test_definition;

typedef const struct micro_benchmark_test_definition *micro_benchmark_test;

MICRO_BENCHMARK_END_DECLS
#endif
