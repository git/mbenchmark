/* sizes.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_TEST_SIZES_H
#define MICRO_BENCHMARK_TEST_SIZES_H

#include "mbenchmark/common.h"
#include "mbenchmark/test/case.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Add a new dimension to the test with the provided sizes.
 *
 * \param tc The test case.
 * \param num_sizes Number of size points on the added dimension.
 * \param sizes Array of size points for the added dimension.
 */
void
micro_benchmark_test_case_add_dimension (micro_benchmark_test_case tc,
                                         size_t num_sizes,
                                         const size_t *sizes);

/**
 * Test parameters.
 * \param tc The test case.
 * \return Number of dimensions of the input provided to the test.
 */
size_t micro_benchmark_test_case_dimensions (micro_benchmark_test_case tc);

/**
 * Test parameters.
 * \param tc The test case.
 * \param dimension The dimension to check.
 * \param n_sizes Output pointer for the number of sizes on this dimension.
 * \param dimension Output pointer for the sizes on this dimension.
 */
void
micro_benchmark_test_case_get_dimension (micro_benchmark_test_case tc,
                                         size_t dimension,
                                         size_t *n_sizes,
                                         const size_t **sizes);

MICRO_BENCHMARK_END_DECLS
#endif
