/* registry.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_REGISTRY_HPP
#define MICRO_BENCHMARK_CXX_REGISTRY_HPP

#include "mbenchmark/c++/state.hpp"
#include "mbenchmark/c++/test.hpp"
#include "mbenchmark/c++/traits.hpp"

#include <functional>
#include <map>
#include <memory>
#include <type_traits>
#include <string>
#include <tuple>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace registry
  {
    // Registration token.
    class token {};

    struct with_constraints_type {};
    constexpr with_constraints_type with_constraints;

    // Forward declaration
    namespace detail
    {
      using micro_benchmark::detail::default_set_up;
      using micro_benchmark::detail::default_tear_down;
      using micro_benchmark::detail::set_up;
      using micro_benchmark::detail::tear_down;
    }

    template <typename Test,
              typename SetUp = detail::default_set_up const&,
              typename TearDown = detail::default_tear_down const&>
    token
    register_test (char const* name, Test&& t,
                   SetUp&& s = detail::set_up,
                   TearDown&& td = detail::tear_down);

    // Implementation details
    namespace detail
    {
      using micro_benchmark::detail::test_base;
      using micro_benchmark::detail::has_constraints;
      using micro_benchmark::detail::has_set_up;
      using micro_benchmark::detail::has_tear_down;
      using micro_benchmark::detail::set_up_ret_t;
      using micro_benchmark::detail::value_type;
      using micro_benchmark::detail::is_auto_class;

      // Register entry
      struct entry
      {
        std::unique_ptr<test_base> test;
        std::vector<std::function<void(test_case&)>> constraints;
      };

      using map = std::map<std::string, entry>;
      map const& registered_tests ();
      void register_static_test (char const*);

      void add_test (char const*, std::unique_ptr<detail::test_base>);
      void add_constraint (char const*, std::function<void(test_case&)>);

      struct empty_set_up_maker
      {
        template <typename T>
        constexpr auto
        operator() (T&& test) const
        {
          using Test = value_type<T>;
          return [t = Test{std::forward<T> (test)}] (state& s) mutable
          {
            return std::make_tuple (std::addressof (t), s.sizes ());
          };
        }
      };

      struct void_set_up_maker
      {
        template <typename T>
        constexpr auto
        operator() (T&& test) const
        {
          using Test = value_type<T>;
          return [t = Test{std::forward<T> (test)}] (state& s) mutable
          {
            t.set_up (s);
            return std::addressof (t);
          };
        }
      };

      struct non_void_set_up_maker
      {
        template <typename T>
        constexpr auto
        operator() (T&& test) const
        {
          using Test = value_type<T>;
          return [t = Test{std::forward<T> (test)}] (state& s) mutable
          {
            return std::make_tuple (std::addressof (t), t.set_up (s));
          };
        }
      };

      template <typename T, bool = has_set_up<T>::value,
                bool = std::is_same<set_up_ret_t<T>, void>::value>
      struct set_up_maker : empty_set_up_maker
      { };

      template <typename T>
      struct set_up_maker<T, true, true> : void_set_up_maker
      { };

      template <typename T>
      struct set_up_maker<T, true, false> : non_void_set_up_maker
      { };

      struct empty_tear_down_maker
      {
        template <typename T>
        constexpr auto
        operator() (T&&) const
        {
          return [] (state&, auto&&) { };
        }
      };

      struct void_tear_down_maker
      {
        template <typename T>
        constexpr auto
        operator() (T&&) const
        {
          return [] (state& s, auto&& ptr)
          {
            assert (ptr);
            ptr->tear_down (s);
          };
        }
      };

      struct non_void_tear_down_maker
      {
        template <typename T>
        constexpr auto
        operator() (T&&) const
        {
          return [] (state& s, auto&& t)
          {
            std::get<0> (t)->tear_down (s, std::get<1> (t));
          };
        }
      };

      template <typename T, bool = has_tear_down<T>::value,
                bool = std::is_same<set_up_ret_t<T>, void>::value>
      struct tear_down_maker : empty_tear_down_maker
      { };

      template <typename T>
      struct tear_down_maker<T, true, true> : void_tear_down_maker
      { };

      template <typename T>
      struct tear_down_maker<T, true, false> : non_void_tear_down_maker
      { };

      template<bool is_auto>
      struct void_test_maker;

      template<>
      struct void_test_maker<true>
      {
        template <typename T>
        constexpr auto
        operator() (T&&) const
        {
          return [] (auto&& ptr)
          {
            assert (ptr);
            (*ptr) ();
          };
        }
      };

      template<>
      struct void_test_maker<false>
      {
        template <typename T>
        constexpr auto
        operator() (T&&) const
        {
          return [] (state& s, auto&& ptr)
          {
            assert (ptr);
            (*ptr) (s);
          };
        }
      };

      template<bool is_auto>
      struct non_void_test_maker;

      template<>
      struct non_void_test_maker<true>
      {
        template <typename T>
        constexpr auto
        operator() (T&&) const
        {
          return [] (auto&& t)
          {
            auto ptr = std::get<0> (t);
            auto& data = std::get<1> (t);
            assert (ptr);
            (*ptr) (data);
          };
        }
      };

      template<>
      struct non_void_test_maker<false>
      {
        template <typename T>
        constexpr auto
        operator() (T&&) const
        {
          return [] (state& s, auto&& t)
          {
            auto ptr = std::get<0> (t);
            auto& data = std::get<1> (t);
            assert (ptr);
            (*ptr) (s, data);
          };
        }
      };

      template <typename T,
                bool is_auto = is_auto_class<T>::value,
                bool = std::is_same<set_up_ret_t<T>, void>::value>
      struct test_maker : non_void_test_maker<is_auto>
      { };

      template <typename T, bool is_auto>
      struct test_maker<T, is_auto, true> : void_test_maker<is_auto>
      { };

      template <typename T, typename = void>
      struct class_register
      {
        template <typename U>
        token
        operator() (char const* name, U&& t) const
        {
          constexpr auto make_test = test_maker<U> {};
          constexpr auto make_set_up = set_up_maker<U> {};
          constexpr auto make_tear_down = tear_down_maker<U> {};
          return register_test (name, make_test (std::forward <U> (t)),
                                make_set_up (std::forward<U> (t)),
                                make_tear_down (std::forward<U> (t)));
        }
      };

      template <typename Test>
      using test_with_constraints_t =
        std::enable_if_t<has_constraints<value_type<Test>>::value>;

      template <typename T>
      struct class_register<T, test_with_constraints_t<T>>
      {
        template <typename U>
        token
        operator() (char const* name, U&& t) const
        {
          constexpr auto make_test = test_maker<U> {};
          constexpr auto make_set_up = set_up_maker<U> {};
          constexpr auto make_tear_down = tear_down_maker<U> {};
          add_constraint (name, value_type<U>::constraints);
          return register_test (name, make_test (std::forward <U> (t)),
                                make_set_up (std::forward<U> (t)),
                                make_tear_down (std::forward<U> (t)));
        }
      };
    }

    template <typename Test, typename SetUp, typename TearDown>
    token
    register_test (char const* name, Test&& t, SetUp&& s, TearDown&& td)
    {
      using micro_benchmark::detail::add_pointer_to_function_t;
      using micro_benchmark::detail::value_type;
      using micro_benchmark::detail::test_base;
      using micro_benchmark::detail::test_data;

      using VT = value_type<Test>;
      using T = add_pointer_to_function_t<VT>;

      using VS = value_type<SetUp>;
      using S = add_pointer_to_function_t<VS>;

      using VD = value_type<TearDown>;
      using D = add_pointer_to_function_t<VD>;

      using type = test_data<T, S, D>;
      using ptr_type = std::unique_ptr<test_base>;
      ptr_type ptr{new type (std::forward<Test> (t), std::forward<SetUp> (s),
                             std::forward<TearDown> (td))};
      detail::register_static_test (name);
      detail::add_test (name, std::move (ptr));
      return {};
    }

    template <typename StringLike, typename Test,
              typename SetUp = detail::default_set_up const&,
              typename TearDown = detail::default_tear_down const&>
    std::enable_if_t<!std::is_same<StringLike,
                                   with_constraints_type>::value,
                     token>
    register_test (StringLike const& c, Test&& t,
                   SetUp&& s = detail::set_up,
                   TearDown&& td = detail::tear_down)
    {
      return register_test (c.c_str (), std::forward<Test> (t),
                            std::forward<SetUp> (s),
                            std::forward<TearDown> (td));
    }

    template <typename Constraints, typename Test,
              typename SetUp = detail::default_set_up const&,
              typename TearDown = detail::default_tear_down const&>
    token
    register_test (with_constraints_type const&, char const* name,
                   Constraints&& c, Test&& t,
                   SetUp&& s = detail::set_up,
                   TearDown&& td = detail::tear_down)
    {
      detail::add_constraint (name, std::forward<Constraints> (c));
      return register_test (name, std::forward<Test> (t),
                            std::forward<SetUp> (s),
                            std::forward<TearDown> (td));
    }

    template <typename Constraints, typename StringLike, typename Test,
              typename SetUp = detail::default_set_up const&,
              typename TearDown = detail::default_tear_down const&>
    token
    register_test (with_constraints_type const&, StringLike const& name,
                   Constraints&& c, Test&& t,
                   SetUp&& s = detail::set_up,
                   TearDown&& td = detail::tear_down)
    {
      detail::add_constraint (name.c_str (), std::forward<Constraints> (c));
      return register_test (name, std::forward<Test> (t),
                            std::forward<SetUp> (s),
                            std::forward<TearDown> (td));
    }

    template <typename Test>
    token
    register_class (char const* name, Test&& t)
    {
      constexpr auto reg = detail::class_register<Test> {};
      return reg (name, std::forward<Test> (t));
    }

    template <typename StringLike, typename Test>
    token
    register_class (StringLike const& name, Test&& t)
    {
      constexpr auto reg = detail::class_register<Test> {};
      return reg (name.c_str (), std::forward<Test> (t));
    }
  }

  using registry::with_constraints;
  using registry::register_test;
  using registry::register_class;
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
