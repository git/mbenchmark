/* report.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_ITERATOR_HPP
#define MICRO_BENCHMARK_CXX_ITERATOR_HPP

#include "mbenchmark/c++/c-handler.hpp"
#include "mbenchmark/c++/io.hpp"

#include <iterator>
#include <limits>
#include <type_traits>
#include <utility>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace iterator_helper
  {
    template <typename Iterator>
    struct range
    {
      using traits = typename std::iterator_traits<Iterator>;
      using iterator_category = typename traits::iterator_category;
      using difference_type = typename traits::difference_type;
      using value_type = typename traits::value_type;
      using reference = typename traits::reference;
      using pointer = typename traits::pointer;

      static_assert (std::is_base_of<std::bidirectional_iterator_tag,
                     iterator_category>::value,
                     "Iterator must be at bidirectional.");

      Iterator begin_ = Iterator();
      Iterator end_ = Iterator();

      Iterator
      begin () const
      {
        return begin_;
      }

      Iterator
      end () const
      {
        return end_;
      }

      friend bool operator== (range const& lhs, range const& rhs)
      {
        return lhs.begin_ == rhs.begin_ && lhs.end_ == rhs.end_;
      }

      friend bool operator!= (range const& lhs, range const& rhs)
      {
        return lhs.begin_ != rhs.begin_ || lhs.end_ != rhs.end_;
      }
    };

    template <typename Range, bool reverse = false>
    struct subrange_extractor
    {
      template <typename T>
      Range
      operator() (T&& v)
      {
        using std::begin;
        using std::end;
        return Range{begin (v), end (v)};
      }
    };

    template <typename Range>
    struct subrange_extractor<Range, true>
    {
      template <typename T>
      Range
      operator() (T&& v)
      {
        using std::rbegin;
        using std::rend;
        return Range{rbegin (v), rend (v)};
      }
    };

    template <typename It1, typename It2, bool reverse = false>
    class composed
    {
      using traits1 = typename range<It1>::traits;
      using traits2 = typename range<It2>::traits;
      using dt1 = typename traits1::difference_type;

      template <typename T>
      static range<It2>
      subrange_from_element (T&& v)
      {
        subrange_extractor<range<It2>, reverse> extract;
        return extract (std::forward<T> (v));
      }

      static range<It2>
      get_second_range (range<It1> r1)
      {
        if (r1.begin () == r1.end ())
          return {};
        return subrange_from_element (*r1.begin ());
      }

      It1 curr_1;
      range<It1> range_1;
      It2 curr_2;
      range<It2> range_2;

      explicit
      composed (range<It1> r1, range<It2> r2)
        : curr_1 (r1.begin()), range_1 (r1), curr_2 (r2.begin()), range_2 (r2)
      { }

    public:
      using iterator_category = typename traits2::iterator_category;
      using difference_type = typename traits2::difference_type;
      using value_type = typename traits2::value_type;
      using reference = typename traits2::reference;
      using pointer = typename traits2::pointer;

      static_assert (std::is_same<difference_type, dt1>::value,
                     "Containers don't have the same difference type.");

      composed () = default;
      ~composed () = default;
      composed (composed const&) = default;
      composed (composed&&) = default;
      composed& operator= (composed const&) = default;
      composed& operator= (composed&&) = default;

      explicit
      composed (range<It1> r1)
        : composed (r1, get_second_range (r1))
      { }

      explicit
      composed (It1 begin, It1 end)
        : composed (range<It1>{begin, end},
                    get_second_range (range<It1>{begin, end}))
      { }

      reference
      operator* () const
      {
        assert (curr_1 != range_1.end ());
        assert (curr_2 != range_2.end ());
        return *curr_2;
      }

      pointer
      operator-> () const
      {
        return &(*this);
      }

      composed&
      operator++ ()
      {
        assert (curr_1 != range_1.end ());
        assert (curr_2 != range_2.end ());
        if (++curr_2 == range_2.end ())
          {
            ++curr_1;
            if (curr_1 == range_1.end ())
              {
                curr_2 = It2{};
                range_2 = range<It2>{};
              }
            else
              {
                range_2 = subrange_from_element (*curr_1);
                curr_2 = range_2.begin ();
              }
          }
        return *this;
      }

      composed&
      operator-- ()
      {
        if (curr_2 == range_2.begin ())
          {
            assert (curr_1 != range_1.begin ());
            --curr_1;
            range_2 = subrange_from_element (*curr_1);
            curr_2 = range_2.end ();
          }
        assert (curr_2 != range_2.begin ());
        --curr_2;
        return *this;
      }

      composed&
      operator+= (difference_type diff)
      {
        assert (diff != std::numeric_limits<difference_type>::min ());
        if (diff < 0)
          return (*this -= -diff);

        for (; diff > 0; --diff)
          ++*this;
        return *this;
      }

      composed&
      operator-= (difference_type diff)
      {
        assert (diff != std::numeric_limits<difference_type>::min ());
        if (diff < 0)
          return (*this += -diff);

        for (; diff > 0; --diff)
          --*this;
        return *this;
      }

      composed
      operator++ (int)
      {
        composed copy{*this};
        ++*this;
        return copy;
      }

      composed
      operator-- (int)
      {
        composed copy{*this};
        --*this;
        return copy;
      }

      friend composed
      operator+ (composed l, difference_type diff)
      {
        return (l += diff);
      }

      friend composed
      operator+ (difference_type diff, composed r)
      {
        return (r += diff);
      }

      friend composed
      operator- (composed l, difference_type diff)
      {
        return (l -= diff);
      }

      friend composed
      operator- (difference_type diff, composed r)
      {
        return (r -= diff);
      }

      friend difference_type
      operator- (composed const& i1, composed const& i2)
      {
        difference_type ret = 0;
        if (i1 < i2)
          {
            composed tmp = i1;
            while (tmp != i2)
              {
                ++ret;
                ++tmp;
              }
          }
        else if (i2 < i1)
          {
            composed tmp = i2;
            while (tmp != i1)
              {
                --ret;
                ++tmp;
              }
          }
        return ret;
      }

      friend bool
      operator== (composed const& l, composed const& r)
      {
        return l.curr_1 == r.curr_1 && l.curr_2 == r.curr_2;
      }

      friend bool
      operator!= (composed const& l, composed const& r)
      {
        return !(l == r);
      }

      friend bool
      operator< (composed const& l, composed const& r)
      {
        //assert (l.range_1 == r.range_1);
        if (l.curr_1 == r.curr_1)
          return l.curr_2 < r.curr_2;
        return l.curr_1 < r.curr_1;
      }

      friend bool
      operator>= (composed const& l, composed const& r)
      {
        return !(r < l);
      }

      friend bool
      operator> (composed const& l, composed const& r)
      {
        return r < l;
      }

      friend bool
      operator<= (composed const& l, composed const& r)
      {
        return !(r < l);
      }
    };
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
