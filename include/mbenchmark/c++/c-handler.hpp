/* c-handler.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_C_HANDLER_HPP
#define MICRO_BENCHMARK_CXX_C_HANDLER_HPP

#include "mbenchmark/c++/version.hpp"

#include <cassert>
#include <cstddef>
#include <memory>
#include <utility>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace detail
  {
    class c_handler
    {
      void const* impl = nullptr;

    public:
      ~c_handler () = default;
      c_handler (c_handler const&) = delete;
      c_handler& operator=(c_handler const&) = delete;

      explicit
      c_handler (void const* ptr = nullptr) noexcept
        : impl (ptr)
      { }

      c_handler (c_handler&& other) noexcept
        : impl (nullptr)
      {
        swap (other);
      }

      c_handler&
      operator= (c_handler&& rhs) noexcept
      {
        c_handler other (std::move (rhs));
        swap (other);
        return *this;
      }

      void
      swap (c_handler& other) noexcept
      {
        using std::swap;
        swap (impl, other.impl);
      }

      template<typename T>
      T
      as () const
      {
        assert (impl);
        return static_cast<T> (const_cast<void*> (impl));
      }
    };

    struct c_suite : c_handler
    {
      explicit c_suite (const char *name);
      ~c_suite ();
    };

    /* Pointer of the suite to keep alive its data.  */
    using c_suite_ptr = std::shared_ptr<c_suite>;
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
