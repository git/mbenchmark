/* test.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_TEST_HPP
#define MICRO_BENCHMARK_CXX_TEST_HPP

#include "mbenchmark/c++/c-handler.hpp"
#include "mbenchmark/c++/state.hpp"
#include "mbenchmark/c++/traits.hpp"

#include <chrono>
#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <memory>
#include <vector>
#include <utility>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace detail
  {
    struct default_set_up
    {
      std::vector<std::size_t>
      operator() (state const& state) const
      {
        return state.sizes ();
      }
    };

    constexpr default_set_up set_up{};

    struct default_tear_down
    {
      template <typename Data>
      void
      operator() (state const&, Data&&) const
      {
      }
    };

    constexpr default_tear_down tear_down{};

    /*  TODO: Provide the erasure as the constructor, like the correct
        pattern (e.g. std::function).  */
    class test_base
    {
    protected:
      test_base () = default;
    public:
      virtual ~test_base () = default;
      test_base (test_base const&) = delete;
      test_base& operator= (test_base const&) = delete;
      test_base (test_base&&) = delete;
      test_base& operator= (test_base&&) = delete;

      virtual void run_set_up (state& state) noexcept = 0;
      virtual void run_tear_down (state& state) noexcept = 0;
      virtual void run_test (state& state) noexcept = 0;
    };

    template <typename SetUp, typename TearDown,
              typename SetUpData = test_set_up_ret_t<SetUp>>
    class test_data_common : public test_base
    {
      SetUp set_up;
      TearDown tear_down;
      SetUpData data;

      void
      run_set_up (state& s) noexcept final
      {
        data = set_up (s);
      }

      void
      run_tear_down (state& s) noexcept final
      {
        tear_down (s, data);
      }

    protected:
      test_data_common () = delete;
      ~test_data_common () override = default;

      template <typename SU, typename TDU>
      explicit test_data_common (SU&& s, TDU&& td)
        : set_up (std::forward<SU> (s)),
          tear_down (std::forward<TDU> (td)),
          data ()
      { }

      SetUpData const&
      get_data () const
      {
        return data;
      }
    };

    template <typename SetUp, typename TearDown>
    class test_data_common<SetUp, TearDown, void> : public test_base
    {
      SetUp set_up;
      TearDown tear_down;

      void
      run_set_up (state& s) noexcept final
      {
        set_up (s);
      }

      void
      run_tear_down (state& s) noexcept final
      {
        tear_down (s);
      }

    protected:
      test_data_common () = delete;
      ~test_data_common () override = default;

      template <typename SU, typename TDU>
      explicit test_data_common (SU&& s, TDU&& td)
        : set_up (std::forward<SU> (s)),
          tear_down (std::forward<TDU> (td))
      { }
    };

    template <typename Test, typename SetUp, typename TearDown,
              typename = test_set_up_ret_t<SetUp>,
              bool is_auto = is_auto_test<Test, SetUp>::value>
    class test_data;

    template <typename Test, typename SetUp, typename TearDown,
              typename SetUpRet>
    class test_data<Test, SetUp, TearDown, SetUpRet, false> final
      : public test_data_common<SetUp, TearDown>
    {
      using base = test_data_common<SetUp, TearDown>;
      Test test;

      void
      run_test (state& state) noexcept final
      {
        test (state, this->get_data ());
      }

    public:
      template <typename TU, typename SU, typename TDU>
      explicit test_data (TU&& t, SU&& s, TDU&& td)
        : base (std::forward<SU> (s), std::forward<TDU> (td)),
          test (std::forward<TU> (t))
      { }

      ~test_data () final = default;
    };

    template <typename Test, typename SetUp, typename TearDown>
    class test_data<Test, SetUp, TearDown, void, false> final
      : public test_data_common<SetUp, TearDown>
    {
      using base = test_data_common<SetUp, TearDown>;
      Test test;

      void
      run_test (state& state) noexcept final
      {
        test (state);
      }

    public:
      template <typename TU, typename SU, typename TDU>
      explicit test_data (TU&& t, SU&& s, TDU&& td)
        : base (std::forward<SU> (s), std::forward<TDU> (td)),
          test (std::forward<TU> (t))
      { }

      ~test_data () final = default;
    };

    template <typename Test, typename SetUp, typename TearDown,
              typename SetUpData>
    class test_data<Test, SetUp, TearDown, SetUpData, true> final
      : public test_data_common<SetUp, TearDown>
    {
      using base = test_data_common<SetUp, TearDown>;
      Test test;

      void
      run_test (state& state) noexcept final
      {
        while (state.keep_running ())
          test (this->get_data ());
      }

    public:
      template <typename TU, typename SU, typename TDU>
      explicit test_data (TU&& t, SU&& s, TDU&& td)
        : base (std::forward<SU> (s), std::forward<TDU> (td)),
          test (std::forward<TU> (t))
      { }

      ~test_data () final = default;
    };

    template <typename Test, typename SetUp, typename TearDown>
    class test_data<Test, SetUp, TearDown, void, true> final
      : public test_data_common<SetUp, TearDown>
    {
      using base = test_data_common<SetUp, TearDown>;
      Test test;

      void
      run_test (state& state) noexcept final
      {
        while (state.keep_running ())
          test ();
      }

    public:
      template <typename TU, typename SU, typename TDU>
      explicit test_data (TU&& t, SU&& s, TDU&& td)
        : base (std::forward<SU> (s), std::forward<TDU> (td)),
          test (std::forward<TU> (t))
      { }

      ~test_data () final = default;
    };

    template <typename T>
    struct add_pointer_to_function
    {
      using type = T;
    };

    template <typename T, typename... Args>
    struct add_pointer_to_function<T (Args...)>
    {
      using type = T (*) (Args...);
    };

    template <typename T>
    using add_pointer_to_function_t =
      typename add_pointer_to_function<T>::type;

    test_case make_test_case (void*, c_suite_ptr const&);

    template <typename Test, typename SetUp, typename TearDown>
    std::unique_ptr<test_base>
    make_test_data (Test&& test, SetUp&& set_up, TearDown&& tear_down)
    {
      using VT = typename detail::value_type<Test>;
      using T = add_pointer_to_function_t<VT>;

      using VS = typename detail::value_type<SetUp>;
      using S = add_pointer_to_function_t<VS>;

      using VD = typename detail::value_type<TearDown>;
      using D = add_pointer_to_function_t<VD>;

      using type = test_data<T, S, D>;
      using ptr_type = std::unique_ptr<test_base>;
      return ptr_type (new type {std::forward<Test> (test),
                                 std::forward<SetUp> (set_up),
                                 std::forward<TearDown> (tear_down)});
    }
  }

  class test_case : detail::c_handler
  {
    friend class suite;
    friend test_case detail::make_test_case (void*, detail::c_suite_ptr const&);
    detail::c_suite_ptr suite;

    test_case (detail::c_handler &&hnd, detail::c_suite_ptr const& suite)
      : detail::c_handler (std::move (hnd)), suite (suite)
    { }

    void register_data_ (std::unique_ptr<detail::test_base>&& base);

    template <typename Test, typename SetUp, typename TearDown>
    void
    register_data_ (Test&& test, SetUp&& set_up, TearDown&& tear_down)
    {
      auto ptr = detail::make_test_data (std::forward<Test> (test),
                                         std::forward<SetUp> (set_up),
                                         std::forward<TearDown> (tear_down));
      register_data_ (std::move (ptr));
    }

    struct max_time_type_
    {
      std::int32_t seconds;
      std::int32_t nanoseconds;
    };

    void max_time_ (max_time_type_);
    max_time_type_ max_time_ () const;
  public:
    ~test_case () = default;
    test_case (test_case&&) = default;
    test_case& operator= (test_case&&) = default;

    template <typename Range>
    void add_dimension (Range dimension)
    {
      using std::begin;
      using std::end;
      add_dimension (begin (dimension), end (dimension));
    }

    template <typename T>
    void add_dimension (std::initializer_list<T> dimension)
    {
      using std::begin;
      using std::end;
      add_dimension (begin (dimension), end (dimension));
    }

    template <typename Iterator>
    void add_dimension (Iterator begin, Iterator end)
    {
      std::vector<std::size_t> dim (begin, end);
      add_dimension (dim);
    }

    void add_dimension (std::vector<std::size_t> const& dim);

    std::size_t number_of_dimensions () const;
    std::vector<std::size_t> get_dimension (std::size_t) const;

    void skip_iterations (std::size_t iterations);
    std::size_t iterations_to_skip () const;

    void limit_iterations (std::size_t min, std::size_t max);
    std::size_t min_iterations () const;
    std::size_t max_iterations () const;

    void limit_samples (std::size_t min, std::size_t max);
    std::size_t min_sample_iterations () const;
    std::size_t max_sample_iterations () const;

    void
    max_time (std::chrono::seconds s)
    {
      std::int32_t is = s.count ();
      max_time_ (max_time_type_ { is, 0 });
    }

    void
    max_time (std::chrono::nanoseconds s)
    {
      std::int32_t is = s.count () / 1000000000;
      std::int32_t ins = s.count () % 1000000000;
      max_time_ (max_time_type_ { is, ins });
    }

    void
    max_time (std::chrono::seconds s, std::chrono::nanoseconds ns)
    {
      std::int32_t is = s.count ();
      std::int32_t ins = ns.count ();
      max_time_ (max_time_type_ { is, ins });
    }

    std::pair<std::chrono::seconds, std::chrono::nanoseconds>
    max_time () const
    {
      auto r = max_time_ ();
      return std::make_pair (std::chrono::seconds (r.seconds),
                             std::chrono::nanoseconds (r.nanoseconds));
    }

  };
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
