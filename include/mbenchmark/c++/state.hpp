/* state.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_STATE_HPP
#define MICRO_BENCHMARK_CXX_STATE_HPP

#include "mbenchmark/c++/c-handler.hpp"

#include <cstddef>
#include <vector>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace detail
  {
    state make_state (void *);
  }

  class state : detail::c_handler
  {
    friend state detail::make_state (void*);
    state (detail::c_handler&& val)
      : detail::c_handler (std::move (val))
    { }

  public:
    state () = delete;
    state (state&&) = default;
    state& operator= (state&&) = default;

    char const* get_name () const;
    void set_name (char const* name);

    template <typename StringLike>
    void
    set_name (StringLike const& name)
    {
      set_name (name.c_str ());
    }

    bool keep_running ();
    std::vector<std::size_t> sizes () const;
  };
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
