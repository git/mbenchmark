/* suite.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_SUITE_HPP
#define MICRO_BENCHMARK_CXX_SUITE_HPP

#include "mbenchmark/c++/c-handler.hpp"
#include "mbenchmark/c++/report.hpp"
#include "mbenchmark/c++/test.hpp"
#include "mbenchmark/c++/registry.hpp"

#include <cstddef>
#include <memory>
#include <vector>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace detail
  {
    using registry::detail::test_with_constraints_t;
    using registry::detail::set_up_maker;
    using registry::detail::tear_down_maker;
    using registry::detail::test_maker;
    using registry::detail::value_type;

    template <typename T, typename = void>
    struct class_register
    {
      template <typename F, typename U>
      auto
      operator() (F&& f, U&& t) const
      {
        constexpr auto make_test = test_maker<U> {};
        constexpr auto make_set_up = set_up_maker<U> {};
        constexpr auto make_tear_down = tear_down_maker<U> {};
        return std::forward<F> (f) (make_test (std::forward <U> (t)),
                                    make_set_up (std::forward<U> (t)),
                                    make_tear_down (std::forward<U> (t)));
      }
    };

    template <typename T>
    struct class_register<T, test_with_constraints_t<T>>
    {
      template <typename F, typename U>
      auto
      operator() (F&& f, U&& t) const
      {
        constexpr auto make_test = test_maker<U> {};
        constexpr auto make_set_up = set_up_maker<U> {};
        constexpr auto make_tear_down = tear_down_maker<U> {};
        auto ret = std::forward<F> (f) (make_test (std::forward <U> (t)),
                                        make_set_up (std::forward<U> (t)),
                                        make_tear_down (std::forward<U> (t)));
        value_type<U>::constraints (ret);
        return ret;
      }
    };
  }

  class suite
  {
    detail::c_suite_ptr impl;
    // See registry.cxx
    test_case register_test_impl_ (const char *name);

  public:
    template<typename StringLike>
    explicit
    suite (StringLike const& name)
      : impl (std::make_shared<detail::c_suite> (name.c_str ()))
    { }

    explicit
    suite (const char* name)
      : impl (std::make_shared<detail::c_suite> (name))
    { }

    template <typename Test,
              typename SetUp = detail::default_set_up const&,
              typename TearDown = detail::default_tear_down const&>
    test_case
    register_test (char const* name, Test&& t, SetUp&& s = detail::set_up,
                   TearDown&& td = detail::tear_down)
    {
      auto test = register_test_impl_ (name);
      test.register_data_ (std::forward<Test> (t), std::forward<SetUp> (s),
                           std::forward<TearDown> (td));
      return test;
    }

    template <typename StringLike, typename Test,
              typename SetUp = detail::default_set_up const&,
              typename TearDown = detail::default_tear_down const&>
    std::enable_if_t<!std::is_same<StringLike,
                                   registry::with_constraints_type>::value,
                     test_case>
    register_test (StringLike const& name, Test&& t, SetUp&& s = detail::set_up,
                   TearDown&& td = detail::tear_down)
    {
      return register_test (name.c_str (), std::forward<Test> (t),
                            std::forward<SetUp> (s),
                            std::forward<TearDown> (td));
    }

    template <typename Constraints, typename Test,
              typename SetUp = detail::default_set_up const&,
              typename TearDown = detail::default_tear_down const&>
    test_case
    register_test (registry::with_constraints_type const&, char const* name,
                   Constraints&& c, Test&& t, SetUp&& s = detail::set_up,
                   TearDown&& td = detail::tear_down)
    {
      auto test = register_test (name, std::forward<Test> (t),
                                 std::forward<SetUp> (s),
                                 std::forward<TearDown> (td));
      std::forward<Constraints> (c) (test);
      return test;
    }

    template <typename StringLike, typename Constraints, typename Test,
              typename SetUp = detail::default_set_up const&,
              typename TearDown = detail::default_tear_down const&>
    test_case
    register_test (registry::with_constraints_type const&,
                   StringLike const& name, Constraints&& c, Test&& t,
                   SetUp&& s = detail::set_up,
                   TearDown&& td = detail::tear_down)
    {
      auto test = register_test (name, std::forward<Test> (t),
                                 std::forward<SetUp> (s),
                                 std::forward<TearDown> (td));
      std::forward<Constraints> (c) (test);
      return test;
    }

    template <typename Test>
    test_case
    register_class (char const* name, Test&& t)
    {
      auto f = [name, this] (auto&& t, auto&& s, auto&& td)
      {
        return this->register_test (name, std::forward<decltype(t)> (t),
                                    std::forward<decltype(s)> (s),
                                    std::forward<decltype(td)> (td));
      };
      using reg = typename detail::class_register<Test>;
      constexpr reg do_it{};
      return do_it (f, std::forward<Test> (t));
    }

    template <typename StringLike, typename Test>
    test_case
    register_class (StringLike const& name, Test&& t)
    {
      auto f = [name, this] (auto&& t, auto&& s, auto&& td)
      {
        return this->register_test (name.c_str (), std::forward<decltype(t)> (t),
                                    std::forward<decltype(s)> (s),
                                    std::forward<decltype(td)> (td));
      };
      using reg = typename detail::class_register<Test>;
      constexpr reg do_it{};
      return do_it (f, std::forward<Test> (t));
    }


    char const* name () const;
    report stored_report () const;
    std::vector<test_case> tests ();
    void run ();
  };
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
