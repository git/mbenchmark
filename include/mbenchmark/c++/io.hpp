/* io.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_IO_HPP
#define MICRO_BENCHMARK_CXX_IO_HPP

#include "mbenchmark/c++/version.hpp"

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace io
  {
    /*  See micro_benchmark_output_type:mbenchmark/report/io.h.  */
    enum class output_type
      {
        console,
        lisp,
        text
      };

    /*  See micro_benchmark_output_stat:mbenchmark/report/io.h.  */
    enum class output_stat
      {
        none = 0,
        mean = 1,
        variance = 2,
        std_deviation = 4,
        basic = 1 | 4,
        all = 1 | 2 | 4
      };

    /*  See micro_benchmark_output_values:mbenchmark/report/io.h.  */
    struct output_values
    {
      bool self_test;
      bool size_constraints;
      bool total_time;
      bool total_iterations;
      bool iterations;
      bool total_samples;
      bool samples;
      bool extra_data;
      output_stat iteration_time;
      output_stat sample_time;
      output_stat sample_iterations;
    };

    output_values default_output_values ();
    void default_output_values (output_values const& v);
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
