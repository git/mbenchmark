/* utility.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_UTILITY_HPP
#define MICRO_BENCHMARK_CXX_UTILITY_HPP

#include "mbenchmark/c++/version.hpp"

#include <type_traits>

#define MICRO_BENCHMARK_MAIN() int main (int c, char* v[]) \
  { return ::micro_benchmark::main (c, v); }

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  int main (int argc, char* argv[]);

  namespace detail
  {
    void do_not_optimize (void const*);
    void compiler_barrier ();
  }

  inline void
  compiler_barrier ()
  {
#ifdef __GNUC__
    asm volatile ("" ::: "memory");
#else
    detail::compiler_barrier ();
#endif
  }

  template <typename T>
  void
  do_not_optimize (T& lvalue)
  {
#ifdef __GNUC__
    asm volatile ("" : "+X" (lvalue) :: "memory");
#else
    detail::do_not_optimize (&lvalue);
#endif
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
