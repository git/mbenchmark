/* traits.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_TRAITS_HPP
#define MICRO_BENCHMARK_CXX_TRAITS_HPP

#include "mbenchmark/c++/version.hpp"

#include <type_traits>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace detail
  {
#if __cplusplus >= 201703L
    using std::void_t;
#else
    template <typename... T>
    struct avoid_alias_bug
    {
      using type = void;
    };

    template <typename... T>
    using void_t = typename avoid_alias_bug<T...>::type;
#endif

    template <typename T>
    struct value
    {
      using type = std::remove_cv_t<std::remove_reference_t<T>>;
    };

    template <typename T>
    using value_type = typename value<T>::type;

    template <typename T>
    value_type<T>&
    as_ref ();

    template <typename T>
    value_type<T> const&
    as_cref ();

    template <typename T, typename = void>
    struct has_constraints : std::false_type
    { };

    template <typename T>
    struct has_constraints<T,
      void_t<decltype (value_type<T>::constraints (as_ref<test_case> ()))>>
      : std::true_type
    { };

    template <typename T, typename = void>
    struct has_set_up : std::false_type
    { };

    template <typename T>
    struct has_set_up<
      T, void_t<decltype (as_ref<T> ().set_up (as_ref<state> ()))>>
      : std::true_type
    { };

    template <typename T, bool = has_set_up<T>::value>
    struct set_up_ret
    {
      using type = std::vector<std::size_t>;
    };

    template <typename T>
    struct set_up_ret<T, true>
    {
      using type = decltype (as_ref<T> ().set_up (as_ref<state> ()));
    };

    template <typename T>
    using set_up_ret_t = typename set_up_ret<T>::type;

    template <typename T,
              typename Data = set_up_ret_t<T>,
              typename = void>
    struct has_tear_down : std::false_type
    { };

    template <typename T, typename Data>
    struct has_tear_down<T, Data,
      void_t<decltype (as_ref<T> ().tear_down (as_ref<state> (),
                                               as_ref<Data> ()))>>
      : std::true_type
    { };

    template <typename T>
    struct has_tear_down<T, void,
      void_t<decltype (as_ref<T> ().tear_down (as_ref<state> ()))>>
      : std::true_type
    { };

    template <typename Test,
              bool = std::is_same<set_up_ret_t<Test>, void>::value,
              typename = void>
    struct is_auto_class : std::true_type
    { };

    template <typename Test>
    struct is_auto_class<Test, true,
      void_t<decltype(as_ref<Test> () (as_ref<state> ()))>>
      : std::false_type
    { };

    template <typename Test>
    struct is_auto_class<Test, false,
      void_t<decltype(as_ref<Test> () (as_ref<state> (),
                                       as_cref<set_up_ret_t<Test>> ()))>>
      : std::false_type
    { };

    template <typename SetUp>
    struct test_set_up_ret
    {
      using return_type = decltype (as_ref<SetUp> () (as_ref<state> ()));
      using type = value_type<return_type>;
    };

    template <typename SetUp>
    using test_set_up_ret_t = typename test_set_up_ret<SetUp>::type;

    template <typename Test, typename SetUpRet, typename = void>
    struct is_auto_test_base : std::true_type
    { };

    template <typename Test, typename SetUpRet>
    struct is_auto_test_base<Test, SetUpRet,
      void_t<decltype(as_ref<Test> () (as_ref<state> (),
                                        as_cref<SetUpRet> ()))>>
      : std::false_type
    { };

    template <typename Test>
    struct is_auto_test_base<Test, void,
      void_t<decltype(as_ref<Test> () (as_ref<state> ()))>>
      : std::false_type
    { };

    template <typename Test, typename SetUp,
              typename SetUpRet = test_set_up_ret_t<SetUp>>
    struct is_auto_test : is_auto_test_base<Test, SetUpRet>
    { };
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
