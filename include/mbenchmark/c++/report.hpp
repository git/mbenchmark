/* report.hpp --- MicroBenchmark C++ library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_CXX_REPORT_HPP
#define MICRO_BENCHMARK_CXX_REPORT_HPP

#include "mbenchmark/c++/c-handler.hpp"
#include "mbenchmark/c++/io.hpp"
#include "mbenchmark/c++/iterator.hpp"

#include <chrono>
#include <cstddef>
#include <cstdio>
#include <iosfwd>
#include <vector>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace detail
  {
    report make_report (void const*, c_suite_ptr const&);
    test_report make_test_report (void const*);
    exec_report make_exec_report (void const*);

    /*  See mbenchmark/stats/time-sample.h  */
    struct time_sample
    {
      bool discarded;
      std::size_t iterations;
      /* TODO: This should be total_time_type.  */
      std::chrono::nanoseconds elapsed;
    };

    struct total_time_type
    {
      std::chrono::seconds seconds;
      std::chrono::nanoseconds nanoseconds;
      template <typename T>
      T to () const
      {
        using std::chrono::duration_cast;

        T a = duration_cast<T> (seconds);
        T b = duration_cast<T> (nanoseconds);
        return a + b;
      }
    };

    template <typename T, typename U, typename = void>
    struct unit_base : private U
    {
      using type = T;
      using unit_conversor = U;
      using unit_type = typename U::unit_type;
      using result_type = typename U::result_type;

      unit_base (unit_type u, type v = type{})
        : U (), u (u), v (v)
      { }

      type value () const { return v; }
      result_type to_unit () const { return this->operator() (u, v); }

    private:
      unit_type u;
      type v;
    };

    template <typename T, typename U>
    struct unit_base<T, U, typename U::unit_type>
    {
      using type = T;
      using unit_conversor = U;
      using unit_type = void;
      using result_type = typename U::result_type;

      unit_base (type v = type{}) : v (v) { }
      type value () const { return v; }
      result_type to_unit () const { return v; }

    private:
      type v;
    };

    template <typename T, typename U, int p>
    struct unit;

    template <typename T, typename U>
    struct unit<T, U, 1> : public unit_base<T, U>
    {
      using squared = unit<T, U, 2>;
      using unit_base<T, U>::unit_base;
    };

    template <typename T, typename U>
    struct unit<T, U, 2> : public unit_base<T, U>
    {
      using unit_base<T, U>::unit_base;
    };

    /*  See mbenchmark/stats/time-sample.h  */
    template <typename Unit>
    struct stat_value
    {
      using unit_type = Unit;
      Unit mean;
      Unit std_deviation;
      typename Unit::squared variance;
    };

    struct time_conversor
    {
      using unit_type = int;
      using result_type = std::chrono::nanoseconds;
      result_type operator() (int type, double value) const;
    };

    struct iteration_conversor
    {
      using unit_type = void;
      using result_type = double;
    };

    template <typename Conversor>
    using stat_unit = unit<double, Conversor, 1>;

    using time_stat_value = stat_value<stat_unit<time_conversor>>;
    using iteration_stat_value = stat_value<stat_unit<iteration_conversor>>;

    template<typename F>
    struct with_args_type
    {
      F fun;

      friend std::ostream&
      operator<< (std::ostream& out, with_args_type&& args)
      {
        args.fun (out);
        return out;
      }
    };
  }

  /**  Report of a test execution.  */
  class exec_report : detail::c_handler
  {
    friend exec_report detail::make_exec_report (void const*);
    explicit exec_report (detail::c_handler&& handler)
      : detail::c_handler (std::move(handler))
    { }

  public:
    /**
     * Creation of an object in invalid state, only move-assignment
     * from another object and destruction are defined.
     */
    exec_report () = default;

    /**  Transfer the validity of the object.  */
    exec_report (exec_report&&) noexcept = default;

    /**  Transfer the validity of the object.  */
    exec_report& operator= (exec_report&&) noexcept = default;

    /**  Name of the test execution associated to this report.  */
    char const* name () const;

    /**  Size dimensions of this execution.  */
    std::vector<std::size_t> sizes () const;

    /**  Iterations performed.  */
    std::size_t iterations () const;

    /**  Total iterations performed.  */
    std::size_t total_iterations () const;

    /**  Total time of this execution.  */
    detail::total_time_type total_time () const;

    /**  Time samples collected on this execution.  */
    std::vector<detail::time_sample> time_samples () const;

    /**  Total samples colected.  */
    std::size_t total_samples () const;

    /**  Samples used for the aggregated data calculation.  */
    std::size_t used_samples () const;

    /**  Time per iteration.  */
    detail::time_stat_value iteration_time () const;

    /**  Time per sample.  */
    detail::time_stat_value sample_time () const;

    /**  Iterations per sample.  */
    detail::iteration_stat_value sample_iterations () const;

    /* TODO: Meters.  */
  };

  /**  Report of a test.  */
  class test_report : detail::c_handler
  {
    using backend = std::vector<exec_report>;
    backend cache;

    friend test_report detail::make_test_report (void const*);
    explicit test_report (detail::c_handler&& handler);

  public:
    using size_type = backend::size_type;
    using reverse_iterator = backend::const_reverse_iterator;
    using const_iterator = backend::const_iterator;
    using iterator = const_iterator;
    using value_type = iterator::value_type;
    using reference = iterator::reference;
    using pointer = iterator::pointer;
    using range = iterator_helper::range<iterator>;
    using reverse_range = iterator_helper::range<reverse_iterator>;;

    /**
     * Creation of an object in invalid state, only move-assignment
     * from another object and destruction are defined.
     */
    test_report () = default;

    /**  Transfer the validity of the object.  */
    test_report (test_report&&) noexcept = default;

    /**  Transfer the validity of the object.  */
    test_report& operator= (test_report&&) noexcept = default;

    /**  Name of the test associated to this report.  */
    char const* name () const;

    /**  The number of test executions contained is zero.  */
    bool empty () const { return cache.empty (); }

    /**  Number of test executions contained.  */
    size_type size () const { return cache.size (); }

    /**  Start of the sequence of test executions.  */
    iterator begin () const { return cache.begin (); }

    /**  End of the sequence of test executions.  */
    iterator end () const { return cache.end (); }

    /**  End of the sequence of test executions.  */
    reverse_iterator rbegin () const { return cache.rbegin (); }

    /**  Start of the sequence of test executions.  */
    reverse_iterator rend () const { return cache.rend (); }

    /**  Named access to the test executions contained on this report.  */
    range tests () const
    { return range{cache.begin (), cache.end ()}; }

    /**  Named access to the test executions contained on this report.  */
    reverse_range reverse_tests () const
    { return reverse_range{cache.rbegin (), cache.rend ()}; }
  };

  /**
   * \briefSuite Report.
   *
   * Valid objects are provided by the suite.
   * \see micro_benchmark::suite::stored_report () const
   */
  class report : detail::c_handler
  {
    using backend = std::vector<test_report>;

    detail::c_suite_ptr suite;
    backend cache;

    friend report detail::make_report (void const*, detail::c_suite_ptr const&);
    explicit report (detail::c_handler&& handler, detail::c_suite_ptr const&);

  public:
    /*  report is a itself range.  */
    using size_type = backend::size_type;
    using const_iterator = backend::const_iterator;
    using iterator = const_iterator;
    using reverse_iterator = backend::const_reverse_iterator;
    using value_type = iterator::value_type;
    using reference = iterator::reference;
    using pointer = iterator::pointer;
    using range = iterator_helper::range<iterator>;
    using reverse_range = iterator_helper::range<reverse_iterator>;

    /*  report and provides a range of executions.  */
    using test_iterator = test_report::iterator;
    using exec_iterator = iterator_helper::composed<iterator, test_iterator>;
    using exec_range = iterator_helper::range<exec_iterator>;

    /*  report and provides a reversed range of executions.  */
    using r_test_iterator = test_report::reverse_iterator;
    using r_exec_iterator =
      iterator_helper::composed<reverse_iterator, r_test_iterator, true>;
    using r_exec_range = iterator_helper::range<r_exec_iterator>;

    /**
     * Creation of an object in invalid state, only move-assignment
     * from another object and destruction are defined.
     */
    report () = default;

    /**  Transfer the validity of the object.  */
    report (report&&) noexcept = default;

    /**  Transfer the validity of the object.  */
    report& operator= (report&&) noexcept = default;

    /**  Name of the suite.  */
    char const* name () const;

    /**  The number of tests contained is zero.  */
    bool empty () const { return cache.empty (); }

    /**  Number of tests contained.  */
    size_type size () const { return cache.size (); }

    /**  Start of the sequence of tests.  */
    iterator begin () const { return cache.begin (); }

    /**  End of the sequence of tests.  */
    iterator end () const { return cache.end (); }

    /**  End of the sequence of tests.  */
    reverse_iterator rbegin () const { return cache.rbegin (); }

    /**  Start of the sequence of tests.  */
    reverse_iterator rend () const { return cache.rend (); }

    /**  Named access to the tests contained on this report.  */
    range tests () const
    { return range{cache.begin (), cache.end ()}; }

    /**  Named access to the tests contained on this report.  */
    reverse_range reverse_tests () const
    { return reverse_range{cache.rbegin (), cache.rend ()}; }

    /**  Range of all executions contained.  */
    exec_range
    executions () const
    {
      using tr = iterator_helper::range<iterator>;
      tr begin{cache.begin (), cache.end ()};
      tr end{cache.end (), cache.end ()};
      return {exec_iterator{begin}, exec_iterator{end}};
    }

    /**  Reverse range of all executions contained.  */
    r_exec_range
    reverse_executions () const
    {
      using tr = iterator_helper::range<reverse_iterator>;
      tr begin{cache.rbegin (), cache.rend()};
      tr end{cache.rend (), cache.rend ()};
      return {r_exec_iterator{begin}, r_exec_iterator{end}};
    }

    /**  Print to stdout with default format and values.  */
    void print () const;

    /**  Print values to stdout with default format.  */
    void print (io::output_type type) const;

    /**  Print values to stdout with default format.  */
    void print (io::output_values const& values) const;

    /**  Print to stream with default format and values.  */
    void print (std::FILE* out) const;

    /**  Print to stream with default format and values.  */
    void print (std::ostream& out) const;

    /**  Print values to stream with default format.  */
    void print (std::FILE* out, io::output_values const& values) const;

    /**  Print values to stream with default format.  */
    void print (std::ostream& out, io::output_values const& values) const;

    /**  Print with format to stream with default values.  */
    void print (std::FILE* out, io::output_type format) const;

    /**  Print with format to stream with default values.  */
    void print (std::ostream& out, io::output_type format) const;

    /**  Print values with format to stream.  */
    void print (std::FILE* out, io::output_type format,
                io::output_values const& values) const;

    /**  Print values with format to stream.  */
    void print (std::ostream& out, io::output_type format,
                io::output_values const& values) const;

    friend std::ostream&
    operator<< (std::ostream& out, report const& report)
    {
      report.print (out);
      return out;
    }

    template <typename... Args>
    auto
    with_args (Args const&... args)
    {
      auto l = [this, args...] (std::ostream& out)
      {
        print (out, args...);
      };
      return detail::with_args_type<decltype (l)>{l};
    }
  };

  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
#endif
