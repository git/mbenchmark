/* common.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_COMMON_H
#define MICRO_BENCHMARK_COMMON_H

#ifdef __cplusplus
#define MICRO_BENCHMARK_BEGIN_DECLS extern "C" {
#define MICRO_BENCHMARK_END_DECLS }
#define MICRO_BENCHMARK_INLINE inline
#else
#define MICRO_BENCHMARK_BEGIN_DECLS
#define MICRO_BENCHMARK_END_DECLS
#if __STDC_VERSION__ < 199901L
#ifdef __GNUC__
#define MICRO_BENCHMARK_INLINE extern inline
#else
#define MICRO_BENCHMARK_INLINE static
#endif
#define bool int
#define true 1
#define false 0
#else
#define MICRO_BENCHMARK_INLINE inline
#include <stdbool.h>
#endif
#endif

#endif
