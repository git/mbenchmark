/* macros.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_UTILITY_MACROS_H
#define MICRO_BENCHMARK_UTILITY_MACROS_H

#include "mbenchmark/test/definition.h"
#ifdef __GNUC__
#include "mbenchmark/utility/gcc-macros.h"
#else
#include "mbenchmark/utility/unknown-compiler-macros.h"
#endif

#define MICRO_BENCHMARK_CONSTRUCTOR_NAME_(i) MB_Constructor_ ## i
#define MICRO_BENCHMARK_CONSTRAINT_NAME_(i) MB_Constraint_ ## i
#define MICRO_BENCHMARK_TEST_NAME_(i) MB_Test_ ## i

#define MICRO_BENCHMARK_REGISTER_CONSTRUCTOR_(pos, name) \
  MICRO_BENCHMARK_CONSTRUCTOR(pos + 500) \
  static void MICRO_BENCHMARK_CONSTRUCTOR_NAME_(pos) (void) \
  { \
    micro_benchmark_register_static_test (name, \
					  &MICRO_BENCHMARK_TEST_NAME_(pos)); \
  }

#define MICRO_BENCHMARK_REGISTER_CONSTRAINT_(pos, name, constraint) \
  MICRO_BENCHMARK_CONSTRUCTOR(pos + 1000) \
  static void MICRO_BENCHMARK_CONSTRAINT_NAME_(pos) (void) \
  { \
    micro_benchmark_register_static_constraint (name, &constraint); \
  }

#define MICRO_BENCHMARK_DECLARE_TEST_(pos) \
  static struct micro_benchmark_test_definition \
  MICRO_BENCHMARK_TEST_NAME_(pos)

#define MICRO_BENCHMARK_DEFINE_AUTO_TEST_(pos, setup, test, teardown) \
  static struct micro_benchmark_test_definition \
  MICRO_BENCHMARK_TEST_NAME_(pos) = \
    { \
      true, \
      (micro_benchmark_set_up_fun) setup, \
      (micro_benchmark_tear_down_fun) teardown, \
      (micro_benchmark_auto_test_fun) test, \
      0 \
    }

#define MICRO_BENCHMARK_DEFINE_DIRECTED_TEST_(pos, setup, test, teardown) \
  static struct micro_benchmark_test_definition \
  MICRO_BENCHMARK_TEST_NAME_(pos) = \
    { \
      false, \
      (micro_benchmark_set_up_fun) setup, \
      (micro_benchmark_tear_down_fun) teardown, \
      0, \
      (micro_benchmark_test_fun) test \
    }

#define MICRO_BENCHMARK_REGISTER_FULL_AT_(pos, name, setup, test, teardown) \
  MICRO_BENCHMARK_DECLARE_TEST_(pos); \
  MICRO_BENCHMARK_REGISTER_CONSTRUCTOR_(pos, name) \
  MICRO_BENCHMARK_DEFINE_AUTO_TEST_(pos, setup, test, teardown)

#define MICRO_BENCHMARK_REGISTER_NAMED_AUTO_TEST(name, setup, test, teardown) \
  MICRO_BENCHMARK_REGISTER_FULL_AT_(__COUNTER__, name, setup, \
				    test, teardown)

#define MICRO_BENCHMARK_REGISTER_AUTO_TEST(setup, test, teardown) \
  MICRO_BENCHMARK_REGISTER_FULL_AT_(__COUNTER__, #test, setup, \
				    test, teardown)

#define MICRO_BENCHMARK_REGISTER_SIMPLE_TEST(test) \
  MICRO_BENCHMARK_REGISTER_FULL_AT_(__COUNTER__, #test, 0, test, 0)

#define MICRO_BENCHMARK_REGISTER_FULL_DT_(pos, name, setup, test, teardown) \
  MICRO_BENCHMARK_DECLARE_TEST_(pos); \
  MICRO_BENCHMARK_REGISTER_CONSTRUCTOR_(pos, name) \
  MICRO_BENCHMARK_DEFINE_DIRECTED_TEST_(pos, setup, test, teardown)

#define MICRO_BENCHMARK_REGISTER_NAMED_TEST(name, setup, test, teardown) \
  MICRO_BENCHMARK_REGISTER_FULL_DT_(__COUNTER__, name, setup, \
				    test, teardown)

#define MICRO_BENCHMARK_REGISTER_FULL_TEST(setup, test, teardown) \
  MICRO_BENCHMARK_REGISTER_FULL_DT_(__COUNTER__, #test, setup, \
				    test, teardown)

#define MICRO_BENCHMARK_REGISTER_TEST(test) \
  MICRO_BENCHMARK_REGISTER_FULL_DT_(__COUNTER__, #test, 0, \
				    test, 0)

#define MICRO_BENCHMARK_CONSTRAINT_TEST(name, constraint) \
  MICRO_BENCHMARK_REGISTER_CONSTRAINT_(__COUNTER__, name, \
				       constraint)

#define MICRO_BENCHMARK_MAIN() \
  int \
  main (int argc, char **argv) \
  { \
    return micro_benchmark_main (argc, argv); \
  } \
  int main (int, char**)

#endif
