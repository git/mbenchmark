/* gcc-macros.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_UTILITY_GCC_MACROS_H
#define MICRO_BENCHMARK_UTILITY_GCC_MACROS_H

#ifndef __cplusplus
#define MICRO_BENCHMARK_CONSTRUCTOR(pos) __attribute__ ((constructor (pos)))
#define MICRO_BENCHMARK_DESTRUCTOR(pos) __attribute__ ((destructor (pos)))
#endif
#define MICRO_BENCHMARK_COMPILER_BARRIER() \
  __asm__ volatile ("" ::: "memory")
#define MICRO_BENCHMARK_DO_NOT_OPTIMIZE(x) \
  __asm__ volatile ("" : "+X" (x) :: "memory")

#endif
