/* log.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_UTILITY_LOG_H
#define MICRO_BENCHMARK_UTILITY_LOG_H

#include "mbenchmark/common.h"

#include <stdio.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef enum micro_benchmark_log_level
{
  MICRO_BENCHMARK_ERROR_LEVEL = 0,
  MICRO_BENCHMARK_WARNING_LEVEL = 1 << 1,
  MICRO_BENCHMARK_INFO_LEVEL = 1 << 2,
  MICRO_BENCHMARK_DEBUG_LEVEL = 1 << 3,
  MICRO_BENCHMARK_TRACE_LEVEL = 1 << 4
} micro_benchmark_log_level;

/** Write log messages to \code{out} (or stderr if it is \code{NULL}).  */
void micro_benchmark_log_set_output (FILE *out);

/** Set the global log level.  */
void micro_benchmark_set_log_level (micro_benchmark_log_level level);

/** Set the log level for a specific module.  */
void micro_benchmark_set_module_log_level (const char *,
                                           micro_benchmark_log_level);

MICRO_BENCHMARK_END_DECLS
#endif
