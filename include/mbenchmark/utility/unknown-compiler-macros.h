/* unknown-compiler-macros.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_UTILITY_UNKNOWN_COMPILER_MACROS_H
#define MICRO_BENCHMARK_UTILITY_UNKNOWN_COMPILER_MACROS_H

#ifndef __cplusplus
#define MICRO_BENCHMARK_CONSTRUCTOR_(pos) \
  _Static_assert (false, "Constructor not implemented for this compiler");
#define MICRO_BENCHMARK_DESTRUCTOR_(pos) \
  _Static_assert (false, "Destructor not implemented for this compiler");
#else
extern "C" void micro_benchmark_do_not_optimize (const void *dummy);
#endif
#define MICRO_BENCHMARK_COMPILER_BARRIER() \
  micro_benchmark_do_not_optimize (0)
#define MICRO_BENCHMARK_DO_NOT_OPTIMIZE(x) \
  micro_benchmark_do_not_optimize (&x)

#endif
