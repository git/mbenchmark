/* extra.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_REPORT_EXTRA_H
#define MICRO_BENCHMARK_REPORT_EXTRA_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/time.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_report_extra_data
{
  /** Name of the extra data.  */
  const char *name;
  /** Size of the arrays.  */
  size_t size;
  /** Array of string keys.  */
  const char **keys;
  /** Array of string values.  */
  const char **values;
} micro_benchmark_report_extra_data;

/**
 * Retrieve the collected data on key, value form.
 * \param ptr Pointer to the internal data.
 */
typedef micro_benchmark_report_extra_data
  (*micro_benchmark_report_extractor_fun) (void *ptr);

MICRO_BENCHMARK_END_DECLS
#endif
