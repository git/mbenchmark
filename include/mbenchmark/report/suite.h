/* report.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_REPORT_SUITE_H
#define MICRO_BENCHMARK_REPORT_SUITE_H

#include "mbenchmark/common.h"
#include "mbenchmark/report/test.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * \brief Internal representation of a benchmark suite results.
 */
struct micro_benchmark_report_;

/**
 * \brief Opaque type for benchmark suite results.
 *
 * This type is managed by the micro_benchmark_suite, which will
 * release its resources once the suite is released.
 */
typedef const struct micro_benchmark_report_ *micro_benchmark_report;

/**
 * \brief Report name.
 *
 * \param r Report of a suite execution.
 * \return The name of the associated suite.
 */
const char *micro_benchmark_report_get_name (micro_benchmark_report r);

/**
 * \brief Number of test run.
 *
 * \param r Report of a suite execution.
 * \return Number of tests executed.
 */
size_t micro_benchmark_report_get_number_of_tests (micro_benchmark_report r);

/**
 * \brief Accessor to each test report.
 *
 * \param r Report of a suite execution.
 * \param pos Position of the test.  Must be less than
 *            micro_benchmark_report_get_number_of_tests (r).
 * \return The report of the test execution.
 */
micro_benchmark_test_report
micro_benchmark_report_get_test_report (micro_benchmark_report r, size_t pos);

MICRO_BENCHMARK_END_DECLS
#endif
