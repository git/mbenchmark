/* exec.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_REPORT_EXEC_H
#define MICRO_BENCHMARK_REPORT_EXEC_H

#include "mbenchmark/common.h"
#include "mbenchmark/stats.h"
#include "mbenchmark/report/extra.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Internal representation for the report of a test execution.  */
struct micro_benchmark_exec_report_;

/** Test report. Its lifetime is associated to the report.  */
typedef const struct micro_benchmark_exec_report_
  *micro_benchmark_exec_report;

/**
 * \brief Name of the execution report.
 *
 * \param r Report of a test case execution
 * \return The name of the test with info of the specific result.
 */
/* *INDENT-OFF* */
const char *
micro_benchmark_exec_report_get_name (micro_benchmark_exec_report);
/* *INDENT-ON* */

/**
 * \brief Size values provided to the test.
 *
 * \param report Report of a test case execution
 * \param sizes The size parameters provided to the test.
 * \param dimensions The number of sizes provided to the test.
 */
void
micro_benchmark_exec_report_get_sizes (micro_benchmark_exec_report report,
                                       const size_t **sizes,
                                       size_t *dimensions);

/**
 * \brief Number of time samples collected.
 *
 * \param report Result of a test case execution.
 * \return Number of samples on this report.
 */
size_t
micro_benchmark_exec_report_num_time_samples (micro_benchmark_exec_report);

/**
 * \brief Time sample collected.
 *
 * \param report Result of a test case execution.
 * \param pos Sample position.  Must be less than
 *            \code{micro_benchmark_exec_report_num_time_samples}.
 * \return Time sample.
 */
micro_benchmark_time_sample
micro_benchmark_exec_report_get_time_sample (micro_benchmark_exec_report,
                                             size_t);

/**
 * \brief Iterations measured on this test execution.
 *
 * \param report Report of a test case execution
 * \return Number of iterations measured.
 */
size_t
micro_benchmark_exec_report_get_iterations (micro_benchmark_exec_report);

/**
 * \brief Total iterations performed on this test execution.
 *
 * \param report Report of a test case execution
 * \return Number of iterations performed.
 */
size_t
micro_benchmark_exec_report_total_iterations (micro_benchmark_exec_report);

/**
 * \brief Total time of the test execution.
 *
 * \param report Report of a test case execution
 * \return Total excecution time.
 */
micro_benchmark_clock_time
micro_benchmark_exec_report_total_time (micro_benchmark_exec_report);

/**
 * \brief Total samples.
 *
 * \param report Result of a test case execution.
 * \return Number of samples taken, including discarded ones.
 */
size_t
micro_benchmark_exec_report_total_samples (micro_benchmark_exec_report);

/**
 * \brief Samples used for the calculations.
 *
 * \param report Result of a test case execution.
 * \return Number of samples taken, including discarded ones.
 */
size_t micro_benchmark_exec_report_used_samples (micro_benchmark_exec_report);

/**
 * \brief Time per iteration.
 *
 * \param report Result of a test case execution.
 * \return Aggregated data, iteration time.
 */
micro_benchmark_stats_value
micro_benchmark_exec_report_iteration_time (micro_benchmark_exec_report);

/**
 * \brief Time per sample.
 *
 * \param report Result of a test case execution.
 * \return Aggregated data, sample time.
 */
micro_benchmark_stats_value
micro_benchmark_exec_report_sample_time (micro_benchmark_exec_report);

/**
 * \brief Iterations per sample.
 *
 * \param report Result of a test case execution.
 * \return Aggregated data, sample iterations.
 */
micro_benchmark_stats_value
micro_benchmark_exec_report_sample_iterations (micro_benchmark_exec_report);

/**
 * \brief Number of custom meters and collectors.
 *
 * \param r Report of a test case execution.
 * \return Number of custom meters used on the test execution.
 */
size_t
micro_benchmark_exec_report_number_of_meters (micro_benchmark_exec_report r);

/**
 * \brief Number of samples.
 *
 * \param report Result of a test case execution.
 * \param meter Meter position.  Must be less than
 *              \code{micro_benchmark_exec_report_number_of_meters}.
 * \return Number of samples on this report.
 */
size_t
micro_benchmark_exec_report_meter_num_samples (micro_benchmark_exec_report,
                                               size_t);

/**
 * \brief Sample collected.
 *
 * \param report Result of a test case execution.
 * \param meter Meter position.  Must be less than
 *              \code{micro_benchmark_exec_report_number_of_meters}.
 * \param pos Sample position.  Must be less than
 *            \code{micro_benchmark_exec_report_meter_num_samples}.
 * \return Sample collected by the meter.
 */
micro_benchmark_stats_generic_sample
micro_benchmark_exec_report_meter_get_sample (micro_benchmark_exec_report,
                                              size_t, size_t);

/**
 * \brief Sample type.
 *
 * \param report Result of a test case execution.
 * \param meter Meter position.  Must be less than
 *              \code{micro_benchmark_exec_report_number_of_meters}.
 * \return Type of the sample of this meter.
 */
micro_benchmark_stats_sample_type
micro_benchmark_exec_report_meter_sample_type (micro_benchmark_exec_report,
                                               size_t);

/**
 * \brief Data accumulated by a provided meter and collector.
 *
 * \param r Report of a test case execution.
 * \param pos Must be less than micro_benchmark_exec_report_number_of_meters
 * \return Data stored by the custom meter.
 */
micro_benchmark_report_extra_data
micro_benchmark_exec_report_get_extra_data (micro_benchmark_exec_report r,
                                            size_t pos);


MICRO_BENCHMARK_END_DECLS
#endif
