/* io.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_REPORT_IO_H
#define MICRO_BENCHMARK_REPORT_IO_H

#include "mbenchmark/common.h"
#include "mbenchmark/report/suite.h"

#include <stddef.h>
#include <stdio.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Available formats. */
typedef enum micro_benchmark_output_type
{
  MICRO_BENCHMARK_CONSOLE_OUTPUT,
  MICRO_BENCHMARK_TEXT_OUTPUT,
  MICRO_BENCHMARK_LISP_OUTPUT
} micro_benchmark_output_type;

/** Available statistical values. */
typedef enum micro_benchmark_output_stat
{
  MICRO_BENCHMARK_STAT_NONE = 0,
  MICRO_BENCHMARK_STAT_MEAN = 1,
  MICRO_BENCHMARK_STAT_VARIANCE = 2,
  MICRO_BENCHMARK_STAT_STD_DEVIATION = 4,
  MICRO_BENCHMARK_STAT_BASIC = 5,
  MICRO_BENCHMARK_STAT_ALL = 7
} micro_benchmark_output_stat;

/** Available output values. */
typedef struct micro_benchmark_output_values
{
  bool self_test;
  bool size_constraints;
  bool total_time;
  bool total_iterations;
  bool iterations;
  bool total_samples;
  bool samples;
  bool extra_data;
  micro_benchmark_output_stat iteration_time;
  micro_benchmark_output_stat sample_time;
  micro_benchmark_output_stat sample_iterations;
} micro_benchmark_output_values;

/** Values printed by default.  */
micro_benchmark_output_values
micro_benchmark_get_default_output_values (void);

/** Set the values printed by default. */
void
micro_benchmark_set_default_output_values (micro_benchmark_output_values);

/**
 * Write the report to a file with the provided format.
 *
 * \param report Result of the suite execution.
 * \param out File where the results will be written.
 * \param type Format of the output.
 */
void
micro_benchmark_write_report (micro_benchmark_report report,
                              FILE *out, micro_benchmark_output_type type);

/**
 * Write the report to a file with the provided format and the customized values.
 *
 * \param report Result of the suite execution.
 * \param out File where the results will be written.
 * \param type Format of the output.
 * \param values Values from the record to be written.
 */
void
micro_benchmark_write_custom_report (micro_benchmark_report report,
                                     FILE *out,
                                     micro_benchmark_output_type t,
                                     const micro_benchmark_output_values *v);

/**
 * Print the report with the default format on the standard output.
 *
 * \param report Report of a suite execution.
 */
void micro_benchmark_print_report (micro_benchmark_report report);

/**
 * Print the report with the default format on the standard output.
 *
 * \param report Report of a suite execution.
 * \param report Custom values to print.
 * \param values Values from the record to be written.
 */
void micro_benchmark_print_custom_report (micro_benchmark_report report,
                                          const micro_benchmark_output_values
                                          *v);

MICRO_BENCHMARK_END_DECLS
#endif
