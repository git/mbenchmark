/* test.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_REPORT_TEST_H
#define MICRO_BENCHMARK_REPORT_TEST_H

#include "mbenchmark/common.h"
#include "mbenchmark/report/exec.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Internal representation for the report of a test case.  */
struct micro_benchmark_test_report_;

/** Test report. Its lifetime is associated to the report.  */
typedef const struct micro_benchmark_test_report_
  *micro_benchmark_test_report;

/**
 * \brief Name of the test case.
 *
 * \param report Report of a test case.
 * \return The name of the test case.
 */
/* *INDENT-OFF* */
const char *
micro_benchmark_test_report_get_name (micro_benchmark_test_report);
/* *INDENT-ON* */

/**
 * \brief Number of executions.
 *
 * \param report Report of a test case.
 * \return The number of different executions of the test case.
 */
size_t
micro_benchmark_test_report_get_num_executions (micro_benchmark_test_report);

/**
 * \brief Execution report accessor.
 *
 * \param report Report of a test case.
 * \param pos Number of the execution.  Must be less than the number
 *            of executions.
 * \return Test execution result.
 */
micro_benchmark_exec_report
micro_benchmark_test_report_get_exec_report (micro_benchmark_test_report,
                                             size_t);

MICRO_BENCHMARK_END_DECLS
#endif
