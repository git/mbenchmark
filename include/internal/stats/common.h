/* common.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_STATS_COMMON_H
#define MICRO_BENCHMARK_INTERNAL_STATS_COMMON_H

#include "mbenchmark/common.h"
#include "mbenchmark/stats.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

size_t mbstats_get_num_samples_ (size_t, micro_benchmark_time_samples);
size_t mbstats_get_num_iterations_ (size_t, micro_benchmark_time_samples);
void mbstats_fill_empty_ (micro_benchmark_stats_value *);
void mbstats_set_use_variance_estimator_ (bool b);
bool mbstats_use_variance_estimator_ (void);

MICRO_BENCHMARK_END_DECLS
#endif
