/* collector.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_STATS_COLLECTOR_H
#define MICRO_BENCHMARK_INTERNAL_STATS_COLLECTOR_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/timer.h"
#include "mbenchmark/stats/meter.h"
#include "mbenchmark/stats/sample.h"
#include "mbenchmark/stats/time-sample.h"
#include "mbenchmark/stats/values.h"

#include "internal/test.h"
#include "internal/constraints.h"
#include "internal/state/constraints.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_meter_sample_collector_
{
  /** Measure device used.  */
  micro_benchmark_meter_definition meter;

  /** Number of samples stored.  */
  size_t num_samples;

  /** Number of samples stored.  */
  size_t written_samples;

  /** Allocated array of samples.  */
  micro_benchmark_stats_generic_sample_data *samples;
} micro_benchmark_meter_sample_collector_;

typedef struct micro_benchmark_time_sample_collector_
{
  /** Measure device used.  */
  micro_benchmark_meter chrono;

  /** Number of time samples stored.  */
  size_t num_samples;

  /** Number of time samples stored.  */
  size_t written_samples;

  /** Allocated array of samples.  */
  struct micro_benchmark_time_sample_ *samples;
} micro_benchmark_time_sample_collector_;

/**
 * \brief Internal representation of a test state.
 */
typedef struct micro_benchmark_stats_collector_
{
  /** Flag of the measuring process.  */
  bool measuring;

  /** Flag to start counting total_iterations.  */
  bool has_skipped_iterations;

  /** Total number of execution iterations.  */
  size_t execution_iterations;

  /** Current number of iterations.  */
  size_t iterations;

  /** Chronometer running the full duration of the test.  */
  micro_benchmark_meter full_chrono;

  /** Sample of the complete execution.  */
  struct micro_benchmark_time_sample_ full_sample;

  /** Sample of the complete execution.  */
  micro_benchmark_clock_time deadline_time;

  /** Timer for the total test execution.  */
  micro_benchmark_timer deadline_timer;

  /** Initial deadline provided to the sample timer.  */
  micro_benchmark_clock_time initial_sample_time;

  /** Current deadline of the sample timer.  */
  micro_benchmark_clock_time sample_time;

  /** Timer for sample collection.  */
  micro_benchmark_timer sample_timer;

  /** Time collection.  */
  micro_benchmark_time_sample_collector_ clock;

  /** Number of measure devices stored.  */
  size_t num_meters;

  /** Custom measuring devices.  */
  micro_benchmark_meter_sample_collector_ *meters;

} micro_benchmark_stats_collector_;

typedef micro_benchmark_stats_collector_ *mbstats_collector_;
typedef micro_benchmark_stats_generic_samples mb_samples_;

void micro_benchmark_stats_collector_init_ (mbstats_collector_,
                                            mbconstraints_ *);

void micro_benchmark_stats_collector_release_ (mbstats_collector_);

void micro_benchmark_stats_collector_start_test_ (mbstats_collector_);

void micro_benchmark_stats_collector_end_test_ (mbstats_collector_);

void micro_benchmark_stats_collector_start_ (mbstats_collector_);

void micro_benchmark_stats_collector_stop_ (mbstats_collector_);

void micro_benchmark_stats_collector_restart_ (mbstats_collector_);

bool micro_benchmark_stats_collected_enough_ (mbstats_collector_,
                                              mbstate_constraints_ *);

void
micro_benchmark_stats_collected_time_samples_ (mbstats_collector_,
                                               micro_benchmark_time_samples *,
                                               size_t *);

micro_benchmark_time_sample
micro_benchmark_stats_collected_full_sample_ (mbstats_collector_);

size_t micro_benchmark_stats_collected_num_meters_ (mbstats_collector_);

void micro_benchmark_stats_collected_samples_from_meter_ (mbstats_collector_,
                                                          size_t,
                                                          mb_samples_ *,
                                                          size_t *);


MICRO_BENCHMARK_END_DECLS
#endif
