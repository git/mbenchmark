/* log.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_UTILITY_LOG_H
#define MICRO_BENCHMARK_INTERNAL_UTILITY_LOG_H

#include "mbenchmark/common.h"
#include "mbenchmark/utility/log.h"

#include <stddef.h>
#include <stdio.h>

#define MB_LOG(level, ...) \
  micro_benchmark_log_ (level, MICRO_BENCHMARK_MODULE, __func__, __VA_ARGS__)

#define MB_ERROR(...) \
  MB_LOG(MICRO_BENCHMARK_ERROR_LEVEL, __FILE__, __LINE__, __VA_ARGS__)
#define MB_WARN(...) \
  MB_LOG(MICRO_BENCHMARK_WARNING_LEVEL, __FILE__, __LINE__, __VA_ARGS__)
#define MB_INFO(...) \
  MB_LOG(MICRO_BENCHMARK_INFO_LEVEL, __FILE__, __LINE__, __VA_ARGS__)
#define MB_DEBUG(...) \
  MB_LOG(MICRO_BENCHMARK_DEBUG_LEVEL, __FILE__, __LINE__, __VA_ARGS__)
#ifdef MICRO_BENCHMARK_BUILD_WITHOUT_TRACES
#define MB_TRACE(...) do {} while (0)
#else
#define MB_TRACE(...) \
  MB_LOG(MICRO_BENCHMARK_TRACE_LEVEL, __FILE__, __LINE__, __VA_ARGS__)
#endif

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Write log message.  */
#ifdef __GNUC__
/* *INDENT-OFF* */
__attribute__((format (printf, 6, 7)))
/* *INDENT-ON* */
#endif
void
micro_benchmark_log_ (micro_benchmark_log_level, const char *,
                      const char *, const char *, size_t, const char *, ...);

MICRO_BENCHMARK_END_DECLS
#endif
