/* xalloc.h --- Abort when there is no memory. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_UTILITY_XALLOC_H
#define MICRO_BENCHMARK_INTERNAL_UTILITY_XALLOC_H

#include <stddef.h>

#include "mbenchmark/common.h"
#include "internal/utility/error.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

#include <assert.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>

/** Memory allocation with error checking.  */
MICRO_BENCHMARK_INLINE void *
xmalloc (size_t size)
{
  assert (size > 0);
  void *ret = malloc (size);
  MB_HANDLE_ERROR (!ret, "Error allocating memory");
  return ret;
}

/** Array allocation with error checking.  */
MICRO_BENCHMARK_INLINE void *
xmalloc_n (size_t size, size_t n)
{
  assert (size > 0);
  assert (SIZE_MAX / size > n);
  return xmalloc (size * n);
}

/** Zeroed array allocation with error checking.  */
MICRO_BENCHMARK_INLINE void *
xcalloc_n (size_t size, size_t n)
{
  assert (size > 0);
  void *ret = calloc (n, size);
  MB_HANDLE_ERROR (!ret, "Error allocating memory");
  return ret;
}

/** Zeroed object allocation with error checking.  */
MICRO_BENCHMARK_INLINE void *
xcalloc (size_t size)
{
  assert (size > 0);
  return xcalloc_n (size, 1);
}

/** Array reallocation with error checking.  */
MICRO_BENCHMARK_INLINE void *
xrealloc_n (void *ptr, size_t size, size_t n)
{
  assert (ptr);
  assert (size > 0);
  assert (n > 0);
  assert (SIZE_MAX / size > n);
  void *ret = realloc (ptr, size * n);
  MB_HANDLE_ERROR (!ret, "Error reallocating memory");
  return ret;
}

/** Duplicate string.  */
MICRO_BENCHMARK_INLINE char *
xstrdup (const char *str)
{
  assert (str);
#ifdef HAVE_STRDUP
  char *ret = strdup (str);
  MB_HANDLE_ERROR (!ret, "Error duplicating string");
#else
  size_t size = strlen (str) + 1;
  char *ret = xmalloc (size);
  memcpy (ret, str, size);
#endif
  return ret;
}

/** Relase allocated memory.  */
MICRO_BENCHMARK_INLINE void
xfree (void *ptr, size_t size)
{
  assert (ptr);
  assert (size > 0);
  (void) size;
  free (ptr);
}

/** Relase allocated array.  */
MICRO_BENCHMARK_INLINE void
xfree_n (void *ptr, size_t size, size_t n)
{
  assert (ptr);
  assert (size > 0);
  assert (n > 0);
  (void) size;
  (void) n;
  free (ptr);
}

MICRO_BENCHMARK_END_DECLS
#endif
