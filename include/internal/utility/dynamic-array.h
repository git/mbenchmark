/* dynamic-array.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_UTILITY_DYNAMIC_ARRAY_H
#define MICRO_BENCHMARK_INTERNAL_UTILITY_DYNAMIC_ARRAY_H

#include "mbenchmark/common.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>

#define MICRO_BENCHMARK_DYN_ARRAY(type) \
  struct \
  { \
    size_t num_allocated; \
    size_t num_values; \
    type *values; \
  }

#define MICRO_BENCHMARK_DYN_ARRAY_INIT(arr, type, default_alloc) \
  do \
    { \
      assert (default_alloc > 0); \
      arr.num_allocated = default_alloc; \
      arr.num_values = 0; \
      arr.values = xmalloc_n (sizeof (type), default_alloc); \
    } \
  while (0)

#define MICRO_BENCHMARK_DYN_ARRAY_SIZE(arr) arr.num_values
#define MICRO_BENCHMARK_DYN_ARRAY_VALUE(arr, pos) arr.values[pos]
#define MICRO_BENCHMARK_DYN_ARRAY_VALUES(arr) arr.values
#define MICRO_BENCHMARK_DYN_ARRAY_LAST(arr) arr.values[arr.num_values - 1]

#define MICRO_BENCHMARK_DYN_ARRAY_PUSH_RAW(arr, type) \
  do \
    { \
      assert (arr.num_allocated >= arr.num_values); \
      assert (arr.num_allocated > 0); \
      if (arr.num_allocated == arr.num_values) \
	{ \
          size_t new_n = arr.num_allocated * 2; \
	  MB_HANDLE_ERROR (arr.num_allocated > new_n, "Size overflow"); \
	  arr.values = xrealloc_n (arr.values, sizeof (type), new_n); \
          arr.num_allocated = new_n; \
        } \
      arr.num_values++; \
    } \
  while (0)

#define MICRO_BENCHMARK_DYN_ARRAY_PUSH(arr, type, init, ...) \
  do \
    { \
      MICRO_BENCHMARK_DYN_ARRAY_PUSH_RAW(arr, type); \
      init (&MICRO_BENCHMARK_DYN_ARRAY_LAST(arr), __VA_ARGS__);	\
    } \
  while (0)

#define MICRO_BENCHMARK_DYN_ARRAY_RELEASE_RAW(arr, type) \
  do \
    { \
      xfree_n (arr.values, sizeof (type), arr.num_allocated); \
      arr.num_allocated = 0; \
      arr.num_values = 0; \
      arr.values = 0; \
    } \
  while (0)

#define MICRO_BENCHMARK_DYN_ARRAY_RELEASE(arr, type, cleanup) \
  do \
    { \
      for (size_t i = 0; i < arr.num_values; ++i) \
	cleanup (&arr.values[i]); \
      MICRO_BENCHMARK_DYN_ARRAY_RELEASE_RAW(arr, type); \
    } \
  while (0)

#endif
