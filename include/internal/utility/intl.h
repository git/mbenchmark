/* intl.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_INTERNAL_UTILITY_INTL_H
#define MICRO_BENCHMARK_INTERNAL_UTILITY_INTL_H

#ifdef ENABLE_NLS
#include <libintl.h>
#define N_(x) x
#define P_(s, pl, n) dngettext(PACKAGE, s, pl, n)
#define _(x) dgettext(PACKAGE, x)
#else
#define N_(x) x
#define _(x) x
#define P_(s, pl, n) (n == 1 ? s : pl)
#endif

#endif
