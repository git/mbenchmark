/* error.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_UTILITY_ERROR_H
#define MICRO_BENCHMARK_INTERNAL_UTILITY_ERROR_H

#include "mbenchmark/common.h"

#ifndef MB_HANDLE_ERROR
#define MB_HANDLE_ERROR(b, m) if (b) micro_benchmark_handle_error_ (m)
#endif

#ifdef __GNUC__
#define MB_ATTR_NO_RETURN __attribute__ ((noreturn))
#else
#define MB_ATTR_NO_RETURN
#endif

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/* *INDENT-OFF* */
void micro_benchmark_handle_error_ (const char *msgid) MB_ATTR_NO_RETURN;
/* *INDENT-ON* */

MICRO_BENCHMARK_END_DECLS
#endif
