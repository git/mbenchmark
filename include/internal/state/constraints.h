/* constraints.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_STATE_CONSTRAINTS_H
#define MICRO_BENCHMARK_INTERNAL_STATE_CONSTRAINTS_H

#include "mbenchmark/common.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Fixture constraints.
 */
typedef struct micro_benchmark_test_state_constraints_
{
  /** Number of size-constrained dimensions.  */
  size_t dimensions;

  /** Array of size constraints.  */
  size_t *sizes;

  /** Number of iterations to skip before taking samples.  */
  size_t iterations_to_skip;

  /** Minimum number of iterations.  */
  size_t min_iterations;

  /** Maximum number of iterations.  */
  size_t max_iterations;

  /** Constraints on number of iterations per sample.  */
  bool has_sample_iterations;

  /** Minimum number of iterations per sample.  */
  size_t min_sample_iterations;

  /** Maximum number of iterations per sample.  */
  size_t max_sample_iterations;
} mbstate_constraints_;

MICRO_BENCHMARK_END_DECLS
#endif
