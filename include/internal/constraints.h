/* constraints.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_H
#define MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_H

#include "mbenchmark/common.h"
#include "internal/constraints/definition.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

void mbconstraints_init_ (mbconstraints_ *);
void mbconstraints_cleanup_ (mbconstraints_ *);

mbconstraints_iteration_ *mbconstraints_get_iteration_ (mbconstraints_ *);
mbconstraints_meter_ *mbconstraints_get_meter_ (mbconstraints_ *);
mbconstraints_size_ *mbconstraints_get_size_ (mbconstraints_ *);
mbconstraints_time_ *mbconstraints_get_time_ (mbconstraints_ *);

MICRO_BENCHMARK_END_DECLS
#endif
