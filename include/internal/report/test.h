/* test.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_INTERNAL_REPORT_TEST_H
#define MICRO_BENCHMARK_INTERNAL_REPORT_TEST_H

#include "mbenchmark/common.h"
#include "mbenchmark/test.h"
#include "mbenchmark/report/test.h"
#include "mbenchmark/report/exec.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_test_report_
{
  /** Test case associated to the report.  */
  micro_benchmark_test_case test;

  /** Number of executions of the test case.  */
  size_t num_executions;

  /** Array of execution reports.  */
  struct micro_benchmark_exec_report_ *executions;

} micro_benchmark_test_report_;

void
micro_benchmark_test_report_init_ (micro_benchmark_test_report_ *report,
                                   micro_benchmark_test_case test);

void
micro_benchmark_test_report_cleanup_ (micro_benchmark_test_report_ *report);

void
micro_benchmark_test_report_allocate_executions_ (micro_benchmark_test_report_
                                                  *, size_t n_executions);

MICRO_BENCHMARK_END_DECLS
#endif
