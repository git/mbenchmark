/* suite.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_INTERNAL_REPORT_SUITE_H
#define MICRO_BENCHMARK_INTERNAL_REPORT_SUITE_H

#include "mbenchmark/common.h"
#include "mbenchmark/report/suite.h"
#include "internal/report/test.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_report_
{
  /** Suite name.  */
  const char *name;

  /** Number of tests reported.  */
  size_t num_tests;

  /** Array of test reports.  */
  micro_benchmark_test_report_ *tests;

} micro_benchmark_report_;

void micro_benchmark_report_init_ (micro_benchmark_report_ *r,
                                   const char *name);

void micro_benchmark_report_cleanup_ (micro_benchmark_report r);

void micro_benchmark_report_allocate_tests_ (micro_benchmark_report_ *r,
                                             micro_benchmark_test_case tests,
                                             size_t n_tests);

/* *INDENT-OFF* */
micro_benchmark_test_report_ *
micro_benchmark_report_get_test_report_ (micro_benchmark_report_ *, size_t);
/* *INDENT-ON* */

MICRO_BENCHMARK_END_DECLS
#endif
