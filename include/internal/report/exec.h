/* exec.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_INTERNAL_REPORT_EXEC_H
#define MICRO_BENCHMARK_INTERNAL_REPORT_EXEC_H

#include "mbenchmark/common.h"
#include "mbenchmark/report/exec.h"
#include "mbenchmark/report/extra.h"
#include "mbenchmark/stats/custom.h"
#include "mbenchmark/stats/time-sample.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_exec_report_extra_data_
{
  void *data;
  size_t num_samples;
  micro_benchmark_stats_generic_sample_data *samples;
  micro_benchmark_custom_sample_collector collector;
} micro_benchmark_exec_report_extra_data_;

typedef struct micro_benchmark_exec_report_
{
  /** Full name of the test execution.  */
  char *name;

  /** Full name of the test execution.  */
  size_t name_size;

  /** Number of size-constrained dimensions.  */
  size_t dimensions;

  /** Size constraints.  */
  size_t *sizes;

  /** Total sample of the execution.  */
  struct micro_benchmark_time_sample_ full_sample;

  /** Time statistics.  */
  micro_benchmark_time_stats_values time_stats;

  /** Number of time samples.  */
  size_t num_time_samples;

  /** Collected time samples.  */
  struct micro_benchmark_time_sample_ *time_samples;

  /** Number of custom meters.  */
  size_t num_custom_meters;

  /** Samples retrieved by the custom meters.  */
  micro_benchmark_exec_report_extra_data_ *custom_meters;

} micro_benchmark_exec_report_;

void
micro_benchmark_exec_report_init_ (micro_benchmark_exec_report_ *report,
                                   const char *name, size_t dimensions,
                                   size_t *sizes);

void micro_benchmark_exec_report_set_name_ (micro_benchmark_exec_report_ *r,
                                            const char *name);

void
micro_benchmark_exec_report_release_ (micro_benchmark_exec_report report);

void
micro_benchmark_exec_report_add_custom_ (micro_benchmark_exec_report_ *r,
                                         void *extra_data, size_t,
                                         micro_benchmark_stats_generic_samples,
                                         micro_benchmark_custom_sample_collector);
void
micro_benchmark_exec_report_set_full_sample_ (micro_benchmark_exec_report_ *,
                                              micro_benchmark_time_sample);

void
micro_benchmark_exec_report_set_time_samples_ (micro_benchmark_exec_report_ *,
                                               size_t,
                                               micro_benchmark_time_samples);

void
micro_benchmark_exec_report_set_time_stats_ (micro_benchmark_exec_report_ *,
                                             micro_benchmark_time_stats_values);

MICRO_BENCHMARK_END_DECLS
#endif
