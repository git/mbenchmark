/* iteration.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_ITERATION_H
#define MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_ITERATION_H

#include "mbenchmark/common.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_test_iteration_constraint_
{
  size_t iterations_to_skip;
  bool has_iterations;
  size_t min_iterations;
  size_t max_iterations;
  bool has_sample_iterations;
  size_t min_sample_iterations;
  size_t max_sample_iterations;
} mbconstraints_iteration_;

void mbconstraints_skip_iterations_ (mbconstraints_iteration_ *, size_t);
size_t mbconstraints_iterations_to_skip_ (const mbconstraints_iteration_ *);

void mbconstraints_limit_iterations_ (mbconstraints_iteration_ *, size_t,
                                      size_t);
bool mbconstraints_has_iterations_ (const mbconstraints_iteration_ *);
size_t mbconstraints_min_iterations_ (const mbconstraints_iteration_ *);
size_t mbconstraints_max_iterations_ (const mbconstraints_iteration_ *);

void mbconstraints_limit_samples_ (mbconstraints_iteration_ *, size_t,
                                   size_t);
bool mbconstraints_has_sample_iterations_ (const mbconstraints_iteration_ *);
size_t
mbconstraints_min_sample_iterations_ (const mbconstraints_iteration_ *);
size_t
mbconstraints_max_sample_iterations_ (const mbconstraints_iteration_ *);

MICRO_BENCHMARK_END_DECLS
#endif
