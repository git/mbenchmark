/* definition.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_DEFINITION_H
#define MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_DEFINITION_H

#include "mbenchmark/common.h"
#include "internal/constraints/iteration.h"
#include "internal/constraints/meter.h"
#include "internal/constraints/size.h"
#include "internal/constraints/time.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_test_constraints_
{
  mbconstraints_iteration_ iteration;
  mbconstraints_meter_ meter;
  mbconstraints_size_ size;
  mbconstraints_time_ time;
} mbconstraints_;

typedef mbconstraints_ *mbconstraintsp_;

MICRO_BENCHMARK_END_DECLS
#endif
