/* meter.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_METER_H
#define MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_METER_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/chrono-provider.h"
#include "mbenchmark/stats/custom.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_test_meter_constraint_
{
  /** Whether a custom chronometer has been provided.  */
  bool has_chronometer;
  /** Type of the clock used for the chronometer.  */
  micro_benchmark_clock_type chrono_type;
  /** Provider of the chronometer.  */
  micro_benchmark_chronometer_provider chrono_provider;
  /** User provided chronometer.  */
  micro_benchmark_meter_definition chrono_tmpl;

  /** Enable custom chronometer stats calculator.  */
  bool has_custom_stats;
  /** Function called to aggregate the collected data.  */
  micro_benchmark_custom_time_calculator custom_stats;

  /** Enable custom sample meters.  */
  bool has_custom_meters;
  /** Number of custom sample meters provided.  */
  size_t num_custom_meters;
  /** Array of custom sample meters provided.  */
  micro_benchmark_custom_meter *custom_meters;
} mbconstraints_meter_;

void mbconstraints_meter_cleanup_ (mbconstraints_meter_ *);

bool mbconstraints_has_chrono_ (const mbconstraints_meter_ *);

micro_benchmark_clock_type
mbconstraints_get_chrono_type_ (const mbconstraints_meter_ *);

micro_benchmark_chronometer_provider
mbconstraints_get_chrono_provider_ (const mbconstraints_meter_ *);

micro_benchmark_meter
mbconstraints_get_chrono_tmpl_ (const mbconstraints_meter_ *);

void mbconstraints_set_chrono_ (mbconstraints_meter_ *,
                                micro_benchmark_clock_type,
                                micro_benchmark_chronometer_provider);

void mbconstraints_set_custom_chrono_ (mbconstraints_meter_ *,
                                       micro_benchmark_clock_type,
                                       micro_benchmark_meter);

bool mbconstraints_has_calculator_ (const mbconstraints_meter_ *);

micro_benchmark_custom_time_calculator
mbconstraints_get_calculator_ (const mbconstraints_meter_ *);

void mbconstraints_set_calculator_ (mbconstraints_meter_ *,
                                    micro_benchmark_custom_time_calculator);

void mbconstraints_add_meter_ (mbconstraints_meter_ *,
                               micro_benchmark_custom_meter);

size_t mbconstraints_num_meters_ (const mbconstraints_meter_ *);

micro_benchmark_custom_meter
mbconstraints_get_meter_n_ (const mbconstraints_meter_ *, size_t);

MICRO_BENCHMARK_END_DECLS
#endif
