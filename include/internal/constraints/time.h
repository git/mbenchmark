/* time.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_TEST_CONSTRAINTS_TIME_H
#define MICRO_BENCHMARK_INTERNAL_TEST_CONSTRAINTS_TIME_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/time.h"
#include "mbenchmark/clock/timer-provider.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_test_timer_constraint_
{
  bool has_timer;
  micro_benchmark_clock_type type;
  micro_benchmark_timer_provider provider;
  micro_benchmark_timer_definition tmpl;
  micro_benchmark_chronometer_provider meter;
  micro_benchmark_meter_definition meter_tmpl;
  void *extra;
} mbconstraints_timer_;

typedef struct micro_benchmark_test_time_constraint_
{
  bool has_deadline;
  micro_benchmark_clock_time deadline;
  mbconstraints_timer_ deadline_timer;

  bool has_sample_time;
  micro_benchmark_clock_time sample_time;
  mbconstraints_timer_ sample_timer;
} mbconstraints_time_;


bool mbconstraints_has_max_time_ (const mbconstraints_time_ *);

micro_benchmark_clock_time
mbconstraints_get_max_time_ (const mbconstraints_time_ *);

void mbconstraints_set_max_time_ (mbconstraints_time_ *,
                                  micro_benchmark_clock_time);

bool mbconstraints_has_sample_time_ (const mbconstraints_time_ *);

micro_benchmark_clock_time
mbconstraints_get_sample_time_ (const mbconstraints_time_ *);

void mbconstraints_set_sample_time_ (mbconstraints_time_ *,
                                     micro_benchmark_clock_time);

bool mbconstraints_has_timer_ (const mbconstraints_time_ *);

micro_benchmark_clock_type
mbconstraints_get_timer_type_ (const mbconstraints_time_ *);

micro_benchmark_timer_provider
mbconstraints_get_timer_provider_ (const mbconstraints_time_ *);

micro_benchmark_timer
mbconstraints_get_timer_tmpl_ (const mbconstraints_time_ *);

micro_benchmark_chronometer_provider
mbconstraints_get_timer_meter_ (const mbconstraints_time_ *);

micro_benchmark_meter
mbconstraints_get_timer_meter_tmpl_ (const mbconstraints_time_ *);

void mbconstraints_set_timer_ (mbconstraints_time_ *,
                               micro_benchmark_clock_type,
                               micro_benchmark_timer_provider);

void mbconstraints_set_custom_timer_ (mbconstraints_time_ *,
                                      micro_benchmark_clock_type,
                                      micro_benchmark_timer, void *);

void mbconstraints_set_timer_from_meter_ (mbconstraints_time_ *,
                                          micro_benchmark_clock_type,
                                          micro_benchmark_chronometer_provider);

MICRO_BENCHMARK_END_DECLS
#endif
