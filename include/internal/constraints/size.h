/* size.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_SIZE_H
#define MICRO_BENCHMARK_INTERNAL_CONSTRAINTS_SIZE_H

#include "mbenchmark/common.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_test_size_dimension_
{
  size_t num_sizes;
  size_t *sizes;
} mbconstraints_size_dimension_;

typedef struct micro_benchmark_test_size_constraint_
{
  bool has_size;
  size_t num_dimensions;
  mbconstraints_size_dimension_ *dimensions;
} mbconstraints_size_;

void mbconstraints_size_cleanup_ (mbconstraints_size_ *);
void mbconstraints_add_dimension_ (mbconstraints_size_ *, size_t,
                                   const size_t *);
size_t mbconstraints_dimensions_ (const mbconstraints_size_ *);
void mbconstraints_get_dimension_ (const mbconstraints_size_ *, size_t,
                                   size_t *, const size_t **);

MICRO_BENCHMARK_END_DECLS
#endif
