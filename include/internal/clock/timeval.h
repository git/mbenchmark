/* timeval.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef MICRO_BENCHMARK_INTERNAL_CLOCK_TIMEVAL_H
#define MICRO_BENCHMARK_INTERNAL_CLOCK_TIMEVAL_H

#ifndef HAVE_STRUCT_TIMEVAL
#error "Struct timeval is not available"
#endif

#include "mbenchmark/common.h"

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <time.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Adjust \code{t->tv_usec} value on the range [0, 1000000).  */
void micro_benchmark_timeval_adjust_ (struct timeval *t);

/** \code{res} = \code{lhs} + \code{rhs}  */
void micro_benchmark_timeval_add_ (struct timeval *res,
                                   const struct timeval *lhs,
                                   const struct timeval *rhs);

/** \code{res} = \code{lhs} - \code{rhs}  */
void micro_benchmark_timeval_sub_ (struct timeval *res,
                                   const struct timeval *lhs,
                                   const struct timeval *rhs);

/**
 * \code{lhs} cmp \code{rhs} ->
 *    - -1 if \code{lhs} < \code{rhs}
 *    - 0  if \code{lhs} == \code{rhs}
 *    - 1  if \code{lhs} > \code{rhs}
 * */
int micro_benchmark_timeval_cmp_ (const struct timeval *lhs,
                                  const struct timeval *rhs);

MICRO_BENCHMARK_END_DECLS
#endif
