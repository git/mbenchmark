/* chrono-adapter.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_CLOCK_CHRONO_ADAPTER_DEF_H
#define MICRO_BENCHMARK_INTERNAL_CLOCK_CHRONO_ADAPTER_DEF_H

#include "mbenchmark/common.h"
#include "mbenchmark/clock/timer.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

extern const micro_benchmark_timer_definition mbtimer_chrono_adapter_tmpl_;

MICRO_BENCHMARK_INLINE micro_benchmark_timer
micro_benchmark_timer_chrono_adapter_ (void)
{
  return &mbtimer_chrono_adapter_tmpl_;
}

MICRO_BENCHMARK_END_DECLS
#endif
