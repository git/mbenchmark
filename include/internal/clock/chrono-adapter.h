/* chrono-adapter.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_CLOCK_CHRONO_ADAPTER_H
#define MICRO_BENCHMARK_INTERNAL_CLOCK_CHRONO_ADAPTER_H

#include "mbenchmark/common.h"
#include "internal/clock/chrono-adapter-def.h"
#include "mbenchmark/stats/meter.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

micro_benchmark_timer
micro_benchmark_timer_adapt_chrono_ (micro_benchmark_clock_type type,
                                     micro_benchmark_meter meter);

micro_benchmark_timer
micro_benchmark_timer_clock_t_ (micro_benchmark_clock_type);

micro_benchmark_timer
micro_benchmark_timer_time_t_ (micro_benchmark_clock_type);

#ifdef HAVE_CLOCK_GETTIME
micro_benchmark_timer
micro_benchmark_timer_clock_gettime_ (micro_benchmark_clock_type);
#endif

#ifdef HAVE_GETTIMEOFDAY
micro_benchmark_timer
micro_benchmark_timer_gettimeofday_ (micro_benchmark_clock_type);
#endif

MICRO_BENCHMARK_END_DECLS
#endif
