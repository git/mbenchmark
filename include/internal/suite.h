/* suite.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_SUITE_H
#define MICRO_BENCHMARK_INTERNAL_SUITE_H

#include "mbenchmark/common.h"
#include "mbenchmark/suite.h"
#include "internal/report.h"
#include "internal/test.h"
#include "internal/utility/dynamic-array.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

typedef struct micro_benchmark_suite_
{
  /** Suite name.  */
  char *name;
  /** Size of the internally allocated name.  */
  size_t name_size;
  /** Flag active if self-test has been added.  */
  bool has_self_test;
  /** Test case array.  */
  /* *INDENT-OFF* */
  MICRO_BENCHMARK_DYN_ARRAY (micro_benchmark_test_case_definition) tests;
  /* *INDENT-ON* */
  /** Suite report.  */
  micro_benchmark_report_ report;
} micro_benchmark_suite_;

void micro_benchmark_suite_register_static_tests_ (micro_benchmark_suite);

bool micro_benchmark_enable_static_test_ (const char *name);

bool micro_benchmark_disable_static_test_ (const char *name);

void micro_benchmark_cleanup_registry_ (void);

MICRO_BENCHMARK_END_DECLS
#endif
