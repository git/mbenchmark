/* state.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_TEST_STATE_H
#define MICRO_BENCHMARK_INTERNAL_TEST_STATE_H

#include "mbenchmark/common.h"
#include "mbenchmark/test.h"
#include "mbenchmark/report/test.h"

#include "internal/state/constraints.h"
#include "internal/stats/collector.h"
#include "internal/report/exec.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * \brief Internal representation of a test state.
 */
typedef struct micro_benchmark_test_state_
{
  /** Test associated to this execution state.  */
  micro_benchmark_test_case test;

  /** Constraints for this fixture.  */
  mbstate_constraints_ constraints;

  /** Stats collector.  */
  micro_benchmark_stats_collector_ stats;

  /** Time stats calculator.  */
  micro_benchmark_custom_time_calculator calculate_stats;

  /** User data pointer.  */
  void *data;

  /** Name of the test associated to this execution.  */
  const char *test_name;

  /** User provided name.  */
  char *name;

  /** User provided name size.  */
  size_t name_size;
} micro_benchmark_test_state_;

void
micro_benchmark_state_init_ (micro_benchmark_test_state state,
                             micro_benchmark_test_case test,
                             size_t dimensions, const size_t *sizes,
                             const char *name);

void micro_benchmark_state_release_ (micro_benchmark_test_state state);

void
micro_benchmark_state_write_report_ (micro_benchmark_test_state,
                                     micro_benchmark_exec_report_ *);

void micro_benchmark_state_set_data_ (micro_benchmark_test_state, void *);

MICRO_BENCHMARK_END_DECLS
#endif
