/* common.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_OUTPUT_COMMON_H
#define MICRO_BENCHMARK_INTERNAL_OUTPUT_COMMON_H

#include "mbenchmark/common.h"
#include "mbenchmark/stats/values.h"

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Localized string for time unit \code{t}.  */
const char *micro_benchmark_time_units_to_string_ (micro_benchmark_stats_unit
                                                   t);

/** Localized string for time unit \code{t} squared.  */
const char
  *micro_benchmark_time_units_sqrd_to_string_ (micro_benchmark_stats_unit t);

/** Localized string for iteration units.  */
const char
  *micro_benchmark_iteration_units_to_string_ (micro_benchmark_stats_unit t);

MICRO_BENCHMARK_END_DECLS
#endif
