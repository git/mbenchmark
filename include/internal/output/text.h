/* text.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_OUTPUT_TEXT_H
#define MICRO_BENCHMARK_INTERNAL_OUTPUT_TEXT_H

#include "mbenchmark/common.h"
#include "mbenchmark/report.h"
#include "mbenchmark/report/io.h"

#include <stdio.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/** Write report with text format.  */
void
micro_benchmark_report_write_text_ (micro_benchmark_report, FILE *,
                                    const micro_benchmark_output_values *);


MICRO_BENCHMARK_END_DECLS
#endif
