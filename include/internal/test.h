/* test.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef MICRO_BENCHMARK_INTERNAL_TEST_H
#define MICRO_BENCHMARK_INTERNAL_TEST_H

#include "mbenchmark/common.h"
#include "mbenchmark/report.h"
#include "mbenchmark/test.h"
#include "internal/constraints.h"
#include "internal/test/definition.h"

#include <stddef.h>

/* *INDENT-OFF* */
MICRO_BENCHMARK_BEGIN_DECLS
/* *INDENT-ON* */

/**
 * Internal definition of the registered test case.
 */
struct micro_benchmark_test_case_definition
{
  char *name;
  size_t name_size;
  bool enabled;
  void *user_data;
  void (*user_data_release) (void *);
  micro_benchmark_test_definition definition;
  mbconstraints_ constraints;
};

void micro_benchmark_test_case_init_ (micro_benchmark_test_case tc,
                                      const char *name,
                                      micro_benchmark_test definition);

void micro_benchmark_test_case_cleanup_ (micro_benchmark_test_case tc);

mbconstraintsp_
micro_benchmark_test_case_constraints_ (micro_benchmark_test_case tc);

void micro_benchmark_test_case_run_ (micro_benchmark_test_case tc,
                                     struct micro_benchmark_test_report_ *tr);

MICRO_BENCHMARK_END_DECLS
#endif
