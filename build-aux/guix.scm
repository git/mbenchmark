;;; guix.scm --- MicroBenchmark GNU Guix package.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.

(use-modules (guix build-system gnu)
             ((guix licenses)
              #:prefix license:)
             (guix download)
             (guix packages)
             (gnu packages guile)
             (gnu packages libunistring)
             (gnu packages multiprecision)
             (gnu packages pkg-config))

(let ((base-url "https://download.savannah.nongnu.org/releases/mbenchmark"))
  (package
    (name "mbenchmark")
    (version "0.0")
    (source (origin
              (method url-fetch)
              (uri (string-append base-url name "-" version ".tar.gz"))
              (sha256
               (base32
                "0000000000000000000000000000000000000000000000000000"))))
    (build-system gnu-build-system)
    (inputs `(("guile" ,guile-3.0-latest)
              ("libgmp" ,gmp)
              ("libunistring" ,libunistring)))
    (native-inputs `(("pkg-config" ,pkg-config)))
    (home-page "https://www.nongnu.org/mbenchmark/")
    (synopsis "Library to measure the performance of code fragments")
    (description
     "MicroBenchmark is a library implemented in C99--with bindings for
other programming languages: currently C++ and GNU Guile
Scheme--designed to measure the performance of code fragments.  The
comparison of these kind of measurements between different
implementations and/or environments is usually called micro benchmark,
hence the library name.")
    (license (list license:lgpl3+ license:fdl1.3+))))
