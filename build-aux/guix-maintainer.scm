;;; guix-maintainer.scm --- MicroBenchmark GNU Guix package.
;;
;; Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
;;
;; This file is part of MicroBenchmark.
;;
;; MicroBenchmark is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; MicroBenchmark is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with MicroBenchmark.  If not, see
;; <http://www.gnu.org/licenses/>.
;;
(use-modules (gnu packages autotools)
             (gnu packages base)
             (gnu packages build-tools)
             (gnu packages code)
             (gnu packages documentation)
             (gnu packages gettext)
             (gnu packages guile)
             (gnu packages libunistring)
             (gnu packages multiprecision)
             (gnu packages package-management)
             (gnu packages perl)
             (gnu packages pkg-config)
             (gnu packages tex)
             (gnu packages texinfo)
             (gnu packages version-control)
             (guix build-system gnu)
             (guix gexp)
             (guix git-download)
             ((guix licenses)
              #:prefix license:)
             (guix packages))

(package
  (name "mbenchmark")
  (version "0.0")
  (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://git.savannah.nongnu.org/git/mbenchmark.git")
                  (commit "master")))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "0000000000000000000000000000000000000000000000000000"))))
  (arguments
   (list #:configure-flags #~(if #$(getenv "DISABLE_COVERAGE")
                                 '()
                                 (list "--enable-coverage=auto"))
         #:make-flags #~ (let ((id #$(getenv "MB_MAINTAINER_ID")))
                           (if id
                               (list (string-append "MB_MAINTAINER_ID=" id))
                               '()))
         #:phases #~(modify-phases %standard-phases
                      (add-after 'unpack 'add-external-sources
                        (lambda* (#:key inputs #:allow-other-keys)
                          (let ((guix-src (assoc-ref inputs "guix-src"))
                                (gnulib-src (assoc-ref inputs "gnulib-src")))
                            (format #t "Copying test-driver.scm from ~a~%"
                                    guix-src)
                            (copy-file (string-append guix-src
                                        "/build-aux/test-driver.scm")
                                       "build-aux/test-driver.scm")
                            (format #t "Copying gitlog-to-changelog from ~a~%"
                                    gnulib-src)
                            (copy-file (string-append gnulib-src
                                        "/build-aux/gitlog-to-changelog")
                                       "build-aux/gitlog-to-changelog"))))
                      (add-after 'bootstrap 'gen-ChangeLog
                        (lambda _
                          ;; TODO: Retrieve git history, not only the
                          ;; checkout, and make reproducible the
                          ;; ChangeLog generation.
                          (call-with-output-file "ChangeLog"
                            (lambda (p)
                              (format p "doc/mbenchmark.info~%")
                              (for-each (lambda (f)
                                          (format p "~a~%" f))
                                        (find-files "."))))))
                      (add-after 'check 'make-dist
                        (lambda _
                          ;; TODO: Distcheck needs patch-source-shebangs again
                          ;; TODO: Unpatch files
                          (invoke "make" "dist")))
                      (add-after 'install 'install-html
                        (lambda _
                          (invoke "make" "install-html")
                          (invoke "make" "doc/web-manual.tar.gz")))
                      (add-after 'install-html 'install-dist
                        (lambda* (#:key outputs #:allow-other-keys)
                          (let ((dist (assoc-ref outputs "dist"))
                                (tarball (string-append "mbenchmark-"
                                                        #$version ".tar.gz")))
                            (install-file "doc/web-manual.tar.gz" dist)
                            (install-file tarball dist)
                            (when (file-exists?
                                   "build-aux/mbenchmark-gcov.tar.gz")
                              (install-file "build-aux/mbenchmark-gcov.tar.gz"
                                            dist))))))))
  (build-system gnu-build-system)
  (outputs '("out" "dist"))
  (inputs `(("guile" ,guile-3.0-latest)
            ("libgmp" ,gmp)
            ("libunistring" ,libunistring)))
  (native-inputs `(("autoconf" ,autoconf)
                   ("autoconf-archive" ,autoconf-archive)
                   ("automake" ,automake)
                   ("gettext" ,gettext-minimal)
                   ("glibc-locales" ,glibc-utf8-locales)
                   ("git" ,git)
                   ("libtool" ,libtool)
                   ("pkg-config" ,pkg-config)
                   ("texinfo" ,texinfo)
                   ("texi2html" ,texi2html)
                   ("texlive-scheme" ,texlive-scheme-basic)
                   ("texlive-latex" ,texlive-collection-latexrecommended)
                   ("texlive-fonts" ,texlive-collection-fontsrecommended)
                   ;; For indent-code.sh
                   ("indent" ,indent)
                   ;; For test-driver.scm
                   ("guix-src" ,(package-source guix))
                   ;; For gitlog-to-changelog
                   ("perl" ,perl)
                   ("gnulib-src" ,(package-source gnulib))))
  (home-page "https://download.savannah.nongnu.org/releases/mbenchmark/")
  (synopsis "Library to measure the performance of code fragments")
  (description
   "MicroBenchmark is a library implemented in C99--with bindings for
other programming languages: currently C++ and GNU Guile
Scheme--designed to measure the performance of code fragments.  The
comparison of these kind of measurements between different
implementations and/or environments is usually called micro benchmark,
hence the library name.")
  (license (list license:lgpl3+ license:fdl1.3+)))
