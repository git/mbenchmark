#!/bin/sh
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is part of MicroBenchmark.
#
# MicroBenchmark is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
#
# MicroBenchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MicroBenchmark.  If not, see
# <http://www.gnu.org/licenses/>.
set -e

: ${VERSION:="0.0"}
: ${ROUNDS:="1"}
: ${BUILDDIR:="$(pwd)/build"}
: ${RUN_EXAMPLES:=no}
: ${REGEN_DOC_EXAMPLES:=no}
: ${CONFIGURE_FLAGS:=""}

release_env='build-aux/guix.scm'
maint_env='build-aux/guix-maintainer.scm'
extra="--ad-hoc wget"
at_env="guix environment --pure -l ${maint_env} ${extra} --"

do_clean () {
    if [ "x${CLEAN-no}" != xno ]; then
        test -e build && chmod 755 -R build
        test -e inst && chmod 755 -R inst
        test -e .git && git clean -xfd
    fi
}

build_tarball () {
    do_clean
    time ${at_env} bash <<EOF
RUN_EXAMPLES="${RUN_EXAMPLES}"
export RUN_EXAMPLES
REGEN_DOC_EXAMPLES="${REGEN_DOC_EXAMPLES}"
export REGEN_DOC_EXAMPLES
BUILDDIR="${BUILDDIR}"
export BUILDDIR
build-aux/create-release.sh ${CONFIGURE_FLAGS}
EOF
}

arch_flag=
toolchain_flag=
guile_flag=
build_with_guix () {
    if [ -z "${tarball}" ]; then
        guix build --file="${maint_env}" \
             ${arch_flag} \
             ${toolchain_flag} \
             ${guile_flag} \
             --with-git-url=mbenchmark="$(pwd)" \
             --keep-failed
    else
        guix build --file="${release_env}" \
             ${arch_flag} \
             ${toolchain_flag} \
             ${guile_flag} \
             --with-source=mbenchmark="${tarball}" \
             --rounds="${ROUNDS}" \
             --keep-failed
    fi
}

iterate_guile_versions () {
    guile_versions="default"
    if [ -n "${ALL_GUILE+useall}" ]; then
        guile_versions="guile@2.0 guile@3.0 guile@2.2"
    fi
    for guile in ${guile_versions}; do
        guile_flag="--with-input=guile=${guile}"
        if [ "x$guile" = xdefault ]; then
            guile_flag=""
        fi
        build_with_guix
    done
}

iterate_toolchains () {
    toolchains="native"
    if [ -n "${ALL_TOOLCHAINS+useall}" ]; then
        gcc_versions=""
        earliest_gcc=4
        latest_gcc=12
        for v in $(seq "${earliest_gcc}" "${latest_gcc}"); do
            gcc_versions="${gcc_versions} gcc-toolchain@${v}"
        done
        clang_versions=""
        earliest_clang=6
        latest_clang=15
        for v in $(seq "${earliest_clang}" "${latest_clang}"); do
            clang_versions="${clang_versions} clang-toolchain@${v}"
        done
        toolchains="${gcc_versions} ${clang_versions}"
    fi

    for toolchain in ${toolchains}; do
        toolchain_flag="--with-c-toolchain=mbenchmark=${toolchain}"
        if [ "x$toolchain" = xnative ]; then
            toolchain_flag=""
        fi
        iterate_guile_versions
    done
}

iterate_architectures () {
    arch="native"
    if [ -n "${ALL_ARCHITECTURES+useall}" ]; then
        arch="$arch x86_64-linux"
        arch="$arch i686-linux"
        arch="$arch aarch64-linux"
        arch="$arch armhf-linux"
        if [ -n "${BOOTSTRAP_ARCHITECTURES+yes}" ]; then
            arch="$arch i586-gnu"
            arch="$arch powerpc64le-linux"
            arch="$arch riscv64-linux"
        fi
        if [ -n "${UNSUPPORTED_ARCHITECTURES+yes}" ]; then
            arch="$arch mips64el-linux"
            arch="$arch powerpc-linux"
        fi
    fi
    for current_arch in ${arch}; do
        arch_flag="--system=${current_arch}"
        if [ "x${current_arch}" = xnative ]; then
            arch_flag=
        fi
        iterate_toolchains
    done
}

check_tarball () {
    tarball="$1"
    if [ -e "${tarball}" ]; then
        echo "Building tarball ${tarball}"
    elif [ -n "${BUILD_TARBALL+yes}" ]; then
        build_tarball
        tarball="${BUILDDIR}/mbenchmark-${VERSION}.tar.gz"
    elif [ "x${tarball}" != x ]; then
        echo "Provided tarball ${tarball} does not exist, building from git"
        tarball=""
    fi
}

check_tarball "$@"
iterate_architectures
