#!/bin/sh
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is part of MicroBenchmark.
#
# MicroBenchmark is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
#
# MicroBenchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MicroBenchmark.  If not, see
# <http://www.gnu.org/licenses/>.
set -e
set -x

: ${MAKEOPTIONS:='-j'}
: ${SRCDIR:=$(pwd)}
: ${BUILDDIR:="${SRCDIR}/build"}
: ${INSTDIR:="${BUILDDIR}/inst"}
: ${RUN_EXAMPLES:=no}
: ${REGEN_DOC_EXAMPLES:=no}

cd ${SRCDIR}

if [ "x${FULL_CLEAN-no}" != xno ]; then
    test -e .git && git clean -xfd
    if [ -e "${BUILDDIR}" ]; then
       chmod 755 -R "${BUILDDIR}"
       rm -rf "${BUILDDIR}"
    fi
    if [ -e "${INSTDIR}" ]; then
       rm -rf "${INSTDIR}"
    fi
fi

if [ ! -e configure ]; then
    autoreconf -fvi
fi

if [ ! -e ${BUILDDIR} ]; then
    mkdir ${BUILDDIR}
fi

cd ${BUILDDIR}
if [ ! -e Makefile ]; then
    "${SRCDIR}/configure" --prefix="${INSTDIR}" "$@"
fi

if [ "x${CLEAN-no}" != xno ]; then
    make clean
fi

make ${MAKEOPTIONS}
make ${MAKEOPTIONS} check RUN_EXAMPLES="${RUN_EXAMPLES}"
if [ "x${REGEN_DOC_EXAMPLES}" != xno ]; then
   make regen-doc-examples
fi
make ${MAKEOPTIONS} info dvi pdf html
make ${MAKEOPTIONS} distcheck

echo "MicroBenchmark is ready for distribution"
exit 0
