#!/bin/sh
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is part of MicroBenchmark.
#
# MicroBenchmark is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
#
# MicroBenchmark is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MicroBenchmark.  If not, see
# <http://www.gnu.org/licenses/>.
#
: ${INDENT:='indent'}
: ${SRCDIR:=.}
: ${INDENT_FLAGS:='--no-tabs'}
internal_types="$(cat <<EOF
FILE
SCM
chrono_data
clock_t
data
impl
mbconstraints_
mbconstraints_iteration_
mbconstraints_meter_
mbconstraints_size_
mbconstraints_size_dimension_
mbconstraints_time_
mbconstraints_timer_
mbconstraintsp_
mbguile_test_data_
mbstate_
mbstate_constraints_
mbstats_collector_
micro_benchmark_exec_report_
micro_benchmark_report_
micro_benchmark_test_report_
output_field
output_row
output_table
report
size_t
time_t
timer_data
timer_t
uint8_t
EOF
)"

syms="$1"
if [ -n "$syms" ] && [ -e "$syms" ]; then
    internal_types="$internal_types $(cat "$syms")"
fi

for s in ${internal_types}; do
    INDENT_FLAGS="$INDENT_FLAGS -T $s"
done

for file in $(find ${SRCDIR} -type f '(' -name '*.c' -o -name '*.h' ')'); do
    ${INDENT} ${INDENT_FLAGS} "${file}"
done
