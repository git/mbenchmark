/* clock-t.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/clock/clock-t.h"
#include "internal/clock/chrono-adapter.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <limits.h>
#include <stdint.h>
#include <time.h>

#define MICRO_BENCHMARK_SUBMODULE "clock_t"
#define MICRO_BENCHMARK_MODULE "chrono:" MICRO_BENCHMARK_SUBMODULE

typedef struct micro_benchmark_meter_data clock_data;
typedef micro_benchmark_meter_definition impl;
typedef struct data
{
  clock_t start;
  clock_t elapsed;
} data;

static micro_benchmark_clock_time
to_time (clock_t ticks)
{
  MB_TRACE ("Converting %d ticks using %d clocks per second\n",
            (int) ticks, (int) CLOCKS_PER_SEC);
  double s = ((double) ticks) / CLOCKS_PER_SEC;
  micro_benchmark_clock_time ret;
  ret.seconds = s;
  ret.nanoseconds = (s - ret.seconds) * 1000000000;
  return ret;
}

static micro_benchmark_clock_time
get_min_resolution (void)
{
  double r = 1.0 / CLOCKS_PER_SEC;
  int32_t s = r;
  double left = r - s;
  int32_t ns = left * 1000000000;
  micro_benchmark_clock_time ret = { s, ns };
  MB_DEBUG ("Resolution is %ds %dns\n", (int) ret.seconds,
            (int) ret.nanoseconds);
  return ret;
}

static micro_benchmark_clock_time
get_max_resolution (void)
{
#if SIZEOF_CLOCK_T * CHAR_BIT == 64
#define MB_CLOCK_MAX UINT64_MAX
#elif SIZEOF_CLOCK_T * CHAR_BIT == 32
#define MB_CLOCK_MAX UINT32_MAX
#elif SIZEOF_CLOCK_T * CHAR_BIT == 16
#define MB_CLOCK_MAX UINT16_MAX
#elif SIZEOF_CLOCK_T * CHAR_BIT == 8
#define MB_CLOCK_MAX UINT8_MAX
#else
#error "Unknown implemented architecture"
#endif
  static const double seconds = ((double) MB_CLOCK_MAX) / CLOCKS_PER_SEC;
  micro_benchmark_clock_time ret = { (int32_t) seconds, 0 };
  if (seconds == 0)
    {
      const int32_t ns = (seconds - ret.seconds) * 1000000000.0;
      ret.nanoseconds = ns;
    }
  return ret;
}

static void
mb_chrono_init (clock_data * d, const micro_benchmark_clock_type *tp)
{
  MB_TRACE ("Init %p with type pointer %p.\n", d, tp);
  assert (d);
  micro_benchmark_clock_type type = tp ? *tp : MICRO_BENCHMARK_CLOCK_PROCESS;
  if (type != MICRO_BENCHMARK_CLOCK_PROCESS)
    MB_WARN ("clock_t only implements process clock but %d was provided\n",
             (int) type);

  data *internal = xmalloc (sizeof (data));
  internal->start = 0;
  internal->elapsed = 0;
  MB_DEBUG ("Allocated internal data %p.\n", internal);

  d->ptr = internal;
  d->name = MICRO_BENCHMARK_SUBMODULE;
  d->sample_type = MICRO_BENCHMARK_SAMPLE_TIME;
  d->min_resolution.time = get_min_resolution ();
  d->max_resolution.time = get_max_resolution ();
  MB_TRACE ("Init %p finished.\n", d);
}

static void
mb_chrono_cleanup (const clock_data * d)
{
  MB_TRACE ("Release %p.\n", d);
  assert (d);
  assert (d->ptr);
  MB_TRACE ("Releasing internal data %p.\n", d->ptr);
  xfree (d->ptr, sizeof (data));
  MB_TRACE ("Released %p\n", d);
}

static void
mb_chrono_start (data *d)
{
  MB_TRACE ("Starting %p.\n", d);
  assert (d);
  d->elapsed = 0;
  d->start = clock ();
  MB_HANDLE_ERROR ((int) d->start == -1, "Unable to get clock_t value");
  MB_TRACE ("Start value %d.\n", (int) d->start);
}

static void
mb_chrono_stop (data *d)
{
  MB_TRACE ("Stopping %p.\n", d);
  assert (d);

  clock_t now = clock ();
  MB_HANDLE_ERROR ((int) now == -1, "Unable to get clock_t value");
  clock_t elapsed = now - d->start;
  d->elapsed += elapsed;
  MB_DEBUG ("Ticks elapsed %d, total ticks elapsed %d.\n", (int) elapsed,
            (int) d->elapsed);
}

static void
mb_chrono_restart (data *d)
{
  MB_TRACE ("Restarting %p.\n", d);
  assert (d);
  d->start = clock ();
  MB_HANDLE_ERROR ((int) d->start == -1, "Unable to get clock_t value");
  MB_TRACE ("Restart value %d.\n", (int) d->start);
}

static micro_benchmark_stats_meter_sample
mb_chrono_get_sample (data *d)
{
  MB_TRACE ("Retrieving sample from %p.\n", d);
  assert (d);
  micro_benchmark_stats_meter_sample ret;
  ret.time = to_time (d->elapsed);
  MB_DEBUG ("Total ticks %d (%d.%09ds)\n", (int) d->elapsed,
            (int) ret.time.seconds, (int) ret.time.nanoseconds);
  d->elapsed = 0;
  return ret;
}

const impl mbchrono_clock_t_tmpl_ = {
  {NULL, MICRO_BENCHMARK_SUBMODULE "-template", MICRO_BENCHMARK_SAMPLE_TIME,
   {}, {}},
  (micro_benchmark_meter_init_fun) mb_chrono_init,
  mb_chrono_cleanup,
  (micro_benchmark_meter_start_fun) mb_chrono_start,
  (micro_benchmark_meter_stop_fun) mb_chrono_stop,
  (micro_benchmark_meter_restart_fun) mb_chrono_restart,
  (micro_benchmark_meter_get_sample_fun) mb_chrono_get_sample
};

micro_benchmark_timer
micro_benchmark_timer_clock_t_ (micro_benchmark_clock_type type)
{
  if (type != MICRO_BENCHMARK_CLOCK_PROCESS)
    MB_WARN ("clock_t only implements process clock but %d was provided\n",
             (int) type);
  return micro_benchmark_timer_adapt_chrono_ (MICRO_BENCHMARK_CLOCK_PROCESS,
                                              &mbchrono_clock_t_tmpl_);
}
