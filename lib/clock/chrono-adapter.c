/* chrono-adapter.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/clock/timer.h"
#include "mbenchmark/stats/meter.h"

#include "internal/clock/chrono-adapter.h"
#ifdef HAVE_CLOCK_GETTIME
#include "internal/clock/clock-gettime.h"
#endif
#ifdef HAVE_GETTIMEOFDAY
#include "internal/clock/gettimeofday.h"
#endif
#include "internal/clock/time-t.h"
#include "internal/clock/clock-t.h"
#include "internal/utility/log.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>

#ifdef HAVE_CLOCK_GETTIME
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER
#define MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER mbchrono_clock_gettime_tmpl_
#endif
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER_TYPE
#define MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER_TYPE MICRO_BENCHMARK_CLOCK_MONOTONIC
#endif
#endif

#ifdef HAVE_GETTIMEOFDAY
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER
#define MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER mbchrono_gettimeofday_tmpl_
#endif
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER_TYPE
#define MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER_TYPE MICRO_BENCHMARK_CLOCK_REALTIME
#endif
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER
#define MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER mbchrono_time_t_tmpl_
#endif
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER_TYPE
#define MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER_TYPE MICRO_BENCHMARK_CLOCK_REALTIME
#endif

#define MICRO_BENCHMARK_SUBMODULE "chrono-adapter"
#define MICRO_BENCHMARK_MODULE "timer:" MICRO_BENCHMARK_SUBMODULE

typedef micro_benchmark_timer_data timer_data;
typedef micro_benchmark_timer_definition impl;

typedef struct data
{
  bool is_running;
  micro_benchmark_meter_definition chrono;
  micro_benchmark_clock_time deadline;
  micro_benchmark_clock_time elapsed;
} data;


static void
mb_timer_init (timer_data *d, micro_benchmark_clock_type type,
               micro_benchmark_meter meter)
{
  MB_TRACE ("Init %p with type %d.\n", d, (int) type);
  assert (d);

  data *internal = xcalloc (sizeof (data));
  MB_DEBUG ("Allocated internal data %p.\n", internal);

  if (meter)
    internal->chrono = *meter;
  else
    internal->chrono = MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER;

  micro_benchmark_stats_meter_init_with_data (&internal->chrono, &type);
  micro_benchmark_meter m = &internal->chrono;
  micro_benchmark_stats_sample_type stype =
    micro_benchmark_stats_meter_get_sample_type (m);

  MB_HANDLE_ERROR (stype != MICRO_BENCHMARK_SAMPLE_TIME,
                   "Provided meter is not a chronometer.\n");
  d->ptr = internal;
  d->name = MICRO_BENCHMARK_SUBMODULE;
  d->type = type;
  d->resolution = m->data.min_resolution.time;
  MB_TRACE ("Init %p finished.\n", d);
}

static void
mb_timer_cleanup (const timer_data *d)
{
  MB_TRACE ("Release %p.\n", d);
  assert (d);
  assert (d->ptr);
  MB_DEBUG ("Releasing internal data %p.\n", d->ptr);
  data *internal = d->ptr;
  micro_benchmark_stats_meter_cleanup (&internal->chrono);
  xfree (d->ptr, sizeof (data));
  MB_TRACE ("Released %p.\n", d);
}

static void
mb_timer_start (data *d, micro_benchmark_clock_time time)
{
  MB_TRACE ("Start timer %p with %ds, %dns.\n", d,
            (int) time.seconds, (int) time.nanoseconds);
  assert (d);
  assert (!d->is_running);

  static const micro_benchmark_clock_time zero;
  d->deadline = time;
  d->elapsed = zero;
  micro_benchmark_stats_meter_start (&d->chrono);
  d->is_running = true;
  MB_TRACE ("Started %p.\n", d);
}

static void
mb_timer_stop (data *d)
{
  MB_TRACE ("Stop timer %p\n", d);
  assert (d);
  assert (d->is_running);
  micro_benchmark_stats_meter_stop (&d->chrono);
  d->is_running = false;
  micro_benchmark_stats_meter_sample curr =
    micro_benchmark_stats_meter_get_sample (&d->chrono);
  d->elapsed.nanoseconds += curr.time.nanoseconds;
  while (d->elapsed.nanoseconds >= 1000000000)
    {
      d->elapsed.seconds++;
      d->elapsed.nanoseconds -= 1000000000;
    }
  d->elapsed.seconds += curr.time.seconds;
  MB_TRACE ("Stopped timer %p\n", d);
}

static void
mb_timer_restart (data *d)
{
  MB_TRACE ("Restart timer %p\n", d);
  assert (d);
  assert (!d->is_running);
  d->is_running = true;
  micro_benchmark_stats_meter_restart (&d->chrono);
  MB_TRACE ("Restarted timer %p\n", d);
}

static bool
mb_timer_is_running (data *d)
{
  MB_TRACE ("Check if the timer %p is running\n", d);
  assert (d);
  if (d->is_running)
    {
      /* XXX: Better data collection.  */
      mb_timer_stop (d);
      mb_timer_restart (d);
    }
  return (d->elapsed.seconds < d->deadline.seconds
          || (d->elapsed.seconds == d->deadline.seconds
              && d->elapsed.nanoseconds < d->deadline.nanoseconds));
}

static micro_benchmark_clock_time
mb_timer_elapsed (data *d)
{
  MB_TRACE ("Get elapsed time from %p\n", d);
  assert (d);
  MB_DEBUG ("Total elapsed %ds %dns\n", (int) d->elapsed.seconds,
            (int) d->elapsed.nanoseconds);
  return d->elapsed;
}


const impl mbtimer_chrono_adapter_tmpl_ = {
  {NULL, MICRO_BENCHMARK_SUBMODULE "-template",
   MICRO_BENCHMARK_DEFAULT_CHRONO_ADAPTER_TYPE, {}},
  (micro_benchmark_timer_init_fun) mb_timer_init,
  mb_timer_cleanup,
  (micro_benchmark_timer_start_fun) mb_timer_start,
  (micro_benchmark_timer_stop_fun) mb_timer_stop,
  (micro_benchmark_timer_restart_fun) mb_timer_restart,
  (micro_benchmark_timer_running_fun) mb_timer_is_running,
  (micro_benchmark_timer_elapsed_fun) mb_timer_elapsed
};

micro_benchmark_timer
micro_benchmark_timer_adapt_chrono_ (micro_benchmark_clock_type type,
                                     micro_benchmark_meter meter)
{
  assert (meter);
  impl *ptr = xmalloc (sizeof (impl));
  *ptr = mbtimer_chrono_adapter_tmpl_;
  micro_benchmark_timer_init_with_extra (ptr, type, meter);
  return ptr;
}
