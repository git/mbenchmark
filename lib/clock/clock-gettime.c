/* clock-gettime.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/stats/meter.h"

#include "internal/clock/clock-gettime.h"
#include "internal/clock/chrono-adapter.h"
#include "internal/clock/timespec.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <limits.h>
#include <stdint.h>
#include <time.h>

#define MICRO_BENCHMARK_SUBMODULE "clock_gettime"
#define MICRO_BENCHMARK_MODULE "chrono:" MICRO_BENCHMARK_SUBMODULE

typedef struct micro_benchmark_meter_data chrono_data;
typedef micro_benchmark_meter_definition impl;
typedef struct data
{
  clockid_t clock;
  struct timespec start;
  struct timespec elapsed;
} data;

static micro_benchmark_clock_type
select_clock (data *impl, micro_benchmark_clock_type type)
{
  switch (type)
    {
    default:
      MB_WARN ("Unknown clock type %d.\n", (int) type);
      /*  Fallthrough  */
    case MICRO_BENCHMARK_CLOCK_MONOTONIC:
      MB_DEBUG ("Using monotonic clock.\n");
      impl->clock = CLOCK_MONOTONIC;
      return MICRO_BENCHMARK_CLOCK_MONOTONIC;

#ifdef HAVE_CLOCK_PROCESS_CPUTIME_ID
    case MICRO_BENCHMARK_CLOCK_PROCESS:
      MB_DEBUG ("Using process time clock.\n");
      impl->clock = CLOCK_PROCESS_CPUTIME_ID;
      return MICRO_BENCHMARK_CLOCK_PROCESS;
#endif

#ifdef HAVE_CLOCK_THREAD_CPUTIME_ID
    case MICRO_BENCHMARK_CLOCK_THREAD:
      MB_DEBUG ("Using thread time clock.\n");
      impl->clock = CLOCK_THREAD_CPUTIME_ID;
      return MICRO_BENCHMARK_CLOCK_THREAD;
#endif

    case MICRO_BENCHMARK_CLOCK_REALTIME:
#ifdef HAVE_CLOCK_TAI
      /* Avoid leap seconds... */
      MB_DEBUG ("Using TAI clock.\n");
      impl->clock = CLOCK_TAI;
#else
      MB_DEBUG ("Using realtime clock.\n");
      impl->clock = CLOCK_REALTIME;
#endif
      return MICRO_BENCHMARK_CLOCK_REALTIME;
    }
}

static micro_benchmark_clock_time
get_min_resolution (clockid_t clock)
{
  struct timespec resolution;
  int res = clock_getres (clock, &resolution);
  MB_HANDLE_ERROR (res == -1, "Unable to get clock resolution");
  micro_benchmark_clock_time ret = { resolution.tv_sec, resolution.tv_nsec };
  MB_DEBUG ("Resolution is %ds, %dns.\n", (int) ret.seconds,
            (int) ret.nanoseconds);
  return ret;
}

static void
mb_chrono_init (chrono_data *d, micro_benchmark_clock_type *tp)
{
  static const micro_benchmark_clock_time max_res = { INT32_MAX, 0 };
  MB_TRACE ("Init %p with type %p.\n", d, tp);
  assert (d);
  micro_benchmark_clock_type type =
    tp ? *tp : MICRO_BENCHMARK_CLOCK_MONOTONIC;
  data *internal = xcalloc (sizeof (data));
  MB_TRACE ("Allocated internal data %p.\n", internal);

  select_clock (internal, type);
  d->ptr = internal;
  d->name = MICRO_BENCHMARK_SUBMODULE;
  d->sample_type = MICRO_BENCHMARK_SAMPLE_TIME;
  d->min_resolution.time = get_min_resolution (internal->clock);
  d->max_resolution.time = max_res;
  MB_TRACE ("Init %p finished.\n", d);
}

static void
mb_chrono_cleanup (const chrono_data *d)
{
  MB_TRACE ("Release %p\n", d);
  assert (d);
  assert (d->ptr);
  MB_TRACE ("Releasing internal data %p.\n", d->ptr);
  xfree (d->ptr, sizeof (data));
  MB_TRACE ("Released %p\n", d);
}

static void
mb_chrono_start (data *d)
{
  MB_TRACE ("Start %p\n", d);
  assert (d);
  static const struct timespec zero;
  d->elapsed = zero;
  int err = clock_gettime (d->clock, &d->start);
  MB_HANDLE_ERROR (err != 0, "Error calling clock_gettime");
}

static void
mb_chrono_stop (data *d)
{
  assert (d);
  struct timespec curr;
  int err = clock_gettime (d->clock, &curr);
  MB_HANDLE_ERROR (err != 0, "Error calling clock_gettime");
  micro_benchmark_timespec_sub_ (&curr, &curr, &d->start);
  micro_benchmark_timespec_add_ (&d->elapsed, &d->elapsed, &curr);
  MB_DEBUG ("Elapsed on %p %ds %dns, total %ds, %dns.\n", d,
            (int) curr.tv_sec, (int) curr.tv_nsec,
            (int) d->elapsed.tv_sec, (int) d->elapsed.tv_nsec);
}

static void
mb_chrono_restart (data *d)
{
  MB_TRACE ("Restarting %p\n", d);
  assert (d);
  int err = clock_gettime (d->clock, &d->start);
  MB_HANDLE_ERROR (err != 0, "Error calling clock_gettime");
}

static micro_benchmark_stats_meter_sample
mb_chrono_get_sample (data *d)
{
  static const struct timespec zero;
  MB_TRACE ("Getting sample from %p\n", d);
  assert (d);
  micro_benchmark_stats_meter_sample ret = {
    .time = {d->elapsed.tv_sec, d->elapsed.tv_nsec}
  };
  d->elapsed = zero;
  return ret;
}

const impl mbchrono_clock_gettime_tmpl_ = {
  {NULL, MICRO_BENCHMARK_SUBMODULE "-template", MICRO_BENCHMARK_SAMPLE_TIME,
   {}, {}},
  (micro_benchmark_meter_init_fun) mb_chrono_init,
  mb_chrono_cleanup,
  (micro_benchmark_meter_start_fun) mb_chrono_start,
  (micro_benchmark_meter_stop_fun) mb_chrono_stop,
  (micro_benchmark_meter_restart_fun) mb_chrono_restart,
  (micro_benchmark_meter_get_sample_fun) mb_chrono_get_sample,
};

micro_benchmark_timer
micro_benchmark_timer_clock_gettime_ (micro_benchmark_clock_type type)
{
  return micro_benchmark_timer_adapt_chrono_ (type,
                                              &mbchrono_clock_gettime_tmpl_);
}
