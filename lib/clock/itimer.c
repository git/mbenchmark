/* itimer.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/clock/timer.h"

#include "internal/clock/itimer.h"
#include "internal/clock/timeval.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

#include <sys/time.h>

#define MICRO_BENCHMARK_SUBMODULE "itimer"
#define MICRO_BENCHMARK_MODULE "timer:" MICRO_BENCHMARK_SUBMODULE

/*  TODO: Threads.  */
static volatile sig_atomic_t real_expired;
static void
handle_sigalarm (int sig)
{
  (void) sig;
  assert (sig == SIGALRM);
  real_expired = true;
}

static volatile sig_atomic_t virtual_expired;
static void
handle_sigvtalarm (int sig)
{
  (void) sig;
  assert (sig == SIGVTALRM);
  virtual_expired = true;
}

static volatile sig_atomic_t prof_expired;
static void
handle_sigprof (int sig)
{
  (void) sig;
  assert (sig == SIGPROF);
  prof_expired = true;
}

typedef micro_benchmark_timer_data timer_data;
typedef micro_benchmark_timer_definition impl;

typedef struct data
{
  volatile sig_atomic_t *expired;
  int signum;
  void (*handler) (int);
  int timer;
  bool running;
  struct itimerval left;
  struct timeval elapsed;
  bool signal_called;
  void (*old_sig) (int);
} data;

static micro_benchmark_clock_type
select_clock (data *d, micro_benchmark_clock_type type)
{
  switch (type)
    {
    default:
      MB_WARN ("Unknown clock type %d.\n", (int) type);
      /*  Fallthrough  */
    case MICRO_BENCHMARK_CLOCK_REALTIME:
      MB_DEBUG ("Using realtime clock.\n");
      d->timer = ITIMER_REAL;
      d->expired = &real_expired;
      d->handler = handle_sigalarm;
      d->signum = SIGALRM;
      return MICRO_BENCHMARK_CLOCK_REALTIME;

    case MICRO_BENCHMARK_CLOCK_MONOTONIC:
      MB_DEBUG ("Using monotonic clock.\n");
      d->timer = ITIMER_PROF;
      d->expired = &prof_expired;
      d->handler = handle_sigprof;
      d->signum = SIGPROF;
      return MICRO_BENCHMARK_CLOCK_MONOTONIC;

    case MICRO_BENCHMARK_CLOCK_PROCESS:
      MB_DEBUG ("Using process time clock.\n");
      d->timer = ITIMER_VIRTUAL;
      d->expired = &virtual_expired;
      d->handler = handle_sigvtalarm;
      d->signum = SIGVTALRM;
      return MICRO_BENCHMARK_CLOCK_PROCESS;
    }
}

static void
mb_timer_init (timer_data *d, micro_benchmark_clock_type type, const void *nu)
{
  (void) nu;
  MB_TRACE ("Init %p with type %d.\n", d, (int) type);
  assert (d);
  assert (!nu);

  data *internal = xcalloc (sizeof (data));
  MB_TRACE ("Allocated internal data %p.\n", internal);

  d->ptr = internal;
  d->name = MICRO_BENCHMARK_SUBMODULE;
  d->type = select_clock (internal, type);
  d->resolution.seconds = 0;
  d->resolution.nanoseconds = 1000;
}

static void
mb_timer_cleanup (const timer_data *d)
{
  MB_TRACE ("Release %p.\n", d);
  assert (d);
  assert (d->ptr);

  MB_TRACE ("Deleting timer.\n");
  data *internal = d->ptr;
  assert (!internal->running);
  if (internal->signal_called)
    {
      void (*curr) (int) = signal (internal->signum, internal->old_sig);
      assert (curr == internal->handler);
      (void) curr;
    }
  MB_TRACE ("Releasing internal data %p.\n", d->ptr);
  xfree (d->ptr, sizeof (data));
  MB_TRACE ("Released %p.\n", d);
}

static void
mb_timer_start (data *d, micro_benchmark_clock_time time)
{
  assert (d);
  MB_TRACE ("Start timer %p with %d.%09ds.\n", d,
            (int) time.seconds, (int) time.nanoseconds);

  d->left.it_value.tv_sec = time.seconds;
  d->left.it_value.tv_usec = time.nanoseconds / 1000;

  assert (!d->running);
  d->running = true;
  *d->expired = false;
  struct itimerval old;
  void (*old_sig) (int) = signal (d->signum, d->handler);
  MB_HANDLE_ERROR (old_sig == SIG_ERR, "Error adding signal handler");
  if (!d->signal_called)
    {
      d->signal_called = true;
      d->old_sig = old_sig;
    }
  int res = setitimer (d->timer, &d->left, &old);
  MB_HANDLE_ERROR (res == -1, "Error arming timer");
  MB_HANDLE_ERROR (old.it_value.tv_sec != 0
                   || old.it_value.tv_usec != 0
                   || old.it_interval.tv_sec != 0
                   || old.it_interval.tv_usec != 0,
                   "Multiple timers are not allowed by itimer");
}

static void
mb_timer_stop (data *d)
{
  MB_TRACE ("Stop timer %p\n", d);
  assert (d);

  static const struct itimerval zero;
  struct itimerval left;
  int res = setitimer (d->timer, &zero, &left);
  MB_HANDLE_ERROR (res == -1, "Error disarming timer");
  assert (d->running);
  d->running = false;

  struct timeval elapsed;
  micro_benchmark_timeval_sub_ (&elapsed, &d->left.it_value, &left.it_value);
  micro_benchmark_timeval_add_ (&d->elapsed, &d->elapsed, &elapsed);
  MB_DEBUG
    ("Elapsed %d.%06ds, total elapsed %d%06ds, left %d.%06ds (%d.%06ds)\n",
     (int) elapsed.tv_sec, (int) elapsed.tv_usec, (int) d->elapsed.tv_sec,
     (int) d->elapsed.tv_usec, (int) left.it_value.tv_sec,
     (int) left.it_value.tv_usec, (int) left.it_interval.tv_sec,
     (int) left.it_interval.tv_usec);

  d->left = left;
  MB_TRACE ("Stopped timer %p\n", d);
}

static void
mb_timer_restart (data *d)
{
  assert (d);
  MB_TRACE ("Resetting timer %p to %d.%06ds, interval %d.%06ds.\n", d,
            (int) d->left.it_value.tv_sec,
            (int) d->left.it_value.tv_usec,
            (int) d->left.it_interval.tv_sec,
            (int) d->left.it_interval.tv_usec);

  assert (!d->running);
  d->running = true;
  struct itimerval old;
  int res = setitimer (d->timer, &d->left, &old);
  MB_HANDLE_ERROR (res == -1, "Error arming timer");
  MB_HANDLE_ERROR (old.it_value.tv_sec != 0
                   || old.it_value.tv_usec != 0
                   || old.it_interval.tv_sec != 0
                   || old.it_interval.tv_usec != 0,
                   "Multiple timers of the same clock are not allowed by itimer");
}

static bool
mb_timer_is_running (data *d)
{
  MB_TRACE ("Check if the timer is running %p\n", d);
  assert (d);
  assert (d->expired);

  if (*d->expired)
    return false;

  struct itimerval curr;
  int res = getitimer (d->timer, &curr);
  MB_HANDLE_ERROR (res == -1, "Error checking timer");
  MB_DEBUG ("Left %d.%06ds, current %d.%06ds (%d.%06ds)\n",
            (int) d->left.it_value.tv_sec,
            (int) d->left.it_value.tv_usec,
            (int) curr.it_value.tv_sec,
            (int) curr.it_value.tv_usec,
            (int) curr.it_interval.tv_sec, (int) curr.it_interval.tv_usec);
  if (!d->running)
    micro_benchmark_timeval_add_ (&curr.it_value, &curr.it_value,
                                  &d->left.it_value);
  return curr.it_value.tv_sec != 0 || curr.it_value.tv_usec != 0;
}

static micro_benchmark_clock_time
mb_timer_elapsed (data *d)
{
  MB_TRACE ("Get elapsed time %p\n", d);
  assert (d);
  micro_benchmark_clock_time ret = {
    d->elapsed.tv_sec,
    d->elapsed.tv_usec * 1000
  };
  MB_TRACE ("Total elapsed %d.%09ds\n", (int) ret.seconds,
            (int) ret.nanoseconds);
  return ret;
}

const impl mbtimer_itimer_tmpl_ = {
  {NULL, MICRO_BENCHMARK_SUBMODULE "-template", 0, {}},
  mb_timer_init,
  mb_timer_cleanup,
  (micro_benchmark_timer_start_fun) mb_timer_start,
  (micro_benchmark_timer_stop_fun) mb_timer_stop,
  (micro_benchmark_timer_restart_fun) mb_timer_restart,
  (micro_benchmark_timer_running_fun) mb_timer_is_running,
  (micro_benchmark_timer_elapsed_fun) mb_timer_elapsed
};
