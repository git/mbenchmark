/* timer.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/clock/timer-provider.h"

#ifdef HAVE_TIMERFD
#include "internal/clock/timerfd.h"
#endif
#ifdef HAVE_TIMER_T
#include "internal/clock/timer-t.h"
#endif
#ifdef HAVE_ITIMER
#include "internal/clock/itimer.h"
#endif
#include "internal/clock/chrono-adapter.h"
#include "internal/clock/chrono-provider.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>

#ifndef MICRO_BENCHMARK_DEFAULT_TIMER_TYPE
#define MICRO_BENCHMARK_DEFAULT_TIMER_TYPE MICRO_BENCHMARK_CLOCK_MONOTONIC
#endif

#ifdef HAVE_TIMER_T
#ifndef MICRO_BENCHMARK_DEFAULT_TIMER_TMPL
#define MICRO_BENCHMARK_DEFAULT_TIMER_TMPL mbtimer_timer_t_tmpl_
#define MICRO_BENCHMARK_DEFAULT_TIMER_FUN micro_benchmark_timer_timer_t_ ()
#endif
#endif

#ifdef HAVE_TIMERFD
#ifndef MICRO_BENCHMARK_DEFAULT_TIMER_TMPL
#define MICRO_BENCHMARK_DEFAULT_TIMER_TMPL mbtimer_timerfd_tmpl_
#define MICRO_BENCHMARK_DEFAULT_TIMER_FUN micro_benchmark_timer_timerfd_ ()
#endif
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_TIMER_TMPL
#define MICRO_BENCHMARK_DEFAULT_TIMER_TMPL mbtimer_chrono_adapter_tmpl_
#define MICRO_BENCHMARK_DEFAULT_TIMER_FUN micro_benchmark_timer_chrono_adapter_ ()
#endif

#define MICRO_BENCHMARK_SUBMODULE "provider"
#define MICRO_BENCHMARK_MODULE "timer:" MICRO_BENCHMARK_SUBMODULE

typedef micro_benchmark_timer_definition impl;

static micro_benchmark_timer default_timer =
  &MICRO_BENCHMARK_DEFAULT_TIMER_TMPL;

void
micro_benchmark_timer_set_default (micro_benchmark_timer c)
{
  if (c)
    default_timer = c;
  else
    default_timer = MICRO_BENCHMARK_DEFAULT_TIMER_FUN;
}

micro_benchmark_timer
micro_benchmark_timer_get_default (void)
{
  return default_timer;
}

static void
handle_missing_implementation (micro_benchmark_timer_provider prov)
{
  switch (prov)
    {
    case MICRO_BENCHMARK_TIMER_PROVIDER_ITIMER:
      MB_WARN ("The itimer implementation is not available, "
               "using default timer\n");
      break;

    case MICRO_BENCHMARK_TIMER_PROVIDER_TIMER_T:
      MB_WARN ("The timer_t implementation is not available, "
               "using default timer\n");
      break;

    case MICRO_BENCHMARK_TIMER_PROVIDER_TIMERFD:
      MB_WARN ("The timerfd implementation is not available, "
               "using default timer\n");
      break;

    case MICRO_BENCHMARK_TIMER_PROVIDER_FROM_METER:
      MB_WARN ("Expected user provided chronometer but it was not provided. "
               "Using default timer.\n");
      break;

    case MICRO_BENCHMARK_TIMER_PROVIDER_USER_PROVIDED:
      MB_WARN ("Expected user provided timer but it was not provided. "
               "Using default timer.\n");
      break;

    default:
      MB_WARN ("The selected implementation %d is not available, "
               "using default timer\n", (int) prov);
      break;
    }
}

micro_benchmark_timer
micro_benchmark_timer_create_default (void)
{
  micro_benchmark_timer tmpl = micro_benchmark_timer_get_default ();
  micro_benchmark_clock_type type = MICRO_BENCHMARK_DEFAULT_TIMER_TYPE;
  return micro_benchmark_timer_from_template (type, tmpl);
}

static micro_benchmark_timer
timer_template (micro_benchmark_timer_provider prov)
{
  switch (prov)
    {
    default:
      handle_missing_implementation (prov);
      /*  Fallthrough  */
    case MICRO_BENCHMARK_TIMER_PROVIDER_DEFAULT:
      return NULL;

#ifdef HAVE_ITIMER
    case MICRO_BENCHMARK_TIMER_PROVIDER_ITIMER:
      return micro_benchmark_itimer_timer_ ();
#endif

#ifdef HAVE_TIMER_T
    case MICRO_BENCHMARK_TIMER_PROVIDER_TIMER_T:
      return micro_benchmark_timer_timer_t_ ();
#endif

#ifdef HAVE_TIMERFD
    case MICRO_BENCHMARK_TIMER_PROVIDER_TIMERFD:
      return micro_benchmark_timer_timerfd_ ();
#endif
    }
}

void
micro_benchmark_timer_set_default_provider (micro_benchmark_timer_provider p)
{
  micro_benchmark_timer_set_default (timer_template (p));
}

micro_benchmark_timer
micro_benchmark_timer_create (micro_benchmark_clock_type type,
                              micro_benchmark_timer_provider prov)
{
  micro_benchmark_timer tmpl = timer_template (prov);
  if (!tmpl)
    tmpl = default_timer;
  assert (tmpl);
  return micro_benchmark_timer_from_template (type, tmpl);
}

micro_benchmark_timer
micro_benchmark_timer_from_meter (micro_benchmark_clock_type type,
                                  micro_benchmark_chronometer_provider p)
{
  MB_TRACE ("Creating timer type %d from provider %d\n", (int) type, (int) p);
  micro_benchmark_meter meter = micro_benchmark_chronometer_template_ (p);
  return micro_benchmark_timer_adapt_chrono_ (type, meter);
}

micro_benchmark_timer
micro_benchmark_timer_from_provided_meter (micro_benchmark_clock_type type,
                                           micro_benchmark_meter meter)
{
  MB_TRACE ("Creating timer type %d from meter %p\n", (int) type, meter);
  return micro_benchmark_timer_adapt_chrono_ (type, meter);
}

micro_benchmark_timer
micro_benchmark_timer_from_template (micro_benchmark_clock_type type,
                                     micro_benchmark_timer tmpl)
{
  MB_TRACE ("Creating timer type %d from template %p\n", (int) type, tmpl);
  assert (tmpl);
  impl *ptr = xmalloc (sizeof (impl));
  *ptr = *tmpl;
  micro_benchmark_timer_init (ptr, type);
  return ptr;
}

void
micro_benchmark_timer_release (micro_benchmark_timer timer)
{
  MB_TRACE ("Releasing %p\n", timer);
  assert (timer);
  micro_benchmark_timer_cleanup (timer);
  xfree ((void *) timer, sizeof (impl));
}
