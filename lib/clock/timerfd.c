/* timerfd.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/clock/timer.h"

#include "internal/clock/timerfd.h"
#include "internal/clock/timespec.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <signal.h>
#include <time.h>
#ifdef HAVE_SYS_TIMERFD_H
#include <sys/timerfd.h>
#endif
#include <unistd.h>

#define MICRO_BENCHMARK_SUBMODULE "timerfd"
#define MICRO_BENCHMARK_MODULE "timer:" MICRO_BENCHMARK_SUBMODULE

typedef micro_benchmark_timer_data timer_data;
typedef micro_benchmark_timer_definition impl;

typedef struct data
{
  clockid_t clock;
  int fd;
  struct itimerspec left;
  struct timespec elapsed;
  bool running;
} data;

static micro_benchmark_clock_type
select_clock (data *d, micro_benchmark_clock_type type)
{
  switch (type)
    {
    default:
      MB_WARN ("Unimplemented clock type %d.\n", (int) type);
      /*  Fallthrough  */
    case MICRO_BENCHMARK_CLOCK_MONOTONIC:
      MB_DEBUG ("Using monotonic clock.\n");
      d->clock = CLOCK_MONOTONIC;
      return MICRO_BENCHMARK_CLOCK_MONOTONIC;

#ifdef HAVE_CLOCK_PROCESS_CPUTIME_ID
    case MICRO_BENCHMARK_CLOCK_PROCESS:
      MB_DEBUG ("Using process time clock.\n");
      d->clock = CLOCK_PROCESS_CPUTIME_ID;
      return MICRO_BENCHMARK_CLOCK_PROCESS;
#endif

#ifdef HAVE_CLOCK_THREAD_CPUTIME_ID
    case MICRO_BENCHMARK_CLOCK_THREAD:
      MB_DEBUG ("Using thread time clock.\n");
      d->clock = CLOCK_THREAD_CPUTIME_ID;
      return MICRO_BENCHMARK_CLOCK_THREAD;
#endif

    case MICRO_BENCHMARK_CLOCK_REALTIME:
#ifdef HAVE_CLOCK_TAI
      /* Avoid leap seconds... */
      MB_DEBUG ("Using TAI clock.\n");
      d->clock = CLOCK_TAI;
#else
      MB_DEBUG ("Using realtime clock.\n");
      d->clock = CLOCK_REALTIME;
#endif
      return MICRO_BENCHMARK_CLOCK_REALTIME;
    }
}

static micro_benchmark_clock_time
get_resolution (clockid_t clock)
{
  struct timespec resolution;
  int res = clock_getres (clock, &resolution);
  MB_HANDLE_ERROR (res == -1, "Unable to get clock resolution");
  micro_benchmark_clock_time ret = { resolution.tv_sec, resolution.tv_nsec };
  MB_DEBUG ("Resolution is %ds, %dns.\n", (int) ret.seconds,
            (int) ret.nanoseconds);
  return ret;
}

static void
mb_timer_init (timer_data *d, micro_benchmark_clock_type type, const void *nu)
{
  (void) nu;
  MB_TRACE ("Init %p with type %d.\n", d, (int) type);
  assert (d);
  assert (!nu);

  data *internal = xcalloc (sizeof (data));
  MB_TRACE ("Allocated internal data %p.\n", internal);

  d->ptr = internal;
  d->name = MICRO_BENCHMARK_SUBMODULE;
  d->type = select_clock (internal, type);
  d->resolution = get_resolution (internal->clock);
  internal->fd = timerfd_create (internal->clock, TFD_NONBLOCK | TFD_CLOEXEC);
  MB_HANDLE_ERROR (internal->fd == -1, "Error creating timerfd");
  MB_TRACE ("Init %p finished.\n", d);
  internal->running = false;
}

static void
mb_timer_cleanup (const timer_data *d)
{
  MB_TRACE ("Release %p.\n", d);
  assert (d);
  assert (d->ptr);

  MB_TRACE ("Deleting timer.\n");
  data *internal = d->ptr;
  assert (!internal->running);
  int ret = close (internal->fd);
  MB_HANDLE_ERROR (ret == -1, "Error closing timerfd");
  MB_TRACE ("Releasing internal data %p.\n", d->ptr);
  xfree (d->ptr, sizeof (data));
  MB_TRACE ("Released %p.\n", d);
}

static void
mb_timer_start (data *d, micro_benchmark_clock_time time)
{
  MB_TRACE ("Start timer %p with %ds, %dns.\n", d,
            (int) time.seconds, (int) time.nanoseconds);
  assert (d);

  static const struct timespec zero;
  d->elapsed = zero;
  d->left.it_value.tv_sec = time.seconds;
  d->left.it_value.tv_nsec = time.nanoseconds;

  d->running = true;
  struct itimerspec old;
  int res = timerfd_settime (d->fd, 0, &d->left, &old);
  MB_HANDLE_ERROR (res == -1, "Error arming timer");
  MB_TRACE ("Old value was %ds, %dns (%ds, %dns)\n",
            (int) old.it_value.tv_sec,
            (int) old.it_value.tv_nsec,
            (int) old.it_interval.tv_sec, (int) old.it_interval.tv_nsec);
  assert (old.it_value.tv_sec == 0);
  assert (old.it_value.tv_nsec == 0);
  assert (old.it_interval.tv_sec == 0);
  assert (old.it_interval.tv_nsec == 0);
  MB_TRACE ("Started %p.\n", d);
}

static void
mb_timer_stop (data *d)
{
  MB_TRACE ("Stop timer %p\n", d);
  assert (d);

  static const struct itimerspec zero;
  struct itimerspec left;
  int res = timerfd_settime (d->fd, 0, &zero, &left);
  MB_HANDLE_ERROR (res == -1, "Error disarming timer");

  struct timespec elapsed;
  micro_benchmark_timespec_sub_ (&elapsed, &d->left.it_value, &left.it_value);
  micro_benchmark_timespec_add_ (&d->elapsed, &d->elapsed, &elapsed);
  MB_DEBUG
    ("Left %d.%09ds (%d.%09ds), elapsed %d.%09ds, total elapsed %d.%09ds.\n",
     (int) left.it_value.tv_sec, (int) left.it_value.tv_nsec,
     (int) left.it_interval.tv_sec, (int) left.it_interval.tv_nsec,
     (int) elapsed.tv_sec, (int) elapsed.tv_nsec, (int) d->elapsed.tv_sec,
     (int) d->elapsed.tv_nsec);

  d->running = false;
  d->left = left;
  MB_TRACE ("Stopped timer %p\n", d);
}

static void
mb_timer_restart (data *d)
{
  MB_TRACE ("Restart timer %p\n", d);
  assert (d);
  MB_TRACE ("Resetting timer to %d.%09ds, interval %d.%09ds.\n",
            (int) d->left.it_value.tv_sec,
            (int) d->left.it_value.tv_nsec,
            (int) d->left.it_interval.tv_sec,
            (int) d->left.it_interval.tv_nsec);

  d->running = true;
  struct itimerspec old;
  int res = timerfd_settime (d->fd, 0, &d->left, &old);
  MB_HANDLE_ERROR (res == -1, "Error arming timer");
  MB_TRACE ("Old value was %ds, %dns (%ds, %dns)\n",
            (int) old.it_value.tv_sec,
            (int) old.it_value.tv_nsec,
            (int) old.it_interval.tv_sec, (int) old.it_interval.tv_nsec);

  assert (old.it_value.tv_sec == 0);
  assert (old.it_value.tv_nsec == 0);
  assert (old.it_interval.tv_sec == 0);
  assert (old.it_interval.tv_nsec == 0);
  MB_TRACE ("Restarted timer %p\n", d);
}

static bool
mb_timer_is_running (data *d)
{
  MB_TRACE ("Check if the timer %p is running\n", d);
  assert (d);
  struct itimerspec curr;
  int res = timerfd_gettime (d->fd, &curr);
  MB_HANDLE_ERROR (res == -1, "Error checking timer");
  MB_TRACE ("Left %d.%09ds (%d.%09ds)\n",
            (int) curr.it_value.tv_sec, (int) curr.it_value.tv_nsec,
            (int) curr.it_interval.tv_sec, (int) curr.it_interval.tv_nsec);
  if (!d->running)
    micro_benchmark_timespec_add_ (&curr.it_value, &curr.it_value,
                                   &d->left.it_value);
  return curr.it_value.tv_sec != 0 || curr.it_value.tv_nsec != 0;
}

static micro_benchmark_clock_time
mb_timer_elapsed (data *d)
{
  MB_TRACE ("Get elapsed time on %p\n", d);
  assert (d);
  micro_benchmark_clock_time ret = {
    d->elapsed.tv_sec,
    d->elapsed.tv_nsec
  };
  MB_DEBUG ("Total elapsed %d.%09ds\n", (int) ret.seconds,
            (int) ret.nanoseconds);
  return ret;
}

const impl mbtimer_timerfd_tmpl_ = {
  {NULL, MICRO_BENCHMARK_SUBMODULE "-template", 0, {}},
  mb_timer_init,
  mb_timer_cleanup,
  (micro_benchmark_timer_start_fun) mb_timer_start,
  (micro_benchmark_timer_stop_fun) mb_timer_stop,
  (micro_benchmark_timer_restart_fun) mb_timer_restart,
  (micro_benchmark_timer_running_fun) mb_timer_is_running,
  (micro_benchmark_timer_elapsed_fun) mb_timer_elapsed
};
