/* timespec.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/clock/timespec.h"

#define MB_MS_IN_S 1000
#define MB_US_IN_S 1000 * MB_MS_IN_S
#define MB_NS_IN_S 1000 * MB_US_IN_S

void
micro_benchmark_timespec_adjust_ (struct timespec *t)
{
  while (t->tv_nsec >= MB_NS_IN_S)
    {
      t->tv_sec += 1;
      t->tv_nsec -= MB_NS_IN_S;
    }

  while (t->tv_nsec < 0)
    {
      t->tv_sec -= 1;
      t->tv_nsec += MB_NS_IN_S;
    }
}

void
micro_benchmark_timespec_add_ (struct timespec *res,
                               const struct timespec *lhs,
                               const struct timespec *rhs)
{
  res->tv_sec = lhs->tv_sec + rhs->tv_sec;
  res->tv_nsec = lhs->tv_nsec + rhs->tv_nsec;
  micro_benchmark_timespec_adjust_ (res);
}

void
micro_benchmark_timespec_sub_ (struct timespec *res,
                               const struct timespec *lhs,
                               const struct timespec *rhs)
{
  res->tv_sec = lhs->tv_sec - rhs->tv_sec;
  res->tv_nsec = lhs->tv_nsec - rhs->tv_nsec;
  micro_benchmark_timespec_adjust_ (res);
}

int
micro_benchmark_timespec_cmp_ (const struct timespec *lhs,
                               const struct timespec *rhs)
{
  if (lhs->tv_sec < rhs->tv_sec || (lhs->tv_sec == rhs->tv_sec
                                    && lhs->tv_nsec < rhs->tv_nsec))
    return -1;
  if (lhs->tv_sec == rhs->tv_sec && lhs->tv_nsec == rhs->tv_nsec)
    return 0;
  return 1;
}
