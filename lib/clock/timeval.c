/* timeval.h --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/clock/timeval.h"

#define MB_MS_IN_S 1000
#define MB_US_IN_S 1000 * MB_MS_IN_S

void
micro_benchmark_timeval_adjust_ (struct timeval *t)
{
  while (t->tv_usec >= MB_US_IN_S)
    {
      t->tv_sec += 1;
      t->tv_usec -= MB_US_IN_S;
    }

  while (t->tv_usec < 0)
    {
      t->tv_sec -= 1;
      t->tv_usec += MB_US_IN_S;
    }
}

void
micro_benchmark_timeval_add_ (struct timeval *res,
                              const struct timeval *lhs,
                              const struct timeval *rhs)
{
  res->tv_sec = lhs->tv_sec + rhs->tv_sec;
  res->tv_usec = lhs->tv_usec + rhs->tv_usec;
  micro_benchmark_timeval_adjust_ (res);
}

void
micro_benchmark_timeval_sub_ (struct timeval *res,
                              const struct timeval *lhs,
                              const struct timeval *rhs)
{
  res->tv_sec = lhs->tv_sec - rhs->tv_sec;
  res->tv_usec = lhs->tv_usec - rhs->tv_usec;
  micro_benchmark_timeval_adjust_ (res);
}

int
micro_benchmark_timeval_cmp_ (const struct timeval *lhs,
                              const struct timeval *rhs)
{
  if (lhs->tv_sec < rhs->tv_sec || (lhs->tv_sec == rhs->tv_sec
                                    && lhs->tv_usec < rhs->tv_usec))
    return -1;
  if (lhs->tv_sec == rhs->tv_sec && lhs->tv_usec == rhs->tv_usec)
    return 0;
  return 1;
}
