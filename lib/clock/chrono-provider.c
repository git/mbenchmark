/* chrono-provider.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/clock/chrono-provider.h"
#include "mbenchmark/stats/meter.h"

#include "internal/clock/chrono-provider.h"
#ifdef HAVE_CLOCK_GETTIME
#include "internal/clock/clock-gettime.h"
#endif
#ifdef HAVE_GETTIMEOFDAY
#include "internal/clock/gettimeofday.h"
#endif
#include "internal/clock/time-t.h"
#include "internal/clock/clock-t.h"
#include "internal/utility/log.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>

#ifdef HAVE_CLOCK_GETTIME
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_TMPL
#define MICRO_BENCHMARK_DEFAULT_CHRONO_TMPL mbchrono_clock_gettime_tmpl_
#endif
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_TYPE
#define MICRO_BENCHMARK_DEFAULT_CHRONO_TYPE MICRO_BENCHMARK_CLOCK_MONOTONIC
#endif
#endif

#ifdef HAVE_GETTIMEOFDAY
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_TMPL
#define MICRO_BENCHMARK_DEFAULT_CHRONO_TMPL mbchrono_gettimeofday_tmpl_
#endif
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_TYPE
#define MICRO_BENCHMARK_DEFAULT_CHRONO_TYPE MICRO_BENCHMARK_CLOCK_REALTIME
#endif
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_TMPL
#define MICRO_BENCHMARK_DEFAULT_CHRONO_TMPL mbchrono_time_t_tmpl_
#endif
#ifndef MICRO_BENCHMARK_DEFAULT_CHRONO_TYPE
#define MICRO_BENCHMARK_DEFAULT_CHRONO_TYPE MICRO_BENCHMARK_CLOCK_REALTIME
#endif

#define MICRO_BENCHMARK_SUBMODULE "provider"
#define MICRO_BENCHMARK_MODULE "chrono:" MICRO_BENCHMARK_SUBMODULE

typedef micro_benchmark_meter_definition impl;

static void
handle_missing_implementation (micro_benchmark_chronometer_provider prov)
{
  switch (prov)
    {
    case MICRO_BENCHMARK_CHRONO_PROVIDER_USER_PROVIDED:
      MB_WARN ("Expected user provided chronometer but none was found,"
               " using default chronometer\n");
      break;

    case MICRO_BENCHMARK_CHRONO_PROVIDER_GETTIMEOFDAY:
      MB_WARN ("The gettimeofday implementation is not available,"
               " using default chronometer\n");
      break;

    case MICRO_BENCHMARK_CHRONO_PROVIDER_CLOCK_GETTIME:
      MB_WARN ("The clock_gettime implementation is not available,"
               " using default chronometer\n");
      break;

    default:
      MB_WARN ("The selected implementation %d is not available, "
               "using default timer\n", (int) prov);
      break;
    }
}

static micro_benchmark_meter default_chrono =
  &MICRO_BENCHMARK_DEFAULT_CHRONO_TMPL;

void
micro_benchmark_chronometer_set_default (micro_benchmark_meter c)
{
  if (c && c->data.sample_type == MICRO_BENCHMARK_SAMPLE_TIME)
    default_chrono = c;
  else
    {
      if (c)
        MB_WARN ("Ignoring provided default %p: not a chronometer.\n", c);
      default_chrono = &MICRO_BENCHMARK_DEFAULT_CHRONO_TMPL;
    }
}

micro_benchmark_meter
micro_benchmark_chronometer_get_default (void)
{
  return default_chrono;
}

micro_benchmark_meter
micro_benchmark_chronometer_template_ (micro_benchmark_chronometer_provider p)
{
  switch (p)
    {
    default:
      handle_missing_implementation (p);
      /*  Fallthrough  */
    case MICRO_BENCHMARK_CHRONO_PROVIDER_DEFAULT:
      return default_chrono;

    case MICRO_BENCHMARK_CHRONO_PROVIDER_TIME_T:
      return micro_benchmark_chrono_time_t_ ();

    case MICRO_BENCHMARK_CHRONO_PROVIDER_CLOCK_T:
      return micro_benchmark_chrono_clock_t_ ();

#ifdef HAVE_GETTIMEOFDAY
    case MICRO_BENCHMARK_CHRONO_PROVIDER_GETTIMEOFDAY:
      return micro_benchmark_chrono_gettimeofday_ ();
#endif

#ifdef HAVE_CLOCK_GETTIME
    case MICRO_BENCHMARK_CHRONO_PROVIDER_CLOCK_GETTIME:
      return micro_benchmark_chrono_clock_gettime_ ();
#endif
    }
}

micro_benchmark_meter
micro_benchmark_chronometer_create_default (void)
{
  micro_benchmark_clock_type type = MICRO_BENCHMARK_DEFAULT_CHRONO_TYPE;
  return micro_benchmark_chronometer_from_meter (type, default_chrono);
}

micro_benchmark_meter
micro_benchmark_chronometer_create (micro_benchmark_clock_type t,
                                    micro_benchmark_chronometer_provider p)
{
  MB_TRACE ("Creating type %d from provider %d.\n", (int) t, (int) p);
  micro_benchmark_meter tmpl = micro_benchmark_chronometer_template_ (p);
  return micro_benchmark_chronometer_from_meter (t, tmpl);
}

micro_benchmark_meter
micro_benchmark_chronometer_from_meter (micro_benchmark_clock_type t,
                                        micro_benchmark_meter tmpl)
{
  MB_TRACE ("Creating type %d from %p.\n", (int) t, tmpl);
  assert (tmpl);
  MB_HANDLE_ERROR (tmpl->data.sample_type != MICRO_BENCHMARK_SAMPLE_TIME,
                   "Provided meter is not a chronometer");
  impl *def = xmalloc (sizeof (impl));
  MB_DEBUG ("Allocated %p.\n", def);
  *def = *tmpl;
  micro_benchmark_stats_meter_init_with_data (def, &t);
  return def;
}

void
micro_benchmark_chronometer_release (micro_benchmark_meter meter)
{
  MB_TRACE ("Releasing %p.\n", meter);
  assert (meter);
  micro_benchmark_stats_meter_cleanup (meter);
  xfree ((void *) meter, sizeof (impl));
}
