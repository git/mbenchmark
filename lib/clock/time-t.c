/* time-t.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/clock/time-t.h"
#include "internal/clock/chrono-adapter.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <limits.h>
#include <stdint.h>
#include <time.h>

#define MICRO_BENCHMARK_SUBMODULE "time_t"
#define MICRO_BENCHMARK_MODULE "chrono:" MICRO_BENCHMARK_SUBMODULE

typedef struct micro_benchmark_meter_data clock_data;
typedef micro_benchmark_meter_definition impl;
typedef struct data
{
  time_t start;
  time_t elapsed;
} data;

static void
mb_chrono_init (clock_data * d, const micro_benchmark_clock_type *tp)
{
  static const micro_benchmark_clock_time min_res = { 1, 0 };
  static const micro_benchmark_clock_time max_res = { INT32_MAX, 0 };
  MB_TRACE ("Init %p with value %p.\n", d, tp);
  assert (d);
  micro_benchmark_clock_type type = tp ? *tp : MICRO_BENCHMARK_CLOCK_REALTIME;
  if (type != MICRO_BENCHMARK_CLOCK_REALTIME)
    MB_WARN ("clock_t only implements process clock but %d was provided\n",
             (int) type);

  data *internal = xmalloc (sizeof (data));
  internal->start = 0;
  internal->elapsed = 0;
  MB_TRACE ("Allocated internal data %p.\n", internal);

  d->ptr = internal;
  d->name = MICRO_BENCHMARK_SUBMODULE;
  d->sample_type = MICRO_BENCHMARK_SAMPLE_TIME;
  d->min_resolution.time = min_res;
  d->max_resolution.time = max_res;
  MB_TRACE ("Init %p finished.\n", d);
}

static void
mb_chrono_cleanup (const clock_data * d)
{
  MB_TRACE ("Release %p.\n", d);
  assert (d);
  assert (d->ptr);
  MB_TRACE ("Releasing internal data %p.\n", d->ptr);
  xfree (d->ptr, sizeof (data));
  MB_TRACE ("Released %p\n", d);
}

static void
mb_chrono_start (data *d)
{
  MB_TRACE ("Start %p.\n", d);
  assert (d);
  d->elapsed = 0;
  time_t res = time (&d->start);
  MB_HANDLE_ERROR ((int) res == -1, "Unable to get time_t value");
  MB_TRACE ("time_t start value %d.\n", (int) res);
}

static void
mb_chrono_stop (data *d)
{
  MB_TRACE ("Stop %p.\n", d);
  assert (d);

  time_t now = time (NULL);
  MB_HANDLE_ERROR ((int) now == -1, "Unable to get time_t value");
  time_t diff = now - d->start;
  d->elapsed += diff;
  MB_DEBUG ("Elapsed %ds, total elapsed %ds.\n", (int) diff,
            (int) d->elapsed);
}

static void
mb_chrono_restart (data *d)
{
  MB_TRACE ("Ending %p.\n", d);
  assert (d);

  time_t res = time (&d->start);
  MB_HANDLE_ERROR ((int) res == -1, "Unable to get time_t value");
  MB_TRACE ("time_t start value %d.\n", (int) res);
}

static micro_benchmark_stats_meter_sample
mb_chrono_get_sample (data *d)
{
  MB_TRACE ("Ending %p.\n", d);
  assert (d);
  assert (d->elapsed <= INT32_MAX);

  micro_benchmark_stats_meter_sample ret = {
    .time = {(int32_t) d->elapsed, 0}
  };
  d->elapsed = 0;
  return ret;
}

const impl mbchrono_time_t_tmpl_ = {
  {NULL, MICRO_BENCHMARK_SUBMODULE "-template", MICRO_BENCHMARK_SAMPLE_TIME,
   {}, {}},
  (micro_benchmark_meter_init_fun) mb_chrono_init,
  mb_chrono_cleanup,
  (micro_benchmark_meter_start_fun) mb_chrono_start,
  (micro_benchmark_meter_stop_fun) mb_chrono_stop,
  (micro_benchmark_meter_restart_fun) mb_chrono_restart,
  (micro_benchmark_meter_get_sample_fun) mb_chrono_get_sample,
};

micro_benchmark_timer
micro_benchmark_timer_time_t_ (micro_benchmark_clock_type type)
{
  if (type != MICRO_BENCHMARK_CLOCK_REALTIME)
    MB_WARN ("time_t only implements realtime but %d was provided\n",
             (int) type);
  return micro_benchmark_timer_adapt_chrono_ (MICRO_BENCHMARK_CLOCK_REALTIME,
                                              &mbchrono_time_t_tmpl_);
}
