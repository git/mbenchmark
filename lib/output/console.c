/* console.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/output/console.h"

#include "mbenchmark/stats.h"
#include "mbenchmark/report/exec.h"
#include "internal/output/common.h"
#include "internal/utility/log.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <limits.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unitypes.h>
#include <unistr.h>
#include <uniconv.h>
#include <uniwidth.h>
#include <uninorm.h>
#include <unistdio.h>

#if !HAVE_OPEN_MEMSTREAM && !HAVE_TMPFILE
#error "Either open_memstream or tmpfile are required to build this file"
#endif

#define MICRO_BENCHMARK_SUBMODULE "console"
#define MICRO_BENCHMARK_MODULE "output:" MICRO_BENCHMARK_SUBMODULE

typedef struct output_field
{
  size_t size;
  char *data;
} output_field;

typedef struct output_row
{
  size_t ncols;
  output_field *cols;
} output_row;

static void
cleanup_row (output_row *row)
{
  if (row->ncols > 0)
    {
      for (size_t i = 0; i < row->ncols; ++i)
        xfree_n (row->cols[i].data, sizeof (row->cols[i].data),
                 row->cols[i].size);
      xfree_n (row->cols, sizeof (output_field), row->ncols);
    }
}

typedef struct output_table
{
  size_t nrows;
  output_row *rows;
} output_table;

static void
cleanup_table (output_table *tbl)
{
  assert (tbl);
  if (tbl->nrows > 0)
    {
      for (size_t i = 0; i < tbl->nrows; ++i)
        cleanup_row (&tbl->rows[i]);
      xfree_n (tbl->rows, sizeof (output_row), tbl->nrows);
      tbl->rows = NULL;
      tbl->nrows = 0;
    }
}

static const char *
stat_value_to_string (micro_benchmark_output_stat v)
{
  switch ((int) v)
    {
    default:
      MB_HANDLE_ERROR (true, "Cannot convert stat value to string");
      return _("*");

    case MICRO_BENCHMARK_STAT_ALL:
      return _("μ/σ²/σ");

    case MICRO_BENCHMARK_STAT_BASIC:
      return _("μ/σ");

    case MICRO_BENCHMARK_STAT_MEAN | MICRO_BENCHMARK_STAT_VARIANCE:
      return _("μ/σ²");

    case MICRO_BENCHMARK_STAT_VARIANCE | MICRO_BENCHMARK_STAT_STD_DEVIATION:
      return _("σ²/σ");

    case MICRO_BENCHMARK_STAT_MEAN:
      return _("μ");

    case MICRO_BENCHMARK_STAT_VARIANCE:
      return _("σ²");

    case MICRO_BENCHMARK_STAT_STD_DEVIATION:
      return _("σ");
    }
}

static size_t
calculate_nrows (micro_benchmark_report report, bool self)
{
  size_t ret = 1;               /*  Always include header row.  */
  const size_t ntests = micro_benchmark_report_get_number_of_tests (report);
  for (size_t i = self ? 0 : 1; i < ntests; ++i)
    {
      micro_benchmark_test_report tr =
        micro_benchmark_report_get_test_report (report, i);
      size_t n_exec = micro_benchmark_test_report_get_num_executions (tr);
      ret += n_exec;

      if (n_exec > 1)
        /* Test name row and separator line.  */
        ret += 2;
    }
  return ret;
}

static size_t
calculate_ncols (const micro_benchmark_output_values *v)
{
  /*  Always include test name column.  */
  size_t ret = 1;
  ret += v->size_constraints ? 1 : 0;
  ret += v->total_time ? 1 : 0;
  ret += v->total_iterations ? 1 : 0;
  ret += v->iterations ? 1 : 0;
  ret += v->total_samples ? 1 : 0;
  ret += v->samples ? 1 : 0;
  ret += v->iteration_time ? 1 : 0;
  ret += v->sample_time ? 1 : 0;
  ret += v->sample_iterations ? 1 : 0;
  return ret;
}

#if HAVE_VASPRINTF
static void
format_data_helper (output_field *field, const char *fmt, va_list args)
{
  int ret = vasprintf (&field->data, fmt, args);
  MB_HANDLE_ERROR (ret <= 0 || !field->data, "Error formatting string");
  field->size = strlen (field->data) + 1;
}

#else /* ! HAVE_VASPRINTF  */
static void
format_data_helper_write_fmt (FILE *tmp, const char *fmt, va_list args)
{
  int ret = vfprintf (tmp, fmt, args);
  MB_HANDLE_ERROR (ret < 0, "Error formatting data");

  ret = fputc ('\0', tmp);
  MB_HANDLE_ERROR (ret == EOF, "Error placing end sentinel");

  ret = fflush (tmp);
  MB_HANDLE_ERROR (ret == EOF, "Error flushing stream");
}

static void
format_data_helper_close (FILE *tmp)
{
  int ret = fclose (tmp);
  MB_HANDLE_ERROR (ret == EOF, "Error closing stream");
}

#if HAVE_OPEN_MEMSTREAM
static void
format_data_helper (output_field *field, const char *fmt, va_list args)
{
  FILE *tmp = open_memstream (&field->data, &field->size);
  MB_HANDLE_ERROR (!tmp, "Error opening memstream");
  format_data_helper_write_fmt (tmp, fmt, args);
  format_data_helper_close (tmp);
}

#elif HAVE_TMPFILE
static void
format_data_helper (output_field *field, const char *fmt, va_list args)
{
  FILE *tmp = tmpfile ();
  MB_HANDLE_ERROR (!tmp, "Error opening temporary file");
  format_data_helper_write_fmt (tmp, fmt, args);

  field->size = ftell (tmp);
  field->data = xmalloc_n (sizeof (char), field->size);
  fseek (tmp, 0, SEEK_SET);
  size_t r = fread (field->data, 1, field->size, tmp);
  MB_HANDLE_ERROR (r != field->size || field->data[f->size - 1] != '\0',
                   "Error reading data");
  assert (field->size - 1 == strlen (field->data));

  format_data_helper_close (tmp);
}
#endif
#endif /*  !HAVE_VASPRINTF  */

static void
format_data (output_field *field, const char *fmt, ...)
{
  /* TODO: Translate to most reduced form.  */
  MB_TRACE ("Writing field %p\n", f);
  va_list args;
  va_start (args, fmt);
  format_data_helper (field, fmt, args);
  va_end (args);

  /* TODO: Transform to most Unicode reduced form, with hopes of
     getting one glyph per code-unit.  */
  MB_DEBUG ("Formatted %zu: \"%s\"\n", field->size, field->data);
}

static int
get_precision (double value)
{
  static const int default_precision = 3;
  if (value > 99.9)
    return default_precision - 3;
  else if (value > 9.99)
    return default_precision - 2;
  else if (value > 0.999)
    return default_precision - 1;
  return default_precision;
}

static void
write_time_value (output_field *ret, micro_benchmark_stats_value v,
                  micro_benchmark_output_stat stat)
{
  const char *units = micro_benchmark_time_units_to_string_ (v.unit);
  const char *unitssqrd = micro_benchmark_time_units_sqrd_to_string_ (v.unit);

  switch ((int) stat)
    {
    default:
      assert (0);

    case MICRO_BENCHMARK_STAT_ALL:
      format_data (ret, _("%.*f%s/%.*f%s/%.*f%s"), get_precision (v.mean),
                   v.mean, units, get_precision (v.variance), v.variance,
                   unitssqrd, get_precision (v.std_deviation),
                   v.std_deviation, units);
      break;

    case MICRO_BENCHMARK_STAT_BASIC:
      format_data (ret, _("%.*f%s/%.*f%s"), get_precision (v.mean), v.mean,
                   units, get_precision (v.std_deviation), v.std_deviation,
                   units);
      break;

    case MICRO_BENCHMARK_STAT_MEAN | MICRO_BENCHMARK_STAT_VARIANCE:
      format_data (ret, _("%.*f%s/%.*f%s"), get_precision (v.mean), v.mean,
                   units, get_precision (v.variance), v.variance, unitssqrd);
      break;

    case MICRO_BENCHMARK_STAT_VARIANCE | MICRO_BENCHMARK_STAT_STD_DEVIATION:
      format_data (ret, _("%.*f%s/%.*f%s"), get_precision (v.variance),
                   v.variance, unitssqrd, get_precision (v.std_deviation),
                   v.std_deviation, units);
      break;

    case MICRO_BENCHMARK_STAT_STD_DEVIATION:
      format_data (ret, _("%.*f%s"), get_precision (v.std_deviation),
                   v.std_deviation, units);
      break;

    case MICRO_BENCHMARK_STAT_VARIANCE:
      format_data (ret, _("%.*f%s"), get_precision (v.variance), v.variance,
                   unitssqrd);
      break;

    case MICRO_BENCHMARK_STAT_MEAN:
      format_data (ret, _("%.*f%s"), get_precision (v.mean), v.mean, units);
      break;
    }
}

static void
write_iteration_value (output_field *ret, micro_benchmark_stats_value v,
                       micro_benchmark_output_stat stat)
{
  const char *units = micro_benchmark_iteration_units_to_string_ (v.unit);

  switch ((int) stat)
    {
    default:
      assert (0);

    case MICRO_BENCHMARK_STAT_ALL:
      format_data (ret, _("%.*f%s/%.*f%s/%.*f%s"), get_precision (v.mean),
                   v.mean, units, get_precision (v.variance), v.variance,
                   units, get_precision (v.std_deviation), v.std_deviation,
                   units);
      break;

    case MICRO_BENCHMARK_STAT_BASIC:
      format_data (ret, _("%.*f%s/%.*f%s"), get_precision (v.mean), v.mean,
                   units, get_precision (v.std_deviation), v.std_deviation,
                   units);
      break;

    case MICRO_BENCHMARK_STAT_MEAN | MICRO_BENCHMARK_STAT_VARIANCE:
      format_data (ret, _("%.*f%s/%.*f%s"), get_precision (v.mean), v.mean,
                   units, get_precision (v.variance), v.variance, units);
      break;

    case MICRO_BENCHMARK_STAT_VARIANCE | MICRO_BENCHMARK_STAT_STD_DEVIATION:
      format_data (ret, _("%.*f%s/%.*f%s"), get_precision (v.variance),
                   v.variance, units, get_precision (v.std_deviation),
                   v.std_deviation, units);
      break;

    case MICRO_BENCHMARK_STAT_STD_DEVIATION:
      format_data (ret, _("%.*f%s"), get_precision (v.std_deviation),
                   v.std_deviation, units);
      break;

    case MICRO_BENCHMARK_STAT_VARIANCE:
      format_data (ret, _("%.*f%s"), get_precision (v.variance), v.variance,
                   units);
      break;

    case MICRO_BENCHMARK_STAT_MEAN:
      format_data (ret, _("%.*f%s"), get_precision (v.mean), v.mean, units);
      break;
    }
}

static void
write_sizes_array_to_stream (FILE *tmp, size_t dimensions,
                             const size_t *sizes)
{
  /* TRANSLATORS: This msgid is used together with...[1]  */
  int ret = fprintf (tmp, _("{%zu"), sizes[0]);
  MB_HANDLE_ERROR (ret <= 0, "Error formatting size array");
  for (size_t d = 1; d < dimensions; ++d)
    {
      /* TRANSLATORS: [1]... this other msgid, to concatenate the
         dimensional vector.  The sequence ends with...[2] */
      ret = fprintf (tmp, _(",%zu"), sizes[d]);
      MB_HANDLE_ERROR (ret <= 0, "Error formatting size array");
    }
  /* TRANSLATORS: [2]... this other msgid.  */
  ret = fprintf (tmp, _("}"));
  MB_HANDLE_ERROR (ret <= 0, "Error formatting size array");
  ret = fputc ('\0', tmp);
  MB_HANDLE_ERROR (ret == EOF, "Error placing end sentinel");
}

#if HAVE_OPEN_MEMSTREAM
static void
write_sizes_array_helper (output_field *field, size_t dimensions,
                          const size_t *sizes)
{
  FILE *tmp = open_memstream (&field->data, &field->size);
  MB_HANDLE_ERROR (!tmp, "Error opening memstream");
  write_sizes_array_to_stream (tmp, dimensions, sizes);
  int ret = fclose (tmp);
  MB_HANDLE_ERROR (ret == EOF, "Error closing stream");
}

#elif HAVE_TMPFILE
static void
write_sizes_array_helper (output_field *field, size_t dimensions,
                          const size_t *sizes)
{
  FILE *tmp = tmpfile ();
  MB_HANDLE_ERROR (!tmp, "Error opening temporary file");
  write_sizes_array_to_stream (tmp, dimensions, sizes);

  field->size = ftell (tmp);
  field->data = xmalloc_n (sizeof (char), f->size);
  fseek (tmp, 0, SEEK_SET);
  size_t r = fread (field->data, 1, field->size, tmp);
  MB_HANDLE_ERROR (r != field->size || field->data[f->size - 1] != '\0',
                   "Error reading data");
  assert (f->size - 1 == strlen (f->data));

  int ret = fclose (tmp);
  MB_HANDLE_ERROR (ret == EOF, "Error closing stream");
}
#endif

static void
write_sizes_array (output_field *field, size_t dimensions,
                   const size_t *sizes)
{
  MB_TRACE ("Writing sizes array %p[%zu] to %p\n", sizes, dimensions, res);
  write_sizes_array_helper (field, dimensions, sizes);
  /* TODO: Transform to most Unicode reduced form (hoping 1 glyph per
     code-unit).  */
  MB_DEBUG ("Formatted %zu: \"%s\"\n", field->size, field->data);
}

static void
write_headers (output_row *r, const micro_benchmark_output_values *v,
               size_t ncols)
{
  assert (v);
  assert (r);
  r->cols = xmalloc_n (sizeof (output_field), ncols);

  size_t pos = 0;
  format_data (&r->cols[pos++], _("Test Name"));

  if (v->total_iterations)
    format_data (&r->cols[pos++], _("Total It."));

  if (v->iterations)
    format_data (&r->cols[pos++], _("Iterations"));

  if (v->iteration_time)
    format_data (&r->cols[pos++], _("It.Time (%s)"),
                 stat_value_to_string (v->iteration_time));

  if (v->total_samples)
    format_data (&r->cols[pos++], _("Total S."));

  if (v->samples)
    format_data (&r->cols[pos++], _("Samples"));

  if (v->sample_time)
    format_data (&r->cols[pos++], _("S.Time (%s)"),
                 stat_value_to_string (v->sample_time));

  if (v->sample_iterations)
    format_data (&r->cols[pos++], _("S.Iter. (%s)"),
                 stat_value_to_string (v->sample_iterations));

  if (v->total_time)
    format_data (&r->cols[pos++], _("Total Time"));

  if (v->size_constraints)
    format_data (&r->cols[pos++], _("Sizes"));

  assert (pos == ncols);
  r->ncols = ncols;
}

static void
write_execution_row (output_row *row, micro_benchmark_exec_report report,
                     const micro_benchmark_output_values *values,
                     size_t ncols)
{
  assert (row);
  assert (report);
  assert (values);
  assert (ncols > 0);

  row->ncols = ncols;
  row->cols = xmalloc_n (sizeof (output_field), ncols);

  size_t pos = 0;
  const char *name = micro_benchmark_exec_report_get_name (report);
  format_data (&row->cols[pos++], "%s", name);

  if (values->total_iterations)
    format_data (&row->cols[pos++], "%zu",
                 micro_benchmark_exec_report_total_iterations (report));

  if (values->iterations)
    format_data (&row->cols[pos++], "%zu",
                 micro_benchmark_exec_report_get_iterations (report));

  if (values->iteration_time)
    {
      micro_benchmark_stats_value it_time =
        micro_benchmark_exec_report_iteration_time (report);
      write_time_value (&row->cols[pos++], it_time, values->iteration_time);
    }

  if (values->total_samples)
    format_data (&row->cols[pos++], "%zu",
                 micro_benchmark_exec_report_total_samples (report));

  if (values->samples)
    format_data (&row->cols[pos++], "%zu",
                 micro_benchmark_exec_report_used_samples (report));

  if (values->sample_time)
    {
      micro_benchmark_stats_value s_time =
        micro_benchmark_exec_report_sample_time (report);
      write_time_value (&row->cols[pos++], s_time, values->sample_time);
    }

  if (values->sample_iterations)
    {
      micro_benchmark_stats_value s_it =
        micro_benchmark_exec_report_sample_iterations (report);
      write_iteration_value (&row->cols[pos++], s_it,
                             values->sample_iterations);
    }

  if (values->total_time)
    {
      static const int total_time_precision = 4;
      micro_benchmark_clock_time elapsed =
        micro_benchmark_exec_report_total_time (report);
      /*  TODO: Extract time printing to its module  */
      double s = elapsed.seconds;
      s += elapsed.nanoseconds / 1000000000.0;
      format_data (&row->cols[pos++], "%.*f%s", total_time_precision, s,
                   _("s"));
    }

  if (values->size_constraints)
    {
      const size_t *sizes;
      size_t dimensions;
      micro_benchmark_exec_report_get_sizes (report, &sizes, &dimensions);
      if (dimensions > 0)
        write_sizes_array (&row->cols[pos++], dimensions, sizes);
      else
        format_data (&row->cols[pos++], "%s", _("∅"));
    }
}

static void
write_test_row (output_row *row, micro_benchmark_test_report test,
                size_t ncols)
{
  assert (row);
  assert (test);
  assert (ncols > 0);
  const char *name = micro_benchmark_test_report_get_name (test);
  row->ncols = ncols;
  row->cols = xmalloc_n (sizeof (output_field), ncols);
  size_t pos = 0;
  format_data (&row->cols[pos++], "%s", name);
  while (pos < ncols)
    format_data (&row->cols[pos++], _("--"));
}

static output_table
parse_report (micro_benchmark_report report,
              const micro_benchmark_output_values *v)
{
  assert (report);
  assert (v);

  const size_t ncols = calculate_ncols (v);
  const size_t nrows = calculate_nrows (report, v->self_test);
  const size_t start = v->self_test ? 0 : 1;

  output_table ret;
  ret.nrows = nrows;
  ret.rows = xmalloc_n (sizeof (output_row), ret.nrows);

  size_t pos = 0;
  write_headers (&ret.rows[pos++], v, ncols);

  const size_t n_tests = micro_benchmark_report_get_number_of_tests (report);
  for (size_t i = start; i < n_tests; ++i)
    {
      micro_benchmark_test_report r =
        micro_benchmark_report_get_test_report (report, i);

      size_t n_cases = micro_benchmark_test_report_get_num_executions (r);
      if (n_cases > 1)
        {
          assert (pos < nrows);
          write_test_row (&ret.rows[pos++], r, ncols);
        }

      for (size_t j = 0; j < n_cases; ++j)
        {
          micro_benchmark_exec_report e =
            micro_benchmark_test_report_get_exec_report (r, j);
          assert (pos < nrows);
          write_execution_row (&ret.rows[pos++], e, v, ncols);
        }
      if (n_cases > 1)
        {
          assert (pos < nrows);
          ret.rows[pos].ncols = 0;
          ret.rows[pos].cols = NULL;
          pos++;
        }
    }
  assert (pos == nrows);
  return ret;
}

static uint8_t *
get_u8_string (const char *str, size_t *size)
{
  assert (str);
  assert (size);

  uint8_t *u8str = u8_strconv_from_locale (str);
  MB_HANDLE_ERROR (!u8str, "Out of memory");

  *size = u8_strlen (u8str);
  return u8str;
}

static size_t
number_of_units (const uint8_t *str, size_t size)
{
  assert (str);
  const char *loc = locale_charset ();

  size_t norm_size;
  uint8_t *norm = u8_normalize (UNINORM_NFC, str, size, NULL, &norm_size);
  MB_HANDLE_ERROR (!norm, "Out of memory");

  size_t ret = u8_width (norm, norm_size, loc);
  MB_DEBUG ("Counted %zu units on \"%s\"\n", ret, str);

  xfree_n (norm, sizeof (uint8_t), norm_size);
  return ret;
}

static size_t
calculate_total_size (const output_table *tbl, const char *sep, size_t ncols,
                      size_t *max_sizes)
{
  assert (tbl);
  assert (max_sizes);
  assert (ncols > 0);
  assert (sep);

  for (size_t i = 0; i < tbl->nrows; ++i)
    {
      assert (ncols == tbl->rows[i].ncols || tbl->rows[i].ncols == 0);
      for (size_t j = 0; j < tbl->rows[i].ncols; ++j)
        {
          size_t stmp;
          uint8_t *tmp = get_u8_string (tbl->rows[i].cols[j].data, &stmp);

          size_t len = number_of_units (tmp, stmp);
          if (max_sizes[j] < len)
            max_sizes[j] = len;

          xfree_n (tmp, sizeof (uint8_t), stmp);
        }
    }

  size_t total_size = max_sizes[0];
  const size_t sep_len = strlen (sep);
  for (size_t i = 1; i < ncols; ++i)
    {
      MB_HANDLE_ERROR (max_sizes[i] > INT_MAX, "Column too big");
      total_size += max_sizes[i] + sep_len;
    }
  return total_size;
}


static void
write_separator_line (FILE *out, const char *sep, size_t n)
{
  assert (out);
  assert (sep);

  for (size_t i = 0; i < n; ++i)
    {
      int ret = fprintf (out, "%s", sep);
      MB_HANDLE_ERROR (ret <= 0, "Error writing separator");
    }

  int ret = fprintf (out, "\n");
  MB_HANDLE_ERROR (ret <= 0, "Error writing separator");
}

static void
fill_blanks (FILE *out, size_t max, const uint8_t *str, size_t sstr)
{
  assert (out);
  assert (str);
  assert (max > 0);

  size_t curr = number_of_units (str, sstr);
  assert (max >= curr);

  MB_DEBUG ("Filling blanks on %p, current %zu from %zu\n", out, curr, max);
  for (size_t i = curr; i < max; ++i)
    {
      int ret = fprintf (out, " ");
      MB_HANDLE_ERROR (ret <= 0, "Error filling blanks");
    }
}

static void
write_row_to_stream (const output_table *tbl, size_t i, FILE *out,
                     const char *sep, size_t ncols, const size_t *max_sizes)
{
  assert (tbl);
  assert (tbl->nrows > i);
  assert (out);
  assert (sep);
  assert (ncols > 0);
  assert (tbl->rows[i].ncols == ncols);
  assert (max_sizes);

  size_t str_size;
  uint8_t *str = get_u8_string (tbl->rows[i].cols[0].data, &str_size);
  fill_blanks (out, max_sizes[0], str, str_size);
  int ret = ulc_fprintf (out, "%U", str);
  MB_HANDLE_ERROR (ret <= 0, "Error writing report data");
  xfree_n (str, sizeof (uint8_t), str_size);

  for (size_t j = 1; j < ncols; ++j)
    {
      ret = fprintf (out, "%s", sep);
      MB_HANDLE_ERROR (ret <= 0, "Error writing separator");
      str = get_u8_string (tbl->rows[i].cols[j].data, &str_size);
      if (i != 0)
        fill_blanks (out, max_sizes[j], str, str_size);
      ret = ulc_fprintf (out, "%U", str);
      MB_HANDLE_ERROR (ret < 0, "Error writing report data");
      if (i == 0)
        fill_blanks (out, max_sizes[j], str, str_size);
      xfree_n (str, sizeof (uint8_t), str_size);
    }
  ret = fprintf (out, "\n");
  MB_HANDLE_ERROR (ret <= 0, "Error writing report data");
}

static size_t
write_table (const output_table *tbl, FILE *out)
{
  assert (tbl);
  assert (out);
  assert (tbl->nrows > 0);
  const size_t ncols = tbl->rows[0].ncols;
  assert (ncols > 0);
  size_t *max_sizes = xcalloc_n (sizeof (size_t), ncols);
  const char *sep = _(" | ");
  size_t total_size = calculate_total_size (tbl, sep, ncols, max_sizes);

  write_separator_line (out, _("="), total_size);
  write_row_to_stream (tbl, 0, out, sep, ncols, max_sizes);
  write_separator_line (out, _("="), total_size);

  for (size_t i = 1; i < tbl->nrows; ++i)
    {
      if (tbl->rows[i].ncols == 0)
        write_separator_line (out, _("-"), total_size);
      else
        write_row_to_stream (tbl, i, out, sep, ncols, max_sizes);
    }
  xfree_n (max_sizes, sizeof (size_t), ncols);
  write_separator_line (out, _("="), total_size);
  return total_size;
}

static void
write_execution_report_extra_data (micro_benchmark_exec_report report,
                                   FILE *out, size_t total_size)
{
  const char *full_name = micro_benchmark_exec_report_get_name (report);
  const size_t n_meters =
    micro_benchmark_exec_report_number_of_meters (report);
  if (n_meters > 0)
    {
      int ret = fprintf (out, _("%s: Extra data\n"), full_name);
      MB_HANDLE_ERROR (ret <= 0, "Error writing extra data");
    }

  for (size_t i = 0; i < n_meters; ++i)
    {
      micro_benchmark_report_extra_data extra =
        micro_benchmark_exec_report_get_extra_data (report, i);
      if (extra.name)
        fprintf (out, _("%s\n"), extra.name);
      for (size_t j = 0; j < extra.size; ++j)
        fprintf (out, _("%s=\"%s\"\n"), extra.keys[j], extra.values[j]);
      if (extra.name)
        write_separator_line (out, _("_"), total_size);
    }
}

static void
write_test_report_extra_data (micro_benchmark_test_report test, FILE *out,
                              size_t total_size)
{
  size_t n_cases = micro_benchmark_test_report_get_num_executions (test);
  if (n_cases > 1)
    write_separator_line (out, _("-"), total_size);

  for (size_t i = 0; i < n_cases; ++i)
    {
      micro_benchmark_exec_report e =
        micro_benchmark_test_report_get_exec_report (test, i);
      write_execution_report_extra_data (e, out, total_size);
    }

  if (n_cases > 1)
    write_separator_line (out, _("-"), total_size);
}

static size_t
calculate_number_of_executions (micro_benchmark_report report)
{
  size_t ret = 0;
  const size_t n_tests = micro_benchmark_report_get_number_of_tests (report);
  for (size_t i = 1; i < n_tests; ++i)
    {
      micro_benchmark_test_report r =
        micro_benchmark_report_get_test_report (report, i);
      ret += micro_benchmark_test_report_get_num_executions (r);
    }
  return ret;
}

void
micro_benchmark_report_write_console_ (micro_benchmark_report report,
                                       FILE *out,
                                       const micro_benchmark_output_values *v)
{
  MB_TRACE ("Writing console report %p.\n", report);
  assert (report);
  assert (out);
  assert (v);

  const size_t n_execs = calculate_number_of_executions (report);
  int ret = fprintf (out, _("%s: %s (%zu %s)\n"), _("Suite"),
                     micro_benchmark_report_get_name (report), n_execs,
                     P_ ("test execution", "test executions", n_execs));
  MB_HANDLE_ERROR (ret <= 0, "Error writing console report");

  output_table tbl = parse_report (report, v);
  size_t total_size = write_table (&tbl, out);
  cleanup_table (&tbl);

  if (v->extra_data)
    {
      const size_t n_tests =
        micro_benchmark_report_get_number_of_tests (report);
      for (size_t i = 0; i < n_tests; ++i)
        {
          micro_benchmark_test_report r =
            micro_benchmark_report_get_test_report (report, i);
          write_test_report_extra_data (r, out, total_size);
        }
    }
}
