/* common.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/output/common.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "common"
#define MICRO_BENCHMARK_MODULE "output:" MICRO_BENCHMARK_SUBMODULE

const char *
micro_benchmark_iteration_units_to_string_ (micro_benchmark_stats_unit unit)
{
  switch (unit)
    {
    default:
      MB_WARN ("Invalid iteration unit %d.\n", (int) unit);
      /*  Fallthrough  */
    case MICRO_BENCHMARK_STATS_UNIT_NONE:
      return _("*");

    case MICRO_BENCHMARK_STATS_ITERATIONS:
      return "";
    }
}

const char *
micro_benchmark_time_units_to_string_ (micro_benchmark_stats_unit unit)
{
  switch (unit)
    {
    default:
      MB_WARN ("Invalid time unit %d.\n", (int) unit);
      /*  Fallthrough  */
    case MICRO_BENCHMARK_STATS_UNIT_NONE:
      return _("*");

    case MICRO_BENCHMARK_STATS_TIME_MIN:
      return _("'");
    case MICRO_BENCHMARK_STATS_TIME_S:
      return _("s");
    case MICRO_BENCHMARK_STATS_TIME_MS:
      return _("ms");
    case MICRO_BENCHMARK_STATS_TIME_US:
      return _("μs");
    case MICRO_BENCHMARK_STATS_TIME_NS:
      return _("ns");
    }
}

const char *
micro_benchmark_time_units_sqrd_to_string_ (micro_benchmark_stats_unit unit)
{
  switch (unit)
    {
    default:
      MB_WARN ("Invalid time unit %d.\n", (int) unit);
      /*  Fallthrough  */
    case MICRO_BENCHMARK_STATS_UNIT_NONE:
      return _("*");

    case MICRO_BENCHMARK_STATS_TIME_MIN:
      return _("'²");
    case MICRO_BENCHMARK_STATS_TIME_S:
      return _("s²");
    case MICRO_BENCHMARK_STATS_TIME_MS:
      return _("ms²");
    case MICRO_BENCHMARK_STATS_TIME_US:
      return _("μs²");
    case MICRO_BENCHMARK_STATS_TIME_NS:
      return _("ns²");
    }
}
