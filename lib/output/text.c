/* text.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/output/text.h"

#include "mbenchmark/stats.h"
#include "mbenchmark/report/exec.h"
#include "internal/output/common.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define MICRO_BENCHMARK_SUBMODULE "text"
#define MICRO_BENCHMARK_MODULE "output:" MICRO_BENCHMARK_SUBMODULE

static void
write_execution_report (const char *suite_name, const char *test_name,
                        micro_benchmark_exec_report report, FILE *out,
                        const micro_benchmark_output_values *values)
{
  const char *full_name = micro_benchmark_exec_report_get_name (report);
  fprintf (out, "%s: %s\n", _("Suite"), suite_name);
  fprintf (out, "%s: %s\n", _("Test"), test_name);
  fprintf (out, "%s: %s\n", _("Test-Case"), full_name);

  if (values->size_constraints)
    {
      const size_t *sizes;
      size_t dimensions;
      micro_benchmark_exec_report_get_sizes (report, &sizes, &dimensions);
      if (dimensions > 0)
        {
          /* TODO: I18n.  */
          fprintf (out, "%s: (", _("Test-Dimensions"));
          fprintf (out, "%zu", sizes[0]);
          for (size_t d = 1; d < dimensions; ++d)
            fprintf (out, ", %zu", sizes[d]);
          fprintf (out, ")\n");
        }
      else
        fprintf (out, "%s: ()\n", _("Test-Dimensions"));
    }

  if (values->total_time)
    {
      micro_benchmark_clock_time elapsed =
        micro_benchmark_exec_report_total_time (report);
      /*  TODO: Extract time printing to its module  */
      double s = elapsed.seconds;
      s += elapsed.nanoseconds / 1000000000.0;
      fprintf (out, "%s: %f seconds\n", _("Total-Time"), s);
    }

  if (values->total_iterations)
    {
      size_t it = micro_benchmark_exec_report_total_iterations (report);
      fprintf (out, "%s: %zu\n", _("Total-Iterations"), it);
    }

  if (values->total_samples)
    {
      size_t total = micro_benchmark_exec_report_total_samples (report);
      fprintf (out, "%s: %zu\n", _("Total-Samples"), total);
    }

  if (values->iterations)
    {
      size_t it = micro_benchmark_exec_report_get_iterations (report);
      fprintf (out, "%s: %zu\n", _("Iterations"), it);
    }

  if (values->samples)
    {
      size_t samples = micro_benchmark_exec_report_used_samples (report);
      fprintf (out, "%s: %zu\n", _("Samples"), samples);
    }

  if (values->iteration_time)
    {
      micro_benchmark_stats_value v =
        micro_benchmark_exec_report_iteration_time (report);
      const char *units = micro_benchmark_time_units_to_string_ (v.unit);
      const char *unitssqrd =
        micro_benchmark_time_units_sqrd_to_string_ (v.unit);

      if (MICRO_BENCHMARK_STAT_MEAN & values->iteration_time)
        fprintf (out, "%s: %f %s\n", _("Iteration-Time"), v.mean, units);

      if (MICRO_BENCHMARK_STAT_STD_DEVIATION & values->iteration_time)
        fprintf (out, "%s: %f %s\n", _("Iteration-Time-Dev"), v.std_deviation,
                 units);

      if (MICRO_BENCHMARK_STAT_VARIANCE & values->iteration_time)
        fprintf (out, "%s: %f %s\n", _("Iteration-Time-Var"), v.variance,
                 unitssqrd);

    }

  if (values->sample_time)
    {
      micro_benchmark_stats_value v =
        micro_benchmark_exec_report_sample_time (report);
      const char *units = micro_benchmark_time_units_to_string_ (v.unit);
      const char *unitssqrd =
        micro_benchmark_time_units_sqrd_to_string_ (v.unit);

      if (MICRO_BENCHMARK_STAT_MEAN & values->sample_time)
        fprintf (out, "%s: %f %s\n", _("Sample-Time"), v.mean, units);

      if (MICRO_BENCHMARK_STAT_STD_DEVIATION & values->sample_time)
        fprintf (out, "%s: %f %s\n", _("Sample-Time-Dev"), v.std_deviation,
                 units);

      if (MICRO_BENCHMARK_STAT_VARIANCE & values->sample_time)
        fprintf (out, "%s: %f %s\n", _("Sample-Time-Var"), v.variance,
                 unitssqrd);
    }

  if (values->sample_iterations)
    {
      micro_benchmark_stats_value v =
        micro_benchmark_exec_report_sample_iterations (report);

      if (MICRO_BENCHMARK_STAT_MEAN & values->sample_iterations)
        fprintf (out, "%s: %f %s\n", _("Iterations-Sample"), v.mean,
                 _("iterations"));
      if (MICRO_BENCHMARK_STAT_STD_DEVIATION & values->sample_iterations)
        fprintf (out, "%s: %f %s\n", _("Iterations-Sample-Dev"),
                 v.std_deviation, _("iterations"));
      if (MICRO_BENCHMARK_STAT_VARIANCE & values->sample_iterations)
        fprintf (out, "%s: %f %s\n", _("Iterations-Sample-Var"), v.variance,
                 _("iterations"));
    }

  if (values->extra_data)
    {
      const size_t n_meters =
        micro_benchmark_exec_report_number_of_meters (report);
      for (size_t i = 0; i < n_meters; ++i)
        {
          micro_benchmark_report_extra_data extra =
            micro_benchmark_exec_report_get_extra_data (report, i);
          if (extra.name)
            fprintf (out, "%s[%zu]: %s\n", _("Meter"), i, extra.name);
          for (size_t j = 0; j < extra.size; ++j)
            fprintf (out, "%s[%zu][%s]: %s\n", _("Meter"), i, extra.keys[j],
                     extra.values[j]);
        }
    }

  fprintf (out, "\n\n");
}

static void
write_test_report (const char *suite_name, micro_benchmark_test_report test,
                   FILE *out, const micro_benchmark_output_values *values)
{
  const char *test_name = micro_benchmark_test_report_get_name (test);
  size_t n_cases = micro_benchmark_test_report_get_num_executions (test);
  for (size_t i = 0; i < n_cases; ++i)
    {
      micro_benchmark_exec_report e =
        micro_benchmark_test_report_get_exec_report (test, i);
      write_execution_report (suite_name, test_name, e, out, values);
    }
}

void
micro_benchmark_report_write_text_ (micro_benchmark_report report, FILE *out,
                                    const micro_benchmark_output_values *v)
{
  MB_TRACE ("Writing text report %p.\n", report);
  assert (report);
  assert (out);
  assert (v);
  const size_t start = v->self_test ? 0 : 1;
  const char *suite_name = micro_benchmark_report_get_name (report);
  fprintf (out, "# %s: %s\n", _("Suite Name"), suite_name);
  size_t n_tests = micro_benchmark_report_get_number_of_tests (report);
  fprintf (out, "# %s: %zu\n\n", _("Number of tests"), n_tests);
  for (size_t i = start; i < n_tests; ++i)
    {
      micro_benchmark_test_report r =
        micro_benchmark_report_get_test_report (report, i);
      write_test_report (suite_name, r, out, v);
    }
}
