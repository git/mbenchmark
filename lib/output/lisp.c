/* lisp.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/output/lisp.h"

#include "mbenchmark/stats.h"
#include "mbenchmark/report/exec.h"
#include "internal/output/common.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "lisp"
#define MICRO_BENCHMARK_MODULE "output:" MICRO_BENCHMARK_SUBMODULE

static void
escape_quote (char **text, size_t *size)
{
  MB_TRACE ("Escaping quotes from %p, size %p.\n", text, size);
  assert (text);
  assert (size);

  char *old = *text;
  size_t n_bk = 0;
  for (size_t i = 0; i < *size; ++i)
    if (old[i] == '"' || old[i] == '\\')
      n_bk++;

  if (n_bk > 0)
    {
      const size_t new_size = *size + n_bk;
      char *copy = xmalloc (new_size);
      for (size_t p = 0, i = 0; i < *size; ++i, ++p)
        {
          if (old[i] == '"' || old[i] == '\\')
            copy[p++] = '\\';
          copy[p] = old[i];
        }
      xfree (old, *size);
      *text = copy;
      *size = new_size;
    }
}

static void
write_numeric_value (FILE *out, const char *fmt, ...)
{
  assert (out);
  assert (fmt);
  va_list args;
  va_start (args, fmt);
  char *old = setlocale (LC_NUMERIC, NULL);
  if (!old)
    {
      int ret = vfprintf (out, fmt, args);
      MB_HANDLE_ERROR (ret <= 0, "Error writing numeric data\n");
    }
  else
    {
      char *curr = xstrdup (old);
      MB_HANDLE_ERROR (!curr, "Out of memory");

      setlocale (LC_NUMERIC, "C");
      int ret = vfprintf (out, fmt, args);
      MB_HANDLE_ERROR (ret <= 0,
                       "Error writing non-localized numeric data\n");
      setlocale (LC_NUMERIC, curr);
      xfree (curr, strlen (curr));
    }
  va_end (args);
}

static void
write_execution_report (micro_benchmark_exec_report report, FILE *out,
                        size_t num,
                        const micro_benchmark_output_values *values)
{
  MB_TRACE ("Writing execution report %p.\n", report);

  write_numeric_value (out, "    (%s (%s %zu)\n", _("execution"),
                       _("number"), num);

  char *full_name = xstrdup (micro_benchmark_exec_report_get_name (report));
  size_t full_name_size = strlen (full_name) + 1;
  escape_quote (&full_name, &full_name_size);
  fprintf (out, "      (%s \"%s\")", _("name"), full_name);
  xfree (full_name, full_name_size);

  if (values->size_constraints)
    {
      const size_t *sizes;
      size_t dimensions;
      micro_benchmark_exec_report_get_sizes (report, &sizes, &dimensions);
      if (dimensions > 0)
        {
          write_numeric_value (out, "\n      (%s (%zu", _("dimensions"),
                               sizes[0]);
          for (size_t d = 1; d < dimensions; ++d)
            write_numeric_value (out, " %zu", sizes[d]);
          fprintf (out, "))");
        }
    }

  if (values->total_time)
    {
      micro_benchmark_clock_time elapsed =
        micro_benchmark_exec_report_total_time (report);
      /*  TODO: Extract time printing to its module  */
      double s = elapsed.seconds;
      s += elapsed.nanoseconds / 1000000000.0;
      write_numeric_value (out, "\n      (%s %f \"%s\")", _("total-time"),
                           s, _("s"));
    }

  if (values->total_iterations)
    {
      size_t it = micro_benchmark_exec_report_total_iterations (report);
      write_numeric_value (out, "\n      (%s %zu)", _("total-iterations"),
                           it);
    }

  if (values->iterations)
    {
      size_t it = micro_benchmark_exec_report_get_iterations (report);
      write_numeric_value (out, "\n      (%s %zu)", _("iterations"), it);
    }

  if (values->total_samples)
    {
      size_t total = micro_benchmark_exec_report_total_samples (report);
      write_numeric_value (out, "\n      (%s %zu)", _("total-samples"),
                           total);
    }

  if (values->samples)
    {
      size_t samples = micro_benchmark_exec_report_used_samples (report);
      write_numeric_value (out, "\n      (%s %zu)", _("used-samples"),
                           samples);
    }

  if (values->iteration_time)
    {
      micro_benchmark_stats_value v =
        micro_benchmark_exec_report_iteration_time (report);
      const char *units = micro_benchmark_time_units_to_string_ (v.unit);
      const char *unitssqrd =
        micro_benchmark_time_units_sqrd_to_string_ (v.unit);

      fprintf (out, "\n      (%s", _("iteration-time"));
      if (MICRO_BENCHMARK_STAT_MEAN & values->iteration_time)
        write_numeric_value (out, "\n        (%s %f \"%s\")", _("mean"),
                             v.mean, units);

      if (MICRO_BENCHMARK_STAT_STD_DEVIATION & values->iteration_time)
        write_numeric_value (out, "\n        (%s %f \"%s\")",
                             _("std-deviation"), v.std_deviation, units);

      if (MICRO_BENCHMARK_STAT_VARIANCE & values->iteration_time)
        write_numeric_value (out, "\n        (%s %f \"%s\")", _("variance"),
                             v.variance, unitssqrd);

      fprintf (out, ")");
    }

  if (values->sample_time)
    {
      micro_benchmark_stats_value v =
        micro_benchmark_exec_report_sample_time (report);
      const char *units = micro_benchmark_time_units_to_string_ (v.unit);
      const char *unitssqrd =
        micro_benchmark_time_units_sqrd_to_string_ (v.unit);

      fprintf (out, "\n      (%s", _("sample-time"));

      if (MICRO_BENCHMARK_STAT_MEAN & values->sample_time)
        write_numeric_value (out, "\n        (%s %f \"%s\")", _("mean"),
                             v.mean, units);

      if (MICRO_BENCHMARK_STAT_STD_DEVIATION & values->sample_time)
        write_numeric_value (out, "\n        (%s %f \"%s\")",
                             _("std-deviation"), v.std_deviation, units);

      if (MICRO_BENCHMARK_STAT_VARIANCE & values->sample_time)
        write_numeric_value (out, "\n        (%s %f \"%s\")", _("variance"),
                             v.variance, unitssqrd);

      fprintf (out, ")");
    }

  if (values->sample_iterations)
    {
      micro_benchmark_stats_value v =
        micro_benchmark_exec_report_sample_iterations (report);

      fprintf (out, "\n      (%s", _("sample-iterations"));

      if (MICRO_BENCHMARK_STAT_MEAN & values->sample_iterations)
        write_numeric_value (out, "\n        (%s %f)", _("mean"), v.mean);

      if (MICRO_BENCHMARK_STAT_STD_DEVIATION & values->sample_iterations)
        write_numeric_value (out, "\n        (%s %f)", _("std-deviation"),
                             v.std_deviation);

      if (MICRO_BENCHMARK_STAT_VARIANCE & values->sample_iterations)
        write_numeric_value (out, "\n        (%s %f)", _("variance"),
                             v.variance);

      fprintf (out, ")");
    }

  if (values->extra_data)
    {
      const size_t n_meters =
        micro_benchmark_exec_report_number_of_meters (report);
      fprintf (out, "\n      (%s (%s %zu)", _("custom-meters"), _("total"),
               n_meters);
      for (size_t i = 0; i < n_meters; ++i)
        {
          micro_benchmark_report_extra_data extra =
            micro_benchmark_exec_report_get_extra_data (report, i);
          const char *mname = extra.name ? extra.name : _("??");
          assert (mname);
          char *name = xstrdup (mname);
          size_t size = strlen (name) + 1;
          escape_quote (&name, &size);
          fprintf (out, "\n        (%s \"%s\"", _("meter"), name);
          xfree (name, size);

          char *key;
          size_t key_size;
          char *value;
          size_t value_size;
          if (extra.size > 0)
            {
              key = xstrdup (extra.keys[0]);
              key_size = strlen (key) + 1;
              escape_quote (&key, &key_size);
              value = xstrdup (extra.values[0]);
              value_size = strlen (value) + 1;
              escape_quote (&value, &value_size);
              fprintf (out, "\n          ((\"%s\" . \"%s\")", key, value);
              xfree (key, key_size);
              xfree (value, value_size);
            }
          for (size_t j = 1; j < extra.size; ++j)
            {
              key = xstrdup (extra.keys[0]);
              key_size = strlen (key) + 1;
              escape_quote (&key, &key_size);
              value = xstrdup (extra.values[0]);
              value_size = strlen (value) + 1;
              escape_quote (&value, &value_size);
              fprintf (out, "\n           (\"%s\" . \"%s\")", key, value);
              xfree (key, key_size);
              xfree (value, value_size);
            }
          fprintf (out, ")");
        }
      fprintf (out, ")");
    }
  fprintf (out, ")\n");
}

static void
write_test_report (micro_benchmark_test_report test, FILE *out,
                   const micro_benchmark_output_values *values)
{
  MB_TRACE ("Writing test report %p.\n", test);
  char *test_name = xstrdup (micro_benchmark_test_report_get_name (test));
  size_t size = strlen (test_name) + 1;
  escape_quote (&test_name, &size);
  fprintf (out, "  (%s (%s \"%s\")\n", _("test"), _("name"), test_name);
  xfree (test_name, size);

  size_t n_cases = micro_benchmark_test_report_get_num_executions (test);

  for (size_t i = 0; i < n_cases; ++i)
    {
      micro_benchmark_exec_report e =
        micro_benchmark_test_report_get_exec_report (test, i);
      write_execution_report (e, out, i, values);
    }

  write_numeric_value (out, "    (%s %zu))\n", _("number-of-runs"), n_cases);
}

void
micro_benchmark_report_write_lisp_ (micro_benchmark_report report,
                                    FILE *out,
                                    const micro_benchmark_output_values
                                    *values)
{
  MB_TRACE ("Writing lisp report %p.\n", report);
  assert (report);
  assert (out);
  assert (values);
  const size_t start = values->self_test ? 0 : 1;
  const char *orig_name = micro_benchmark_report_get_name (report);
  char *suite_name = xstrdup (orig_name);
  size_t size = strlen (suite_name) + 1;
  escape_quote (&suite_name, &size);
  fprintf (out, "(%s (%s \"%s\")\n", _("suite"), _("name"), suite_name);
  xfree (suite_name, size);

  size_t n_tests = micro_benchmark_report_get_number_of_tests (report);
  for (size_t i = start; i < n_tests; ++i)
    {
      micro_benchmark_test_report r =
        micro_benchmark_report_get_test_report (report, i);
      write_test_report (r, out, values);
    }

  write_numeric_value (out, "  (%s %zu))\n", _("number-of-tests"), n_tests);
}
