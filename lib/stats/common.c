/* common.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/stats.h"
#include "internal/stats.h"
#include "internal/stats/common.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <math.h>

#define MICRO_BENCHMARK_SUBMODULE "common"
#define MICRO_BENCHMARK_MODULE "stats:" MICRO_BENCHMARK_SUBMODULE

typedef micro_benchmark_custom_time_calculator calc;

#if HAVE_GMP
#define MB_DEFAULT_CALCULATOR mbstats_mp_
#else
#define MB_DEFAULT_CALCULATOR mbstats_basic_
#endif

static calc default_calculator = &MB_DEFAULT_CALCULATOR;

calc
micro_benchmark_get_default_time_calculator (void)
{
  return default_calculator;
}

void
micro_benchmark_set_default_time_calculator (calc c)
{
  if (c)
    default_calculator = c;
  else
    default_calculator = &MB_DEFAULT_CALCULATOR;
}

size_t
mbstats_get_num_samples_ (size_t num, micro_benchmark_time_samples samples)
{
  size_t n_samples = 0;
  for (size_t i = 0; i < num; ++i)
    {
      if (samples[i].discarded)
        {
          MB_DEBUG ("Sample %zu was discarded.\n", i);
          continue;
        }

      if (samples[i].iterations == 0)
        {
          MB_DEBUG ("Sample %zu does not have iterations.\n", i);
          continue;
        }

      if (samples[i].elapsed.seconds == 0
          && samples[i].elapsed.nanoseconds == 0)
        MB_WARN ("Sample %zu has zero time.\n", i);

      n_samples++;
    }
  return n_samples;
}

size_t
mbstats_get_num_iterations_ (size_t num, micro_benchmark_time_samples samples)
{
  size_t iterations = 0;
  for (size_t i = 0; i < num; ++i)
    {
      if (samples[i].discarded)
        continue;

      iterations += samples[i].iterations;
    }
  return iterations;
}

void
mbstats_fill_empty_ (micro_benchmark_stats_value *ret)
{
  assert (ret);
  ret->unit = MICRO_BENCHMARK_STATS_UNIT_NONE;
  ret->mean = INFINITY;
  ret->variance = INFINITY;
  ret->std_deviation = INFINITY;
}

static bool variance_estimator = true;

void
mbstats_set_use_variance_estimator_ (bool b)
{
  variance_estimator = b;
}

bool
mbstats_use_variance_estimator_ (void)
{
  return variance_estimator;
}
