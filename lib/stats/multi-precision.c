/* multi-precision.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/stats/multi-precision.h"
#include "internal/stats/common.h"
#include "internal/utility/log.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <math.h>
#include <string.h>

#include <gmp.h>

#define MICRO_BENCHMARK_SUBMODULE "multi-precision"
#define MICRO_BENCHMARK_MODULE "stats:" MICRO_BENCHMARK_SUBMODULE

typedef void (*extractor) (mpz_t, micro_benchmark_time_samples sample);

static void
calculate_mean (mpz_t mean, size_t num, micro_benchmark_time_samples samples,
                extractor ex)
{
  size_t n_val = 0;
  mpz_t sum, tmp;
  mpz_inits (sum, tmp, NULL);
  for (size_t i = 0; i < num; ++i)
    {
      if (samples[i].discarded || samples[i].iterations == 0)
        continue;

      ex (tmp, &samples[i]);
      mpz_add (sum, sum, tmp);
      n_val++;
    }
  assert (n_val > 0);
  mpz_cdiv_q_ui (mean, sum, n_val);
  mpz_clears (sum, tmp, NULL);
}

static void
calculate_variance (mpz_t variance, const mpz_t mean, size_t num,
                    micro_benchmark_time_samples samples, extractor ex)
{
  size_t n_val = 0;
  mpz_t sum, tmp;
  mpz_inits (sum, tmp, NULL);
  for (size_t i = 0; i < num; ++i)
    {
      if (samples[i].discarded || samples[i].iterations == 0)
        continue;

      ex (tmp, &samples[i]);
      mpz_sub (tmp, tmp, mean);
      mpz_mul (tmp, tmp, tmp);
      mpz_add (sum, sum, tmp);
      n_val++;
    }
  assert (n_val > 0);
  if (mbstats_use_variance_estimator_ ())
    {
      if (n_val > 1)
        mpz_cdiv_q_ui (variance, sum, n_val - 1);
      else
        mpz_set_si (variance, -1);
    }
  else
    mpz_cdiv_q_ui (variance, sum, n_val);
  mpz_clears (sum, tmp, NULL);
}

typedef double (*conv) (mpz_t, bool);

static double
to_nanoseconds (mpz_t value, bool sq)
{
  (void) sq;
  return mpz_get_d (value);
}

static double
to_microseconds (mpz_t value, bool sq)
{
  if (sq)
    return mpz_get_d (value) / 10000000.0;
  return mpz_get_d (value) / 1000;
}

static double
to_milliseconds (mpz_t value, bool sq)
{
  if (sq)
    return mpz_get_d (value) / (1000000.0 * 1000000.0);
  return mpz_get_d (value) / 1000000.0;
}

static double
to_seconds (mpz_t value, bool sq)
{
  if (sq)
    return mpz_get_d (value) / (1000000000.0 * 1000000000.0);
  return mpz_get_d (value) / 1000000000;
}

static double
to_minutes (mpz_t value, bool sq)
{
  if (sq)
    return mpz_get_d (value) / (1000000000.0 * 1000000000.0 * 60.0 * 60.0);
  return mpz_get_d (value) / (1000000000 * 60.0);
}

static conv
select_time_unit (const mpz_t mean, micro_benchmark_stats_unit *unit)
{
  double total = mpz_get_d (mean);
  long s = total / 1000000000;
  long ns = total - s * 1000000000.0;
  if (s >= 60)
    {
      MB_DEBUG ("Using minutes.\n");
      *unit = MICRO_BENCHMARK_STATS_TIME_MIN;
      return to_minutes;
    }
  if (s > 0 || ns > 999999999)
    {
      MB_DEBUG ("Using seconds.\n");
      *unit = MICRO_BENCHMARK_STATS_TIME_S;
      return to_seconds;
    }
  if (ns > 999999)
    {
      MB_DEBUG ("Using milliseconds.\n");
      *unit = MICRO_BENCHMARK_STATS_TIME_MS;
      return to_milliseconds;
    }

  if (ns > 999)
    {
      MB_DEBUG ("Using microseconds.\n");
      *unit = MICRO_BENCHMARK_STATS_TIME_US;
      return to_microseconds;
    }

  MB_DEBUG ("Using nanoseconds.\n");
  *unit = MICRO_BENCHMARK_STATS_TIME_NS;
  return to_nanoseconds;
}

static micro_benchmark_stats_value
to_time_values (mpz_t mean, mpz_t variance)
{
  micro_benchmark_stats_value ret = { 0 };
  if (mpz_cmp_ui (mean, 0) <= 0)
    mbstats_fill_empty_ (&ret);
  else
    {
      conv to_unit = select_time_unit (mean, &ret.unit);
      ret.mean = to_unit (mean, false);
      if (mpz_cmp_si (variance, 0) < 0)
        {
          ret.variance = INFINITY;
          ret.std_deviation = INFINITY;
        }
      else
        {
          ret.variance = to_unit (variance, true);
          mpz_sqrt (variance, variance);
          ret.std_deviation = to_unit (variance, false);
        }
    }
  return ret;
}

static void
extract_iteration_time (mpz_t res, micro_benchmark_time_samples sample)
{
  assert (res);
  assert (sample);
  assert (sample->iterations > 0);
  mpz_set_si (res, sample->elapsed.seconds);
  mpz_mul_ui (res, res, 1000000000);
  mpz_add_ui (res, res, sample->elapsed.nanoseconds);
  mpz_cdiv_q_ui (res, res, sample->iterations);
}

static micro_benchmark_stats_value
calculate_iteration_time (size_t num, micro_benchmark_time_samples samples)
{
  assert (num > 0);
  mpz_t mean, variance;
  mpz_inits (mean, variance, NULL);
  calculate_mean (mean, num, samples, &extract_iteration_time);
  calculate_variance (variance, mean, num, samples, &extract_iteration_time);
  micro_benchmark_stats_value ret = to_time_values (mean, variance);
  mpz_clears (mean, variance, NULL);
  return ret;
}

static void
extract_sample_time (mpz_t res, micro_benchmark_time_samples sample)
{
  assert (res);
  assert (sample);
  assert (sample->iterations > 0);
  mpz_set_si (res, sample->elapsed.seconds);
  mpz_mul_ui (res, res, 1000000000);
  mpz_add_ui (res, res, sample->elapsed.nanoseconds);
}

static micro_benchmark_stats_value
calculate_sample_time (size_t num, micro_benchmark_time_samples samples)
{
  assert (samples);
  assert (num > 0);
  mpz_t mean, variance;
  mpz_inits (mean, variance, NULL);
  calculate_mean (mean, num, samples, &extract_sample_time);
  calculate_variance (variance, mean, num, samples, &extract_sample_time);
  micro_benchmark_stats_value ret = to_time_values (mean, variance);
  mpz_clears (mean, variance, NULL);
  return ret;
}

static micro_benchmark_stats_value
to_iteration_values (mpz_t mean, mpz_t variance)
{
  assert (mean);
  assert (variance);
  micro_benchmark_stats_value ret = { 0 };
  if (mpz_cmp_ui (mean, 0) <= 0)
    mbstats_fill_empty_ (&ret);
  else
    {
      ret.unit = MICRO_BENCHMARK_STATS_ITERATIONS;
      ret.mean = mpz_get_d (mean);
      if (mpz_cmp_si (variance, 0) < 0)
        {
          ret.variance = INFINITY;
          ret.std_deviation = INFINITY;
        }
      else
        {
          ret.variance = mpz_get_d (variance);
          ret.std_deviation = sqrt (ret.variance);
        }
    }
  return ret;
}

static void
extract_sample_iterations (mpz_t res, micro_benchmark_time_samples sample)
{
  assert (res);
  assert (sample);
  mpz_set_ui (res, sample->iterations);
}

static micro_benchmark_stats_value
calculate_sample_iterations (size_t num, micro_benchmark_time_samples samples)
{
  assert (num > 0);
  assert (samples);
  mpz_t mean, variance;
  mpz_inits (mean, variance, NULL);
  calculate_mean (mean, num, samples, &extract_sample_iterations);
  calculate_variance (variance, mean, num, samples,
                      &extract_sample_iterations);
  micro_benchmark_stats_value ret = to_iteration_values (mean, variance);
  mpz_clears (mean, variance, NULL);
  return ret;
}

micro_benchmark_time_stats_values
mbstats_mp_ (size_t num, micro_benchmark_time_samples samples)
{
  micro_benchmark_time_stats_values ret = { 0 };
  ret.total_samples = num;
  ret.samples = mbstats_get_num_samples_ (num, samples);
  ret.iterations = mbstats_get_num_iterations_ (num, samples);
  if (num == 0)
    {
      MB_WARN ("Unable to calculate statistics without samples\n");
      mbstats_fill_empty_ (&ret.iteration_time);
      mbstats_fill_empty_ (&ret.sample_time);
      mbstats_fill_empty_ (&ret.sample_iterations);
    }
  else
    {
      ret.iteration_time = calculate_iteration_time (num, samples);
      ret.sample_time = calculate_sample_time (num, samples);
      ret.sample_iterations = calculate_sample_iterations (num, samples);
    }
  return ret;
}
