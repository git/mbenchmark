/* basic.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/stats/basic.h"
#include "internal/stats/common.h"
#include "internal/utility/log.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <math.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "basic"
#define MICRO_BENCHMARK_MODULE "stats:" MICRO_BENCHMARK_SUBMODULE

static double
mergesum (size_t s, const double *ptr)
{
  if (s == 0)
    return 0.0;

  if (s == 1)
    return ptr[0];

  if (s == 2)
    return ptr[0] + ptr[1];

  const size_t mid = s / 2;
  double first_half = mergesum (mid, ptr);
  double second_half = mergesum (s - mid, ptr + mid);
  return first_half + second_half;
}

typedef double (*to_unit) (micro_benchmark_clock_time);

static double
to_nanoseconds (micro_benchmark_clock_time t)
{
  return (t.seconds * 1000000000.0) + t.nanoseconds;
}

static double
to_microseconds (micro_benchmark_clock_time t)
{
  return (t.seconds * 1000000.0) + (t.nanoseconds / 1000.0);
}

static double
to_milliseconds (micro_benchmark_clock_time t)
{
  return (t.seconds * 1000.0) + (t.nanoseconds / 1000000.0);
}

static double
to_seconds (micro_benchmark_clock_time t)
{
  return t.seconds + (t.nanoseconds / 1000000000.0);
}

static double
to_minutes (micro_benchmark_clock_time t)
{
  return to_seconds (t) / 60;
}

static to_unit
select_conversor (micro_benchmark_clock_time elapsed, size_t mult,
                  micro_benchmark_stats_unit *units)
{
  const double total = (elapsed.seconds * 1000000000.0) + elapsed.nanoseconds;
  if (elapsed.seconds / mult >= 60.0)
    {
      MB_DEBUG ("Using minutes.\n");
      *units = MICRO_BENCHMARK_STATS_TIME_MIN;
      return to_minutes;
    }
  if (total / mult > 999999999)
    {
      MB_DEBUG ("Using seconds.\n");
      *units = MICRO_BENCHMARK_STATS_TIME_S;
      return to_seconds;
    }
  if (total / mult > 999999)
    {
      MB_DEBUG ("Using milliseconds.\n");
      *units = MICRO_BENCHMARK_STATS_TIME_MS;
      return to_milliseconds;
    }

  if (total / mult > 999)
    {
      MB_DEBUG ("Using microseconds.\n");
      *units = MICRO_BENCHMARK_STATS_TIME_US;
      return to_microseconds;
    }

  MB_DEBUG ("Using nanoseconds.\n");
  *units = MICRO_BENCHMARK_STATS_TIME_NS;
  return to_nanoseconds;
}

static to_unit
select_conversor_from_samples (size_t num,
                               micro_benchmark_time_samples samples,
                               micro_benchmark_stats_unit *units, bool it)
{
  to_unit conversor = NULL;
  for (size_t i = 0; i < num; ++i)
    {
      if (samples[i].discarded || samples[i].iterations == 0)
        continue;

      to_unit old_conversor = conversor;
      if (it)
        conversor = select_conversor (samples[i].elapsed,
                                      samples[i].iterations, units);
      else
        conversor = select_conversor (samples[i].elapsed, 1, units);

      if (old_conversor == conversor)
        return conversor;
      else if (old_conversor)
        MB_DEBUG ("Sample %zu resorted to a different unit conversor\n", i);
    }
  assert (conversor);
  return conversor;
}


static void
fill_stats_values (micro_benchmark_stats_value *ret, size_t num_values,
                   double *values)
{
  assert (ret);
  assert (values);
  if (num_values == 0)
    {
      mbstats_fill_empty_ (ret);
      return;
    }

  double sum = mergesum (num_values, values);
  ret->mean = sum / num_values;
  if (!mbstats_use_variance_estimator_ ())
    {
      for (size_t i = 0; i < num_values; ++i)
        values[i] = pow (values[i] - ret->mean, 2);
      sum = mergesum (num_values, values);
      ret->variance = sum / num_values;
      ret->std_deviation = sqrt (ret->variance);
    }
  else if (num_values > 1)
    {
      for (size_t i = 0; i < num_values; ++i)
        values[i] = pow (values[i] - ret->mean, 2);
      sum = mergesum (num_values, values);
      ret->variance = sum / (num_values - 1);
      ret->std_deviation = sqrt (ret->variance);
    }
  else
    {
      ret->variance = INFINITY;
      ret->std_deviation = INFINITY;
    }
}

static micro_benchmark_stats_value
calculate_iteration_time (size_t num, micro_benchmark_time_samples samples)
{
  assert (num > 0);
  micro_benchmark_stats_value ret = { 0 };

  to_unit conversor =
    select_conversor_from_samples (num, samples, &ret.unit, true);
  double *times = xmalloc_n (sizeof (double), num);
  size_t num_times = 0;
  for (size_t i = 0; i < num; ++i)
    {
      if (samples[i].discarded || samples[i].iterations == 0)
        continue;

      double total = conversor (samples[i].elapsed);
      MB_TRACE ("Sample %zu: %f %d\n", i, total, (int) ret.unit);
      times[num_times++] = total / samples[i].iterations;
    }
  fill_stats_values (&ret, num_times, times);
  xfree_n (times, sizeof (double), num);
  return ret;
}

static micro_benchmark_stats_value
calculate_sample_time (size_t num, micro_benchmark_time_samples samples)
{
  assert (num > 0);
  micro_benchmark_stats_value ret = { 0 };
  to_unit conversor =
    select_conversor_from_samples (num, samples, &ret.unit, false);
  double *times = xmalloc_n (sizeof (double), num);
  size_t num_times = 0;
  for (size_t i = 0; i < num; ++i)
    {
      if (samples[i].discarded || samples[i].iterations == 0)
        continue;
      times[num_times++] = conversor (samples[i].elapsed);
    }
  fill_stats_values (&ret, num_times, times);
  xfree_n (times, sizeof (double), num);
  return ret;
}

static micro_benchmark_stats_value
calculate_sample_iterations (size_t num, micro_benchmark_time_samples samples)
{
  assert (num > 0);
  double *iterations = xmalloc_n (sizeof (double), num);
  size_t num_samples = 0;
  for (size_t i = 0; i < num; ++i)
    {
      if (samples[i].discarded || samples[i].iterations == 0)
        continue;

      iterations[num_samples++] = samples[i].iterations;
    }
  micro_benchmark_stats_value ret = { 0 };
  ret.unit = MICRO_BENCHMARK_STATS_ITERATIONS;
  fill_stats_values (&ret, num_samples, iterations);
  xfree_n (iterations, sizeof (double), num);
  return ret;
}

micro_benchmark_time_stats_values
mbstats_basic_ (size_t num, micro_benchmark_time_samples samples)
{
  micro_benchmark_time_stats_values ret = { 0 };
  ret.total_samples = num;
  ret.samples = mbstats_get_num_samples_ (num, samples);
  ret.iterations = mbstats_get_num_iterations_ (num, samples);
  if (num == 0)
    {
      MB_WARN ("Unable to calculate statistics without samples\n");
      mbstats_fill_empty_ (&ret.iteration_time);
      mbstats_fill_empty_ (&ret.sample_time);
      mbstats_fill_empty_ (&ret.sample_iterations);
    }
  else
    {
      ret.iteration_time = calculate_iteration_time (num, samples);
      ret.sample_time = calculate_sample_time (num, samples);
      ret.sample_iterations = calculate_sample_iterations (num, samples);
    }
  return ret;
}
