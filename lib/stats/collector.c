/* collector.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/stats/collector.h"
#include "internal/clock/chrono-provider.h"
#include "internal/constraints.h"
#include "internal/stats.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "collector"
#define MICRO_BENCHMARK_MODULE "stats:" MICRO_BENCHMARK_SUBMODULE

#ifndef MICRO_BENCHMARK_DEFAULT_DEADLINE
#define MICRO_BENCHMARK_DEFAULT_DEADLINE 5
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_MIN_SAMPLE
#define MICRO_BENCHMARK_DEFAULT_MIN_SAMPLE 100000
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_ALLOC_SAMPLES
#define MICRO_BENCHMARK_DEFAULT_ALLOC_SAMPLES 128
#endif

static const micro_benchmark_clock_time default_deadline = {
  MICRO_BENCHMARK_DEFAULT_DEADLINE, 0
};

static const micro_benchmark_clock_time min_sample = {
  0, MICRO_BENCHMARK_DEFAULT_MIN_SAMPLE
};

static micro_benchmark_clock_time
max_time (micro_benchmark_clock_time lhs, micro_benchmark_clock_time rhs)
{
  return (lhs.seconds > rhs.seconds
          || (lhs.seconds == rhs.seconds
              && lhs.nanoseconds > rhs.nanoseconds)) ? lhs : rhs;
}

/*  TODO: Move to clock library.  */
static micro_benchmark_clock_time
mult_time (int m, micro_benchmark_clock_time old)
{
  assert (m > 0);
  old.seconds *= m;
  size_t ns = 0;
  for (int i = 0; i < m; ++i)
    {
      ns += old.seconds;
      if (ns >= 1000000000)
        {
          old.nanoseconds++;
          ns -= 1000000000;
        }
    }
  old.nanoseconds = ns;
  return old;
}

static micro_benchmark_timer
create_timer_from_meter (micro_benchmark_clock_type type,
                         micro_benchmark_chronometer_provider meter,
                         micro_benchmark_meter tmpl)
{
  switch (meter)
    {
    default:
      return micro_benchmark_timer_from_meter (type, meter);

    case MICRO_BENCHMARK_CHRONO_PROVIDER_USER_PROVIDED:
      return micro_benchmark_timer_from_provided_meter (type, tmpl);
    }
}


static micro_benchmark_timer
create_deadline_timer (const mbconstraints_time_ *tc)
{
  micro_benchmark_clock_type type = mbconstraints_get_timer_type_ (tc);
  micro_benchmark_timer_provider provider =
    mbconstraints_get_timer_provider_ (tc);

  switch (provider)
    {
    default:
      return micro_benchmark_timer_create (type, provider);

    case MICRO_BENCHMARK_TIMER_PROVIDER_USER_PROVIDED:
      {
        micro_benchmark_timer tmpl = mbconstraints_get_timer_tmpl_ (tc);
        return micro_benchmark_timer_from_template (type, tmpl);
      }

    case MICRO_BENCHMARK_TIMER_PROVIDER_FROM_METER:
      {
        micro_benchmark_chronometer_provider meter =
          mbconstraints_get_timer_meter_ (tc);
        micro_benchmark_meter tmpl = mbconstraints_get_timer_meter_tmpl_ (tc);
        return create_timer_from_meter (type, meter, tmpl);
      }
    }
}

static void
extract_deadline (const mbconstraints_time_ *tc, micro_benchmark_timer *timer,
                  micro_benchmark_clock_time *deadline)
{
  assert (tc);
  assert (timer);
  assert (deadline);

  if (mbconstraints_has_timer_ (tc))
    *timer = create_deadline_timer (tc);
  else
    *timer = micro_benchmark_timer_create_default ();

  static const int min_samples = 2;
  micro_benchmark_clock_time res =
    micro_benchmark_timer_get_resolution (*timer);
  micro_benchmark_clock_time min = mult_time (min_samples, res);
  if (mbconstraints_has_max_time_ (tc))
    {
      micro_benchmark_clock_time max = mbconstraints_get_max_time_ (tc);
      *deadline = max_time (max, min);
    }
  else
    *deadline = max_time (default_deadline, min);
}

/* TODO: Use sample timer.  */
static void
extract_sample_data (mbconstraints_ *tc,
                     micro_benchmark_timer *timer,
                     micro_benchmark_clock_time *sample)
{
  assert (tc);
  assert (timer);
  assert (sample);
  const mbconstraints_time_ *t = mbconstraints_get_time_ (tc);
  if (mbconstraints_has_timer_ (t))
    *timer = create_deadline_timer (t);
  else
    *timer = micro_benchmark_timer_create_default ();

  micro_benchmark_clock_time res =
    micro_benchmark_timer_get_resolution (*timer);
  if (mbconstraints_has_sample_time_ (t))
    {
      micro_benchmark_clock_time max = mbconstraints_get_sample_time_ (t);
      *sample = max_time (max, res);
    }
  else
    *sample = max_time (min_sample, res);
}

static micro_benchmark_meter
create_full_chrono (mbconstraints_ *tc)
{
  assert (tc);

  mbconstraints_meter_ *mc = mbconstraints_get_meter_ (tc);
  if (!mbconstraints_has_chrono_ (mc))
    return micro_benchmark_chronometer_create_default ();

  micro_benchmark_clock_type type = mbconstraints_get_chrono_type_ (mc);
  micro_benchmark_chronometer_provider provider =
    mbconstraints_get_chrono_provider_ (mc);

  switch (provider)
    {
    default:
      return micro_benchmark_chronometer_create (type, provider);

    case MICRO_BENCHMARK_CHRONO_PROVIDER_USER_PROVIDED:
      {
        micro_benchmark_meter tmpl = mbconstraints_get_chrono_tmpl_ (mc);
        return micro_benchmark_chronometer_from_meter (type, tmpl);
      }
    }
}

static micro_benchmark_time_sample_collector_
create_clock (mbconstraints_ *tc)
{
  static const size_t default_samples = MICRO_BENCHMARK_DEFAULT_ALLOC_SAMPLES;
  assert (tc);

  micro_benchmark_time_sample_collector_ ret;
  ret.chrono = create_full_chrono (tc);
  ret.num_samples = default_samples;
  ret.written_samples = 0;
  ret.samples = xmalloc_n (sizeof (struct micro_benchmark_time_sample_),
                           default_samples);
  return ret;
}

static void
allocate_custom_collectors (mbstats_collector_ c, mbconstraints_ *tc)
{
  (void) c;
  assert (c);
  assert (tc);

  mbconstraints_meter_ *m = mbconstraints_get_meter_ (tc);
  if (mbconstraints_num_meters_ (m) > 0)
    assert (!"Unimplemented");
}

void
micro_benchmark_stats_collector_init_ (mbstats_collector_ c,
                                       mbconstraints_ *tc)
{
  static const struct micro_benchmark_time_sample_ zero;
  MB_TRACE ("Init %p with test case %p.\n", c, tc);
  assert (c);
  assert (tc);
  c->has_skipped_iterations = false;
  c->measuring = false;
  c->execution_iterations = 0;
  c->iterations = 0;
  c->full_sample = zero;
  c->num_meters = 0;
  c->meters = NULL;

  mbconstraints_time_ *ttc = mbconstraints_get_time_ (tc);
  extract_deadline (ttc, &c->deadline_timer, &c->deadline_time);
  MB_DEBUG ("Created deadline timer %p (%ds %dns).\n", c->deadline_timer,
            (int) c->deadline_time.seconds,
            (int) c->deadline_time.nanoseconds);

  extract_sample_data (tc, &c->sample_timer, &c->sample_time);
  c->initial_sample_time = c->sample_time;
  MB_DEBUG ("Created sample timer %p (%ds %dns).\n", c->sample_timer,
            (int) c->sample_time.seconds, (int) c->sample_time.nanoseconds);

  c->full_chrono = create_full_chrono (tc);
  MB_DEBUG ("Created full chrono %p.\n", c->full_chrono);

  c->clock = create_clock (tc);
  MB_DEBUG ("Created chrono %p.\n", c->clock.chrono);
  allocate_custom_collectors (c, tc);
}

void
micro_benchmark_stats_collector_release_ (mbstats_collector_ c)
{
  MB_TRACE ("Release %p.\n", c);
  assert (c);
  assert (c->full_chrono);
  assert (c->deadline_timer);
  assert (c->sample_timer);

  micro_benchmark_timer_release (c->deadline_timer);
  micro_benchmark_timer_release (c->sample_timer);
  micro_benchmark_chronometer_release (c->full_chrono);
  micro_benchmark_chronometer_release (c->clock.chrono);

  xfree_n (c->clock.samples, sizeof (struct micro_benchmark_time_sample_),
           c->clock.num_samples);

  if (c->num_meters > 0)
    {
      for (size_t i = 0; i < c->num_meters; ++i)
        {
          const char *name =
            micro_benchmark_stats_meter_get_name (&c->meters[i].meter);
          MB_DEBUG ("Cleaning up meter %s\n", name);
          micro_benchmark_stats_meter_cleanup (&c->meters[i].meter);
          xfree_n (c->meters[i].samples,
                   sizeof (micro_benchmark_stats_generic_sample_data),
                   c->meters[i].num_samples);
        }
      xfree_n (c->meters, sizeof (micro_benchmark_meter_sample_collector_),
               c->num_meters);
    }
}

void
micro_benchmark_stats_collector_start_test_ (mbstats_collector_ c)
{
  MB_TRACE ("Start test %p.\n", c);
  assert (c);
  assert (c->full_chrono);
  micro_benchmark_stats_meter_start (c->full_chrono);
}

void
micro_benchmark_stats_collector_end_test_ (mbstats_collector_ c)
{
  MB_TRACE ("End test %p.\n", c);
  assert (c);
  assert (c->full_chrono);
  micro_benchmark_stats_meter_stop (c->full_chrono);
  MB_DEBUG ("Stopped clock %p\n", (void *) c->full_chrono);
  micro_benchmark_stats_meter_sample total =
    micro_benchmark_stats_meter_get_sample (c->full_chrono);
  c->full_sample.elapsed = total.time;
  c->full_sample.iterations += c->execution_iterations;
  MB_INFO ("Elapsed: %d.%09ds\n",
           (int) c->full_sample.elapsed.seconds,
           (int) c->full_sample.elapsed.nanoseconds);
}


void
micro_benchmark_stats_collector_start_ (mbstats_collector_ c)
{
  MB_TRACE ("Start collection at %p.\n", c);
  assert (c);
  assert (!c->measuring);
  c->measuring = true;
  micro_benchmark_timer_start (c->sample_timer, c->sample_time);
  micro_benchmark_timer_start (c->deadline_timer, c->deadline_time);
  for (size_t i = 0; i < c->num_meters; ++i)
    micro_benchmark_stats_meter_start (&c->meters[i].meter);
  micro_benchmark_stats_meter_start (c->clock.chrono);
}

void
micro_benchmark_stats_collector_stop_ (mbstats_collector_ c)
{
  assert (c);
  assert (c->measuring);
  micro_benchmark_stats_meter_stop (c->clock.chrono);
  for (size_t i = 0; i < c->num_meters; ++i)
    micro_benchmark_stats_meter_stop (&c->meters[i].meter);
  micro_benchmark_timer_stop (c->deadline_timer);
  micro_benchmark_timer_stop (c->sample_timer);
  c->measuring = false;
  MB_TRACE ("Stopped collection at %p.\n", c);
}

void
micro_benchmark_stats_collector_restart_ (mbstats_collector_ c)
{
  MB_TRACE ("Restart collection at %p.\n", c);
  assert (c);
  assert (!c->measuring);
  c->measuring = true;
  micro_benchmark_timer_restart (c->sample_timer);
  micro_benchmark_timer_restart (c->deadline_timer);
  for (size_t i = 0; i < c->num_meters; ++i)
    micro_benchmark_stats_meter_restart (&c->meters[i].meter);
  micro_benchmark_stats_meter_restart (c->clock.chrono);
}

typedef struct micro_benchmark_time_sample_ time_sample;
typedef micro_benchmark_stats_generic_sample_data sample;

static void
push_time_sample (mbstats_collector_ c, bool ok)
{
  if (c->clock.written_samples == c->clock.num_samples)
    {
      c->clock.num_samples *= 2;
      c->clock.samples = xrealloc_n (c->clock.samples,
                                     sizeof (time_sample),
                                     c->clock.num_samples);
    }
  size_t pos = c->clock.written_samples++;
  micro_benchmark_stats_meter_sample s =
    micro_benchmark_stats_meter_get_sample (c->clock.chrono);
  c->clock.samples[pos].elapsed = s.time;
  c->clock.samples[pos].discarded = !ok;
  c->clock.samples[pos].iterations = c->iterations;
}

static void
push_generic_sample (mbstats_collector_ c, bool ok, size_t i)
{
  if (c->meters[i].written_samples == c->meters[i].num_samples)
    {
      c->meters[i].num_samples *= 2;
      c->meters[i].samples = xrealloc_n (c->meters[i].samples,
                                         sizeof (sample),
                                         c->meters[i].num_samples);
    }

  size_t pos = c->meters[i].written_samples++;
  micro_benchmark_stats_meter_sample s =
    micro_benchmark_stats_meter_get_sample (&c->meters[i].meter);
  c->meters[i].samples[pos].value = s;
  c->meters[i].samples[pos].discarded = !ok;
  c->meters[i].samples[pos].iterations = c->iterations;
}

static void
push_sample (mbstats_collector_ c, bool ok)
{
  push_time_sample (c, ok);
  for (size_t i = 0; i < c->num_meters; ++i)
    push_generic_sample (c, ok, i);
  c->iterations = 0;
}

static bool
do_first_call (mbstats_collector_ c, mbstate_constraints_ *ct)
{
  MB_TRACE ("Before iterations have been skipped");
  if (ct->iterations_to_skip > 0)
    {
      if (ct->iterations_to_skip == c->iterations)
        {
          MB_INFO ("Iterations skipped: %zu\n", c->iterations);
          c->full_sample.iterations = c->iterations;
          push_sample (c, false);
          c->has_skipped_iterations = true;
          micro_benchmark_timer_start (c->deadline_timer, c->deadline_time);
          micro_benchmark_timer_stop (c->deadline_timer);
        }
      else
        c->iterations++;
    }
  else
    {
      MB_DEBUG ("No iterations to skip, starting measures\n");
      c->has_skipped_iterations = true;
    }

  return false;
}

static void
increment_sample_time (micro_benchmark_clock_time *t,
                       micro_benchmark_clock_time ref)
{
  assert (t->seconds > 0 || t->nanoseconds > 0);
  t->seconds += ref.seconds;
  t->nanoseconds += ref.nanoseconds;
  while (t->nanoseconds >= 1000000000)
    {
      t->seconds++;
      t->nanoseconds -= 1000000000;
    }
}

bool
micro_benchmark_stats_collected_enough_ (mbstats_collector_ c,
                                         mbstate_constraints_ *ct)
{
  assert (c);
  assert (ct);
  if (!c->has_skipped_iterations)
    return do_first_call (c, ct);

  c->iterations++;
  c->execution_iterations++;
  bool must_sample = !micro_benchmark_timer_is_running (c->sample_timer);

  if ((must_sample && c->iterations >= ct->min_sample_iterations)
      || c->iterations == ct->max_sample_iterations)
    {
      push_sample (c, true);
      MB_DEBUG ("Pushed sample new sample (timer %d)\n", must_sample);
      /* TODO: Reduce overhead.  */
      if (must_sample && ct->max_sample_iterations != 0)
        increment_sample_time (&c->sample_time, c->initial_sample_time);
      /* TODO: Reset timer.  */
      MB_DEBUG ("Resetting timer to %d.%09ds\n",
                (int) c->sample_time.seconds,
                (int) c->sample_time.nanoseconds);
      micro_benchmark_timer_start (c->sample_timer, c->sample_time);
      micro_benchmark_timer_stop (c->sample_timer);
    }

  if (c->execution_iterations < ct->min_iterations)
    return false;

  bool timer_running = micro_benchmark_timer_is_running (c->deadline_timer);
  if (!timer_running || (ct->max_iterations > 0
                         && c->execution_iterations == ct->max_iterations))
    {
      MB_DEBUG ("Ending the test (timer %d).\n", timer_running);
      if (c->iterations > 0)
        push_sample (c, true);
      return true;
    }

  return false;
}

void
micro_benchmark_stats_collected_time_samples_ (mbstats_collector_ c,
                                               micro_benchmark_time_samples *
                                               s, size_t *size)
{
  MB_DEBUG ("Start collection at %p.\n", c);
  assert (c);
  assert (s);
  assert (size);
  *size = c->clock.written_samples;
  *s = c->clock.samples;
}

micro_benchmark_time_sample
micro_benchmark_stats_collected_full_sample_ (mbstats_collector_ co)
{
  MB_DEBUG ("Retrieving full sample %p.\n", co);
  assert (co);
  return &co->full_sample;
}

size_t
micro_benchmark_stats_collected_num_meters_ (mbstats_collector_ c)
{
  MB_DEBUG ("Retrieving number of custom meters of %p.\n", c);
  assert (c);
  return c->num_meters;
}

void
micro_benchmark_stats_collected_samples_from_meter_ (mbstats_collector_ c,
                                                     size_t pos,
                                                     mb_samples_ * sp,
                                                     size_t *size)
{
  MB_DEBUG ("Retrieving samples from meter %zu of %p.\n", pos, c);
  assert (c);
  assert (sp);
  assert (size);
  assert (c->num_meters > pos);
  *sp = c->meters[pos].samples;
  *size = c->meters[pos].written_samples;
}
