/* main.c --- MicroBenchmark Guile Binding. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/guile/main.h"

#include "mbenchmark/all.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <libguile.h>

static SCM
main_adapter (SCM args)
{
  int length = scm_to_int (scm_length (args));
  assert (length >= 0);
  scm_dynwind_begin (0);
  char **c_args = xmalloc_n (sizeof (char *), length + 1);
  scm_dynwind_free (c_args);
  scm_gc_register_allocation ((length + 1) * sizeof (char *));

  SCM left = args;
  for (int i = 0; i < length; ++i)
    {
      SCM head = scm_car (left);
      left = scm_cdr (left);
      c_args[i] = scm_to_locale_string (head);
      scm_dynwind_free (c_args[i]);
    }
  c_args[length] = NULL;
  int ret = micro_benchmark_main (length, c_args);
  scm_dynwind_end ();
  return scm_from_int (ret);
}

void
micro_benchmark_guile_init_main (void)
{
  scm_c_define_gsubr ("main/internal", 1, 0, 0, main_adapter);
}
