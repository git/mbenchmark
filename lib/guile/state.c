/* state.c --- MicroBenchmark Guile Binding. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/all.h"
#include "internal/guile/state.h"

#include <assert.h>

static SCM state_type;

SCM
make_state (micro_benchmark_test_state state)
{
  assert (state);
  SCM scm_state = scm_make_foreign_object_1 (state_type, state);
  return scm_state;
}

static SCM
is_state (SCM state)
{
  scm_assert_foreign_object_type (state_type, state);
  return SCM_BOOL_T;
}

static micro_benchmark_test_state
get_state (SCM state)
{
  scm_assert_foreign_object_type (state_type, state);
  micro_benchmark_test_state c_state = scm_foreign_object_ref (state, 0);
  assert (c_state);
  return c_state;
}

static SCM
keep_running (SCM state)
{
  micro_benchmark_test_state c_state = get_state (state);
  bool running = micro_benchmark_state_keep_running (c_state);
  return running ? SCM_BOOL_T : SCM_BOOL_F;
}

static SCM
state_sizes (SCM state)
{
  micro_benchmark_test_state c_state = get_state (state);
  SCM sizes = SCM_EOL;
  for (size_t i = micro_benchmark_state_get_dimensions (c_state); i > 0; --i)
    {
      size_t v = micro_benchmark_state_get_size (c_state, i - 1);
      SCM val = scm_from_size_t (v);
      sizes = scm_cons (val, sizes);
    }
  return sizes;
}

static SCM
state_name (SCM state)
{
  micro_benchmark_test_state c_state = get_state (state);
  const char *c_name = micro_benchmark_state_get_name (c_state);
  return scm_from_locale_string (c_name);
}

static SCM
set_state_name (SCM state, SCM name)
{
  scm_dynwind_begin (0);
  micro_benchmark_test_state c_state = get_state (state);
  char *c_name = scm_to_locale_string (name);
  if (!c_name)
    abort ();
  scm_dynwind_free (c_name);
  micro_benchmark_state_set_name (c_state, c_name);
  scm_dynwind_end ();
  return SCM_UNSPECIFIED;
}

static void
empty_finalizer (SCM state)
{
  // Do nothing, managed by the runner
  (void) state;
}

void
micro_benchmark_guile_init_state (void)
{
  SCM name = scm_from_utf8_symbol ("state");
  SCM slots = scm_list_1 (scm_from_utf8_symbol ("pointer"));
  scm_t_struct_finalize finalizer = empty_finalizer;
  state_type = scm_make_foreign_object_type (name, slots, finalizer);

  scm_c_define_gsubr ("state?/internal", 1, 0, 0, is_state);
  scm_c_define_gsubr ("keep-running?", 1, 0, 0, keep_running);
  scm_c_define_gsubr ("state-sizes", 1, 0, 0, state_sizes);
  scm_c_define_gsubr ("state-name", 1, 0, 0, state_name);
  scm_c_define_gsubr ("set-state-name!", 2, 0, 0, set_state_name);
}
