/* log.c --- MicroBenchmark Guile Binding. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/guile/log.h"

#include "mbenchmark/all.h"

#include <assert.h>
#include <stdlib.h>
#include <libguile.h>

static SCM
global_log_level (SCM level)
{
  micro_benchmark_log_level c_level = scm_to_int (level);
  micro_benchmark_set_log_level (c_level);
  return SCM_UNSPECIFIED;
}

static SCM
module_log_level (SCM module, SCM level)
{
  scm_dynwind_begin (0);
  char *c_module = scm_to_locale_string (module);
  if (!c_module)
    abort ();
  scm_dynwind_free (c_module);

  micro_benchmark_log_level c_level = scm_to_int (level);
  micro_benchmark_set_module_log_level (c_module, c_level);
  scm_dynwind_end ();
  return SCM_UNSPECIFIED;
}

void
micro_benchmark_guile_init_log (void)
{
  scm_c_define ("log/error", scm_from_int (MICRO_BENCHMARK_ERROR_LEVEL));
  scm_c_define ("log/warn", scm_from_int (MICRO_BENCHMARK_WARNING_LEVEL));
  scm_c_define ("log/info", scm_from_int (MICRO_BENCHMARK_INFO_LEVEL));
  scm_c_define ("log/debug", scm_from_int (MICRO_BENCHMARK_DEBUG_LEVEL));
  scm_c_define ("log/trace", scm_from_int (MICRO_BENCHMARK_TRACE_LEVEL));

  scm_c_define_gsubr ("set-log-level!/internal", 1, 0, 0, global_log_level);
  scm_c_define_gsubr ("set-module-log-level!/internal", 2, 0, 0,
                      module_log_level);
}
