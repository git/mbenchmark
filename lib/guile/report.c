/* report.c --- MicroBenchmark Guile Binding. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/guile/report.h"

#include "mbenchmark/all.h"
#include "internal/guile/suite.h"

#include <assert.h>
#include <stdio.h>
#include <libguile.h>

static SCM report_type;
static SCM exec_report_type;

static SCM
make_report (micro_benchmark_report report)
{
  assert (report);
  /* Cast const away for Guile foreign object interface.  */
  void *ptr = (void *) report;
  SCM scm_report = scm_make_foreign_object_1 (report_type, ptr);
  return scm_report;
}

static micro_benchmark_report
get_report (SCM report)
{
  scm_assert_foreign_object_type (report_type, report);
  void *ptr = scm_foreign_object_ref (report, 0);
  assert (ptr);
  return (micro_benchmark_report) ptr;
}

static SCM
make_exec_report (micro_benchmark_exec_report report)
{
  assert (report);
  void *ptr = (void *) report;
  SCM scm_report = scm_make_foreign_object_1 (exec_report_type, ptr);
  return scm_report;
}

static micro_benchmark_exec_report
get_exec_report (SCM report)
{
  scm_assert_foreign_object_type (exec_report_type, report);
  void *ptr = scm_foreign_object_ref (report, 0);
  assert (ptr);
  return (micro_benchmark_exec_report) ptr;
}

static SCM
report_name (SCM report)
{
  micro_benchmark_report c_report = get_report (report);
  const char *c_name = micro_benchmark_report_get_name (c_report);
  return scm_from_locale_string (c_name);
}

static SCM
exec_report_name (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  const char *c_name = micro_benchmark_exec_report_get_name (c_report);
  return scm_from_locale_string (c_name);
}

static SCM
write_report_to_port (micro_benchmark_report report,
                      micro_benchmark_output_type type,
                      const micro_benchmark_output_values *values, SCM port)
{
  char *text = NULL;
  size_t size = 0;
  FILE *out;
#if HAVE_OPEN_MEMSTREAM
  out = open_memstream (&text, &size);
#elif HAVE_TMPFILE
  out = tmpfile ();
#else
#error "Implementation depends on open_memstream or tmpfile"
#endif
  micro_benchmark_write_custom_report (report, out, type, values);
#if !HAVE_OPEN_MEMSTREAM
  size_t nwritten = ftell (out);
  int fseek_ret = fseek (out, 0, SEEK_SET);
  /* TODO: Check errors.  */
  assert (fseek_ret == 0);
  (void) fseek_ret;
  size = nwritten + 1;
  text = xmalloc (size);
  size_t nread = fread (text, sizeof (*text), nwritten, out);
  assert (nread == nwritten);
  text[nread] = '\0';
#endif
  /* TODO: Check errors.  */
  int fclose_ret = fclose (out);
  /* TODO: Check errors.  */
  assert (fclose_ret == 0);
  (void) fclose_ret;

  assert (size > 0);
  SCM scm_text = scm_take_locale_stringn (text, size);
  if (scm_is_false (port))
    return scm_text;

  scm_put_string (port, scm_text, SCM_UNDEFINED, SCM_UNDEFINED);
  return SCM_UNSPECIFIED;
}

static micro_benchmark_output_stat
parse_output_stat (SCM stat)
{
  if (!scm_is_true (stat))
    return MICRO_BENCHMARK_STAT_NONE;
  int v = scm_to_int (stat);
  assert (v <= MICRO_BENCHMARK_STAT_ALL && v > 0);
  return (micro_benchmark_output_stat) v;
}

enum output_ids
{
  self_test_id,
  size_constraints_id,
  total_time_id,
  total_iterations_id,
  iterations_id,
  total_samples_id,
  samples_id,
  extra_data_id,
  iteration_time_id,
  sample_time_id,
  sample_iterations_id
};

static micro_benchmark_output_values
parse_values (SCM lst)
{
  micro_benchmark_output_values ret =
    micro_benchmark_get_default_output_values ();
  SCM curr = lst;
  while (!scm_is_null (lst))
    {
      SCM head = scm_car (curr);
      curr = scm_cdr (curr);
      int vt = scm_to_int (scm_car (head));
      switch ((enum output_ids) vt)
        {
        case self_test_id:
          ret.self_test = scm_is_true (scm_cdr (head));
          break;

        case size_constraints_id:
          ret.size_constraints = scm_is_true (scm_cdr (head));
          break;

        case total_time_id:
          ret.total_time = scm_is_true (scm_cdr (head));
          break;

        case total_iterations_id:
          ret.total_iterations = scm_is_true (scm_cdr (head));
          break;

        case iterations_id:
          ret.iterations = scm_is_true (scm_cdr (head));
          break;

        case total_samples_id:
          ret.total_samples = scm_is_true (scm_cdr (head));
          break;

        case samples_id:
          ret.samples = scm_is_true (scm_cdr (head));
          break;

        case extra_data_id:
          ret.extra_data = scm_is_true (scm_cdr (head));
          break;

        case iteration_time_id:
          ret.iteration_time = parse_output_stat (scm_cdr (head));
          break;

        case sample_time_id:
          ret.sample_time = parse_output_stat (scm_cdr (head));
          break;

        case sample_iterations_id:
          ret.sample_iterations = parse_output_stat (scm_cdr (head));
          break;

        default:
          assert (0);
          break;
        }
    }
  return ret;
}

static SCM
print_report (SCM report, SCM port, SCM type, SCM extra)
{
  micro_benchmark_report c_report = get_report (report);
  if (!scm_is_null (extra) || scm_is_true (port) || scm_is_true (type))
    {
      micro_benchmark_output_values v = parse_values (extra);
      micro_benchmark_output_type c_type;
      if (scm_is_false (type))
        c_type = MICRO_BENCHMARK_CONSOLE_OUTPUT;
      else
        c_type = scm_to_int (type);
      return write_report_to_port (c_report, c_type, &v, port);
    }

  micro_benchmark_print_report (c_report);
  return SCM_UNSPECIFIED;
}

/* Print the report.  */
static SCM
for_each_report (SCM report, SCM fun, SCM self)
{
  micro_benchmark_report c_report = get_report (report);
  const size_t first = scm_is_true (self) ? 0 : 1;
  const size_t max = micro_benchmark_report_get_number_of_tests (c_report);
  for (size_t i = first; i < max; ++i)
    {
      micro_benchmark_test_report tr =
        micro_benchmark_report_get_test_report (c_report, i);
      const char *c_name = micro_benchmark_test_report_get_name (tr);
      SCM name = scm_from_locale_string (c_name);
      const size_t emax = micro_benchmark_test_report_get_num_executions (tr);
      for (size_t j = 0; j < emax; ++j)
        {
          micro_benchmark_exec_report er =
            micro_benchmark_test_report_get_exec_report (tr, j);
          SCM ereport = make_exec_report (er);
          scm_call_2 (fun, name, ereport);
        }
    }
  return SCM_UNSPECIFIED;
}

/* Print the report.  */
static SCM
fold_reports (SCM report, SCM fun, SCM init, SCM self)
{
  micro_benchmark_report c_report = get_report (report);
  const size_t first = scm_is_true (self) ? 0 : 1;
  const size_t max = micro_benchmark_report_get_number_of_tests (c_report);
  SCM ret = init;
  for (size_t i = first; i < max; ++i)
    {
      micro_benchmark_test_report tr =
        micro_benchmark_report_get_test_report (c_report, i);
      const char *c_name = micro_benchmark_test_report_get_name (tr);
      SCM name = scm_from_locale_string (c_name);
      const size_t emax = micro_benchmark_test_report_get_num_executions (tr);
      for (size_t j = 0; j < emax; ++j)
        {
          micro_benchmark_exec_report er =
            micro_benchmark_test_report_get_exec_report (tr, j);
          SCM ereport = make_exec_report (er);
          ret = scm_call_3 (fun, name, ereport, ret);
        }
    }
  return ret;
}

static SCM
get_report_from_suite (SCM suite)
{
  micro_benchmark_suite c_suite = get_suite (suite);
  assert (c_suite);
  micro_benchmark_report report = micro_benchmark_suite_get_report (c_suite);
  assert (report);
  return make_report (report);
}

static void
empty_finalizer (SCM report)
{
  // Do nothing, managed by the runner
  (void) report;
}

static SCM
get_sizes (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  const size_t *sizes;
  size_t dimensions = 0;
  micro_benchmark_exec_report_get_sizes (c_report, &sizes, &dimensions);
  SCM ret = SCM_EOL;
  for (size_t i = dimensions; i > 0; --i)
    ret = scm_cons (scm_from_size_t (sizes[i - 1]), ret);
  return ret;
}

static SCM
time_sample_to_scm (micro_benchmark_time_sample sample)
{
  assert (sample);
  SCM time = scm_cons (scm_from_int32 (sample->elapsed.seconds),
                       scm_from_int32 (sample->elapsed.nanoseconds));
  return scm_list_3 (scm_from_bool (sample->discarded),
                     scm_from_size_t (sample->iterations), time);
}

static SCM
get_time_samples (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  const size_t num = micro_benchmark_exec_report_num_time_samples (c_report);
  SCM ret = SCM_EOL;
  for (size_t i = num; i > 0; --i)
    {
      micro_benchmark_time_sample s =
        micro_benchmark_exec_report_get_time_sample (c_report, i - 1);
      ret = scm_cons (time_sample_to_scm (s), ret);
    }
  return ret;
}

static SCM
get_iterations (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  const size_t it = micro_benchmark_exec_report_get_iterations (c_report);
  return scm_from_size_t (it);
}

static SCM
get_total_iterations (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  const size_t it = micro_benchmark_exec_report_total_iterations (c_report);
  return scm_from_size_t (it);
}

static SCM
get_total_time (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  micro_benchmark_clock_time t =
    micro_benchmark_exec_report_total_time (c_report);
  return scm_cons (scm_from_int32 (t.seconds),
                   scm_from_int32 (t.nanoseconds));
}

static SCM
get_total_samples (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  const size_t s = micro_benchmark_exec_report_total_samples (c_report);
  return scm_from_size_t (s);
}

static SCM
get_used_samples (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  const size_t s = micro_benchmark_exec_report_used_samples (c_report);
  return scm_from_size_t (s);
}

static SCM
stat_value_to_scm (micro_benchmark_stats_value value)
{
  return scm_list_4 (scm_from_int ((int) value.unit),
                     scm_from_double (value.mean),
                     scm_from_double (value.variance),
                     scm_from_double (value.std_deviation));
}

static SCM
get_iteration_time (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  micro_benchmark_stats_value s =
    micro_benchmark_exec_report_iteration_time (c_report);
  return stat_value_to_scm (s);
}


static SCM
get_sample_time (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  micro_benchmark_stats_value s =
    micro_benchmark_exec_report_sample_time (c_report);
  return stat_value_to_scm (s);
}


static SCM
get_sample_iterations (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  micro_benchmark_stats_value s =
    micro_benchmark_exec_report_sample_iterations (c_report);
  return stat_value_to_scm (s);
}


static SCM
get_extra_data (SCM report)
{
  micro_benchmark_exec_report c_report = get_exec_report (report);
  const size_t num = micro_benchmark_exec_report_number_of_meters (c_report);
  SCM ret = SCM_EOL;
  for (size_t i = num; i > 0; --i)
    {
      micro_benchmark_report_extra_data d =
        micro_benchmark_exec_report_get_extra_data (c_report, i - 1);
      SCM name;
      if (d.name)
        name = scm_from_locale_string (d.name);
      else
        name = scm_from_utf8_symbol ("unknown");
      SCM tail = SCM_EOL;
      for (size_t i = 0; i < d.size; ++i)
        {
          SCM key = scm_from_locale_string (d.keys[i]);
          SCM value = scm_from_locale_string (d.values[i]);
          tail = scm_cons (scm_cons (key, value), tail);
        }
      ret = scm_cons (scm_cons (name, tail), ret);
    }
  return ret;
}

void
micro_benchmark_guile_init_report (void)
{
  SCM name;
  scm_t_struct_finalize finalizer;
  SCM slots = scm_list_1 (scm_from_utf8_symbol ("pointer"));

  name = scm_from_utf8_symbol ("report");
  finalizer = empty_finalizer;
  report_type = scm_make_foreign_object_type (name, slots, finalizer);

  name = scm_from_utf8_symbol ("exec-report");
  finalizer = empty_finalizer;
  exec_report_type = scm_make_foreign_object_type (name, slots, finalizer);

  scm_c_define ("output/console",
                scm_from_int (MICRO_BENCHMARK_CONSOLE_OUTPUT));
  scm_c_define ("output/lisp", scm_from_int (MICRO_BENCHMARK_LISP_OUTPUT));
  scm_c_define ("output/text", scm_from_int (MICRO_BENCHMARK_TEXT_OUTPUT));

  scm_c_define ("stats/mean", scm_from_int (MICRO_BENCHMARK_STAT_MEAN));
  scm_c_define ("stats/variance",
                scm_from_int (MICRO_BENCHMARK_STAT_VARIANCE));
  scm_c_define ("stats/std-deviation",
                scm_from_int (MICRO_BENCHMARK_STAT_STD_DEVIATION));
  scm_c_define ("stats/basic", scm_from_int (MICRO_BENCHMARK_STAT_BASIC));
  scm_c_define ("stats/all", scm_from_int (MICRO_BENCHMARK_STAT_ALL));

  scm_c_define ("values/self-test", scm_from_int (self_test_id));
  scm_c_define ("values/size-constraints",
                scm_from_int (size_constraints_id));
  scm_c_define ("values/total-time", scm_from_int (total_time_id));
  scm_c_define ("values/total-iterations",
                scm_from_int (total_iterations_id));
  scm_c_define ("values/iterations", scm_from_int (iterations_id));
  scm_c_define ("values/total-samples", scm_from_int (total_samples_id));
  scm_c_define ("values/samples", scm_from_int (samples_id));
  scm_c_define ("values/extra-data", scm_from_int (extra_data_id));
  scm_c_define ("values/iteration-time", scm_from_int (iteration_time_id));
  scm_c_define ("values/sample-time", scm_from_int (sample_time_id));
  scm_c_define ("values/sample-iterations",
                scm_from_int (sample_iterations_id));

  scm_c_define_gsubr ("get-report/internal", 1, 0, 0, get_report_from_suite);
  scm_c_define_gsubr ("report-name/internal", 1, 0, 0, report_name);
  scm_c_define_gsubr ("exec-report-name/internal", 1, 0, 0, exec_report_name);
  scm_c_define_gsubr ("print-report/internal", 4, 0, 0, print_report);
  scm_c_define_gsubr ("for-each-report/internal", 3, 0, 0, for_each_report);
  scm_c_define_gsubr ("fold-reports/internal", 4, 0, 0, fold_reports);

  scm_c_define_gsubr ("exec-report-sizes/internal", 1, 0, 0, get_sizes);
  scm_c_define_gsubr ("exec-report-time-samples/internal", 1, 0, 0,
                      get_time_samples);
  scm_c_define_gsubr ("exec-report-iterations/internal", 1, 0, 0,
                      get_iterations);
  scm_c_define_gsubr ("exec-report-total-iterations/internal", 1, 0, 0,
                      get_total_iterations);
  scm_c_define_gsubr ("exec-report-total-time/internal", 1, 0, 0,
                      get_total_time);
  scm_c_define_gsubr ("exec-report-total-samples/internal", 1, 0, 0,
                      get_total_samples);
  scm_c_define_gsubr ("exec-report-used-samples/internal", 1, 0, 0,
                      get_used_samples);
  scm_c_define_gsubr ("exec-report-iteration-time/internal", 1, 0, 0,
                      get_iteration_time);
  scm_c_define_gsubr ("exec-report-sample-time/internal", 1, 0, 0,
                      get_sample_time);
  scm_c_define_gsubr ("exec-report-sample-iterations/internal", 1, 0, 0,
                      get_sample_iterations);
  scm_c_define_gsubr ("exec-report-extra-data/internal", 1, 0, 0,
                      get_extra_data);
}
