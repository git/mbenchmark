/* test.c --- MicroBenchmark Guile Binding. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/guile/test.h"

#include "mbenchmark/all.h"
#include "internal/guile/suite.h"
#include "internal/guile/state.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <libguile.h>

static SCM test_type;

typedef struct mbguile_test_data_
{
  SCM set_up;
  SCM tear_down;
  SCM test;
  SCM args;
} mbguile_test_data_;

static mbguile_test_data_ *
make_data (SCM test, SCM set_up, SCM tear_down)
{
  mbguile_test_data_ *ptr = xmalloc (sizeof (mbguile_test_data_));
  scm_gc_register_allocation (sizeof (mbguile_test_data_));
  ptr->set_up = set_up;
  ptr->tear_down = tear_down;
  ptr->test = test;
  ptr->args = SCM_EOL;
  scm_gc_protect_object (ptr->set_up);
  scm_gc_protect_object (ptr->tear_down);
  scm_gc_protect_object (ptr->test);
  return ptr;
}

static SCM
make_test (micro_benchmark_test_case test)
{
  assert (test);
  return scm_make_foreign_object_1 (test_type, test);
}

static micro_benchmark_test_case
get_test (SCM test)
{
  scm_assert_foreign_object_type (test_type, test);
  micro_benchmark_test_case c_test = scm_foreign_object_ref (test, 0);
  assert (c_test);
  return c_test;
}

static void
free_data (mbguile_test_data_ *data)
{
  scm_gc_unprotect_object (data->set_up);
  scm_gc_unprotect_object (data->tear_down);
  scm_gc_unprotect_object (data->test);
  xfree (data, sizeof (mbguile_test_data_));
}

static mbguile_test_data_ *
mbguile_set_up (micro_benchmark_test_state state)
{
  assert (state);
  mbguile_test_data_ *data = micro_benchmark_state_get_data (state);
  SCM scm_state = make_state (state);
  SCM ret = scm_call_1 (data->set_up, scm_state);
  data->args = ret;
  scm_gc_protect_object (data->args);
  return data;
}

static void
mbguile_test (micro_benchmark_test_state state)
{
  assert (state);
  mbguile_test_data_ *data = micro_benchmark_state_get_data (state);
  SCM scm_state = make_state (state);
  scm_call_2 (data->test, scm_state, data->args);
}

static void
mbguile_tear_down (micro_benchmark_test_state state, mbguile_test_data_ *data)
{
  SCM scm_state = make_state (state);
  scm_apply_1 (data->tear_down, scm_state, data->args);
  scm_gc_unprotect_object (data->args);
  data->args = SCM_EOL;
}

static SCM registry = SCM_EOL;

static void
push_data (SCM name, SCM test, SCM set_up, SCM tear_down)
{
  SCM data = scm_list_5 (name, test, set_up, tear_down, SCM_EOL);
  registry = scm_cons (data, registry);
}

static SCM
find_entry (SCM name)
{
  SCM curr = registry;
  while (!scm_is_null (curr))
    {
      SCM last = scm_car (curr);
      curr = scm_cdr (curr);
      SCM t_name = scm_car (last);
      if (scm_is_true (scm_equal_p (name, t_name)))
        return last;
    }
  return SCM_BOOL_F;
}

static void
add_static_data (micro_benchmark_test_case test)
{
  const char *name = micro_benchmark_test_case_get_name (test);
  SCM entry = find_entry (scm_from_locale_string (name));
  if (scm_is_false (entry))
    abort ();

  SCM scm_test = scm_cadr (entry);
  SCM set_up = scm_caddr (entry);
  SCM tear_down = scm_cadddr (entry);
  mbguile_test_data_ *data = make_data (scm_test, set_up, tear_down);
  micro_benchmark_test_case_set_data (test, data,
                                      (void (*)(void *)) free_data);

  SCM rest = scm_cddddr (entry);
  SCM constraints = scm_car (rest);
  SCM test_value = make_test (test);
  SCM curr = constraints;
  while (!scm_is_null (curr))
    {
      SCM last = scm_car (curr);
      curr = scm_cdr (curr);
      scm_call_1 (last, test_value);
    }
}

static SCM
register_test (SCM name, SCM test, SCM set_up, SCM tear_down)
{
  static const micro_benchmark_test_definition def = {
    false,
    (micro_benchmark_set_up_fun) mbguile_set_up,
    (micro_benchmark_tear_down_fun) mbguile_tear_down,
    NULL,
    mbguile_test
  };
  char *c_name = scm_to_locale_string (name);
  /* TODO: Check.  */
  SCM entry = find_entry (name);
  if (!c_name || scm_is_true (entry))
    abort ();
  micro_benchmark_register_static_test (c_name, &def);
  micro_benchmark_register_static_constraint (c_name, add_static_data);
  push_data (name, test, set_up, tear_down);
  return SCM_UNSPECIFIED;
}

static SCM
register_constraint (SCM name, SCM fun)
{
  SCM entry = find_entry (name);
  if (scm_is_false (entry))
    abort ();
  SCM constraints_list = scm_cddddr (entry);
  SCM old = scm_car (constraints_list);
  scm_set_car_x (constraints_list, scm_cons (fun, old));
  return SCM_UNSPECIFIED;
}

static SCM
add_test (SCM suite, SCM name, SCM test, SCM set_up, SCM tear_down)
{
  micro_benchmark_suite c_suite = get_suite (suite);

  static const micro_benchmark_test_definition def = {
    false,
    (micro_benchmark_set_up_fun) mbguile_set_up,
    (micro_benchmark_tear_down_fun) mbguile_tear_down,
    NULL,
    mbguile_test
  };
  scm_dynwind_begin (0);
  char *c_name = scm_to_locale_string (name);
  scm_dynwind_free (c_name);
  /* TODO: Check.  */
  if (!c_name)
    abort ();
  micro_benchmark_test_case test_case =
    micro_benchmark_suite_register_test (c_suite, c_name, &def);
  scm_dynwind_end ();

  mbguile_test_data_ *data = make_data (test, set_up, tear_down);
  micro_benchmark_test_case_set_data (test_case, data,
                                      (void (*)(void *)) free_data);
  return make_test (test_case);
}

static SCM
add_dimension (SCM test, SCM sizes)
{
  micro_benchmark_test_case c_test = get_test (test);
  /* TODO: Check vector.  */
  SCM len = scm_length (sizes);
  size_t c_len = scm_to_size_t (len);
  scm_dynwind_begin (0);
  size_t *c_sizes = xmalloc_n (sizeof (size_t), c_len);
  scm_dynwind_free (c_sizes);
  for (size_t i = 0; i < c_len; ++i)
    {
      SCM item = scm_list_ref (sizes, scm_from_size_t (i));
      c_sizes[i] = scm_to_size_t (item);
    }
  micro_benchmark_test_case_add_dimension (c_test, c_len, c_sizes);
  scm_dynwind_end ();
  return SCM_BOOL_T;
}

static SCM
dimensions (SCM test)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t dim = micro_benchmark_test_case_dimensions (c_test);
  return scm_from_size_t (dim);
}

static SCM
skip_iterations (SCM test, SCM iterations)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t c_it = scm_to_size_t (iterations);
  micro_benchmark_test_case_skip_iterations (c_test, c_it);
  return SCM_BOOL_T;
}

static SCM
iterations_to_skip (SCM test)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t it = micro_benchmark_test_case_iterations_to_skip (c_test);
  return scm_from_size_t (it);
}

static SCM
limit_iterations (SCM test, SCM min, SCM max)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t c_min = scm_to_size_t (min);
  size_t c_max = scm_to_size_t (max);
  micro_benchmark_test_case_limit_iterations (c_test, c_min, c_max);
  return SCM_BOOL_T;
}

static SCM
min_iterations (SCM test)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t it = micro_benchmark_test_case_min_iterations (c_test);
  return scm_from_size_t (it);
}

static SCM
max_iterations (SCM test)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t it = micro_benchmark_test_case_max_iterations (c_test);
  return scm_from_size_t (it);
}

static SCM
limit_samples (SCM test, SCM min, SCM max)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t c_min = scm_to_size_t (min);
  size_t c_max = scm_to_size_t (max);
  micro_benchmark_test_case_limit_samples (c_test, c_min, c_max);
  return SCM_BOOL_T;
}

static SCM
min_sample_iterations (SCM test)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t it = micro_benchmark_test_case_min_sample_iterations (c_test);
  return scm_from_size_t (it);
}

static SCM
max_sample_iterations (SCM test)
{
  micro_benchmark_test_case c_test = get_test (test);
  size_t it = micro_benchmark_test_case_max_sample_iterations (c_test);
  return scm_from_size_t (it);
}

static SCM
set_max_time (SCM test, SCM sec, SCM ns)
{
  micro_benchmark_test_case c_test = get_test (test);
  micro_benchmark_clock_time t;
  t.seconds = scm_to_int32 (sec);
  t.nanoseconds = scm_to_int32 (ns);
  micro_benchmark_test_case_set_max_time (c_test, t);
  return SCM_UNSPECIFIED;
}


static SCM
max_time (SCM test)
{
  micro_benchmark_test_case c_test = get_test (test);
  micro_benchmark_clock_time t =
    micro_benchmark_test_case_get_max_time (c_test);
  return scm_cons (scm_from_int32 (t.seconds),
                   scm_from_int32 (t.nanoseconds));
}


static void
empty_finalizer (SCM test)
{
  // The suite should outlive this object
  (void) test;
}

void
micro_benchmark_guile_init_test (void)
{
  SCM name = scm_from_utf8_symbol ("test");
  scm_t_struct_finalize finalizer = empty_finalizer;
  SCM slots = scm_list_1 (scm_from_utf8_symbol ("pointer"));
  test_type = scm_make_foreign_object_type (name, slots, finalizer);

  scm_c_define_gsubr ("register-test!/internal", 4, 0, 0, register_test);
  scm_c_define_gsubr ("register-constraint!/internal", 2, 0, 0,
                      register_constraint);
  scm_c_define_gsubr ("add-test!/internal", 5, 0, 0, add_test);

  scm_c_define_gsubr ("add-dimension!/internal", 2, 0, 0, add_dimension);
  scm_c_define_gsubr ("dimensions/internal", 1, 0, 0, dimensions);

  scm_c_define_gsubr ("skip-iterations!/internal", 2, 0, 0, skip_iterations);
  scm_c_define_gsubr ("iterations-to-skip/internal", 1, 0, 0,
                      iterations_to_skip);

  scm_c_define_gsubr ("limit-iterations!/internal", 3, 0, 0,
                      limit_iterations);
  scm_c_define_gsubr ("max-iterations/internal", 1, 0, 0, min_iterations);
  scm_c_define_gsubr ("min-iterations/internal", 1, 0, 0, max_iterations);

  scm_c_define_gsubr ("limit-samples!/internal", 3, 0, 0, limit_samples);
  scm_c_define_gsubr ("max-sample-iterations/internal", 1, 0, 0,
                      min_sample_iterations);
  scm_c_define_gsubr ("min-sample-iterations/internal", 1, 0, 0,
                      max_sample_iterations);

  scm_c_define_gsubr ("set-max-time!/internal", 3, 0, 0, set_max_time);
  scm_c_define_gsubr ("max-time/internal", 1, 0, 0, max_time);
}
