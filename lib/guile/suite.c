/* suite.c --- MicroBenchmark Guile Binding. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/guile/suite.h"

#include "mbenchmark/all.h"

#include <assert.h>
#include <stdlib.h>
#include <libguile.h>

static SCM suite_type;

micro_benchmark_suite
get_suite (SCM suite)
{
  scm_assert_foreign_object_type (suite_type, suite);
  micro_benchmark_suite c_suite = scm_foreign_object_ref (suite, 0);
  assert (c_suite);
  return c_suite;
}

static SCM
is_suite (SCM suite)
{
  scm_assert_foreign_object_type (suite_type, suite);
  return SCM_BOOL_T;
}

static SCM
make_suite (SCM name)
{
  scm_dynwind_begin (0);
  char *c_name = scm_to_locale_string (name);
  if (!c_name)
    abort ();
  scm_dynwind_free (c_name);
  micro_benchmark_suite suite = micro_benchmark_suite_create (c_name);
  assert (suite);
  scm_dynwind_end ();
  return scm_make_foreign_object_1 (suite_type, suite);
}

static void
finalize_suite (SCM suite)
{
  micro_benchmark_suite c_suite = scm_foreign_object_ref (suite, 0);
  assert (c_suite);
  micro_benchmark_suite_release (c_suite);
}

static SCM
suite_name (SCM suite)
{
  micro_benchmark_suite c_suite = get_suite (suite);
  const char *name = micro_benchmark_suite_get_name (c_suite);
  return scm_from_locale_string (name);
}

static SCM
suite_number_of_tests (SCM suite)
{
  micro_benchmark_suite c_suite = get_suite (suite);
  size_t n = micro_benchmark_suite_get_number_of_tests (c_suite);
  return scm_from_size_t (n);
}

static SCM
run_suite (SCM suite)
{
  micro_benchmark_suite c_suite = get_suite (suite);
  micro_benchmark_suite_run (c_suite);
  return SCM_UNSPECIFIED;
}

void
micro_benchmark_guile_init_suite (void)
{
  SCM slots = scm_list_1 (scm_from_utf8_symbol ("pointer"));
  SCM name = scm_from_utf8_symbol ("suite");
  scm_t_struct_finalize finalizer = finalize_suite;
  suite_type = scm_make_foreign_object_type (name, slots, finalizer);

  scm_c_define_gsubr ("suite?/internal", 1, 0, 0, is_suite);
  scm_c_define_gsubr ("make-suite", 1, 0, 0, make_suite);
  scm_c_define_gsubr ("suite-name", 1, 0, 0, suite_name);
  scm_c_define_gsubr ("suite-number-of-tests", 1, 0, 0,
                      suite_number_of_tests);
  scm_c_define_gsubr ("run-suite!", 1, 0, 0, run_suite);
}
