/* constraints.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/constraints.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "common"
#define MICRO_BENCHMARK_MODULE "constraints:" MICRO_BENCHMARK_SUBMODULE

void
mbconstraints_init_ (mbconstraints_ *ptr)
{
  MB_TRACE ("Init %p\n", ptr);
  assert (ptr);
  memset (ptr, 0, sizeof (mbconstraints_));
}

void
mbconstraints_cleanup_ (mbconstraints_ *ptr)
{
  MB_TRACE ("Cleanup %p\n", ptr);
  assert (ptr);
  mbconstraints_size_cleanup_ (mbconstraints_get_size_ (ptr));
  mbconstraints_meter_cleanup_ (mbconstraints_get_meter_ (ptr));
}

mbconstraints_iteration_ *
mbconstraints_get_iteration_ (mbconstraints_ *ptr)
{
  assert (ptr);
  return &ptr->iteration;
}

mbconstraints_meter_ *
mbconstraints_get_meter_ (mbconstraints_ *ptr)
{
  assert (ptr);
  return &ptr->meter;
}

mbconstraints_size_ *
mbconstraints_get_size_ (mbconstraints_ *ptr)
{
  assert (ptr);
  return &ptr->size;
}

mbconstraints_time_ *
mbconstraints_get_time_ (mbconstraints_ *ptr)
{
  assert (ptr);
  return &ptr->time;
}
