/* sizes.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/constraints/size.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "sizes"
#define MICRO_BENCHMARK_MODULE "constraints:" MICRO_BENCHMARK_SUBMODULE

static const size_t size_dim = sizeof (mbconstraints_size_dimension_);

static void
copy_dimension (mbconstraints_size_dimension_ *lvalue, size_t num_sizes,
                const size_t *sizes)
{
  assert (lvalue);
  assert (sizes);
  assert (num_sizes > 0);
  size_t *ptr = xmalloc_n (sizeof (size_t), num_sizes);
  memcpy (ptr, sizes, sizeof (size_t) * num_sizes);
  lvalue->sizes = ptr;
  lvalue->num_sizes = num_sizes;
}

static void
cleanup_dimension (mbconstraints_size_dimension_ *dim)
{
  assert (dim);
  assert (dim->sizes);
  assert (dim->num_sizes > 0);
  xfree_n (dim->sizes, sizeof (size_t), dim->num_sizes);
  dim->sizes = NULL;
  dim->num_sizes = 0;
}

void
mbconstraints_size_cleanup_ (mbconstraints_size_ *tc)
{
  assert (tc);
  if (tc->has_size)
    {
      assert (tc->num_dimensions > 0);
      assert (tc->dimensions);
      for (size_t i = 0; i < tc->num_dimensions; ++i)
        cleanup_dimension (&tc->dimensions[i]);

      xfree_n (tc->dimensions, sizeof (mbconstraints_size_dimension_),
               tc->num_dimensions);
    }

  tc->has_size = false;
  tc->dimensions = NULL;
  tc->num_dimensions = 0;
}

void
mbconstraints_add_dimension_ (mbconstraints_size_ *tc, size_t num_sizes,
                              const size_t *sizes)
{
  assert (tc);
  assert (sizes);
  assert (num_sizes > 0);
  const size_t dim = tc->num_dimensions++;
  if (tc->has_size)
    {
      assert (tc->dimensions);
      tc->dimensions =
        xrealloc_n (tc->dimensions, size_dim, tc->num_dimensions);
    }
  else
    {
      tc->dimensions = xmalloc_n (size_dim, tc->num_dimensions);
      tc->has_size = true;
    }
  copy_dimension (&tc->dimensions[dim], num_sizes, sizes);
}

size_t
mbconstraints_dimensions_ (const mbconstraints_size_ *tc)
{
  assert (tc);
  return tc->num_dimensions;
}

void
mbconstraints_get_dimension_ (const mbconstraints_size_ *tc, size_t dimension,
                              size_t *n_sizes, const size_t **sizes)
{
  assert (tc);
  assert (n_sizes);
  assert (sizes);
  assert (dimension < tc->num_dimensions);
  *n_sizes = tc->dimensions[dimension].num_sizes;
  *sizes = tc->dimensions[dimension].sizes;
}
