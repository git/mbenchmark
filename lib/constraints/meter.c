/* meters.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/constraints/meter.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "meters"
#define MICRO_BENCHMARK_MODULE "constraints:" MICRO_BENCHMARK_SUBMODULE

void
mbconstraints_meter_cleanup_ (mbconstraints_meter_ *tc)
{
  if (tc->has_custom_meters)
    {
      xfree_n (tc->custom_meters, sizeof (micro_benchmark_custom_meter),
               tc->num_custom_meters);
      tc->custom_meters = NULL;
      tc->num_custom_meters = 0;
      tc->has_custom_meters = false;
    }
}

bool
mbconstraints_has_calculator_ (const mbconstraints_meter_ *tc)
{
  assert (tc);
  return tc->has_custom_stats;
}

micro_benchmark_custom_time_calculator
mbconstraints_get_calculator_ (const mbconstraints_meter_ *tc)
{
  assert (tc);
  assert (mbconstraints_has_calculator_ (tc));
  return tc->custom_stats;
}

void
mbconstraints_set_calculator_ (mbconstraints_meter_ *tc,
                               micro_benchmark_custom_time_calculator c)
{
  assert (tc);
  tc->has_custom_stats = true;
  tc->custom_stats = c;
}

void
mbconstraints_add_meter_ (mbconstraints_meter_ *tc,
                          micro_benchmark_custom_meter m)
{
  assert (tc);
  const size_t pos = tc->num_custom_meters++;
  tc->custom_meters = xrealloc_n (tc->custom_meters,
                                  sizeof (micro_benchmark_custom_meter),
                                  tc->num_custom_meters);
  tc->has_custom_meters = true;
  tc->custom_meters[pos] = m;
}

size_t
mbconstraints_num_meters_ (const mbconstraints_meter_ *tc)
{
  assert (tc);
  return tc->num_custom_meters;
}

micro_benchmark_custom_meter
mbconstraints_get_meter_n_ (const mbconstraints_meter_ *tc, size_t pos)
{
  assert (tc);
  assert (pos < mbconstraints_num_meters_ (tc));
  return tc->custom_meters[pos];
}

bool
mbconstraints_has_chrono_ (const mbconstraints_meter_ *tc)
{
  assert (tc);
  return tc->has_chronometer;
}

micro_benchmark_clock_type
mbconstraints_get_chrono_type_ (const mbconstraints_meter_ *tc)
{
  assert (mbconstraints_has_chrono_ (tc));
  return tc->chrono_type;
}

micro_benchmark_chronometer_provider
mbconstraints_get_chrono_provider_ (const mbconstraints_meter_ *tc)
{
  assert (mbconstraints_has_chrono_ (tc));
  return tc->chrono_provider;
}

micro_benchmark_meter
mbconstraints_get_chrono_tmpl_ (const mbconstraints_meter_ *tc)
{
  assert (mbconstraints_has_chrono_ (tc));
  static const micro_benchmark_chronometer_provider up =
    MICRO_BENCHMARK_CHRONO_PROVIDER_USER_PROVIDED;
  assert (mbconstraints_get_chrono_provider_ (tc) == up);
  (void) up;
  return &tc->chrono_tmpl;
}

void
mbconstraints_set_chrono_ (mbconstraints_meter_ *tc,
                           micro_benchmark_clock_type type,
                           micro_benchmark_chronometer_provider p)
{
  assert (tc);
  assert (p != MICRO_BENCHMARK_CHRONO_PROVIDER_USER_PROVIDED);
  tc->has_chronometer = true;
  tc->chrono_type = type;
  tc->chrono_provider = p;
}

void
mbconstraints_set_custom_chrono_ (mbconstraints_meter_ *tc,
                                  micro_benchmark_clock_type type,
                                  micro_benchmark_meter m)
{
  assert (tc);
  assert (m);
  tc->has_chronometer = true;
  tc->chrono_type = type;
  tc->chrono_provider = MICRO_BENCHMARK_CHRONO_PROVIDER_USER_PROVIDED;
  tc->chrono_tmpl = *m;
}
