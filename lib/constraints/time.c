/* time.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/constraints/time.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "time"
#define MICRO_BENCHMARK_MODULE "constraints:" MICRO_BENCHMARK_SUBMODULE

bool
mbconstraints_has_max_time_ (const mbconstraints_time_ *tc)
{
  assert (tc);
  return tc->has_deadline;
}

micro_benchmark_clock_time
mbconstraints_get_max_time_ (const mbconstraints_time_ *tc)
{
  assert (tc);
  assert (mbconstraints_has_max_time_ (tc));
  return tc->deadline;
}

void
mbconstraints_set_max_time_ (mbconstraints_time_ *tc,
                             micro_benchmark_clock_time d)
{
  assert (tc);
  tc->has_deadline = true;
  tc->deadline = d;
}

bool
mbconstraints_has_sample_time_ (const mbconstraints_time_ *tc)
{
  assert (tc);
  return tc->has_sample_time;
}

micro_benchmark_clock_time
mbconstraints_get_sample_time_ (const mbconstraints_time_ *tc)
{
  assert (tc);
  assert (mbconstraints_has_sample_time_ (tc));
  return tc->sample_time;
}

void
mbconstraints_set_sample_time_ (mbconstraints_time_ *tc,
                                micro_benchmark_clock_time s)
{
  assert (tc);
  tc->has_sample_time = true;
  tc->sample_time = s;
}

bool
mbconstraints_has_timer_ (const mbconstraints_time_ *tc)
{
  assert (tc);
  return tc->deadline_timer.has_timer;
}

micro_benchmark_clock_type
mbconstraints_get_timer_type_ (const mbconstraints_time_ *tc)
{
  assert (mbconstraints_has_timer_ (tc));
  return tc->deadline_timer.type;
}

micro_benchmark_timer_provider
mbconstraints_get_timer_provider_ (const mbconstraints_time_ *tc)
{
  assert (mbconstraints_has_timer_ (tc));
  return tc->deadline_timer.provider;
}

micro_benchmark_timer
mbconstraints_get_timer_tmpl_ (const mbconstraints_time_ *tc)
{
  assert (mbconstraints_has_timer_ (tc));
  const micro_benchmark_timer_provider up =
    MICRO_BENCHMARK_TIMER_PROVIDER_USER_PROVIDED;
  assert (mbconstraints_get_timer_provider_ (tc) == up);
  (void) up;
  return &tc->deadline_timer.tmpl;
}

micro_benchmark_chronometer_provider
mbconstraints_get_timer_meter_ (const mbconstraints_time_ *tc)
{
  assert (mbconstraints_has_timer_ (tc));
  const micro_benchmark_timer_provider fm =
    MICRO_BENCHMARK_TIMER_PROVIDER_FROM_METER;
  assert (mbconstraints_get_timer_provider_ (tc) == fm);
  (void) fm;
  return tc->deadline_timer.meter;
}

micro_benchmark_meter
mbconstraints_get_timer_meter_tmpl_ (const mbconstraints_time_ *tc)
{
  assert (mbconstraints_has_timer_ (tc));
  const micro_benchmark_chronometer_provider up =
    MICRO_BENCHMARK_CHRONO_PROVIDER_USER_PROVIDED;
  assert (mbconstraints_get_timer_meter_ (tc) == up);
  (void) up;
  return &tc->deadline_timer.meter_tmpl;
}

void
mbconstraints_set_timer_ (mbconstraints_time_ *tc,
                          micro_benchmark_clock_type type,
                          micro_benchmark_timer_provider prov)
{
  assert (tc);
  assert (prov != MICRO_BENCHMARK_TIMER_PROVIDER_USER_PROVIDED
          && prov != MICRO_BENCHMARK_TIMER_PROVIDER_FROM_METER);
  tc->deadline_timer.has_timer = true;
  tc->deadline_timer.type = type;
  tc->deadline_timer.provider = prov;
  tc->deadline_timer.extra = NULL;
}

void
mbconstraints_set_custom_timer_ (mbconstraints_time_ *tc,
                                 micro_benchmark_clock_type type,
                                 micro_benchmark_timer timer, void *extra)
{
  assert (tc);
  assert (timer);
  tc->deadline_timer.has_timer = true;
  tc->deadline_timer.type = type;
  tc->deadline_timer.provider = MICRO_BENCHMARK_TIMER_PROVIDER_USER_PROVIDED;
  tc->deadline_timer.tmpl = *timer;
  tc->deadline_timer.extra = extra;
}

void
mbconstraints_set_timer_from_meter_ (mbconstraints_time_ *tc,
                                     micro_benchmark_clock_type type,
                                     micro_benchmark_chronometer_provider p)
{
  assert (tc);
  tc->deadline_timer.has_timer = true;
  tc->deadline_timer.type = type;
  tc->deadline_timer.provider = MICRO_BENCHMARK_TIMER_PROVIDER_FROM_METER;
  tc->deadline_timer.meter = p;
  tc->deadline_timer.extra = NULL;
}
