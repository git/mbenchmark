/* iteration.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/constraints/iteration.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "iterations"
#define MICRO_BENCHMARK_MODULE "constraints:" MICRO_BENCHMARK_SUBMODULE

void
mbconstraints_skip_iterations_ (mbconstraints_iteration_ *tc,
                                size_t iterations_to_skip)
{
  assert (tc);
  tc->iterations_to_skip = iterations_to_skip;
}

size_t
mbconstraints_iterations_to_skip_ (const mbconstraints_iteration_ *tc)
{
  assert (tc);
  return tc->iterations_to_skip;
}

void
mbconstraints_limit_iterations_ (mbconstraints_iteration_ *tc,
                                 size_t min_iterations, size_t max_iterations)
{
  assert (tc);
  tc->has_iterations = true;
  tc->min_iterations = min_iterations;
  tc->max_iterations = max_iterations;
}

bool
mbconstraints_has_iterations_ (const mbconstraints_iteration_ *tc)
{
  assert (tc);
  return tc->has_iterations;
}

size_t
mbconstraints_min_iterations_ (const mbconstraints_iteration_ *tc)
{
  assert (tc);
  return tc->min_iterations;
}

size_t
mbconstraints_max_iterations_ (const mbconstraints_iteration_ *tc)
{
  assert (tc);
  return tc->max_iterations;
}

void
mbconstraints_limit_samples_ (mbconstraints_iteration_ *tc,
                              size_t min_iterations, size_t max_iterations)
{
  assert (tc);
  tc->has_sample_iterations = true;
  tc->min_sample_iterations = min_iterations;
  tc->max_sample_iterations = max_iterations;
}

bool
mbconstraints_has_sample_iterations_ (const mbconstraints_iteration_ *tc)
{
  assert (tc);
  return tc->has_sample_iterations;
}

size_t
mbconstraints_min_sample_iterations_ (const mbconstraints_iteration_ *tc)
{
  assert (tc);
  return tc->min_sample_iterations;
}


size_t
mbconstraints_max_sample_iterations_ (const mbconstraints_iteration_ *tc)
{
  assert (tc);
  return tc->max_sample_iterations;
}
