/* error.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/utility/functions.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

static void (*error_handler) (void) = abort;

void
micro_benchmark_set_error_handler (void (*hnd) (void))
{
  assert (hnd);
  error_handler = hnd;
}

void
micro_benchmark_handle_error_ (const char *msgid)
{
  assert (msgid);
  int old_errno = errno;
  const char *msgstr = _(msgid);
  errno = old_errno;
  perror (msgstr);
  errno = old_errno;
  error_handler ();
  /* Ensure the process do not return.  */
  exit (EXIT_FAILURE);
}
