/* log.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/utility/log.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#ifndef MICRO_BENCHMARK_DEFAULT_LOG_OUTPUT
#define MICRO_BENCHMARK_DEFAULT_LOG_OUTPUT stderr
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_LOG_LEVEL
#define MICRO_BENCHMARK_DEFAULT_LOG_LEVEL MICRO_BENCHMARK_WARNING_LEVEL
#endif

static micro_benchmark_log_level global_log_level =
  MICRO_BENCHMARK_DEFAULT_LOG_LEVEL;

static FILE *output = NULL;

void
micro_benchmark_log_set_output (FILE *out)
{
  if (out)
    output = out;
  else
    output = MICRO_BENCHMARK_DEFAULT_LOG_OUTPUT;
}

typedef struct module_info
{
  const char *name;
  micro_benchmark_log_level level;
} module_info;

typedef struct module_data
{
  size_t n_modules;
  module_info *modules;
} module_data;

static module_data data = { 0 };

void
micro_benchmark_set_log_level (micro_benchmark_log_level level)
{
  global_log_level = level;
}

void
micro_benchmark_set_module_log_level (const char *module,
                                      micro_benchmark_log_level level)
{
  for (size_t i = 0; i < data.n_modules; ++i)
    if (strcmp (module, data.modules[i].name) == 0)
      {
        data.modules[i].level = level;
        return;
      }

  assert (data.n_modules != SIZE_MAX);
  if (data.n_modules == 0)
    data.modules = xmalloc_n (sizeof (module_info), data.n_modules + 1);
  else
    data.modules = xrealloc_n (data.modules, sizeof (module_info),
                               data.n_modules + 1);
  data.modules[data.n_modules].name = module;
  data.modules[data.n_modules].level = level;
  data.n_modules++;
}

static bool
ignore_level (micro_benchmark_log_level level, const char *module)
{
  /* Look for exact match.  */
  for (size_t i = 0; i < data.n_modules; ++i)
    if (strcmp (module, data.modules[i].name) == 0)
      return level > data.modules[i].level;

  /* Look for exact parent modules.  */
  for (size_t i = 0; i < data.n_modules; ++i)
    if (strncmp (module, data.modules[i].name,
                 strlen (data.modules[i].name)) == 0)
      return level > data.modules[i].level;

  return level > global_log_level;
}

static const char *
log_level_str (micro_benchmark_log_level level)
{
  switch (level)
    {
    case MICRO_BENCHMARK_ERROR_LEVEL:
      return _("ERROR");
    case MICRO_BENCHMARK_WARNING_LEVEL:
      return _("WARN");
    case MICRO_BENCHMARK_INFO_LEVEL:
      return _("INFO");
    case MICRO_BENCHMARK_DEBUG_LEVEL:
      return _("DEBUG");
    case MICRO_BENCHMARK_TRACE_LEVEL:
      return _("TRACE");
    default:
      return _("?????");
    }
}

void
micro_benchmark_log_ (micro_benchmark_log_level level, const char *module,
                      const char *fun, const char *file, size_t line,
                      const char *msg, ...)
{
  if (ignore_level (level, module) || !output)
    return;

  if ((int) global_log_level > (int) MICRO_BENCHMARK_INFO_LEVEL)
    fprintf (output, _("[%s] %s:%s:%zu:%s: "), log_level_str (level), module,
             file, line, fun);
  else
    fprintf (output, _("[%s] %s: "), log_level_str (level), module);
  va_list args;
  va_start (args, msg);
  int ret = vfprintf (output, _(msg), args);
  va_end (args);
  MB_HANDLE_ERROR (ret < 0, "Error printing log");
}
