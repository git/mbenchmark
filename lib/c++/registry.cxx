/* registry.cxx --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/c++/registry.hpp"
#include "mbenchmark/c++/report.hpp"
#include "mbenchmark/c++/test.hpp"
#include "mbenchmark/c++/suite.hpp" // Implement suite::register_test_impl_

#include "mbenchmark/suite.h"
#include "mbenchmark/state.h"
#include "mbenchmark/test.h"
#include "mbenchmark/utility/functions.h"

#include <cassert>
#include <utility>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace
  {
    using detail::test_base;
    using detail::make_report;
    using detail::make_test_case;
    using detail::make_state;

    void*
    set_up (::micro_benchmark_test_state s)
    {
      void* ptr = micro_benchmark_state_get_data (s);
      test_base* base = static_cast<test_base*> (ptr);
      state st = make_state (s);
      base->run_set_up (st);
      return base;
    }

    void
    test (::micro_benchmark_test_state s)
    {
      void* ptr = micro_benchmark_state_get_data (s);
      test_base* base = static_cast<test_base*> (ptr);
      state st = make_state (s);
      base->run_test (st);
    }

    void
    tear_down (::micro_benchmark_test_state s, void* ptr)
    {
      test_base* base = static_cast<test_base*> (ptr);
      state st = make_state (s);
      base->run_tear_down (st);
    }

    ::micro_benchmark_test_definition def = {
      false, set_up, tear_down, nullptr, test
    };

    void constraints (micro_benchmark_test_case test)
    {
      auto name = micro_benchmark_test_case_get_name (test);
      auto const& m = registry::detail::registered_tests ();
      auto it = m.find (name);
      assert (it != m.end ());
      auto const& el = it->second;
      assert (static_cast<bool> (el.test));
      micro_benchmark_test_case_set_data (test, el.test.get(), [] (void*) {});

      test_case tc = make_test_case (test, nullptr);
      for (auto&& f : el.constraints)
        f (tc);
    }
  }

  test_case
  suite::register_test_impl_ (const char *name)
  {
    assert (impl);
    micro_benchmark_suite suite = impl->as<micro_benchmark_suite> ();
    micro_benchmark_test_case tc =
      micro_benchmark_suite_register_test (suite, name, &def);
    return make_test_case (tc, impl);
  }

  namespace registry
  {
    namespace detail
    {
      namespace
      {
        using ptr_type = std::unique_ptr<detail::test_base>;
        using fun_type = std::function<void(test_case&)>;
        map tests;
      }

      void
      register_static_test (char const* name)
      {
        assert (name);
        micro_benchmark_register_static_test (name, &def);
        micro_benchmark_register_static_constraint (name, &constraints);
      }

      map const&
      registered_tests ()
      {
        return tests;
      }

      void
      add_test (char const* name, ptr_type t)
      {
        assert (name);
        auto& v = tests[name];
        v.test = std::move (t);
      }

      void
      add_constraint (char const* name, fun_type f)
      {
        assert (name);
        auto& v = tests[name];
        v.constraints.push_back (std::move (f));
      }
    }
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
