/* suite.cxx --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/c++/suite.hpp"
#include "mbenchmark/c++/test.hpp"
#include "mbenchmark/c++/registry.hpp"
#include "mbenchmark/suite.h"
#include "mbenchmark/utility/functions.h"

#include <cassert>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace
  {
    using detail::c_handler;
    using detail::c_suite;
  }

  detail::c_suite::c_suite (const char *name)
    : c_handler (micro_benchmark_suite_create (name))
  {
  }

  detail::c_suite::~c_suite ()
  {
    micro_benchmark_suite suite = as<micro_benchmark_suite> ();
    micro_benchmark_suite_release (suite);
  }

  char const*
  suite::name () const
  {
    assert (impl);
    micro_benchmark_suite suite = impl->as<micro_benchmark_suite> ();
    return micro_benchmark_suite_get_name (suite);
  }


  std::vector<test_case>
  suite::tests ()
  {
    assert (impl);
    micro_benchmark_suite suite = impl->as<micro_benchmark_suite> ();
    auto n = micro_benchmark_suite_get_number_of_tests (suite);
    std::vector<test_case> ret;
    ret.reserve (n);
    for (size_t i = 1; i < n; ++i)
      {
        micro_benchmark_test_case tc =
          micro_benchmark_suite_get_test (suite, i);
        ret.push_back (make_test_case (tc, impl));
      }
    return ret;
  }

  report
  suite::stored_report () const
  {
    assert (impl);
    micro_benchmark_suite suite = impl->as<micro_benchmark_suite> ();
    micro_benchmark_report r = micro_benchmark_suite_get_report (suite);
    return make_report (r, impl);
  }

  void
  suite::run ()
  {
    assert (impl);
    micro_benchmark_suite suite = impl->as<micro_benchmark_suite> ();
    micro_benchmark_suite_run (suite);
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
