/* state.cxx --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/c++/state.hpp"

#include "mbenchmark/state.h"

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  state
  detail::make_state (void *ptr)
  {
    return state{c_handler (ptr)};
  }

  bool
  state::keep_running ()
  {
    micro_benchmark_test_state state = as<micro_benchmark_test_state> ();
    return micro_benchmark_state_keep_running (state);
  }

  std::vector<std::size_t>
  state::sizes () const
  {
    micro_benchmark_test_state state = as<micro_benchmark_test_state> ();
    auto dim = micro_benchmark_state_get_dimensions (state);
    std::vector<std::size_t> ret;
    ret.reserve (dim);
    for (std::size_t i = 0; i < dim; ++i)
      ret.push_back (micro_benchmark_state_get_size (state, i));
    return ret;
  }

  void
  state::set_name (char const* name)
  {
    micro_benchmark_test_state state = as<micro_benchmark_test_state> ();
    micro_benchmark_state_set_name (state, name);
  }

  char const*
  state::get_name () const
  {
    micro_benchmark_test_state state = as<micro_benchmark_test_state> ();
    return micro_benchmark_state_get_name (state);
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
