/* report.cxx --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/c++/report.hpp"
#include "mbenchmark/report.h"
#include <iostream>

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  namespace
  {
    using detail::c_handler;
    using detail::c_suite_ptr;
    using detail::time_conversor;
    using detail::iteration_conversor;
    using detail::iteration_stat_value;
    using detail::time_stat_value;
    using detail::time_sample;
    using io::output_stat;
    using io::output_type;
    using io::output_values;

    time_stat_value
    to_time_value (::micro_benchmark_stats_value v)
    {
      using unit_type = time_stat_value::unit_type;
      int unit = static_cast<int> (v.unit);
      unit_type mean{unit, v.mean};
      unit_type stdd{unit, v.std_deviation};
      unit_type::squared var{unit, v.variance};
      return time_stat_value {mean, stdd, var};
    }

    time_sample
    to_time_sample (::micro_benchmark_time_sample s)
    {
      assert (s);
      std::chrono::seconds sec(s->elapsed.seconds);
      std::chrono::nanoseconds ns(s->elapsed.nanoseconds);
      return time_sample {s->discarded, s->iterations, sec + ns};
    }

    output_stat
    from_c_stat (::micro_benchmark_output_stat s)
    {
      int v = static_cast<int> (s);
      return static_cast<output_stat> (v);
    }

    ::micro_benchmark_output_stat
    to_c_stat (output_stat s)
    {
      int v = static_cast<int> (s);
      return static_cast<::micro_benchmark_output_stat> (v);
    }

    output_values
    from_c_values (::micro_benchmark_output_values values)
    {
      output_values v = { };
      v.self_test = values.self_test;
      v.size_constraints = values.size_constraints;
      v.total_time = values.total_time;
      v.total_iterations = values.total_iterations;
      v.iterations = values.iterations;
      v.total_samples = values.total_samples;
      v.samples = values.samples;
      v.extra_data = values.extra_data;
      v.iteration_time = from_c_stat (values.iteration_time);
      v.sample_time = from_c_stat (values.sample_time);
      v.sample_iterations = from_c_stat (values.sample_iterations);
      return v;
    }

    ::micro_benchmark_output_values
    to_c_values (const output_values& values)
    {
      ::micro_benchmark_output_values v = { };
      v.self_test = values.self_test;
      v.size_constraints = values.size_constraints;
      v.total_time = values.total_time;
      v.total_iterations = values.total_iterations;
      v.iterations = values.iterations;
      v.total_samples = values.total_samples;
      v.samples = values.samples;
      v.extra_data = values.extra_data;
      v.iteration_time = to_c_stat (values.iteration_time);
      v.sample_time = to_c_stat (values.sample_time);
      v.sample_iterations = to_c_stat (values.sample_iterations);
      return v;
    }

    ::micro_benchmark_output_type
    to_c_type (output_type type)
    {
      switch (type)
        {
        default:
          assert (0);
        case output_type::console:
          return ::MICRO_BENCHMARK_CONSOLE_OUTPUT;
        case output_type::lisp:
          return ::MICRO_BENCHMARK_LISP_OUTPUT;
        case output_type::text:
          return ::MICRO_BENCHMARK_TEXT_OUTPUT;
        }
    }

    template <typename F>
    std::string
    adapt_file (F&& f) noexcept
    {
      char *text = NULL;
      size_t size = 0;
      FILE *out = open_memstream (&text, &size);
      std::forward<F> (f) (out);
      fflush (out);
      fclose (out);
      std::string ret{text, size};
      std::free (text);
      return ret;
    }
  }

  std::chrono::nanoseconds
  time_conversor::operator() (int unit, double value) const
  {
    using count = std::size_t;
    using std::chrono::microseconds;
    using std::chrono::milliseconds;
    using std::chrono::nanoseconds;
    using std::chrono::seconds;
    switch (static_cast<::micro_benchmark_stats_unit> (unit))
      {
      default:
        assert (0);
      case ::MICRO_BENCHMARK_STATS_UNIT_NONE:
        return nanoseconds(-1);
      case ::MICRO_BENCHMARK_STATS_TIME_MIN:
        return seconds (count (value * 60));
      case ::MICRO_BENCHMARK_STATS_TIME_S:
        return seconds (count (value));
      case ::MICRO_BENCHMARK_STATS_TIME_MS:
        return milliseconds (count (value));
      case ::MICRO_BENCHMARK_STATS_TIME_US:
        return microseconds (count (value));
      case ::MICRO_BENCHMARK_STATS_TIME_NS:
        return nanoseconds (count (value));
      }
  }

  exec_report
  detail::make_exec_report (void const* ptr)
  {
    exec_report r{c_handler (ptr)};
    return r;
  }

  char const*
  exec_report::name () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    return ::micro_benchmark_exec_report_get_name (rep);
  }

  std::vector<std::size_t>
  exec_report::sizes () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    std::size_t const* s;
    size_t dim;
    ::micro_benchmark_exec_report_get_sizes (rep, &s, &dim);
    return std::vector<std::size_t>{s, s + dim};
  }

  std::size_t
  exec_report::iterations () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    return ::micro_benchmark_exec_report_get_iterations (rep);
  }

  std::size_t
  exec_report::total_iterations () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    return ::micro_benchmark_exec_report_total_iterations (rep);
  }

  detail::total_time_type
  exec_report::total_time () const
  {
    using s = std::chrono::seconds;
    using ns = std::chrono::nanoseconds;
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    ::micro_benchmark_clock_time elapsed =
        ::micro_benchmark_exec_report_total_time (rep);
    return {s{elapsed.seconds}, ns{elapsed.nanoseconds}};
  }

  std::vector<time_sample>
  exec_report::time_samples () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    std::size_t const n = ::micro_benchmark_exec_report_num_time_samples (rep);
    std::vector<time_sample> ret;
    ret.reserve (n);
    for (std::size_t i = 0; i < n; ++i)
      {
        ::micro_benchmark_time_sample sample =
          ::micro_benchmark_exec_report_get_time_sample (rep, i);
        ret.push_back (to_time_sample (sample));
      }
    return ret;
  }

  std::size_t
  exec_report::total_samples () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    return ::micro_benchmark_exec_report_total_samples (rep);
  }

  std::size_t
  exec_report::used_samples () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    return ::micro_benchmark_exec_report_used_samples (rep);
  }

  time_stat_value
  exec_report::iteration_time () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    ::micro_benchmark_stats_value v =
        ::micro_benchmark_exec_report_iteration_time (rep);
    return to_time_value (v);
  }

  time_stat_value
  exec_report::sample_time () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    ::micro_benchmark_stats_value v =
        ::micro_benchmark_exec_report_sample_time (rep);
    return to_time_value (v);
  }

  iteration_stat_value
  exec_report::sample_iterations () const
  {
    ::micro_benchmark_exec_report rep = as<::micro_benchmark_exec_report> ();
    ::micro_benchmark_stats_value v =
        ::micro_benchmark_exec_report_sample_iterations (rep);
    return iteration_stat_value {v.mean, v.std_deviation, v.variance};
  }

  test_report
  detail::make_test_report (void const* ptr)
  {
    test_report r{c_handler (ptr)};
    return r;
  }

  test_report::test_report (c_handler&& handler)
    : c_handler (std::move(handler))
  {
    ::micro_benchmark_test_report rep = as<::micro_benchmark_test_report> ();
    std::size_t const n = ::micro_benchmark_test_report_get_num_executions (rep);
    cache.reserve (n);
    for (std::size_t i = 0; i < n; ++i)
      {
        ::micro_benchmark_exec_report tr =
          ::micro_benchmark_test_report_get_exec_report (rep, i);
        cache.push_back (detail::make_exec_report (tr));
      }
  }

  char const*
  test_report::name () const
  {
    ::micro_benchmark_test_report rep = as<::micro_benchmark_test_report> ();
    return ::micro_benchmark_test_report_get_name (rep);
  }

  report
  detail::make_report (void const* ptr, c_suite_ptr const& suite)
  {
    report r{c_handler (ptr), suite};
    return r;
  }

  report::report (c_handler&& handler, detail::c_suite_ptr const& suite)
    : c_handler (std::move(handler)), suite (suite)
  {
    ::micro_benchmark_report rep = as<::micro_benchmark_report> ();
    std::size_t const n = ::micro_benchmark_report_get_number_of_tests (rep);
    cache.reserve (n);
    for (std::size_t i = 0; i < n; ++i)
      {
        ::micro_benchmark_test_report tr =
          ::micro_benchmark_report_get_test_report (rep, i);
        cache.push_back (detail::make_test_report (tr));
      }
  }

  char const*
  report::name () const
  {
    ::micro_benchmark_report rep = as<::micro_benchmark_report> ();
    return ::micro_benchmark_report_get_name (rep);
  }

  void
  report::print () const
  {
    ::micro_benchmark_report rep = as<::micro_benchmark_report> ();
    ::micro_benchmark_print_report (rep);
  }

  void
  report::print (io::output_type type) const
  {
    print (stdout, type);
  }

  void
  report::print (io::output_values const& values) const
  {
    ::micro_benchmark_report rep = as<::micro_benchmark_report> ();
    ::micro_benchmark_output_values v = to_c_values (values);
    ::micro_benchmark_print_custom_report (rep, &v);
  }

  void
  report::print (std::FILE* out) const
  {
    print (out, io::output_type::console);
  }

  void
  report::print (std::FILE* out, io::output_type type) const
  {
    ::micro_benchmark_report rep = as<::micro_benchmark_report> ();
    ::micro_benchmark_write_report (rep, out, to_c_type (type));
  }

  void
  report::print (std::FILE* out, io::output_values const& values) const
  {
    print (out, io::output_type::console, values);
  }

  void
  report::print (std::FILE* out, io::output_type type,
                 io::output_values const& values) const
  {
    ::micro_benchmark_report rep = as<::micro_benchmark_report> ();
    ::micro_benchmark_output_values v = to_c_values (values);
    ::micro_benchmark_write_custom_report (rep, out, to_c_type (type), &v);
  }

  void
  report::print (std::ostream& out) const
  {
    print (out, io::output_type::console);
  }

  void
  report::print (std::ostream& out, io::output_type type) const
  {
    ::micro_benchmark_report rep = as<::micro_benchmark_report> ();
    auto t = to_c_type (type);
    out << adapt_file ([&] (FILE* f)
    { ::micro_benchmark_write_report (rep, f, t); });
    out.flush ();
  }

  void
  report::print (std::ostream& out, io::output_values const& values) const
  {
    print (out, io::output_type::console, values);
  }

  void
  report::print (std::ostream& out, io::output_type type,
                 io::output_values const& values) const
  {
    ::micro_benchmark_report rep = as<::micro_benchmark_report> ();
    ::micro_benchmark_output_values v = to_c_values (values);
    auto t = to_c_type (type);
    out << adapt_file ([&] (FILE* f)
    { ::micro_benchmark_write_custom_report (rep, f, t, &v); });
    out.flush ();
  }

  io::output_values
  io::default_output_values ()
  {
    return from_c_values (::micro_benchmark_get_default_output_values ());
  }

  void
  io::default_output_values (io::output_values const& v)
  {
    ::micro_benchmark_set_default_output_values (to_c_values (v));
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
