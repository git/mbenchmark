/* test.cxx --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/c++/test.hpp"

#include "mbenchmark/test.h"

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  test_case
  detail::make_test_case (void* ptr, c_suite_ptr const& suite)
  {
    return test_case (c_handler{ptr}, suite);
  }

  void
  test_case::register_data_ (std::unique_ptr<detail::test_base>&& base)
  {
    auto deleter = [] (void* p)
    {
      delete static_cast<detail::test_base*> (p);
    };
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    micro_benchmark_test_case_set_data (tc, base.release (), deleter);
  }

  void
  test_case::add_dimension (std::vector<std::size_t> const& dim)
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    micro_benchmark_test_case_add_dimension (tc, dim.size (), dim.data ());
  }

  std::size_t
  test_case::number_of_dimensions () const
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    return micro_benchmark_test_case_dimensions (tc);
  }

  std::vector<std::size_t>
  test_case::get_dimension (std::size_t dim) const
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();

    std::size_t n;
    std::size_t const* sizes;
    micro_benchmark_test_case_get_dimension (tc, dim, &n, &sizes);
    std::vector<std::size_t> ret;
    ret.reserve (n);
    for (std::size_t i = 0; i < n; ++i)
      ret.push_back (sizes[i]);
    return ret;
  }

  void
  test_case::skip_iterations (std::size_t iterations)
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    micro_benchmark_test_case_skip_iterations (tc, iterations);
  }

  std::size_t
  test_case::iterations_to_skip () const
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    return micro_benchmark_test_case_iterations_to_skip (tc);
  }

  void
  test_case::limit_iterations (std::size_t min, std::size_t max)
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    micro_benchmark_test_case_limit_iterations (tc, min, max);
  }

  std::size_t
  test_case::min_iterations () const
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    return micro_benchmark_test_case_min_iterations (tc);
  }

  std::size_t
  test_case::max_iterations () const
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    return micro_benchmark_test_case_max_iterations (tc);
  }

  void
  test_case::limit_samples (std::size_t min, std::size_t max)
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    micro_benchmark_test_case_limit_samples (tc, min, max);
  }

  std::size_t
  test_case::min_sample_iterations () const
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    return micro_benchmark_test_case_min_sample_iterations (tc);
  }

  std::size_t
  test_case::max_sample_iterations () const
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    return micro_benchmark_test_case_max_sample_iterations (tc);
  }

  void
  test_case::max_time_ (max_time_type_ t)
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    micro_benchmark_clock_time ct = { t.seconds, t.nanoseconds };
    micro_benchmark_test_case_set_max_time (tc, ct);
  }

  test_case::max_time_type_
  test_case::max_time_ () const
  {
    micro_benchmark_test_case tc = as<micro_benchmark_test_case> ();
    auto t = micro_benchmark_test_case_get_max_time (tc);
    return { t.seconds, t.nanoseconds };
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
