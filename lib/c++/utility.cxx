/* utility.cxx --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/c++/utility.hpp"
#undef MICRO_BENCHMARK_MAIN
#include "mbenchmark/utility.h"

#include <cassert>

#if MB_COVERAGE_ENABLED && defined(__clang__)
#include <cstdlib>
extern "C" int micro_benchmark_cxx_workaround_missing_atexit;
int micro_benchmark_cxx_workaround_missing_atexit =
  [] ()
  {
    using fptr = void (*) ();
    fptr f = [] () {};
    return std::atexit (f);
  } ();
#endif

namespace micro_benchmark
{
  MICRO_BENCHMARK_BEGIN_VERSION_NAMESPACE
  int
  main (int argc, char* argv[])
  {
    assert (argc > 0);
    assert (argv);
    return ::micro_benchmark_main (argc, argv);
  }

  void
  detail::do_not_optimize (void const* ptr)
  {
    MICRO_BENCHMARK_DO_NOT_OPTIMIZE (ptr);
  }

  void
  detail::compiler_barrier ()
  {
    MICRO_BENCHMARK_COMPILER_BARRIER ();
  }
  MICRO_BENCHMARK_END_VERSION_NAMESPACE
}
