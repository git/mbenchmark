/* suite.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/suite.h"

#include "internal/report/suite.h"
#include "internal/suite.h"
#include "internal/test.h"
#include "internal/test/self-test.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "creation"
#define MICRO_BENCHMARK_MODULE "suite:" MICRO_BENCHMARK_SUBMODULE

static const size_t default_allocated_tests = 2;

micro_benchmark_suite
micro_benchmark_suite_create (const char *name)
{
  assert (name);
  micro_benchmark_suite ret =
    xmalloc (sizeof (struct micro_benchmark_suite_));
  MB_TRACE ("Allocated %p\n", ret);
  ret->name = xstrdup (name);
  ret->name_size = strlen (name) + 1;
  MB_TRACE ("Allocated name %p\n", ret->name);
  ret->has_self_test = false;
  MICRO_BENCHMARK_DYN_ARRAY_INIT (ret->tests,
                                  micro_benchmark_test_case_definition,
                                  default_allocated_tests);
  micro_benchmark_report_init_ (&ret->report, ret->name);
  return ret;
}

void
micro_benchmark_suite_release (micro_benchmark_suite suite)
{
  assert (suite);
  micro_benchmark_report_cleanup_ (&suite->report);
  MICRO_BENCHMARK_DYN_ARRAY_RELEASE (suite->tests,
                                     micro_benchmark_test_case_definition,
                                     micro_benchmark_test_case_cleanup_);
  MB_TRACE ("Freeing name %p\n", suite->name);
  xfree (suite->name, suite->name_size);
  MB_TRACE ("Freeing %p\n", suite);
  xfree (suite, sizeof (struct micro_benchmark_suite_));
}

const char *
micro_benchmark_suite_get_name (micro_benchmark_suite suite)
{
  assert (suite);
  return suite->name;
}

size_t
micro_benchmark_suite_get_number_of_tests (micro_benchmark_suite suite)
{
  assert (suite);
  return MICRO_BENCHMARK_DYN_ARRAY_SIZE (suite->tests);
}

micro_benchmark_test_case
micro_benchmark_suite_get_test (micro_benchmark_suite suite, size_t pos)
{
  assert (suite);
  assert (pos > 0);
  assert (pos < MICRO_BENCHMARK_DYN_ARRAY_SIZE (suite->tests));
  return &MICRO_BENCHMARK_DYN_ARRAY_VALUE (suite->tests, pos);
}

micro_benchmark_report
micro_benchmark_suite_get_report (micro_benchmark_suite suite)
{
  assert (suite);
  return &suite->report;
}

size_t
micro_benchmark_suite_run (micro_benchmark_suite suite)
{
  assert (suite);
  const size_t num_tests = MICRO_BENCHMARK_DYN_ARRAY_SIZE (suite->tests);
  if (micro_benchmark_report_get_number_of_tests (&suite->report) > 0)
    {
      MB_WARN ("micro_benchmark_suite_run was called twice on the "
               "suite %s (%p).\n", suite->name, suite);
    }
  else if (num_tests > 0)
    {
      micro_benchmark_test_case_definition *tests =
        MICRO_BENCHMARK_DYN_ARRAY_VALUES (suite->tests);
      micro_benchmark_report_allocate_tests_ (&suite->report, tests,
                                              num_tests);
      for (size_t i = 0; i < num_tests; ++i)
        {
          micro_benchmark_test_report_ *rep =
            micro_benchmark_report_get_test_report_ (&suite->report, i);
          micro_benchmark_test_case_run_ (&tests[i], rep);
        }
    }
  return micro_benchmark_report_get_number_of_tests (&suite->report);
}
