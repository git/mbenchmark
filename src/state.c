/* state.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/state.h"
#include "internal/state.h"
#include "internal/report.h"
#include "internal/test.h"
#include "internal/stats.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <limits.h>
#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif
#include <string.h>

#define MICRO_BENCHMARK_MODULE "state"

/** Default sample min iterations. */
static const size_t default_min_iterations = 2;
/** Default sample max iterations. */
static const size_t default_max_iterations = 65536;

/*  TODO: Use test_case interface.  */

static void
init_test_constraints (mbstate_constraints_ *constraints,
                       mbconstraints_ *test)
{
  MB_TRACE ("Init constraints %p from %p.\n", constraints, test);

  mbconstraints_iteration_ *ic = mbconstraints_get_iteration_ (test);
  const size_t it = mbconstraints_iterations_to_skip_ (ic);
  constraints->iterations_to_skip = it;

  if (mbconstraints_has_iterations_ (ic))
    {
      const size_t min_it = mbconstraints_min_iterations_ (ic);
      const size_t max_it = mbconstraints_max_iterations_ (ic);
      constraints->min_iterations = min_it;
      constraints->max_iterations = max_it;
    }
  else
    {
      const size_t max = SIZE_MAX / default_max_iterations;
      constraints->min_iterations = default_min_iterations;
      constraints->max_iterations = max;
    }
  MB_DEBUG ("Set [%zu, %zu] total iterations.\n",
            constraints->min_iterations, constraints->max_iterations);

  if (mbconstraints_has_sample_iterations_ (ic))
    {
      const size_t min_it = mbconstraints_min_sample_iterations_ (ic);
      const size_t max_it = mbconstraints_max_sample_iterations_ (ic);
      constraints->min_sample_iterations = min_it;
      constraints->max_sample_iterations = max_it;
    }
  else
    {
      constraints->min_sample_iterations = 1;
      constraints->max_sample_iterations = default_max_iterations;
    }

  if (constraints->min_sample_iterations > constraints->max_iterations / 2)
    constraints->min_sample_iterations = constraints->max_iterations / 2;

  if (constraints->max_sample_iterations > constraints->max_iterations)
    constraints->max_sample_iterations = constraints->max_iterations;

  MB_DEBUG ("Set [%zu, %zu] sample iterations.\n",
            constraints->min_sample_iterations,
            constraints->max_sample_iterations);
}

void
micro_benchmark_state_init_ (micro_benchmark_test_state state,
                             micro_benchmark_test_case test,
                             size_t dimensions, const size_t *sizes,
                             const char *name)
{
  MB_TRACE ("Init %p from %p and %zu dimensions.\n", state, test, dimensions);
  assert (state);
  assert (test);
  assert (name);

  state->test_name = name;
  state->name = NULL;
  state->name_size = 0;
  state->data = micro_benchmark_test_case_get_data (test);

  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (test);
  init_test_constraints (&state->constraints, c);

  state->test = test;
  state->constraints.dimensions = dimensions;
  if (dimensions > 0)
    {
      assert (sizes);
      state->constraints.sizes = xmalloc_n (sizeof (size_t), dimensions);
      MB_DEBUG ("Allocated sizes %p with %zu dimensions.\n",
                state->constraints.sizes, dimensions);
      for (size_t i = 0; i < dimensions; ++i)
        {
          MB_TRACE ("Dimension %zu: %zu\n", i, sizes[i]);
          state->constraints.sizes[i] = sizes[i];
        }
    }
  else
    state->constraints.sizes = NULL;

  micro_benchmark_stats_collector_init_ (&state->stats, c);

  mbconstraints_meter_ *mc = mbconstraints_get_meter_ (c);
  if (mbconstraints_has_calculator_ (mc))
    state->calculate_stats = mbconstraints_get_calculator_ (mc);
  else
    state->calculate_stats = micro_benchmark_get_default_time_calculator ();

  MB_DEBUG ("Using statistical calculator %p.\n", state->calculate_stats);
}

void
micro_benchmark_state_release_ (micro_benchmark_test_state state)
{
  MB_TRACE ("Release %p.\n", state);
  assert (state);
  micro_benchmark_stats_collector_release_ (&state->stats);

  if (state->constraints.dimensions > 0)
    {
      MB_DEBUG ("Releasing sizes %p.\n", state->constraints.sizes);
      xfree_n (state->constraints.sizes, sizeof (size_t),
               state->constraints.dimensions);
    }

  if (state->name_size > 0)
    xfree (state->name, state->name_size);
}

size_t
micro_benchmark_state_get_size (micro_benchmark_test_state state,
                                size_t dimension)
{
  MB_TRACE ("Retrieving on %p size from dimension %zu.\n", state, dimension);
  assert (state);
  if (state->constraints.dimensions > dimension)
    return state->constraints.sizes[dimension];
  MB_ERROR ("Dimension %zu does not exist.\n", dimension);
  return 0;
}

size_t
micro_benchmark_state_get_dimensions (micro_benchmark_test_state state)
{
  MB_TRACE ("Retrieving dimensions from %p.\n", state);
  assert (state);
  return state->constraints.dimensions;
}


void
micro_benchmark_state_set_data_ (micro_benchmark_test_state state, void *ptr)
{
  MB_TRACE ("Setting user data %p on %p.\n", ptr, state);
  assert (state);
  state->data = ptr;
}

void *
micro_benchmark_state_get_data (micro_benchmark_test_state state)
{
  MB_TRACE ("Retrieving user data from %p.\n", state);
  assert (state);
  return state->data;
}

void
micro_benchmark_state_set_name (micro_benchmark_test_state state,
                                const char *name)
{
  assert (state);

  if (state->name_size > 0)
    xfree (state->name, state->name_size);

  if (name)
    {
      state->name = xstrdup (name);
      state->name_size = strlen (name) + 1;
    }
  else
    {
      state->name = NULL;
      state->name_size = 0;
    }
}

const char *
micro_benchmark_state_get_name (micro_benchmark_test_state state)
{
  assert (state);
  if (state->name_size > 0)
    {
      assert (state->name);
      return state->name;
    }
  return state->test_name;
}

bool
micro_benchmark_state_keep_running (micro_benchmark_test_state state)
{
  assert (state);
  micro_benchmark_stats_collector_ *coll = &state->stats;
  micro_benchmark_stats_collector_stop_ (coll);
  MB_TRACE ("Check running %p.\n", state);
  bool ret =
    micro_benchmark_stats_collected_enough_ (coll, &state->constraints);
  MB_DEBUG ("Keep running %d.\n", (int) ret);
  micro_benchmark_stats_collector_restart_ (&state->stats);
  return !ret;
}

void
micro_benchmark_state_write_report_ (micro_benchmark_test_state state,
                                     micro_benchmark_exec_report_ *report)
{
  MB_TRACE ("Writing %p onto report %p.\n", state, report);
  assert (state);
  assert (report);

  if (state->name_size > 0)
    {
      const char *name = micro_benchmark_state_get_name (state);
      micro_benchmark_exec_report_set_name_ (report, name);
    }

  micro_benchmark_time_sample full =
    micro_benchmark_stats_collected_full_sample_ (&state->stats);
  micro_benchmark_exec_report_set_full_sample_ (report, full);
  MB_DEBUG ("Written full sample.\n");

  micro_benchmark_time_samples samples;
  size_t num_samples;
  micro_benchmark_stats_collected_time_samples_ (&state->stats, &samples,
                                                 &num_samples);
  micro_benchmark_exec_report_set_time_samples_ (report, num_samples,
                                                 samples);
  MB_DEBUG ("Calculating stats with %p.\n", state->calculate_stats);
  micro_benchmark_time_stats_values stats =
    state->calculate_stats (num_samples, samples);
  micro_benchmark_exec_report_set_time_stats_ (report, stats);
  MB_DEBUG ("Written time stats.\n");

  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (state->test);
  mbconstraints_meter_ *mc = mbconstraints_get_meter_ (c);
  const size_t num_meters =
    micro_benchmark_stats_collected_num_meters_ (&state->stats);
  assert (num_meters == mbconstraints_num_meters_ (mc));
  for (size_t i = 0; i < num_meters; ++i)
    {
      micro_benchmark_custom_meter cm = mbconstraints_get_meter_n_ (mc, i);
      micro_benchmark_custom_sample_collector sc = cm.collector;
      micro_benchmark_stats_generic_samples samples;
      size_t num_samples;
      micro_benchmark_stats_collected_samples_from_meter_ (&state->stats,
                                                           i, &samples,
                                                           &num_samples);
      MB_DEBUG ("Retrieved %zu samples: %p.\n", num_samples, samples);
      void *data = sc.create_data ();
      sc.parse_samples (data, num_samples, samples);
      micro_benchmark_exec_report_add_custom_ (report, data, num_samples,
                                               samples, sc);
      MB_DEBUG ("Written stats from meter %zu.\n", i);
    }
}
