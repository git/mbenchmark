/* register.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/suite.h"
#include "mbenchmark/utility.h"
#include "internal/suite.h"
#include "internal/test.h"
#include "internal/test/self-test.h"
#include "internal/utility/dynamic-array.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "registration"
#define MICRO_BENCHMARK_MODULE "suite:" MICRO_BENCHMARK_SUBMODULE

static micro_benchmark_test_case
register_test (micro_benchmark_suite suite,
               const char *name, micro_benchmark_test test)
{
  MB_INFO ("Registering test %s onto suite %s\n", name, suite->name);
  MICRO_BENCHMARK_DYN_ARRAY_PUSH (suite->tests,
                                  micro_benchmark_test_case_definition,
                                  micro_benchmark_test_case_init_,
                                  name, test);
  return &MICRO_BENCHMARK_DYN_ARRAY_LAST (suite->tests);
}

static void
register_self_test (micro_benchmark_suite suite)
{
  MB_TRACE ("Registering self test on suite %p.\n", suite);
  micro_benchmark_test_case self_test =
    register_test (suite, _("Self-Test"), micro_benchmark_self_test_ ());
  micro_benchmark_self_test_apply_constraints_ (self_test);
  suite->has_self_test = true;
}

micro_benchmark_test_case
micro_benchmark_suite_register_test (micro_benchmark_suite suite,
                                     const char *name,
                                     micro_benchmark_test test)
{
  MB_TRACE ("Registering test.\n");
  assert (suite);
  assert (name);
  assert (test);
  if (!suite->has_self_test)
    register_self_test (suite);

  return register_test (suite, name, test);
}

/* Static registry.  */
struct entry
{
  char *name;
  size_t name_size;
  bool enabled;
  micro_benchmark_test test;
  /* *INDENT-OFF* */
  MICRO_BENCHMARK_DYN_ARRAY (micro_benchmark_static_constraint) constraints;
  /* *INDENT-ON* */
};

static void
init_entry (struct entry *e, const char *name)
{
  assert (name);
  assert (e);
  e->name = xstrdup (name);
  e->name_size = strlen (name) + 1;
  e->test = NULL;
  e->enabled = false;
  MICRO_BENCHMARK_DYN_ARRAY_INIT (e->constraints,
                                  micro_benchmark_static_constraint, 1);
}

static void
cleanup_entry (struct entry *entry)
{
  assert (entry);
  xfree (entry->name, entry->name_size);
  MICRO_BENCHMARK_DYN_ARRAY_RELEASE_RAW (entry->constraints,
                                         micro_benchmark_static_constraint);
}

/* *INDENT-OFF* */
static MICRO_BENCHMARK_DYN_ARRAY (struct entry) registry;
/* *INDENT-ON* */

void
micro_benchmark_cleanup_registry_ (void)
{
  if (MICRO_BENCHMARK_DYN_ARRAY_VALUES (registry))
    MICRO_BENCHMARK_DYN_ARRAY_RELEASE (registry, struct entry, cleanup_entry);
}


static struct entry *
find_entry (const char *name)
{
  for (size_t i = 0; i < MICRO_BENCHMARK_DYN_ARRAY_SIZE (registry); ++i)
    {
      struct entry *e = &MICRO_BENCHMARK_DYN_ARRAY_VALUE (registry, i);
      if (strcmp (name, e->name) == 0)
        return e;
    }

  return NULL;
}

static struct entry *
find_or_allocate (const char *name)
{
  struct entry *val = find_entry (name);
  if (!val)
    {
      MICRO_BENCHMARK_DYN_ARRAY_PUSH (registry, struct entry, init_entry,
                                      name);
      val = &MICRO_BENCHMARK_DYN_ARRAY_LAST (registry);
    }
  return val;
}

bool
micro_benchmark_enable_static_test_ (const char *name)
{
  struct entry *e = find_entry (name);
  if (e)
    e->enabled = true;
  return e;
}

bool
micro_benchmark_disable_static_test_ (const char *name)
{
  struct entry *e = find_entry (name);
  if (e)
    e->enabled = false;
  return e;
}

void
micro_benchmark_register_static_test (const char *name,
                                      micro_benchmark_test test)
{
  if (!MICRO_BENCHMARK_DYN_ARRAY_VALUES (registry))
    MICRO_BENCHMARK_DYN_ARRAY_INIT (registry, struct entry, 1);
  struct entry *e = find_or_allocate (name);
  e->test = test;
  e->enabled = true;
}

void
micro_benchmark_register_static_constraint (const char *name,
                                            micro_benchmark_static_constraint
                                            c)
{
  struct entry *e = find_or_allocate (name);
  MICRO_BENCHMARK_DYN_ARRAY_PUSH_RAW (e->constraints,
                                      micro_benchmark_static_constraint);
  MICRO_BENCHMARK_DYN_ARRAY_LAST (e->constraints) = c;
}

void
micro_benchmark_suite_register_static_tests_ (micro_benchmark_suite suite)
{
  for (size_t i = 0; i < MICRO_BENCHMARK_DYN_ARRAY_SIZE (registry); ++i)
    {
      struct entry *e = &MICRO_BENCHMARK_DYN_ARRAY_VALUE (registry, i);
      assert (e->name);
      assert (e->test);
      micro_benchmark_test_case test =
        micro_benchmark_suite_register_test (suite, e->name, e->test);
      micro_benchmark_test_case_set_enabled (test, e->enabled);

      const size_t size = MICRO_BENCHMARK_DYN_ARRAY_SIZE (e->constraints);
      for (size_t j = 0; j < size; ++j)
        {
          micro_benchmark_static_constraint constraint =
            MICRO_BENCHMARK_DYN_ARRAY_VALUE (e->constraints, j);
          constraint (test);
        }
    }
}
