/* main.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/utility.h"

#include "mbenchmark/report/io.h"
#include "mbenchmark/suite.h"
#include "mbenchmark/test.h"
#include "internal/suite.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#error "Getopt needed to parse options"
#endif

#define MICRO_BENCHMARK_SUBMODULE "main"
#define MICRO_BENCHMARK_MODULE "utility:" MICRO_BENCHMARK_SUBMODULE

typedef struct parsed_args
{
  const char *suite_name;
  micro_benchmark_output_type output_type;
  FILE *output;
  FILE *log;
  bool brief;
  bool full;
  micro_benchmark_output_values values;
} parsed_args;

typedef enum ids
{
  enable_test_id = 1000,
  disable_test_id,
  output_type_id,
  log_level_id,
  version_id,
  help_output_id,
  self_test_id,
  sizes_id,
  total_time_id,
  total_iterations_id,
  iterations_id,
  total_samples_id,
  samples_id,
  extra_data_id,
  iteration_time_id,
  sample_time_id,
  sample_iterations_id,
} ids;

static const struct option options[] = {
  {"help", no_argument, NULL, 'h'},
  {"help-output", no_argument, NULL, help_output_id},
  {"version", no_argument, NULL, version_id},
  {"suite-name", required_argument, NULL, 's'},
  {"output", required_argument, NULL, 'o'},
  {"output-type", required_argument, NULL, output_type_id},
  {"brief", no_argument, NULL, 'b'},
  {"full", no_argument, NULL, 'f'},
  {"print-self-test", optional_argument, NULL, self_test_id},
  {"print-sizes", optional_argument, NULL, sizes_id},
  {"print-total-time", optional_argument, NULL, total_time_id},
  {"print-total-iterations", optional_argument, NULL, total_iterations_id},
  {"print-iterations", optional_argument, NULL, iterations_id},
  {"print-total-samples", optional_argument, NULL, total_samples_id},
  {"print-samples", optional_argument, NULL, samples_id},
  {"print-extra-data", optional_argument, NULL, extra_data_id},
  {"print-iteration-time", required_argument, NULL, iteration_time_id},
  {"print-sample-time", required_argument, NULL, sample_time_id},
  {"print-sample-iterations", required_argument, NULL, sample_iterations_id},
  {"log-file", required_argument, NULL, 'l'},
  {"log-level", required_argument, NULL, log_level_id},
  {"verbose", no_argument, NULL, 'v'},
  {"enable-test", required_argument, NULL, enable_test_id},
  {"disable-test", required_argument, NULL, disable_test_id}
};

static void
print_usage (const char *program_name)
{
  printf (_("Usage: %s [OPTIONS]\n"), program_name);
  exit (EXIT_FAILURE);
}

static void
print_copyright (void)
{
  puts (_("Copyright (C) 2023 Miguel Ángel Arruga Vivas"));
  printf (_("%s is free software; see the source for copying conditions.\n"
            "There is NO warranty; not even for MERCHANTABILITY or FITNESS "
            "FOR A\nPARTICULAR PURPOSE.\n\n"), PACKAGE_NAME);
}

static void
print_help (const char *program_name)
{
  printf (_("%s: MicroBenchmark executable.\n"), program_name);
  printf (_("  %s [OPTIONS]\n\n"), program_name);
  puts ("Available options:");
  printf (_("\t-h|--help\t\t%s\n"), _("Show this information"));
  printf (_("\t-v|--version\t\t%s\n"),
          _("Show the version of the program and exit"));
  printf (_("\t-s|--suite-name=NAME\t%s\n"),
          _("Use NAME for the suite instead of argv[0]"));

  puts ("\nOptions controlling the output:");
  printf (_("\t-o|--output=FILE\t%s\n"),
          _("Print the result to FILE instead of stdout"));
  printf (_("\t--output-type=TYPE\t%s\n"),
          /* TRANSLATORS: console, text and lisp are literal
             values for TYPE.  */
          _("Select the output TYPE (console, text, lisp)"));
  printf (_("\t-b|--brief\t\t%s\n"), _("Print only the main vailes"));
  printf (_("\t-f|--full\t\t%s\n"), _("Print all collected data"));
  printf (_("\t--help-output\t\t%s\n"),
          _("Print the options controlling the output values"));
  puts ("\nOptions controlling the log output:");
  printf (_("\t-l|--log-file=FILE\t%s\n"),
          _("Select the file used for logging"));
  printf (_("\t--log-level=LEVEL\t%s\n\t\t\t\t(%s)%s\n"),
          _("Logging level of the framework"),
#ifdef MICRO_BENCHMARK_BUILD_WITHOUT_TRACES
          _("debug, info, warn, error"),
#else
          _("trace, debug, info, warn, error"),
#endif
          "");
  puts ("\nOptions controlling the test execution:");
  printf (_("\t--enable-test=TEST\t%s\n"),
          _("Enable the execution of the selected TEST"));
  printf (_("\t--disable-test=TEST\t%s\n"),
          _("Disable the execution of the selected TEST"));
  printf (_("\nReport bugs to <%s>\n"), PACKAGE_BUGREPORT);
  print_copyright ();
  exit (EXIT_SUCCESS);
}

static void
print_help_output (const char *program_name)
{
  printf (_("%s: MicroBenchmark executable.\n"), program_name);
  printf (_("  %s [OPTIONS]\n\n"), program_name);
  puts ("Generic output options:");
  printf (_("\t-o|--output=FILE\t%s\n"),
          _("Print the result to FILE instead of stdout"));
  printf (_("\t--output-type=TYPE\t%s\n"),
          _("Select the output TYPE (console, text, lisp)"));
  printf (_("\t-b|--brief\t\t%s\n"), _("Print only the main vailes"));
  printf (_("\t-f|--full\t\t%s\n"), _("Print all collected data"));

  puts ("Detailed output control options:");
  printf (_("\t--print-self-test[=yes|no]\t%s\n"),
          _("Print self test results"));
  printf (_("\t--print-sizes[=yes|no]\t%s\n"), _("Print size constraints"));
  printf (_("\t--print-total-time[=yes|no]\n\t\t%s\n"),
          _("Print the total time of each execution "));
  printf (_("\t--print-total-iterations[=yes|no]\n\t\t%s\n"),
          _("Print the total iterations of each execution "));
  printf (_("\t--print-total-samples[=yes|no]\n\t\t%s\n"),
          _("Print the total number of samples taken"));
  printf (_("\t--print-samples[=yes|no]\n\t\t%s\n"),
          _("Print the samples used for statistical calculations"));
  printf (_("\t--print-iteration-time[=STAT]\t%s\n\t\t\t\t\t%s\n"),
          _("Print STAT calculated iteration time, values"),
          /* TRANSLATORS: none, mean, var, stdd, basic and all are
             literal values for STAT.  */
          _("none, mean, var, stdd, basic, all"));
  printf (_("\t--print-sample-time[=STAT]\t%s\n\t\t\t\t\t%s\n"),
          _("Print STAT calculated sample time, values"),
          _("none, mean, var, stdd, basic, all"));
  printf (_("\t--print-sample-iterations[=STAT]\t%s\n\t\t\t\t\t%s\n"),
          _("Print STAT calculated sample iterations, values"),
          _("none, mean, var, stdd, basic, all"));
  printf (_("\t--print-extra-data[=yes|no]\t%s\n"),
          _("Print extra data collected by the test"));
  printf (_("\nReport bugs to <%s>\n"), PACKAGE_BUGREPORT);
  print_copyright ();
  exit (EXIT_SUCCESS);
}

static void
print_version (const char *program_name)
{
  printf (_("%s: %s %s\n\n"), program_name, PACKAGE_NAME, PACKAGE_VERSION);
  print_copyright ();
  exit (EXIT_SUCCESS);
}

static void
parse_log_level (const char *level)
{
  if (strcmp (level, "error") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_ERROR_LEVEL);
  else if (strcmp (level, "warn") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_WARNING_LEVEL);
  else if (strcmp (level, "info") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_INFO_LEVEL);
  else if (strcmp (level, "debug") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_DEBUG_LEVEL);
#ifndef MICRO_BENCHMARK_BUILD_WITHOUT_TRACES
  else if (strcmp (level, "trace") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_TRACE_LEVEL);
#endif
  else
    {
      /* TODO: modules.  */
      fprintf (stderr, _("Error: Unknown log level %s\n"), level);
      exit (EXIT_FAILURE);
    }
}

static FILE *
open_output (const char *file)
{
  FILE *ret = fopen (file, "w");
  if (!ret)
    {
      fprintf (stderr, _("Error: Unable to open file %s\n"), file);
      exit (EXIT_FAILURE);
    }
  return ret;
}

static micro_benchmark_output_type
parse_output_type (const char *type)
{
  if (strcmp (type, "console") == 0)
    return MICRO_BENCHMARK_CONSOLE_OUTPUT;
  if (strcmp (type, "lisp") == 0)
    return MICRO_BENCHMARK_LISP_OUTPUT;
  if (strcmp (type, "text") == 0)
    return MICRO_BENCHMARK_TEXT_OUTPUT;

  fprintf (stderr, _("Error: Unknown output type %s\n"), type);
  exit (EXIT_FAILURE);
}

static bool
parse_yes_no_arg (const char *arg)
{
  if (!arg || strcmp (arg, "yes") == 0 || strcmp (arg, "y") == 0)
    return true;
  if (strcmp (arg, "no") == 0 || strcmp (arg, "n") == 0)
    return false;

  fprintf (stderr, _("Error: yes/no parameter %s is not valid\n"), arg);
  exit (EXIT_FAILURE);
}

static micro_benchmark_output_stat
parse_stat_arg (const char *arg)
{
  if (!arg || strcmp (arg, "basic") == 0 || strcmp (arg, "b") == 0)
    return MICRO_BENCHMARK_STAT_BASIC;
  if (strcmp (arg, "all") == 0 || strcmp (arg, "a") == 0)
    return MICRO_BENCHMARK_STAT_ALL;
  if (strcmp (arg, "stdd") == 0 || strcmp (arg, "s") == 0)
    return MICRO_BENCHMARK_STAT_STD_DEVIATION;
  if (strcmp (arg, "var") == 0 || strcmp (arg, "v") == 0)
    return MICRO_BENCHMARK_STAT_VARIANCE;
  if (strcmp (arg, "mean") == 0 || strcmp (arg, "m") == 0)
    return MICRO_BENCHMARK_STAT_MEAN;
  if (strcmp (arg, "none") == 0 || strcmp (arg, "n") == 0)
    return MICRO_BENCHMARK_STAT_NONE;

  fprintf (stderr, _("Error: stat parameter %s is not valid\n"), arg);
  exit (EXIT_FAILURE);
}

static void
set_brief_values (micro_benchmark_output_values *values)
{
  values->self_test = false;
  values->size_constraints = false;
  values->total_time = false;
  values->total_iterations = false;
  values->iterations = true;
  values->total_samples = false;
  values->samples = false;
  values->extra_data = false;
  values->iteration_time = MICRO_BENCHMARK_STAT_MEAN;
  values->sample_time = MICRO_BENCHMARK_STAT_NONE;
  values->sample_iterations = MICRO_BENCHMARK_STAT_NONE;
}

static void
set_full_values (micro_benchmark_output_values *values)
{
  values->self_test = true;
  values->size_constraints = true;
  values->total_time = true;
  values->total_iterations = true;
  values->iterations = true;
  values->total_samples = true;
  values->samples = true;
  values->extra_data = true;
  values->iteration_time = MICRO_BENCHMARK_STAT_ALL;
  values->sample_time = MICRO_BENCHMARK_STAT_ALL;
  values->sample_iterations = MICRO_BENCHMARK_STAT_ALL;
}

static parsed_args
parse_args (int argc, char **argv)
{
  parsed_args ret = { 0 };
  ret.values = micro_benchmark_get_default_output_values ();
  ret.suite_name = argv[0];
  ret.output_type = MICRO_BENCHMARK_CONSOLE_OUTPUT;
  int opt;
  do
    {
      int index;
      opt = getopt_long (argc, argv, "hvo:l:s:b", options, &index);
      switch (opt)
        {
        default:
          break;

        case '?':
          print_usage (argv[0]);
          break;
        case 'h':
          print_help (argv[0]);
          break;
        case 'v':
          micro_benchmark_set_log_level (MICRO_BENCHMARK_INFO_LEVEL);
          break;
        case 's':
          ret.suite_name = optarg;
          break;
        case 'o':
          ret.output = open_output (optarg);
          break;
        case 'l':
          ret.log = open_output (optarg);
          micro_benchmark_log_set_output (ret.log);
          break;
        case 'b':
          ret.brief = true;
          set_brief_values (&ret.values);
          break;
        case 'f':
          ret.full = true;
          set_full_values (&ret.values);
          break;

        case enable_test_id:
          MB_INFO ("Enabling test %s\n", optarg);
          if (!micro_benchmark_enable_static_test_ (optarg))
            {
              MB_ERROR ("Test %s not found.\n", optarg);
              exit (EXIT_FAILURE);
            }
          break;
        case disable_test_id:
          MB_INFO ("Disabling test %s\n", optarg);
          if (!micro_benchmark_disable_static_test_ (optarg))
            {
              MB_ERROR ("Test %s not found.\n", optarg);
              exit (EXIT_FAILURE);
            }
          break;

        case self_test_id:
          ret.values.self_test = parse_yes_no_arg (optarg);
          break;
        case sizes_id:
          ret.values.size_constraints = parse_yes_no_arg (optarg);
          break;
        case total_time_id:
          ret.values.total_time = parse_yes_no_arg (optarg);
          break;
        case total_iterations_id:
          ret.values.total_iterations = parse_yes_no_arg (optarg);
          break;
        case iterations_id:
          ret.values.iterations = parse_yes_no_arg (optarg);
          break;
        case total_samples_id:
          ret.values.total_samples = parse_yes_no_arg (optarg);
          break;
        case samples_id:
          ret.values.samples = parse_yes_no_arg (optarg);
          break;
        case extra_data_id:
          ret.values.extra_data = parse_yes_no_arg (optarg);
          break;
        case iteration_time_id:
          ret.values.iteration_time = parse_stat_arg (optarg);
          break;
        case sample_time_id:
          ret.values.sample_time = parse_stat_arg (optarg);
          break;
        case sample_iterations_id:
          ret.values.sample_iterations = parse_stat_arg (optarg);
          break;

        case output_type_id:
          ret.output_type = parse_output_type (optarg);
          break;
        case log_level_id:
          parse_log_level (optarg);
          break;

        case help_output_id:
          print_help_output (argv[0]);
          break;
        case version_id:
          print_version (argv[0]);
          break;
        }
    }
  while (opt != -1);

  if (ret.full && ret.brief)
    {
      fprintf (stderr, "Incompatible options --full and --brief provided\n");
      exit (EXIT_FAILURE);
    }
  return ret;
}

int
micro_benchmark_main (int argc, char **argv)
{
#if ENABLE_NLS
  setlocale (LC_ALL, "");
#endif

  assert (argc > 0);
  assert (argv);
  parsed_args args = parse_args (argc, argv);
  MB_INFO ("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
  micro_benchmark_suite suite =
    micro_benchmark_suite_create (args.suite_name);
  MB_DEBUG ("Creating suite %s...\n", args.suite_name);

  MB_DEBUG ("Registering static tests...\n");
  micro_benchmark_suite_register_static_tests_ (suite);

  MB_DEBUG ("Configuring output values...\n");

  MB_INFO ("Running suite %s...\n", args.suite_name);
  micro_benchmark_suite_run (suite);

  MB_INFO ("Writing suite %s report...\n", args.suite_name);
  micro_benchmark_report report = micro_benchmark_suite_get_report (suite);

  if (args.output)
    {
      MB_DEBUG ("Writing report to %p\n", args.output);
      micro_benchmark_write_custom_report (report, args.output,
                                           args.output_type, &args.values);
    }
  else if (args.output_type != MICRO_BENCHMARK_CONSOLE_OUTPUT)
    {
      MB_DEBUG ("Writing report with format %d\n", (int) args.output_type);
      micro_benchmark_write_custom_report (report, stdout,
                                           args.output_type, &args.values);
    }
  else
    {
      MB_DEBUG ("Pinting report to standard output\n");
      micro_benchmark_print_custom_report (report, &args.values);
    }

  micro_benchmark_suite_release (suite);
  MB_DEBUG ("Released suite %s.\n", args.suite_name);

  if (args.output)
    fclose (args.output);

  if (args.log)
    {
      MB_DEBUG ("Closing log file.\n");
      micro_benchmark_log_set_output (NULL);
      fclose (args.log);
    }

  return EXIT_SUCCESS;
}
