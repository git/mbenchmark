/* suite.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/report/suite.h"

#include "internal/report.h"
#include "internal/test.h"
#include "internal/report/test.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "suite"
#define MICRO_BENCHMARK_MODULE "report:" MICRO_BENCHMARK_SUBMODULE

const char *
micro_benchmark_report_get_name (micro_benchmark_report report)
{
  MB_TRACE ("Retrieving name of %p.\n", report);
  assert (report);
  return report->name;
}

size_t
micro_benchmark_report_get_number_of_tests (micro_benchmark_report report)
{
  MB_TRACE ("Retrieving number of tests of %p.\n", report);
  assert (report);
  return report->num_tests;
}

micro_benchmark_test_report_ *
micro_benchmark_report_get_test_report_ (micro_benchmark_report_ *report,
                                         size_t pos)
{
  MB_TRACE ("Retrieving writable test report %zu of %p.\n", pos, report);
  assert (report);
  assert (pos < report->num_tests);
  return &report->tests[pos];
}

micro_benchmark_test_report
micro_benchmark_report_get_test_report (micro_benchmark_report report,
                                        size_t pos)
{
  MB_TRACE ("Retrieving test report %zu of %p.\n", pos, report);
  assert (report);
  assert (pos < report->num_tests);
  return &report->tests[pos];
}

void
micro_benchmark_report_init_ (micro_benchmark_report_ *report,
                              const char *name)
{
  MB_TRACE ("Init %p.\n", report);
  assert (report);
  assert (name);
  report->name = name;
  report->num_tests = 0;
  report->tests = NULL;
}

void
micro_benchmark_report_cleanup_ (micro_benchmark_report report)
{
  MB_TRACE ("Release %p.\n", report);
  assert (report);
  if (report->num_tests > 0)
    {
      for (size_t i = 0; i < report->num_tests; ++i)
        micro_benchmark_test_report_cleanup_ (&report->tests[i]);
      xfree_n (report->tests, sizeof (micro_benchmark_test_report_),
               report->num_tests);
    }
}

void
micro_benchmark_report_allocate_tests_ (micro_benchmark_report_ *report,
                                        micro_benchmark_test_case tests,
                                        size_t n_tests)
{
  MB_TRACE ("Allocating %zu tests (%p) on %p.\n", n_tests, tests, report);
  assert (report);
  assert (!report->tests);
  assert (tests);
  assert (n_tests > 0);
  assert (report->num_tests == 0);

  report->num_tests = n_tests;
  report->tests = xmalloc_n (sizeof (micro_benchmark_test_report_), n_tests);
  for (size_t i = 0; i < n_tests; ++i)
    micro_benchmark_test_report_init_ (&report->tests[i], &tests[i]);
}
