/* exec.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/report/exec.h"

#include "internal/report.h"
#include "internal/report/exec.h"
#include "internal/state.h"
#include "internal/utility/error.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "exec"
#define MICRO_BENCHMARK_MODULE "report:" MICRO_BENCHMARK_SUBMODULE

typedef micro_benchmark_exec_report_ report;

static char *
format_name (const char *name, size_t dimensions, const size_t *sizes)
{
  const int buffer_size = strlen (name) + 20 * dimensions + 1;
  char *buffer = xmalloc (buffer_size);
  int ret = snprintf (buffer, buffer_size, "%s", name);
  MB_HANDLE_ERROR (ret < 0, "Error formating name");
  if (dimensions > 0)
    {
      char *curr = buffer + ret;
      int size = buffer_size - ret;
      ret = snprintf (curr, size, "/%zu", sizes[0]);
      MB_HANDLE_ERROR (ret < 0, "Error formatting name");
      for (size_t i = 1; i < dimensions; ++i)
        {
          curr += ret;
          size -= ret;
          MB_HANDLE_ERROR (size <= 0, "Error formatting name");
          ret = snprintf (curr, size, "/%zu", sizes[i]);
          MB_HANDLE_ERROR (ret < 0, "Error formatting name");
        }
    }
  return buffer;
}

void
micro_benchmark_exec_report_init_ (report *r, const char *name,
                                   size_t dimensions, size_t *sizes)
{
  static const micro_benchmark_time_stats_values zero_stats;
  static const struct micro_benchmark_time_sample_ zero_sample;
  MB_TRACE ("Init %p with sizes %p\n", r, sizes);
  assert (r);
  assert (name);

  r->dimensions = dimensions;
  r->name = format_name (name, dimensions, sizes);
  r->name_size = strlen (r->name) + 1;
  MB_DEBUG ("Name \"%s\", test \"%s\", dimensions %zu\n",
            r->name, name, dimensions);

  r->sizes = sizes;
  r->full_sample = zero_sample;
  r->num_time_samples = 0;
  r->time_samples = NULL;
  r->time_stats = zero_stats;
  r->num_custom_meters = 0;
  r->custom_meters = NULL;
  MB_TRACE ("Init %p done\n", r);
}

void
micro_benchmark_exec_report_release_ (const report *r)
{
  MB_TRACE ("Releasing %p\n", r);
  assert (r);

  if (r->time_samples)
    xfree_n (r->time_samples, sizeof (*r->time_samples), r->num_time_samples);

  if (r->custom_meters)
    {
      MB_DEBUG ("Releasing custom meters %p\n", r->custom_meters);
      for (size_t i = 0; i < r->num_custom_meters; ++i)
        {
          micro_benchmark_exec_report_extra_data_ *d = &r->custom_meters[i];
          MB_TRACE ("Releasing %p\n", d->data);
          assert (d->collector.release_data);
          d->collector.release_data (d->data);
          if (d->samples)
            xfree_n (d->samples, sizeof (*d->samples), d->num_samples);
        }
      xfree_n (r->custom_meters, sizeof (*r->custom_meters),
               r->num_custom_meters);
    }

  if (r->dimensions > 0)
    {
      MB_DEBUG ("Releasing sizes %p\n", r->sizes);
      xfree_n (r->sizes, sizeof (size_t), r->dimensions);
    }

  xfree (r->name, r->name_size);
  MB_TRACE ("Released %p\n", r);
}

void
micro_benchmark_exec_report_set_name_ (micro_benchmark_exec_report_ *report,
                                       const char *name)
{
  assert (report);
  assert (name);
  xfree (report->name, report->name_size);
  report->name = xstrdup (name);
  report->name_size = strlen (name) + 1;
}

const char *
micro_benchmark_exec_report_get_name (const report *r)
{
  MB_TRACE ("Getting full name of %p\n", r);
  assert (r);
  return r->name;
}

void
micro_benchmark_exec_report_get_sizes (const report *r,
                                       const size_t **sizes,
                                       size_t *dimensions)
{
  MB_TRACE ("Getting sizes of %p on sizes %p, dimensions %p\n", r,
            sizes, dimensions);
  assert (r);
  assert (sizes);
  assert (dimensions);
  *sizes = r->sizes;
  *dimensions = r->dimensions;
}

size_t
micro_benchmark_exec_report_num_time_samples (const report *r)
{
  MB_TRACE ("Getting number of time samples on report %p\n", r);
  assert (r);
  return r->num_time_samples;
}

micro_benchmark_time_sample
micro_benchmark_exec_report_get_time_sample (const report *r, size_t pos)
{
  MB_TRACE ("Getting number of time samples on report %p\n", r);
  assert (r);
  assert (pos < r->num_time_samples);
  return &r->time_samples[pos];
}

void
micro_benchmark_exec_report_set_time_samples_ (report *r, size_t num_samples,
                                               micro_benchmark_time_samples s)
{
  MB_TRACE ("Setting time samples on report %p\n", r);
  assert (r);
  assert (s);
  static const size_t size = sizeof (struct micro_benchmark_time_sample_);
  if (r->time_samples)
    xfree_n (r->time_samples, size, r->num_time_samples);
  r->num_time_samples = num_samples;
  if (num_samples > 0)
    {
      r->time_samples = xmalloc_n (size, num_samples);
      memcpy (r->time_samples, s, num_samples * size);
    }
  else
    r->time_samples = NULL;
}

void
micro_benchmark_exec_report_set_time_stats_ (report *r,
                                             micro_benchmark_time_stats_values
                                             ts)
{
  MB_TRACE ("Setting time stats of %p\n", r);
  assert (r);
  r->time_stats = ts;
}

void
micro_benchmark_exec_report_set_full_sample_ (report *r,
                                              micro_benchmark_time_sample s)
{
  MB_TRACE ("Setting full sample of %p\n", r);
  assert (r);
  r->full_sample = *s;
}

size_t
micro_benchmark_exec_report_get_iterations (const report *r)
{
  MB_TRACE ("Getting iterations of %p\n", r);
  assert (r);
  return r->time_stats.iterations;
}

size_t
micro_benchmark_exec_report_total_iterations (const report *r)
{
  MB_TRACE ("Getting total iterations of %p\n", r);
  assert (r);
  return r->full_sample.iterations;
}

micro_benchmark_clock_time
micro_benchmark_exec_report_total_time (const report *r)
{
  MB_TRACE ("Getting total time of %p\n", r);
  assert (r);
  return r->full_sample.elapsed;
}

size_t
micro_benchmark_exec_report_total_samples (const report *r)
{
  MB_TRACE ("Getting total samples of %p\n", r);
  assert (r);
  return r->time_stats.total_samples;
}

size_t
micro_benchmark_exec_report_used_samples (const report *r)
{
  MB_TRACE ("Getting used samples of %p\n", r);
  assert (r);
  return r->time_stats.samples;
}

micro_benchmark_stats_value
micro_benchmark_exec_report_iteration_time (const report *r)
{
  MB_TRACE ("Getting iteration time of %p\n", r);
  assert (r);
  return r->time_stats.iteration_time;
}

micro_benchmark_stats_value
micro_benchmark_exec_report_sample_time (const report *r)
{
  MB_TRACE ("Getting sample time of %p\n", r);
  assert (r);
  return r->time_stats.sample_time;
}

micro_benchmark_stats_value
micro_benchmark_exec_report_sample_iterations (const report *r)
{
  MB_TRACE ("Getting sample iterations of %p\n", r);
  assert (r);
  return r->time_stats.sample_iterations;
}

void
micro_benchmark_exec_report_add_custom_ (report *r, void *data, size_t num,
                                         micro_benchmark_stats_generic_samples
                                         samples,
                                         micro_benchmark_custom_sample_collector
                                         collector)
{
  MB_TRACE ("Adding custom data %p on %p\n", data, r);
  assert (r);
  size_t pos = r->num_custom_meters++;
  r->custom_meters =
    xrealloc_n (r->custom_meters,
                sizeof (micro_benchmark_exec_report_extra_data_),
                r->num_custom_meters);
  r->custom_meters[pos].data = data;
  r->custom_meters[pos].num_samples = num;
  static const size_t size =
    sizeof (micro_benchmark_stats_generic_sample_data);
  if (num > 0)
    {
      r->custom_meters[pos].samples = xmalloc_n (size, num);
      memcpy (r->custom_meters[pos].samples, samples, size * num);
    }
  else
    r->custom_meters[pos].samples = NULL;
  r->custom_meters[pos].collector = collector;
}

size_t
micro_benchmark_exec_report_number_of_meters (const report *r)
{
  MB_TRACE ("Getting number of meters of %p\n", r);
  assert (r);
  return r->num_custom_meters;
}

size_t
micro_benchmark_exec_report_meter_num_samples (const report *r, size_t meter)
{
  MB_TRACE ("Getting number of samples for meter %zu of %p\n", meter, r);
  assert (r);
  assert (meter < r->num_custom_meters);
  return r->custom_meters[meter].num_samples;
}

micro_benchmark_stats_generic_sample
micro_benchmark_exec_report_meter_get_sample (const report *r,
                                              size_t meter, size_t pos)
{
  MB_TRACE ("Getting samples for meter %zu of %p\n", meter, r);
  assert (r);
  assert (meter < r->num_custom_meters);
  assert (pos < r->custom_meters[meter].num_samples);
  return &r->custom_meters[meter].samples[pos];
}

micro_benchmark_stats_sample_type
micro_benchmark_exec_report_meter_sample_type (const report *r, size_t meter)
{
  MB_TRACE ("Getting sample type for meter %zu of %p\n", meter, r);
  assert (r);
  assert (meter < r->num_custom_meters);
  assert (r->custom_meters[meter].collector.sample_type);
  return r->custom_meters[meter].collector.sample_type ();
}


micro_benchmark_report_extra_data
micro_benchmark_exec_report_get_extra_data (const report *r, size_t meter)
{
  MB_TRACE ("Getting extra data %zu of %p\n", meter, r);
  assert (r);
  assert (meter < r->num_custom_meters);
  assert (r->custom_meters[meter].collector.get_data);
  void *ptr = r->custom_meters[meter].data;
  return r->custom_meters[meter].collector.get_data (ptr);
}
