/* test.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/report/test.h"

#include "internal/report/exec.h"
#include "internal/report/test.h"
#include "internal/test.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "test"
#define MICRO_BENCHMARK_MODULE "report:" MICRO_BENCHMARK_SUBMODULE

typedef micro_benchmark_test_report_ report;

void
micro_benchmark_test_report_init_ (report *r, micro_benchmark_test_case test)
{
  MB_TRACE ("Init %p with test case %p.\n", r, test);
  assert (r);
  assert (test);
  r->test = test;
  r->num_executions = 0;
  r->executions = NULL;
}

void
micro_benchmark_test_report_cleanup_ (report *r)
{
  MB_TRACE ("Release %p.\n", r);
  assert (r);

  if (r->num_executions > 0)
    {
      for (size_t i = 0; i < r->num_executions; ++i)
        micro_benchmark_exec_report_release_ (&r->executions[i]);
      xfree_n (r->executions, sizeof (*r->executions), r->num_executions);
      r->num_executions = 0;
    }
  r->executions = NULL;
  MB_TRACE ("Released %p.\n", r);
}

void
micro_benchmark_test_report_allocate_executions_ (report *r,
                                                  size_t n_executions)
{
  MB_TRACE ("Allocation executions on %p.\n", r);
  assert (r);
  assert (!r->executions);
  assert (n_executions > 0);

  MB_DEBUG ("Allocating %zu executions on %p.\n", n_executions, r);
  r->num_executions = n_executions;
  r->executions = xmalloc_n (sizeof (*r->executions), n_executions);
  MB_DEBUG ("Allocated executions pointer %p.\n", r->executions);
}

size_t
micro_benchmark_test_report_get_num_executions (const report *r)
{
  MB_TRACE ("Retrieving number of executions from %p.\n", r);
  assert (r);
  return r->num_executions;
}

micro_benchmark_exec_report
micro_benchmark_test_report_get_exec_report (const report *r, size_t pos)
{
  MB_TRACE ("Retrieving execution report %zu from %p.\n", pos, r);
  assert (r);
  assert (pos < r->num_executions);
  return &r->executions[pos];
}

const char *
micro_benchmark_test_report_get_name (const report *r)
{
  MB_TRACE ("Retrieving test name from %p.\n", r);
  assert (r);
  return r->test->name;
}
