/* io.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/report/io.h"

#include "internal/output/console.h"
#include "internal/output/lisp.h"
#include "internal/output/text.h"
#include "internal/utility/error.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "io"
#define MICRO_BENCHMARK_MODULE "report:" MICRO_BENCHMARK_SUBMODULE

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_SELF_TEST
#define MICRO_BENCHMARK_DEFAULT_PRINT_SELF_TEST true
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_SIZE
#define MICRO_BENCHMARK_DEFAULT_PRINT_SIZE false
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_TIME
#define MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_TIME true
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_ITERATIONS
#define MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_ITERATIONS false
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_ITERATIONS
#define MICRO_BENCHMARK_DEFAULT_PRINT_ITERATIONS true
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_SAMPLES
#define MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_SAMPLES false
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLES
#define MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLES false
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_EXTRA
#define MICRO_BENCHMARK_DEFAULT_PRINT_EXTRA false
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_ITERATION_TIMES
#define MICRO_BENCHMARK_DEFAULT_PRINT_ITERATION_TIMES MICRO_BENCHMARK_STAT_BASIC
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLE_TIMES
#define MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLE_TIMES MICRO_BENCHMARK_STAT_NONE
#endif

#ifndef MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLE_ITERATIONS
#define MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLE_ITERATIONS MICRO_BENCHMARK_STAT_NONE
#endif

static micro_benchmark_output_values default_values = {
  MICRO_BENCHMARK_DEFAULT_PRINT_SELF_TEST,
  MICRO_BENCHMARK_DEFAULT_PRINT_SIZE,
  MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_TIME,
  MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_ITERATIONS,
  MICRO_BENCHMARK_DEFAULT_PRINT_ITERATIONS,
  MICRO_BENCHMARK_DEFAULT_PRINT_TOTAL_SAMPLES,
  MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLES,
  MICRO_BENCHMARK_DEFAULT_PRINT_EXTRA,
  MICRO_BENCHMARK_DEFAULT_PRINT_ITERATION_TIMES,
  MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLE_TIMES,
  MICRO_BENCHMARK_DEFAULT_PRINT_SAMPLE_ITERATIONS
};

micro_benchmark_output_values
micro_benchmark_get_default_output_values (void)
{
  return default_values;
}

void
micro_benchmark_set_default_output_values (micro_benchmark_output_values val)
{
  default_values = val;
}

void
micro_benchmark_print_report (micro_benchmark_report report)
{
  micro_benchmark_print_custom_report (report, &default_values);
}

void
micro_benchmark_print_custom_report (micro_benchmark_report report,
                                     const micro_benchmark_output_values *v)
{
  micro_benchmark_write_custom_report (report, stdout,
                                       MICRO_BENCHMARK_CONSOLE_OUTPUT, v);
}

void
micro_benchmark_write_report (micro_benchmark_report report,
                              FILE *out, micro_benchmark_output_type type)
{
  assert (report);
  assert (out);
  micro_benchmark_write_custom_report (report, out, type, &default_values);
}

void
micro_benchmark_write_custom_report (micro_benchmark_report report,
                                     FILE *out,
                                     micro_benchmark_output_type type,
                                     const micro_benchmark_output_values *v)
{
  MB_TRACE ("Writing report %p type %d\n", report, (int) type);
  assert (report);
  assert (out);

  switch (type)
    {
    default:
      MB_WARN ("Unknown output type, using lisp format\n");
      /*  Fallthrough  */
    case MICRO_BENCHMARK_LISP_OUTPUT:
      micro_benchmark_report_write_lisp_ (report, out, v);
      break;

    case MICRO_BENCHMARK_TEXT_OUTPUT:
      micro_benchmark_report_write_text_ (report, out, v);
      break;

    case MICRO_BENCHMARK_CONSOLE_OUTPUT:
      micro_benchmark_report_write_console_ (report, out, v);
      break;
    }

  int ret = fflush (out);
  MB_HANDLE_ERROR (ret != 0, "Error writing report");
}
