/* utility.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/utility.h"
#include "internal/suite.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"

#include <stdlib.h>
#include <string.h>

#define MICRO_BENCHMARK_MODULE "utility"

void
micro_benchmark_do_not_optimize (const void *dummy)
{
  (void) dummy;
#ifndef __GNUC__
#error "Unimplemented compiler barrier for this compiler"
#endif
  MICRO_BENCHMARK_COMPILER_BARRIER ();
}

static void
set_log_level_from_env (const char *level)
{
  if (strcmp (level, "error") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_ERROR_LEVEL);
  else if (strcmp (level, "warn") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_WARNING_LEVEL);
  else if (strcmp (level, "info") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_INFO_LEVEL);
  else if (strcmp (level, "debug") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_DEBUG_LEVEL);
#ifndef MICRO_BENCHMARK_BUILD_WITHOUT_TRACES
  else if (strcmp (level, "trace") == 0)
    micro_benchmark_set_log_level (MICRO_BENCHMARK_TRACE_LEVEL);
#endif
  else
    MB_WARN ("Unknown log level %s\n", level);
}

/* *INDENT-OFF* */
MICRO_BENCHMARK_CONSTRUCTOR (101) /* *INDENT-ON* */
void
micro_benchmark_init (void)
{
#if ENABLE_NLS
  bindtextdomain (PACKAGE, LOCALEDIR);
#endif
  micro_benchmark_log_set_output (NULL);
  const char *level = getenv ("MICRO_BENCHMARK_LOG_LEVEL");
  if (level)
    set_log_level_from_env (level);
}

/* *INDENT-OFF* */
MICRO_BENCHMARK_DESTRUCTOR (101) /* *INDENT-ON* */
void
micro_benchmark_cleanup (void)
{
  /* Avoid leaking memory.  */
  micro_benchmark_cleanup_registry_ ();
}
