/* self-test.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "internal/test/self-test.h"

#include "mbenchmark/utility.h"
#include "mbenchmark/test/definition.h"
#include "internal/utility/log.h"
#include "internal/utility/intl.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>

#define MICRO_BENCHMARK_SUBMODULE "self-test"
#define MICRO_BENCHMARK_MODULE "test:" MICRO_BENCHMARK_SUBMODULE

#ifndef MICRO_BENCHMARK_SELF_TEST_SKIP_ITERATIONS
#define MICRO_BENCHMARK_SELF_TEST_SKIP_ITERATIONS 0
#endif

#ifndef MICRO_BENCHMARK_SELF_TEST_MAX_TIME_S
#define MICRO_BENCHMARK_SELF_TEST_MAX_TIME_S 0
#endif

#ifndef MICRO_BENCHMARK_SELF_TEST_MAX_TIME_NS
#define MICRO_BENCHMARK_SELF_TEST_MAX_TIME_NS 200000000
#endif


#ifndef MICRO_BENCHMARK_SELF_TEST_MAX_ITERATIONS
#define MICRO_BENCHMARK_SELF_TEST_MAX_ITERATIONS 1024 * 64
#endif

static void
test_empty (void)
{
}

static void
test_barrier (void *ptr)
{
  MICRO_BENCHMARK_DO_NOT_OPTIMIZE (ptr);
}

static volatile int a = 1;
static volatile int b = 2;
static volatile int c;
static const size_t reps = 16;

static void
test_read_write (void)
{
  for (size_t i = 0; i < reps; ++i)
    c = a;
}

static void
test_addition (void)
{
  for (size_t i = 0; i < reps; ++i)
    c = a + b;
}

static void
test_multiplication (void)
{
  for (size_t i = 0; i < reps; ++i)
    c = a * b;
}

typedef enum self_test_type
{
  EMPTY,
  BARRIER,
  READ_WRITE,
  ADDITION,
  MULTIPLICATION,
  WARMED_UP
} self_test_type;

/* TODO: Enhance test.  */
static void
self_test (micro_benchmark_test_state state)
{
  assert (state);
  self_test_type type =
    (self_test_type) micro_benchmark_state_get_size (state, 0);
  switch (type)
    {
    default:
      micro_benchmark_state_set_name (state, "empty");
      while (micro_benchmark_state_keep_running (state))
        test_empty ();
      break;

    case BARRIER:
      micro_benchmark_state_set_name (state, "barrier");
      while (micro_benchmark_state_keep_running (state))
        {
          MICRO_BENCHMARK_COMPILER_BARRIER ();
          test_barrier (&type);
        }
      break;

    case READ_WRITE:
      micro_benchmark_state_set_name (state, "read+write");
      while (micro_benchmark_state_keep_running (state))
        test_read_write ();
      break;

    case ADDITION:
      micro_benchmark_state_set_name (state, "addition");
      while (micro_benchmark_state_keep_running (state))
        test_addition ();
      break;

    case MULTIPLICATION:
      micro_benchmark_state_set_name (state, "multiplication");
      while (micro_benchmark_state_keep_running (state))
        test_multiplication ();
      break;
    }
}

static const struct micro_benchmark_test_definition def = {
  false,
  NULL,
  NULL,
  NULL,
  self_test
};

micro_benchmark_test
micro_benchmark_self_test_ (void)
{
  return &def;
}

void
micro_benchmark_self_test_apply_constraints_ (micro_benchmark_test_case tc)
{
  assert (tc);
  const size_t si = MICRO_BENCHMARK_SELF_TEST_SKIP_ITERATIONS;
  micro_benchmark_test_case_skip_iterations (tc, si);
  micro_benchmark_clock_time d = {
    MICRO_BENCHMARK_SELF_TEST_MAX_TIME_S,
    MICRO_BENCHMARK_SELF_TEST_MAX_TIME_NS
  };
  micro_benchmark_test_case_set_max_time (tc, d);
  const size_t variations[] = {
    EMPTY,
    BARRIER,
    READ_WRITE,
    ADDITION,
    MULTIPLICATION,
    WARMED_UP
  };
  const size_t num_variations = sizeof (variations) / sizeof (*variations);
  micro_benchmark_test_case_add_dimension (tc, num_variations, variations);

  const size_t max_it = MICRO_BENCHMARK_SELF_TEST_MAX_ITERATIONS;
  micro_benchmark_test_case_limit_iterations (tc, 0, max_it);
}
