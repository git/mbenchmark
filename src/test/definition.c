/* definition.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/test/definition.h"
#include "mbenchmark/state.h"

#include "internal/test/definition.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>

#define MICRO_BENCHMARK_SUBMODULE "definition"
#define MICRO_BENCHMARK_MODULE "test:" MICRO_BENCHMARK_SUBMODULE

static void *
default_setup (micro_benchmark_test_state s)
{
  MB_TRACE ("State %p\n", s);
  assert (s);
  const char *name = micro_benchmark_state_get_name (s);
  assert (name);
  void *data = micro_benchmark_state_get_data (s);
  MB_DEBUG ("Running test %s with data %p.\n", name, data);
  return data;
}

static void
default_teardown (micro_benchmark_test_state s, void *ptr)
{
  MB_TRACE ("State %p, ptr %p\n", s, ptr);
  (void) ptr;
  assert (s);
  const char *name = micro_benchmark_state_get_name (s);
  assert (name);
  MB_DEBUG ("End of %s.\n", name);
}

void
micro_benchmark_test_copy_ (micro_benchmark_test_definition *lvalue,
                            const micro_benchmark_test_definition *rvalue)
{
  lvalue->is_auto = rvalue->is_auto;
  lvalue->set_up = rvalue->set_up ? rvalue->set_up : default_setup;
  lvalue->tear_down =
    rvalue->tear_down ? rvalue->tear_down : default_teardown;
  if (lvalue->is_auto)
    {
      lvalue->auto_test = rvalue->auto_test;
      lvalue->test = NULL;
    }
  else
    {
      lvalue->test = rvalue->test;
      lvalue->auto_test = NULL;
    }
}
