/* case.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/test/case.h"

#include "internal/constraints.h"
#include "internal/report.h"
#include "internal/test.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

#define MICRO_BENCHMARK_SUBMODULE "case"
#define MICRO_BENCHMARK_MODULE "test:" MICRO_BENCHMARK_SUBMODULE

void
micro_benchmark_test_case_init_ (micro_benchmark_test_case test,
                                 const char *name,
                                 micro_benchmark_test definition)
{
  MB_TRACE ("Init %p with definition %p\n", test, definition);
  assert (test);
  assert (name);
  assert (definition);
  test->name_size = strlen (name) + 1;
  test->name = xstrdup (name);
  test->enabled = true;
  micro_benchmark_test_copy_ (&test->definition, definition);
  mbconstraints_init_ (&test->constraints);
  test->user_data = NULL;
  test->user_data_release = NULL;
  MB_DEBUG ("Created test case \"%s\"\n", test->name);
}

void
micro_benchmark_test_case_cleanup_ (micro_benchmark_test_case test)
{
  MB_TRACE ("Relase %p\n", test);
  assert (test);
  assert (test->name);
  mbconstraints_cleanup_ (&test->constraints);
  MB_DEBUG ("Releasing test case \"%s\"\n", test->name);
  xfree (test->name, test->name_size);
  if (test->user_data_release)
    {
      MB_DEBUG ("Releasing user data %p\n", test->user_data);
      test->user_data_release (test->user_data);
    }
}

mbconstraints_ *
micro_benchmark_test_case_constraints_ (micro_benchmark_test_case test)
{
  MB_TRACE ("Retrieving constraints from %p\n", test);
  assert (test);
  return &test->constraints;
}

const char *
micro_benchmark_test_case_get_name (micro_benchmark_test_case test)
{
  MB_TRACE ("Retrieving name from %p\n", test);
  assert (test);
  return test->name;
}

micro_benchmark_test
micro_benchmark_test_case_get_definition (micro_benchmark_test_case test)
{
  MB_TRACE ("Retrieving definition %p\n", test);
  assert (test);
  return &test->definition;
}

bool
micro_benchmark_test_case_is_enabled (micro_benchmark_test_case test)
{
  MB_TRACE ("Retrieving status of %p\n", test);
  assert (test);
  return test->enabled;
}

void
micro_benchmark_test_case_set_enabled (micro_benchmark_test_case test, bool e)
{
  MB_TRACE ("Setting enabled %p to %d\n", test, e);
  assert (test);
  test->enabled = e;
}

void
micro_benchmark_test_case_set_data (micro_benchmark_test_case test, void *ptr,
                                    void (*cleanup) (void *))
{
  MB_TRACE ("Setting data of %p to %p\n", test, ptr);
  assert (test);
  assert ((!ptr && !cleanup) || (ptr && cleanup));

  if (test->user_data_release)
    {
      MB_DEBUG ("Releasing old data %p\n", test->user_data);
      test->user_data_release (test->user_data);
    }

  test->user_data = ptr;
  test->user_data_release = cleanup;
}

void *
micro_benchmark_test_case_get_data (micro_benchmark_test_case test)
{
  MB_TRACE ("Getting data of %p\n", test);
  assert (test);
  return test->user_data;
}
