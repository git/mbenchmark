/* time.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/test/time.h"

#include "internal/constraints.h"
#include "internal/test.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "time"
#define MICRO_BENCHMARK_MODULE "test:" MICRO_BENCHMARK_SUBMODULE

micro_benchmark_clock_time
micro_benchmark_test_case_get_max_time (micro_benchmark_test_case tc)
{
  assert (tc);
  const mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  return mbconstraints_get_max_time_ (&c->time);
}

void
micro_benchmark_test_case_set_max_time (micro_benchmark_test_case tc,
                                        micro_benchmark_clock_time d)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_set_max_time_ (&c->time, d);
}
