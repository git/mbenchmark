/* runner.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/test.h"

#include "internal/constraints.h"
#include "internal/report.h"
#include "internal/state.h"
#include "internal/test.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>
#include <limits.h>

#define MICRO_BENCHMARK_SUBMODULE "runner"
#define MICRO_BENCHMARK_MODULE "test:" MICRO_BENCHMARK_SUBMODULE

/* TODO: Use test_case interface instead of constraints.  */

static size_t
calculate_runs (mbconstraints_ *tc)
{
  MB_TRACE ("Calculating runs with %p.\n", tc);
  assert (tc);
  const mbconstraints_size_ *sc = mbconstraints_get_size_ (tc);
  const size_t ndim = mbconstraints_dimensions_ (sc);
  if (ndim == 0)
    return 1;

  size_t runs = 1;
  for (size_t i = 0; i < ndim; ++i)
    {
      size_t nsizes;
      const size_t *sizes;
      mbconstraints_get_dimension_ (sc, i, &nsizes, &sizes);
      assert (sizes);
      assert (nsizes > 0);
      assert (nsizes < SIZE_MAX);
      runs *= nsizes;
    }
  return runs;
}

static size_t *
allocate_combination (mbconstraints_ *tc, size_t pos, size_t num_dimensions)
{
  MB_TRACE ("Allocating combination %p of %zu/%zu.\n", tc, pos,
            num_dimensions);
  assert (tc);
  const mbconstraints_size_ *sc = mbconstraints_get_size_ (tc);
  assert (pos < calculate_runs (tc));
  assert (mbconstraints_dimensions_ (sc) == num_dimensions);

  size_t *values = xmalloc_n (sizeof (size_t), num_dimensions);
  for (size_t i = 0; i < num_dimensions; ++i)
    values[i] = 0;

  for (size_t count = 0; count < pos; ++count)
    {
      ++values[0];
      for (size_t i = 0; i < num_dimensions; ++i)
        {
          size_t nsizes;
          const size_t *sizes;
          mbconstraints_get_dimension_ (sc, i, &nsizes, &sizes);
          assert (nsizes < SIZE_MAX);
          if (values[i] == nsizes)
            {
              assert (i + 1 != num_dimensions);
              values[i] = 0;
              ++values[i + 1];
            }
          MB_TRACE ("Combination %zu: [%zu] = %zu.\n", count, i, values[i]);
        }
    }
  return values;
}

static size_t *
allocate_sizes (mbconstraints_ *tc, size_t pos)
{
  MB_TRACE ("Allocating sizes %p for position %zu.\n", tc, pos);
  assert (tc);
  const mbconstraints_size_ *sc = mbconstraints_get_size_ (tc);
  const size_t num_dimensions = mbconstraints_dimensions_ (sc);
  assert (num_dimensions > 0);
  assert (pos < calculate_runs (tc));

  size_t *ret = allocate_combination (tc, pos, num_dimensions);
  for (size_t i = 0; i < num_dimensions; ++i)
    {
      size_t nsizes;
      const size_t *sizes;
      mbconstraints_get_dimension_ (sc, i, &nsizes, &sizes);
      assert (nsizes > 0);
      assert (ret[i] < nsizes);
      MB_DEBUG ("Position %zu -> %zu.\n", i, ret[i]);
      ret[i] = sizes[ret[i]];
    }
  return ret;
}

static void
calculate_sizes (mbconstraints_ *tc, size_t pos, size_t *dimensions,
                 size_t **sizes)
{
  MB_TRACE ("Calculating %p sizes %p/%p with %p.\n", tc, sizes, dimensions,
            constraints);
  assert (sizes);
  assert (tc);
  assert (dimensions);
  const mbconstraints_size_ *sc = mbconstraints_get_size_ (tc);
  assert (pos < calculate_runs (tc));

  const size_t ndim = mbconstraints_dimensions_ (sc);
  if (ndim > 0)
    {
      MB_DEBUG ("Calculating sizes for position %zu.\n", pos);
      *dimensions = ndim;
      *sizes = allocate_sizes (tc, pos);
    }
  else
    {
      MB_DEBUG ("No size constraint found.\n");
      *dimensions = 0;
      *sizes = NULL;
    }
}

typedef micro_benchmark_test_state_ mbstate_;

static void
create_states (micro_benchmark_test_case test,
               micro_benchmark_test_report_ *report,
               mbstate_ **states, size_t *num_states)
{
  MB_TRACE ("Creating states for test %p.\n", test);
  assert (test);
  assert (report);
  assert (states);
  assert (num_states);

  mbconstraints_ *constraints = micro_benchmark_test_case_constraints_ (test);
  mbconstraints_size_ *sc = mbconstraints_get_size_ (constraints);

  const size_t ndim = mbconstraints_dimensions_ (sc);
  size_t num_runs = 1;
  if (ndim > 0)
    num_runs = calculate_runs (constraints);

  mbstate_ *s = xmalloc_n (sizeof (mbstate_), num_runs);
  micro_benchmark_test_report_allocate_executions_ (report, num_runs);

  for (size_t i = 0; i < num_runs; ++i)
    {
      size_t dimensions;
      size_t *sizes;
      calculate_sizes (constraints, i, &dimensions, &sizes);
      MB_DEBUG ("Calculated %zu dimensions.\n", dimensions);

      /* Takes ownership of sizes array.  */
      const char *name = micro_benchmark_test_case_get_name (test);
      micro_benchmark_exec_report_init_ (&report->executions[i], name,
                                         dimensions, sizes);

      micro_benchmark_state_init_ (&s[i], test, dimensions, sizes, name);
    }

  *states = s;
  *num_states = num_runs;
}

/*  TODO: Optimize loop, create states on demand.  */
void
micro_benchmark_test_case_run_ (micro_benchmark_test_case test,
                                struct micro_benchmark_test_report_ *report)
{
  MB_TRACE ("Running test case %p onto report %p.\n", test, report);
  assert (test);
  assert (report);

  if (!micro_benchmark_test_case_is_enabled (test))
    {
      const char *name = micro_benchmark_test_case_get_name (test);
      MB_INFO ("Skipping test case \"%s\"...\n", name);
      return;
    }

  micro_benchmark_set_up_fun setup = test->definition.set_up;
  micro_benchmark_tear_down_fun teardown = test->definition.tear_down;
  micro_benchmark_auto_test_fun auto_test = test->definition.auto_test;
  micro_benchmark_test_fun test_fun = test->definition.test;
  assert (setup);
  assert (teardown);

  MB_DEBUG ("Using setup %p, teardown %p, auto %p, test %p.\n", setup,
            teardown, auto_test, test);

  mbstate_ *states;
  size_t n_executions;
  create_states (test, report, &states, &n_executions);
  assert (states);
  assert (report->executions);

  MB_INFO ("Running test \"%s\"...\n",
           micro_benchmark_test_case_get_name (test));
  for (size_t i = 0; i < n_executions; ++i)
    {
      micro_benchmark_test_state state = &states[i];
      micro_benchmark_exec_report_ *er = &report->executions[i];

      MB_INFO ("Running test \"%s\"...\n",
               micro_benchmark_exec_report_get_name (er));
      micro_benchmark_stats_collector_start_test_ (&state->stats);

      void *ptr = setup (state);
      if (test->definition.is_auto)
        {
          assert (auto_test);
          micro_benchmark_stats_collector_start_ (&state->stats);
          while (micro_benchmark_state_keep_running (state))
            auto_test (ptr);
        }
      else
        {
          assert (test_fun);
          micro_benchmark_state_set_data_ (state, ptr);
          micro_benchmark_stats_collector_start_ (&state->stats);
          test_fun (state);
        }

      micro_benchmark_stats_collector_stop_ (&state->stats);
      teardown (state, ptr);
      micro_benchmark_stats_collector_end_test_ (&state->stats);

      micro_benchmark_state_write_report_ (state, er);
      micro_benchmark_state_release_ (&states[i]);
    }
  MB_INFO ("End of test \"%s\".\n",
           micro_benchmark_test_case_get_name (test));

  xfree_n (states, sizeof (mbstate_), n_executions);
}
