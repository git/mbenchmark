/* case.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/test/iterations.h"

#include "internal/constraints.h"
#include "internal/test.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "iterations"
#define MICRO_BENCHMARK_MODULE "test:" MICRO_BENCHMARK_SUBMODULE

void
micro_benchmark_test_case_skip_iterations (micro_benchmark_test_case tc,
                                           size_t iterations_to_skip)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_iteration_ *ci = mbconstraints_get_iteration_ (c);
  mbconstraints_skip_iterations_ (ci, iterations_to_skip);
}

size_t
micro_benchmark_test_case_iterations_to_skip (micro_benchmark_test_case tc)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_iteration_ *ci = mbconstraints_get_iteration_ (c);
  return mbconstraints_iterations_to_skip_ (ci);
}

void
micro_benchmark_test_case_limit_iterations (micro_benchmark_test_case tc,
                                            size_t min_iterations,
                                            size_t max_iterations)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_iteration_ *ci = mbconstraints_get_iteration_ (c);
  mbconstraints_limit_iterations_ (ci, min_iterations, max_iterations);
}

size_t
micro_benchmark_test_case_min_iterations (micro_benchmark_test_case tc)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_iteration_ *ci = mbconstraints_get_iteration_ (c);
  return mbconstraints_min_iterations_ (ci);
}

size_t
micro_benchmark_test_case_max_iterations (micro_benchmark_test_case tc)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_iteration_ *ci = mbconstraints_get_iteration_ (c);
  return mbconstraints_max_iterations_ (ci);
}

void
micro_benchmark_test_case_limit_samples (micro_benchmark_test_case tc,
                                         size_t min_iterations,
                                         size_t max_iterations)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_iteration_ *ci = mbconstraints_get_iteration_ (c);
  mbconstraints_limit_samples_ (ci, min_iterations, max_iterations);
}

size_t
micro_benchmark_test_case_min_sample_iterations (micro_benchmark_test_case tc)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_iteration_ *ci = mbconstraints_get_iteration_ (c);
  return mbconstraints_min_sample_iterations_ (ci);
}


size_t
micro_benchmark_test_case_max_sample_iterations (micro_benchmark_test_case tc)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_iteration_ *ci = mbconstraints_get_iteration_ (c);
  return mbconstraints_max_sample_iterations_ (ci);
}
