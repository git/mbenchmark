/* sizes.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/test/sizes.h"

#include "internal/constraints.h"
#include "internal/report.h"
#include "internal/test.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"
#include "internal/utility/xalloc.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "sizes"
#define MICRO_BENCHMARK_MODULE "test:" MICRO_BENCHMARK_SUBMODULE

void
micro_benchmark_test_case_add_dimension (micro_benchmark_test_case tc,
                                         size_t num_sizes,
                                         const size_t *sizes)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_size_ *s = mbconstraints_get_size_ (c);
  mbconstraints_add_dimension_ (s, num_sizes, sizes);
}

size_t
micro_benchmark_test_case_dimensions (micro_benchmark_test_case tc)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_size_ *s = mbconstraints_get_size_ (c);
  return mbconstraints_dimensions_ (s);
}

void
micro_benchmark_test_case_get_dimension (micro_benchmark_test_case tc,
                                         size_t dimension,
                                         size_t *n_sizes,
                                         const size_t **sizes)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_size_ *s = mbconstraints_get_size_ (c);
  const size_t dsize = mbconstraints_dimensions_ (s);
  assert (dimension < dsize);
  (void) dsize;
  return mbconstraints_get_dimension_ (s, dimension, n_sizes, sizes);
}
