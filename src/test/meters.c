/* meters.c --- MicroBenchmark library. */
/*
 * Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 *
 * This file is part of MicroBenchmark.
 *
 * MicroBenchmark is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * MicroBenchmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MicroBenchmark.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "mbenchmark/test/meters.h"

#include "internal/constraints.h"
#include "internal/test.h"
#include "internal/utility/intl.h"
#include "internal/utility/log.h"

#include <assert.h>

#define MICRO_BENCHMARK_SUBMODULE "meters"
#define MICRO_BENCHMARK_MODULE "test:" MICRO_BENCHMARK_SUBMODULE

void
micro_benchmark_test_case_set_chrono (micro_benchmark_test_case tc,
                                      micro_benchmark_clock_type type,
                                      micro_benchmark_chronometer_provider p)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_meter_ *m = mbconstraints_get_meter_ (c);
  mbconstraints_set_chrono_ (m, type, p);
}

void
micro_benchmark_test_case_set_custom_chrono (micro_benchmark_test_case tc,
                                             micro_benchmark_clock_type type,
                                             micro_benchmark_meter m)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_meter_ *mc = mbconstraints_get_meter_ (c);
  mbconstraints_set_custom_chrono_ (mc, type, m);
}

void
micro_benchmark_test_case_set_calculator (micro_benchmark_test_case tc,
                                          micro_benchmark_custom_time_calculator
                                          c)
{
  assert (tc);
  mbconstraints_ *cn = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_meter_ *mc = mbconstraints_get_meter_ (cn);
  mbconstraints_set_calculator_ (mc, c);
}

void
micro_benchmark_test_case_add_meter (micro_benchmark_test_case tc,
                                     micro_benchmark_custom_meter m)
{
  assert (tc);
  mbconstraints_ *c = micro_benchmark_test_case_constraints_ (tc);
  mbconstraints_meter_ *mc = mbconstraints_get_meter_ (c);
  mbconstraints_add_meter_ (mc, m);
}
