# mbenchmark.m4 --- Build configuration for MicroBenchmark.
#
# Copyright © 2023 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
#
# This file is free software; the author gives unlimited permission to
# copy and/or distribute it, with or without modifications, as long as
# this notice is preserved.
#
# serial 1
AC_DEFUN([MICRO_BENCHMARK_TEST_LINK_C_CODE],
 [AC_LANG_PUSH([C])
  AC_LINK_IFELSE(
   [AC_LANG_PROGRAM(
     [[#include <mbenchmark/all.h>
     ]],
     [[micro_benchmark_suite suite = micro_benchmark_suite_create ("test");
       /*  TODO: expected checks.  */
       micro_benchmark_suite_release (suite);
     ]])],
   [micro_benchmark_test_ok=yes],
   [micro_benchmark_test_ok=no])
  AC_LANG_POP([C])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_C_DIR],
 [mb_prefix=$1
  AS_IF([test "x$mb_lib" = x],
   [oldCPPFLAGS=$CPPFLAGS
    AS_IF([test "x$mb_prefix" = x],
     [mb_tmp_cpp=
     ],
     [AC_MSG_CHECKING([if $mb_prefix/include exists])
      AS_IF([test -e "$mb_prefix/include"],
       [CPPFLAGS="$CPPFLAGS -I$mb_prefix/include"
        mb_tmp_cpp="-I$mb_prefix/include"
        AC_MSG_RESULT([yes])],
       [mb_tmp_cpp=""
        AC_MSG_RESULT([no])])
     ])
    AC_CHECK_HEADER([mbenchmark/all.h],
     [mb_missing_header=no],
     [mb_missing_header=yes],
     [])

    oldLIBS=$LIBS
    AS_IF([test "x$mb_missing_header" = xyes],
     [:],
     [test "x$mb_prefix" = x],
     [AC_LANG_PUSH([C])
      AC_CHECK_LIB([mbenchmark], [micro_benchmark_init],
       [AC_MSG_CHECKING([default path libmbenchmark usability])
        MICRO_BENCHMARK_TEST_LINK_C_CODE
        AC_MSG_RESULT([$micro_benchmark_test_ok])
        AS_IF([test "x$micro_benchmark_test_ok" = xyes],
         [mb_cpp=
          mb_lib="-lmbenchmark"
          mb_ltlib="-lmbenchmark"
         ])
       ])
      AC_LANG_POP([C])
     ],
     [AC_MSG_CHECKING([for libmbenchmark.so in])
      mb_libdir="$mb_prefix/lib"
      AC_MSG_RESULT([$mb_libdir])
      dnl TODO: Renames.
      oldLIBS=$LIBS
      LIBS="$LIBS -L $mb_libdir -lmbenchmark"
      AC_LANG_PUSH([C])
      AC_MSG_CHECKING([libmbenchmark usability])
      MICRO_BENCHMARK_TEST_LINK_C_CODE
      AC_MSG_RESULT([$micro_benchmark_test_ok])

      AS_IF([test "x$micro_benchmark_test_ok" = xyes],
       [mb_lib="-L $mb_libdir -lmbenchmark"
        mb_cpp=$mb_tmp_cpp
        AC_MSG_CHECKING([for libmbenchmark.la in $mb_libdir])
        AS_IF([test -e "$mb_libdir/libmbenchmark.la"],
         [mb_ltlib="$mb_libdir/libmbenchmark.la"
          AC_MSG_RESULT([found])],
         [AC_MSG_RESULT([not found])])
       ])
      AC_LANG_POP([C])
     ])
    LIBS=$oldLIBS
    CPPFLAGS=$oldCPPFLAGS
   ])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_C_FLAGS],
 [micro_benchmark_found=no
  micro_benchmark_cppflags=
  micro_benchmark_libs=
  micro_benchmark_ltlibs=
  AC_ARG_WITH([micro-benchmark],
   [AS_HELP_STRING([--with-micro-benchmark=DIR],
     [Select MicroBenchmark installation])],
   [micro_benchmark_selected=$withval],
   [micro_benchmark_selected=check])

  AS_IF([test "x$micro_benchmark_selected" != xno],
   [micro_benchnmark_test_ok=
    mb_cpp=
    mb_lib=
    mb_ltlib=
    AS_IF([test "x$micro_benchmark_selected" = xcheck],
     [MICRO_BENCHMARK_CHECK_C_DIR([])
      AS_IF([test "x$micro_benchmark_test_ok" != xyes],
       [AS_IF([test "x$micro_benchmark_test_ok" != xcheck],
         [AC_MSG_FAILURE([MicroBenchmark not found on $micro_benchmark_selected])],
         [micro_benchmark_found=no])],
       [micro_benchmark_found=yes])
     ],
     [test "x$micro_benchmark_selected" = xyes],
     [m4_foreach([base], [[$prefix], [/usr], [/usr/local], [/opt/mbenchmark]],
       [MICRO_BENCHMARK_CHECK_C_DIR(base)])dnl
      AS_IF([test "x$micro_benchmark_test_ok" != xno],
       [micro_benchmark_found=yes],
       [micro_benchmark_found=no])],
     [test "x$micro_benchmark_selected" != xno],
     [MICRO_BENCHMARK_CHECK_C_DIR([$micro_benchmark_selected])
      AS_IF([test "x$micro_benchmark_test_ok" != xno],
       [micro_benchmark_found=yes],
       [micro_benchmark_found=no])
     ])
    AS_IF([test "x$micro_benchmark_found" = xyes],
     [micro_benchmark_cppflags=$mb_cpp
      micro_benchmark_libs=$mb_lib
      micro_benchmark_ltlibs=$mb_ltlib],
     [test "x$micro_benchmark_selected" = xcheck"],
     [AC_MSG_NOTICE([MicroBenchmark is not usable])],
     [test "x$micro_benchmark_selected" != xno"],
     [AC_MSG_FAILURE([Selected MicroBenchmark installation is not usable])
     ])
   ])
 ])

dnl XXX parameters
AC_DEFUN([MICRO_BENCHMARK_CHECK_C],
 [AC_REQUIRE([MICRO_BENCHMARK_CHECK_C_FLAGS])
  m4_ifdef([AM_CONDITIONAL],
    [AM_CONDITIONAL([HAVE_MICRO_BENCHMARK],
      [test "x$micro_benchmark_found" = xyes])])
  AS_IF([test "x$micro_benchmark_found" = xyes],
   [AC_DEFINE([HAVE_MICRO_BENCHMARK], [1], [MicroBenchmark availability])])
  AC_SUBST([MICRO_BENCHMARK_CFLAGS], [$micro_benchmark_cppflags])
  AC_SUBST([MICRO_BENCHMARK_LIBS], [$micro_benchmark_libs])
  AC_SUBST([MICRO_BENCHMARK_LTLIBS], [$micro_benchmark_ltlibs])
 ])


AC_DEFUN([MICRO_BENCHMARK_TEST_LINK_CXX_CODE],
 [AC_LANG_PUSH([C++])
  AC_LINK_IFELSE(
   [AC_LANG_PROGRAM(
     [[#include <mbenchmark/all.hpp>
     ]],
     [[micro_benchmark::suite suite ("test");]])],
   [micro_benchmark_test_ok=yes],
   [micro_benchmark_test_ok=no])
  AC_LANG_POP([C++])
 ])

AC_DEFUN([MICRO_BENCHMARK_CHECK_CXX_DIR],
 [mb_prefix=$1
  AS_IF([test "x$mb_cxxlib" = x],
   [oldCPPFLAGS=$CPPFLAGS
    CPPFLAGS="$CPPFLAGS $micro_benchmark_cppflags"
    AC_LANG_PUSH([C++])
    AC_CHECK_HEADER([mbenchmark/all.hpp],
     [mb_missing_header=no],
     [mb_missing_header=yes],
     [])
    AC_LANG_POP([C++])

    oldLIBS=$LIBS
    AS_IF([test "x$mb_missing_header" = xyes],
     [:],
     [test "x$mb_prefix" = x],
     [AC_LANG_PUSH([C++])
      AC_CHECK_LIB([mbenchmark-c++], [main],
       [AC_MSG_CHECKING([default path libmbenchmark usability])
        MICRO_BENCHMARK_TEST_LINK_CXX_CODE
        AC_MSG_RESULT([$micro_benchmark_test_ok])
        AS_IF([test "x$micro_benchmark_test_ok" = xyes],
         [mb_cxxlib="-lmbenchmark-c++"
          mb_cxxltlib="-lmbenchmark-c++"
         ])
       ])
      AC_LANG_POP([C++])
     ],
     [AC_MSG_CHECKING([for libmbenchmark-c++.so in])
      mb_libdir="$mb_prefix/lib/mbenchmark"
      AC_MSG_RESULT([$mb_libdir])
      oldLIBS=$LIBS
      LIBS="$LIBS -L $mb_libdir -lmbenchmark-c++"
      AC_LANG_PUSH([C++])
      dnl TODO: Renames.
      AC_MSG_CHECKING([libmbenchmark-c++ usability])
      MICRO_BENCHMARK_TEST_LINK_CXX_CODE
      AC_MSG_RESULT([$micro_benchmark_test_ok])

      AS_IF([test "x$micro_benchmark_test_ok" = xyes],
       [mb_cxxlib="-L $mb_libdir -lmbenchmark-c++"
        AC_MSG_CHECKING([for libmbenchmark-c++.la in $mb_libdir])
        AS_IF([test -e "$mb_libdir/libmbenchmark.la"],
         [mb_cxxltlib="$mb_libdir/libmbenchmark.la"
          AC_MSG_RESULT([found])],
         [AC_MSG_RESULT([not found])])
        ])
       ])
      AC_LANG_POP([C++])
     ])
    LIBS=$oldLIBS
    CPPFLAGS=$oldCPPFLAGS
   ])
 ])

dnl XXX parameters
AC_DEFUN([MICRO_BENCHMARK_CHECK_CXX_FLAGS],
 [AC_REQUIRE([MICRO_BENCHMARK_CHECK_C_FLAGS])
  micro_benchmark_cxxlibs=
  micro_benchmark_cxxltlibs=
  micro_benchmark_found=no
  AS_IF([test "x$micro_benchmark_selected" != xno],
   [micro_benchnmark_test_ok=
    mb_cxxlib=
    mb_cxxltlib=
    AS_IF([test "x$micro_benchmark_selected" = xcheck],
     [MICRO_BENCHMARK_CHECK_CXX_DIR([])
      AS_IF([test "x$micro_benchmark_test_ok" != xyes],
       [AS_IF([test "x$micro_benchmark_test_ok" != xcheck],
         [AC_MSG_FAILURE([MicroBenchmark not found on $micro_benchmark_selected])],
         [micro_benchmark_found=no])],
       [micro_benchmark_found=yes])
     ],
     [test "x$micro_benchmark_selected" = xyes],
     [m4_foreach([base], [[$prefix], [/usr], [/usr/local], [/opt/mbenchmark]],
       [MICRO_BENCHMARK_CHECK_CXX_DIR(base)])dnl
      AS_IF([test "x$micro_benchmark_test_ok" != xno],
       [micro_benchmark_found=yes],
       [micro_benchmark_found=no])],
     [test "x$micro_benchmark_selected" != xno],
     [MICRO_BENCHMARK_CHECK_CXX_DIR([$micro_benchmark_selected])
      AS_IF([test "x$micro_benchmark_test_ok" != xno],
       [micro_benchmark_found=yes],
       [micro_benchmark_found=no])
     ])
    AS_IF([test "x$micro_benchmark_found" = xyes],
     [micro_benchmark_cxxlibs=$mb_cxxlib
      micro_benchmark_ltlibs=$mb_cxxltlib],
     [test "x$micro_benchmark_selected" = xcheck"],
     [AC_MSG_NOTICE([MicroBenchmark is not usable])],
     [test "x$micro_benchmark_selected" != xno"],
     [AC_MSG_FAILURE([Selected MicroBenchmark installation is not usable])
     ])
   ])
 ])

dnl XXX parameters
AC_DEFUN([MICRO_BENCHMARK_CHECK_CXX],
 [AC_REQUIRE([MICRO_BENCHMARK_CHECK_CXX_FLAGS])
  m4_ifdef([AM_CONDITIONAL],
    [AM_CONDITIONAL([HAVE_MICRO_BENCHMARK_CXX],
      [test "x$micro_benchmark_cxx_found" = xyes])])
  AS_IF([test "x$micro_benchmark_found" = xyes],
   [AC_DEFINE([HAVE_MICRO_BENCHMARK_CXX], [1],
     [MicroBenchmark C++ availability])])
  AC_SUBST([MICRO_BENCHMARK_CXXFLAGS], [$micro_benchmark_cppflags])
  AC_SUBST([MICRO_BENCHMARK_CXXLIBS], [$micro_benchmark_cxxlibs])
  AC_SUBST([MICRO_BENCHMARK_CXXLTLIBS], [$micro_benchmark_cxxltlibs])
 ])

dnl XXX parameters
AC_DEFUN([MICRO_BENCHMARK_CHECK_GUILE],
 [dnl TODO: Better checks
  GUILE_MODULE_REQUIRED_EXPORT([(mbenchmark)], [main])
  GUILE_MODULE_REQUIRED_EXPORT([(mbenchmark log)], [set-log-level!])
  GUILE_MODULE_REQUIRED_EXPORT([(mbenchmark report)], [report?])
  GUILE_MODULE_REQUIRED_EXPORT([(mbenchmark state)], [keep-running?])
  GUILE_MODULE_REQUIRED_EXPORT([(mbenchmark suite)], [run-suite!])
  GUILE_MODULE_REQUIRED_EXPORT([(mbenchmark test)], [register-test!])
 ])
